
本期概要
- 加密指令集K发布0.9.2版本
- RV中国峰会
- Intel欲收购SiFive

---

# 重点聚焦

#### RV中国峰会

"[RISC-V 2021 中国峰会同地举办（co-located）活动预告](https://posts.careerengine.us/p/60c16a676cb3893770bfa3c1?from=latest-posts-panel&type=title)(From posts.careerengine.us 2021.06.11)"  
"[Imagination 中国区战略市场生态副总裁时昕将带来《RISC-V Ecosystem：More than A CPU Core》的主题分享](http://imgtec.eetrend.com/blog/2021/100113492.html)(From imgtec.eetrend.com 2021.06.11)"  
"[熊谱翔，RT-Thread 创始人：RV64上的微内核操作系统rt-smart](https://www.eet-china.com/mp/a56053.html)(From www.eet-china.com 2021.06.11)"

#### Intel收购SiFive

"[Intel出价20亿美元，SiFive愿意接受吗？](https://www.eet-china.com/news/2021061111942.html)(From www.eet-china.com 2021.06.11)"
> 据彭博社披露，RISC-V初创公司SiFive收到英特尔20亿美元的收购意向提议，但是否最终落实还不得而知。知情人士称，这家由加州大学伯克利教授及学生研究团队创办，基于开源指令集架构RISC-V的微处理器内核IP开发商一直与多家潜在收购者接触洽谈。这次买家是英特尔，而且出价高达20亿美元，SiFive会心动吗？ 笔者认为，这笔交易不会达成，SiFive会选择继续保持独立运营。

"[英特尔提出以超过20亿美元（编者注：约合127亿元人民币）的价格收购SiFive](https://finance.sina.com.cn/tech/2021-06-11/doc-ikqcfnca0377958.shtml)(From finance.sina.com.cn 2021.06.11)"  
https://finance.sina.com.cn/tech/2021-06-11/doc-ikqcfnca0367062.shtml

#### K加密指令集0.9.2发布

github：https://github.com/riscv/riscv-crypto/releases  
wiki：https://wiki.riscv.org/display/TECH/Scalar+Crypto+Standardization+Status+Summary

<a name="040e68498fe94af14a2d31d5106f68eb"></a>
#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅亚农历山大: [RISC-V双周报](https://www.rvmcu.com/article-show-id-557.html) ：[5月21日-6月3日](https://mp.weixin.qq.com/s?__biz=MzU2MjM4MTcyNQ==&mid=2247489824&idx=1&sn=ab61804989effe13cd6f9317aa702d3b&chksm=fc6b0fd2cb1c86c4745c76cff2fd6231d2018fb04e8ec2dc23f9ce163da0decb977e2cb52f09&scene=178&cur_album_id=1713615520249217030#rd)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：


---



<a name="27f27f1566779837ca690f0417d9ac7a"></a>
## 技术动态

"[sensAI 4.0面世：简化AL/ML模型在智能网络边缘设备的部署](https://www.eet-china.com/mp/a55025.html)(From www.eet-china.com 2021.06.11)"
> 支持使用TensorFlow Lite和莱迪思Propel进行基于嵌入式处理器的设计；包括全新的莱迪思sensAI Studio工具，轻松实现ML模型训练

---

<a name="3f25c0aec37715d98fb24001e6f90d0b"></a>
## 产业风云

#### 产品动态


"[景略半导体发布全新一代交换机芯片技术平台，支持车载和工业网络TSN实时传输需求](https://www.laoyaoba.com/n/783062)(From www.laoyaoba.com 2021.06.11)"
> BlueWhale(tm) 创新的技术架构，基于EtherNext™高速以太网PHY技术并可内置RISC-V MCU或CPU，具有高度的可配置性，能够根据客户的需求在短时间内快速推出交换机芯片产品，以满足不断变化的市场需求。

"[东软载波：载波通信模组中标，MCU产品价格随行就市](https://laoyaoba.com/n/783067)(From laoyaoba.com 2021.06.11)"
> 东软载波指出，公司的MCU产品价格变化随行就市，RISC-v架构产品正在研发中

"[RISC-V的杀手级应用即将到来](http://news.moore.ren/industry/277548.htm)(From news.moore.ren 2021.06.11)"
> 有人推动使用 RISC-V 作为 AI 和目标工作负载的基础，为 ISA 提供了进入更大系统的跳板

"[为有效支撑“长安链·协作网络”运行，全球性能领先的区块链先进算力平台已在中关村科学城北区启动建设](https://finance.sina.com.cn/tech/2021-06-10/doc-ikqcfnca0249804.shtml)(From finance.sina.com.cn 2021.06.11)"
> 北京微芯区块链与边缘计算研究院院长董进发布全球首款96核区块链专用加速芯片和“长安链·协作网络”等重大成果。  
> "[长安链发布首款自主可控96核区块链芯片  该芯片可使区块链交易性能提升50倍](http://news.sciencenet.cn/htmlnews/2021/6/459408.shtm)(From news.sciencenet.cn 2021.06.11)"

#### 企业动态


"[地平线计划赴美IPO上市](https://www.eet-china.com/kj/63567.html)(From www.eet-china.com 2021.06.11)"
> 汽车智能芯片公司地平线在C轮融资额达9亿美元，近日传出有计划美国IPO上市，

"[两股东拟清仓式减持 乐鑫科技低开8.7%](https://finance.sina.com.cn/stock/gujiayidong/2021-06-04/doc-ikqciyzi7635758.shtml)(From finance.sina.com.cn 2021.06.11)"
> Shinvest Holding Ltd．、亚东北辰创业投资有限公司


<a name="6395d96e54fcd2acd131c715687c276b"></a>
## 其他动态

#### 评论文集

"[一文读懂全球半导体市场](https://www.eet-china.com/mp/a55244.html)(From www.eet-china.com 2021.06.11)"
> 2020年，全球半导体市场规模达到4400亿美元，同比增长6.8%，以存储器和专用芯片为代表的半导体产品开始进入景气周期；中国集成电路市场需求持续旺盛，全年集成电路市场销售额增至16339.7亿元，同比增长8.26%。预计2021年市场规模将进一步增长。

"[RISC-V理事会被中国企业“占领”了](https://posts.careerengine.us/p/60bd787300fa33524506ef1f?from=latest-posts-panel&type=previewimage)(From posts.careerengine.us 2021.06.11)"
> 实际上只有两家非中国公司，分别是西数Western Digital和SiFive； 其他的11家全是中国公司，并且10家来自中国内地，1家来自中国台湾。

"[中国集成电路市场需求仍将增长 后摩尔时代三大创新领域值得关注](https://finance.sina.com.cn/stock/hyyj/2021-06-09/doc-ikqciyzi8649658.shtml)(From finance.sina.com.cn 2021.06.11)"
> 在6月9日2021年世界半导体大会开幕式上，工信部电子信息司司长乔跃山致辞介绍，当前，全球半导体产业进入重大的调整期，集成电路产业的风险与机遇并存。他指出，在经济全球化的时代，开放融通是不可阻挡的历史趋势。

#### 博文推荐

"[从GPU谈异构，这是ARM处理器架构师的趣谈](https://www.163.com/dy/article/GC2KAJ2S0511AQHO.html?f=post2020_dy_recommends)(From www.163.com 2021.06.11)"

"[RISC-V自动向量化测试报告](https://zhuanlan.zhihu.com/p/377577892)(From zhuanlan.zhihu.com 2021.06.11)"  
"[补记在RISC-V Forum上来不及show的slides](https://zhuanlan.zhihu.com/p/378137193)(From zhuanlan.zhihu.com 2021.06.11)"
> 这部分内容主要是关于在移植V8的过程中，我对RISC-V指令集在支撑V8的JIT功能上的effectiveness的思考。  
> 如果硬件怎么样改一改，就可以让软件跑更好？

"[qemu 虚拟机运行 u-boot / opensbi](https://whycan.com/t_6614_2.html)(From whycan.com 2021.06.11)"  
"[全志D1芯片真的存在一个正室？](https://zhuanlan.zhihu.com/p/376790086)(From zhuanlan.zhihu.com 2021.06.11)"

---

<a name="2010a69b8aaca066239ad10c98eb0e7c"></a>
## 一周论文

#### "[支持RISC-V向量指令的汇编器设计与实现](https://scjg.cnki.net/kcms/detail/detail.aspx?filename=JSJK202012010&dbcode=CJFQ&dbname=CJFD2020&v=)(From scjg.cnki.net 2021.06.11)"
> 向量运算可以有效提高计算机的运算效率,减少不必要的硬件开销,随着CPU运算能力的提升和寄存器位数扩展等硬件的进一步发展,向量运算成为实际芯片架构设计中最常用的提高处理器性能的技术。受到业界广泛关注的RISC-V体系结构也借助向量技术提高性能,但目前开源版本的RISC-V汇编器只支持标量指令程序,不支持向量指令的汇编。基于GNU的Binutils汇编器,设计并实现了支持RISC-V向量指令的汇编器,该汇编器可完成向量指令的汇编和反汇编工作,同时其扩展实现也可以为其他指令模块的扩展支持提供参考。 

#### "[轻量级安全内存：RISC-V嵌入式微处理器安全增强](https://scjg.cnki.net/kcms/detail/detail.aspx?filename=JSJK20210409000&dbcode=CJFQ&dbname=CAPJ2021&v=)(From scjg.cnki.net 2021.06.11)"
> 物联网时代，随着多样化的嵌入式设备开始进入主流市场，人们对其安全性的担忧也越发强烈。特别是随着非易失性存储器开始被配备到嵌入式设备中，就需要考虑如何保护配备非易失性存储器的嵌入式设备的安全。安全内存，就是这样一种通过保护内存来增强嵌入式设备安全性的有效手段。本文通过设计一种安全内存加密引擎来实现安全内存。在保证该安全内存加密引擎足够轻量、低开销的同时，将其集成到了RISC-V嵌入式微处理器中，并通过FPGA对该安全内存加密引擎进行了评估。评估结果表明，安全内存加密引擎能够在提升RISC-V嵌入式微处理器安全性的同时，保证其合理的访存性能以及较小的面积开销。研究结果对本领域有良好的参考价值和应用前景。 

#### [PDF][A RISC-V in-network accelerator for flexible high-performance low-power packet processing](https://spcl.inf.ethz.ch/Publications/.pdf/pspin-isca.pdf)
S Di Girolamo, A Kurth, A Calotoiu, T Benz…
> The capacity of offloading data and control tasks to the network is becoming increasingly important, especially if we consider the faster growth of network speed when compared to CPU frequencies. In-network compute alleviates the host CPU …

#### [Build Your OwnRISC-VCPU: Even Home-Brew Processors Can use Hot New Tech](https://ieeexplore.ieee.org/abstract/document/9444942/)
F Szkandera - IEEE Spectrum, 2021
> It's a certain kind of itch that drives people to voluntarily build their own CPU. We start thinking about the papered-over gap in our understanding, the one that lurks between how logic gates and flip-flops work individually and how machine code …

#### [Secure RISCV Design for Side-Channel Evaluation Platform](https://search.proquest.com/openview/6d6023b1451fff331368b20582095686/1?pq-origsite=gscholar&cbl=18750&diss=y)
B Thakar - 2021
> … Common Evaluation Platform(CEP). CEP is a  **RISC-V**  based simulation framework … execution, and can have adverse affects if the underlying framework is compromised. Our research focuses on security of  **RISC-V**  architecture and its simulation framework …

#### [Performance Evaluation ofRISC-VArchitecture](https://stm.bookpi.org/AAER-V13/article/view/1167)
L Alluri, M Bhaskar, HJ Magadam - Advanced Aspects of Engineering Research Vol …, 2021
> This study shows the Gem5 simulator to evaluate the performance of a  **RISC-V**  architecture-based processor. The Gem5 simulator is used to investigate the
processor architecture's performance metrics such as bandwidth, latency …

#### [PDF][Revisiting Challenges for Selective Data Protection of Real Applications](https://arxiv.org/pdf/2105.14251)
L Ma, J Xu, J Sun, Y Zhou, X Xie, W Shen, R Chang… - arXiv preprint arXiv …, 2021
> … Then we design and implement a prototype system for selective data protection and evaluate the overhead using the  **RISC-V**  Spike simulator … We implement a system prototype on theRISC-Vsimulator with new instructions, tagged registers, and cache …

#### [PDF][Inference speed comparison using convolutions in neural networks on various SoC hardware platforms using MicroPython](http://ceur-ws.org/Vol-2872/paper10.pdf)
K Dokic, B Radisica - 2021
> … This paper explores the possibility of using the Python programming language in different versions to create, train, and implement convolutional neural networks on two SoCs based on different architectures (ARM and  **RISC-V** ). The influence of the number of filters in the …

####[ Accessible, FPGA Resource-Optimized Simulation of Multi-Clock Systems in FireSim](https://ieeexplore.ieee.org/abstract/document/9444782/)
D Biancolin, A Magyar, S Karandikar, A Amid, B Nikolic… - IEEE Micro, 2021
> … FireSim is integrated into the Chipyard SoC design framework [8], which contains a large corpus of SoC IP developed by a growing com- munity of designers, including  **RISC-V**  processor cores, cache generators, hardware accelerators, and periphery IP …

#### [PDF][Integer-Only Neural Network Quantization Scheme Based on Shift-Batch-Normalization](https://arxiv.org/pdf/2106.00127)
Q Guo, Y Wang, X Cui - arXiv preprint arXiv:2106.00127, 2021
> … V. FUTURE WORK We are trying to design a  **RISC** -Vco-processor using hardware architecture co-designing. The co-processor will 3The results for 8 bit quantization is worse than 4 bit quantization. It's weird, but we haven't solve this problem yet. 5 Page 6 …

#### [PDF][Improving the Performance and Availability of Microservice-Based Cloud Applications](https://raw.githubusercontent.com/pacslab/publications/master/pdf/Goli_Alireza_202104_MSc.pdf)
A Goli - 2021
> … Specifically, it benefits cloud providers by increasing the utilization of their servers that might not be appealing to users or platforms such as ARM and  **RISC-V**  to handle computations [51]. Moreover, multitenancy reduces the …

#### [PDF][The future of hardware development: a perspective from systems researchers](https://dl.acm.org/doi/pdf/10.1145/3458336.3465298)
Y Zhang - Proceedings of the Workshop on Hot Topics in …, 2021
> … How to scale out a hardware platform and evaluate its distributed setting? What is the role of open-source hardware? New open hardware standards
like  **RISC-V** [7] and p4 [5] are growing rapidly … 2020. arXiv:2010.12114 …

#### [PDF][Highly-Adaptive Mixed-Precision MAC Unit for Smart and Low-Power Edge Computing](https://hal-lirmm.ccsd.cnrs.fr/lirmm-03241639/document)
G Devic, M France-Pillois, J Salles, G Sassatelli… - 19th IEEE Interregional …, 2021
… accelerators. Another approach relies on general- purpose compute units with Single
Instruction Multiple Data (SIMD) support for reduced data bit-width precision, as in
ARM Cortex-M [1] or  **RISC-V**  based RI5CY [2] processors …

#### [PDF][Hardware Accelerators for Graph Convolutional Networks](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2021/EECS-2021-148.pdf)
K Ahmad - 2021
> … accelerator. We also propose the addition of a sparse-to- dense decompression DMA Engine to Gemmini, providing a reference implementation in Spike—the  **RISC-V** ISA-level simulator—and C tests. I. INTRODUCTION Many …

#### [HTML][Introducing KeyRing self-timed microarchitecture and timing-driven design flow](https://ietresearch.onlinelibrary.wiley.com/doi/full/10.1049/cdt2.12032)
M Fiorentino, C Thibeault, Y Savaria - IET Computers & Digital Techniques
> … static timing analysis. Finally, two 32-bit  **RISC-V**  processors are presented; called KeyV and based on KeyRing microarchitectures, they are synthesized in a 65 nm technology using the proposed EDA flow. Postsynthesis results …

#### [HLS-Compatible, Embedded-Processor Stream Links](https://ieeexplore.ieee.org/abstract/document/9444050/)
E Micallef, Y Xiao, A DeHon - 2021 IEEE 29th Annual International Symposium on …, 2021
> … 3–12. [6] J. Gray, “GRVI phalanx: A massively parallel  **RISC-V**  FPGA accel- erator accelerator,” in Proceedings of the IEEE Symposium on Field- Programmable Custom Computing Machines, 2016, pp. 17–20. [7] ——, “GRVI …

#### [A Safari through FPGA-based Neural Network Compilation and Design Automation Flows](https://ieeexplore.ieee.org/abstract/document/9444092/)
P Plagwitz, F Hannig, M Ströbel, C Strohmeyer, J Teich - 2021 IEEE 29th Annual …, 2021
> … projects. MARLANN is an overlay accelerator that lacks a compiler but is open-source and documented [18]. TinBiNN is very similar, but no source code is available [19]. It is based on the ORCA **RISC-V** softcore processor. B …

#### [PDF][Emerging Tunnel FET and Spintronics based Hardware Secure Circuit Design with Ultra-low Energy Consumption](https://www.researchsquare.com/article/rs-262353/latest.pdf)
A Japa, SK Sahoo, R Vaddi, MK Majumder - 2021

#### [PDF][Enhancing OS Memory Management Performance: A](https://www.researchgate.net/profile/Zryan-Najat/publication/351969783_Enhancing_OS_Memory_Management_Performance_A_Review/links/60b25df7299bf1f6d58479fc/Enhancing-OS-Memory-Management-Performance-A-Review.pdf)
NR Omar, RH Saeed, JA Ahmed, SB Muhammad…
> … and construct integrated systems that backing multi-processing. The design of Embedded NoC (MMNoC) is a prototype platform with MMNoC and dual **RISC-V** processors. The prototype platform is synthesized with the 28nm …

#### [Custom Hardware Architectures for Deep Learning on Portable Devices: A Review](https://ieeexplore.ieee.org/abstract/document/9447019/)
KS Zaman, MBI Reaz, SHM Ali, AAA Bakar… - IEEE Transactions on …, 2021

#### [OmpSs@ FPGA framework for high performance FPGA computing](https://ieeexplore.ieee.org/abstract/document/9445632/)
JM Deharo, J Bosch, A Filgueras, MV Pinol… - IEEE Transactions on …, 2021

#### [Pseudo-3D Physical Design Flow for Monolithic 3D ICs: Comparisons and Enhancements](https://dl.acm.org/doi/abs/10.1145/3453480)
H Park, BW Ku, K Chang, DE Shim, SK Lim - ACM Transactions on Design …, 2021

#### [Optimizing the Integration Area and Performance of VLIW Architectures by Hardware/Software Co-design](https://link.springer.com/content/pdf/10.1007/978-3-030-68527-0_3.pdf)
A Florea, T Vasilas - Modelling and Development of Intelligent Systems: 7th …, 2021
> … Such example is  **RISC-V**  architecture [7], developed at the University of California, Berkeley, which integrates systems ranging from data-center chips to IoT devices … 764–769. IEEE (2004) 7. Patterson, D., Waterman, A.: The …

#### [A Software/Hardware Co-Design of Crystals-Dilithium Signature Scheme](https://dl.acm.org/doi/abs/10.1145/3447812)
Z Zhou, D He, Z Liu, M Luo, KKR Choo - ACM Transactions on Reconfigurable …, 2021
> … In the work of Alkim et al. [4], an optimized  **RISC-V**  implementation of the polynomial arithmetic used by the Kyber and NewHope scheme is proposed, designed to achieve better performance by extending optimizations for …

#### [Omegaflow: a high-performance dependency-based architecture](https://dl.acm.org/doi/abs/10.1145/3447818.3460367)
Y Zhou, Z Yu, C Zhang, Y Xu, H Wang, S Wang, N Sun… - Proceedings of the ACM …, 2021

#### [PLANAR: a programmable accelerator for near-memory data rearrangement](https://dl.acm.org/doi/abs/10.1145/3447818.3460368)
A Barredo, A Armejach, J Beard, M Moreto - … of the ACM International Conference on …, 2021

---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

