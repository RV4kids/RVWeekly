本期概要

- 新系列博文：使用RISC-V模拟器，实现一个操作系统
- 西门子收购 OneSpin Solutions




---

<a name="82ae6d81b00abeda0d645eda47b6dab2"></a>
## 重点聚焦

<br />
<br />

<a name="040e68498fe94af14a2d31d5106f68eb"></a>
#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅农亚历山大: [RISC-V双周报](https://www.rvmcu.com/article-show-id-557.html
)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：

---



<a name="27f27f1566779837ca690f0417d9ac7a"></a>
## 技术动态

<br />"[GCC 11稳定版下周发布 支持新CPU 引入Intel AMX ](https://www.cnbeta.com/articles/tech/1120085.htm
)(From www.cnbeta.com 2021.05.02)"
>  RISC-V上的GCC 11现在支持Address Sanitizer、IFUNC以及其他改进。

<a name="d5341f1457ce4cc45dd68f9fe821e6d2"></a>
#### Linux动态


<a name="bd674270d844156c441d6c491a844358"></a>
#### QEMU动态


<a name="afed03f5250206b6e0a59dbb2c657056"></a>
#### 一周问答


<a name="1834af42f819fedd0144c869ec9b4fb2"></a>
#### 社区动态
"[成为资本加入RISC-V董事会及技术指导委员会 李世默当选理事 ](http://finance.eastmoney.com/a/202104301909002416.html
)(From finance.eastmoney.com 2021.05.02)"
> 成为资本成立于1999年，投资领域包括制造业、消费品、教育、互联网、石油天然气等行业的全阶段。作为RISC-V国际协会创始会员及RISC-V发明人所创公司SiFive的重要股东，成为资本与SiFive在中国共同出资设立了上海赛昉科技科技有限公司(简称“赛昉科技”)，赛昉科技是中国RISC-V技术和生态的领导者。


---

<a name="3f25c0aec37715d98fb24001e6f90d0b"></a>
## 产业风云
"[西门子收购 OneSpin Solutions，进一步扩展业界领先的 IC 验证产品组合 ](http://article.cechina.cn/21/0430/12/20210430120627.htm
)(From article.cechina.cn 2021.05.02)"

"[英特尔晶圆代工业务2025年以后才能盈利 ](https://www.esmchina.com/news/7787.html
)(From www.esmchina.com 2021.05.02)"
> 英特尔代工服务事业部（IFS，Intel Foundry Services）。IFS结合了先进的制程和封装技术，且支持x86内核、ARM和RISC-V生态系统IP的生产，能为客户交付世界级的IP组合。
> "[Intel CEO基辛格提到，目前有50多家公司在洽谈中，他们将是公司的潜在代工客户，不过具体名单没有提及 ](http://vga.itsogo.net/71/7194.html
)(From vga.itsogo.net 2021.05.02)"

"[全球“芯片荒”或持续至2022年 ](https://www.riscv-mcu.com/article-show-id-714.html
)(From www.riscv-mcu.com 2021.05.02)"<br />"[中国工程师最喜欢的10大蓝牙芯片 ](https://www.eet-china.com/news/202104260800.html
)(From www.eet-china.com 2021.05.02)"
> ASPENCORE《电子工程专辑》分析师团队按照蓝牙的四大应用（音频传输、数据传输、位置服务、设备网络）挑选出10个比较流行的蓝牙设备和应用类别，每个类别推荐3家蓝牙芯片厂商及其代表性产品型号。

"[龙芯中科深度布局知识产权工作 ](https://laoyaoba.com/n/778974
)(From laoyaoba.com 2021.05.02)"<br />"[MCU大缺货，盛群（HOLTEK）下半年再涨价 ](https://www.rvmcu.com/article-show-id-721.html
)(From www.rvmcu.com 2021.05.02)"<br />"[比苹果M1更牛的芯片，明年高通可能要造出来了 ](https://www.eet-china.com/news/202104290738.html
)(From www.eet-china.com 2021.05.02)"
> 苹果M1证明了Arm也能用来造高性能处理器，但并不意味着其他同在Arm阵营的企业有这种能力。和苹果存在直接竞争关系，且同在Arm阵营的，市场表现比较出色的应该就是高通了：无论是在手机处理器，还是在PC处理器上。

<a name="ad4e97987f4fce7ae157a484526e2762"></a>
#### 评论文集
"[在华为之后，阿里巴巴将成为国产自主芯片的担纲人 ](https://zhuanlan.zhihu.com/p/367500181
)(From zhuanlan.zhihu.com 2021.05.02 by [柏铭科技](https://www.zhihu.com/column/baiming007))"
> Risc-V架构可以利用落后的工艺发挥出更高的性能，从而国产芯片可以形成自己完善产业链，增强竞争力。
> 可以说阿里巴巴采用Risc-V架构的玄铁910在独立自主研发方面更有意义，比华为的麒麟芯片进一步摆脱了美国技术的限制，增强了国产芯片的自主研发能力。

"[RISC-V 能打 50 年！不必期待 RISC-VI —— 对话 RISC-V CTO Mark Himelstein  ](https://www.sohu.com/a/463710138_115128
)(From www.sohu.com 2021.05.02)"<br />"[中科昊芯李任伟：缺芯让本土芯片企业“敲门”更容易了 ](https://finance.sina.com.cn/tech/2021-04-30/doc-ikmyaawc2727480.shtml
)(From finance.sina.com.cn 2021.05.02)"
> 《全球缺芯，中国厂商们如何抓机遇？》。国科嘉和执行合伙人陈洪武、九合创投创始人王啸、中科昊芯创始人兼董事长李任伟、联盛德CEO李庆一起深度剖析了当前芯片短缺的困境和危机背后的机遇。中科昊芯创始人兼董事长李任伟表示，全球缺芯让本土芯片企业的推广工作变容易了，大家的心态都发生了改变，大企业更愿意试用本土芯片企业的产品了。

"[四个领域将获最大红利 | RISC-V全新报告 ](https://www.eda365.com/article-185189-1.html
)(From www.eda365.com 2021.05.02)"
> 先进的高性能多核SoC
> 高端多核SoC
> 基本型SoC
> FPGA


---



<a name="6395d96e54fcd2acd131c715687c276b"></a>
## 其他动态


<a name="1af25ae617de29d87abbdf1175b21a48"></a>
#### 视频推荐


<a name="a713c779ca5999a6954be10d0219afb2"></a>
#### 博文推荐
"[设计一个 RISC-V CPU，软件工程师如何学习硬件设计 ](https://fheadline.com/2021/04/20210424141011014J.html
)(From fheadline.com 2021.05.02)"<br />"[从零开始实现一个操作系统 ](https://zhuanlan.zhihu.com/p/366131834
)(From zhuanlan.zhihu.com 2021.05.02)"
> 使用RISC-V模拟器，实现一个操作系统。

"[RISC-V GNU工具链的编译与安装 ](https://zhuanlan.zhihu.com/p/364638851
)(From zhuanlan.zhihu.com 2021.05.02)"<br />"[Tengine 支持 RISC-V 模型部署-全志D1 ](https://zhuanlan.zhihu.com/p/368640131
)(From zhuanlan.zhihu.com 2021.05.02)"
> 由于 C910 同 C906 属于 RV64 的同一系列，指令集兼容，因此将 Tengine 移植到 D1 上只需使用 D1 的交叉编译工具完成“一键编译”即可，Tengine 已经在 v1.4 版本的编译模块中完成了微调，可无缝切换到对 D1 工具链的支持。

"[理解一下RISC-V auipc+jalr 这对指令 ](https://zhuanlan.zhihu.com/p/354654279
)(From zhuanlan.zhihu.com 2021.05.02)"<br />"[RISC-V的P扩展 ](https://chowdera.com/2021/04/20210430114723902F.html
)(From chowdera.com 2021.05.02)"<br />"[RISC-V嵌入式开发（3）：UART串口通信 ](https://zhuanlan.zhihu.com/p/368788214
)(From zhuanlan.zhihu.com 2021.05.02)"<br />"[RISC-V简单介绍 ](https://blog.csdn.net/qq_29023095/article/details/116294980
)(From blog.csdn.net 2021.05.02)"<br />"[南大《计算机系统基础》课程实验 ](https://www.zhihu.com/column/c_1360905350493839361
)(From www.zhihu.com 2021.05.02)"

---

<a name="2010a69b8aaca066239ad10c98eb0e7c"></a>
## 一周论文
<a name="c839388f085ee79bc93daef39451e644"></a>
#### **[PDF]** [MircoRV32: An Open Source **RISC**-**V **Cross-Level Platform for Education and Research](http://www.informatik.uni-bremen.de/agra/doc/work/destion2021-final45.pdf)
S Ahmadi-Pour, V Herdt, R Drechsler - 2021
> 在本文中，我们提出μRV32（MicroRV32）是一个用于教育和研究的开源RISC-V平台。µRV32在一个32位RISC-V内核旁边集成了几个外设，并与一个通用总线系统相互连接。它支持裸机应用和FreeRTOS操作系统。除了用现代SpinalHDL语言实现的RTL（µRV32 RTL）外，我们还提供了相应的二进制兼容的虚拟原型（VP），它是用符合标准的SystemC TLM实现的（µRV32 VP）。结合VP和RTL描述，为RISC-V背景下的高级跨级方法学铺平了道路。此外，基于现成的开源工具流程，µRV32 RTL可以导出到Verilog描述中，用Verilator工具进行仿真或合成到FPGA上。该工具流程非常容易获得，并且在Linux下完全支持。作为实验的一部分，我们提供了一套现成的应用基准，并报告了µRV32在RTL、VP和FPGA层面的执行性能结果，以及一个概念验证的FPGA综合统计。



<a name="8567b5caeee905b92092a32d420c879c"></a>
#### **[PDF]** [A CNN ACCELERATOR WITH EMBEDDED **RISC**-**V **CONTROLLERS](https://www.semiconchina.org/Semicon_China_Manager/upload/kindeditor/file/20210419/20210419152255_306.pdf)
L Zhang, X Zhou, C Guo
> 卷积神经网络是机器学习中一项很有前途的技术。由于其庞大的计算和数据要求，它需要用特定的加速器来运行，以达到合理的能源效率。提高加速器的性能已成为研究热点。混合精度结构可以用来提高硬件利用率，从而减少面积和功率。然而，混合精度的流量控制非常复杂，以至于耗费了太多的硬件资源。本文介绍了一种带有嵌入式RISC-V控制器的CNN加速器，以极低的成本实现灵活的控制。ASIC的合成结果表明，拟议的带有两个嵌入式内核的设计面积比基本设计少5%。



<a name="6f51a18c655e622b1ed73503161fd621"></a>
#### [Bootstrapping a Libre, Self-Hosting **RISC**-**V **Computer](https://apps.dtic.mil/sti/citations/AD1126983)
GL Somlo - 2021<br />

<a name="e149f1f8a03f8775b1647772b5248b99"></a>
#### [Software Implementation and Comparison of ZUC-256, SNOW-V, and AES-256 on **RISC**-**V **Platform](https://ieeexplore.ieee.org/abstract/document/9404134/)
M Wei, G Yang, F Kong - 2021 IEEE International Conference on Information …, 2021
> 作为5G通信中加密基元的候选者，ZUC-256、SNOW-V和AES-256在保密性和完整性方面提供了256位安全。已经有一些关于它们在不同平台上的软件实现的研究。在本文中，ZUC-256、SNOW-V和AES-256是在RISC-V CPU上用标准C语言实现的，没有AES新指令。从密钥流生成和消息验证码生成两个方面对它们的实现性能进行了测量和比较，结果表明ZUC-256在RISC-V平台上比SNOW-V和AES-256算法要快。



<a name="14d8f2298bec3a95baa738b16d3bf7e4"></a>
#### [Low-Power License Plate Detection and Recognition on a **RISC**-**V **Multi-Core MCU-based Vision System](https://ieeexplore.ieee.org/abstract/document/9401730/)
L Lamberti, M Rusci, M Fariselli, F Paci, L Benini - 2021 IEEE International …, 2021
> 在本文中，我们首次（据我们所知）展示了用于自动车牌识别（ALPR）的基于MCU的低功率边缘设备。该设计利用了一个9核RISC-V处理器GAP8，加上一个QVGA超低功耗灰度成像器。拟议的视觉处理管道使用了基于SSDlite-MobilenetV2的多模型推理方法进行车牌检测和LPRNet的光学字符识别，在公共数据集上，第一个任务的mAP得分达到38.9%，后者的识别率超过99.13%。在真实世界的数据上，当LP作物的尺寸小到30×5像素时，该管道可以识别出注册号码。由于应用了压缩和优化策略，多模型推理（687 MMAC）在GAP8上运行时，实现了1.09 FPS的吞吐量，功率成本为117 mW。我们的解决方案是第一个嵌入如此复杂的网络水平的MCU级设备，与之前采用Raspberry Pi3的移动级ALPR系统相比，能源效率提高了73倍。拟议的设计没有采用任何硬接线加速引擎，因此为未来的算法改进保留了充分的灵活性。



<a name="a56c474111fe6a0027dec4fdd6ca3396"></a>
#### [Tiny-FPU: Low-Cost Floating-Point Support for Small **RISC**-**V **MCU Cores](https://ieeexplore.ieee.org/abstract/document/9401149/)
L Bertaccini, M Perotti, S Mach, PD Schiavone… - 2021 IEEE International …, 2021
> 在物联网（IoT）领域，微控制器（MCU）被用来收集和处理来自传感器的数据，并将它们传输到云端。需要浮点（FP）算术范围和精度的应用可以使用高效的硬件浮点单元（FPU）或使用软件仿真来实现。FPU优化了性能和代码大小，而软件仿真则使硬件成本最小化。我们提出了一种新的面积优化的、符合IEEE 754标准的RISC-V FPU（Tiny-FPU），并探讨了RISC-V指令集架构双精度和单精度FP扩展在MCU级处理器上的三种不同实现的面积、代码大小、性能、功率和能源效率。我们表明，Tiny-FPU，在其双精度和单精度版本中，分别比为性能和能源效率而优化的双精度和单精度FPU小54%和37%。当RISC-V内核与Tiny-FPU耦合时，与通过软件模拟FP操作的同一内核相比，我们实现了高达18.5倍和15.5倍的速度提升。



<a name="2fc8da7e510d1da00804fa74bb63c126"></a>
#### 


<a name="a873b3066b803b6d102ba59b761fc7ca"></a>
#### [VOSySmonitoRV: a mixed-criticality solution on 64bit Linux-capable **RISC**-**V **platform](https://webthesis.biblio.polito.it/secure/cgi/set_lang?lang=it&referrer=https%3A%2F%2Fwebthesis.biblio.polito.it%2F18147%2F)
F Caforio - 2021
> 这篇论文是在位于法国格勒诺布尔的Virtual Open Systems公司实习6个月期间完成的。该公司专门为汽车、物联网边缘、云计算解决方案设计和实施低功耗多核和异构平台上的高性能混合关键性虚拟化解决方案。如今，嵌入式系统在许多领域得到了广泛的应用，通常是在混合关键性环境中，即在丰富的操作系统旁边，需要一个具有一定时间和安全约束的实时组件的系统。VOSySmonitor被提议作为基于Arm TrustZone的Arm架构上的混合关键性嵌入式系统的解决方案。论文工作包括在一个64位RISC-V的Linux平台上评估和实现这个解决方案，即VOSySmonitoRV。RISC-V是一个创新和开放的指令集架构，最初是在伯克利设计的，用于支持教育和研究。RISC-V ISA的重要性在于它的开源许可和开放标准，由于其冻结的ISA，每个人都可以投资编写软件，并在类似RISC-V的处理器上永远运行。RISC-V是可扩展的，它的特权架构被批准，也被冻结，允许许多可能的软件堆栈，由于特权级别的不同而定义不同的执行环境。VOSySmonitoRV具有虚拟化的优势，允许两个或更多的操作系统以隔离的方式安全地共同执行，但出于安全原因，它没有利用虚拟化支持。VOSySmonitoRV在M模式下执行，是较高的特权级别。第一个结果是一个原型，从一个公司开始，是在SiFive HiFive Unleashed平台上完成的。两个操作系统的共同执行是在不同的硬件（核心）上进行的，为Linux分配三个硬件，为实时操作系统分配一个。通过这种方式，由于PMP单元提供了内存和外设的隔离，因此有了真正强大的隔离。然而，实时操作系统的工作负荷可以在很长一段时间内由预定的空闲任务来描述。为了有效地使用RTOS系统，并使Linux具有几乎原生的性能，VOSySmonitoRV最具挑战性的功能是在一个单一的系统上共同执行一个安全关键的操作系统和一个非关键的操作系统。这一功能的可行性取决于操作系统之间上下文切换的延迟，因为它必须在一个合理的阈值之下，否则两个操作系统都会有不可接受的性能损失，特别是对于实时操作系统。评估是通过一个定制的基准来完成的，该基准测量特权级别之间上下文切换的延迟。结果对共享核心的实现非常有希望，确保它是一个进一步的优化而不是损失。已经开发了第二个带有共享核心的原型，但它还没有完全投入使用。



<a name="33ad877b033956d3f499edf8a8831858"></a>
#### [UVM Environment for **RISC**-**V **processor](https://webthesis.biblio.polito.it/secure/cgi/set_lang?lang=en&referrer=https%3A%2F%2Fwebthesis.biblio.polito.it%2F18053%2F)
L Barraco - 2021
> 本论文的重点是处理器的功能验证，这里的DUV（验证中的设备）是RISC-V RV32IMFCXpulp。  功能验证确保数字设计的实现在大规模生产之前符合规范。  由于器件尺寸和复杂性的快速增长，功能验证已经成为设计过程中的瓶颈。 功能验证可以占用70％的时间，而设计阶段需要大约30％的时间。 特别是，被验证的器件支持I-M-F-C-X扩展，它们分别是基整数、整数乘除、单精度浮点、压缩和PULP专用。很明显，RTL（寄存器传输级）是非常复杂的，因此，一个适当的验证框架是必要的。 这项工作主要是基于使用通用验证方法（UVM）为RISCV处理器和Python脚本建立一个完整的验证环境，以便根据ISA（指令集架构）生成随机刺激物。 UVM类库为我们提供了验证环境中通常需要的一些组件，如。    -驱动器，它将发生器产生的刺激转化为被验证的设计的实际输入。    -监控器，收集设计的内部信号和它的输出到交易对象。  	计分板--检查器，验证实际结果是否与预期结果相符。

<a name="203726684426b723a55eb9e8d1d02e70"></a>
#### [Design of a fault tolerant instruction decode stage in **RISC**-**V **core against soft and hard errors](https://webthesis.biblio.polito.it/secure/cgi/set_lang?lang=en&referrer=https%3A%2F%2Fwebthesis.biblio.polito.it%2F17871%2F)
M Neri - 2021
> 在现代集成电路中结合性能、功耗和容错是一个真正的挑战。这项工作的目标是提出一种可能的技术，在不影响性能的情况下，检测和纠正RISC-V内核执行单元的瞬时和永久错误。TMR、Standby-Sparing、Alpha Counting和其他技术混合在一起，以保护ALU和Mul-tiplier免受单一瞬时错误和多个永久错误的影响。这种技术有可能应用于任何其他关键部件，其主要优点是它允许受永久性错误影响的部件在其余无问题的子部件中重复使用，从而最大限度地利用资源。

<a name="283076db6baa6edf7e07424d3b05c050"></a>
#### [Design of a fault tolerant **RISC**-**V **instruction execute stage for safety critical applications](https://webthesis.biblio.polito.it/secure/cgi/set_lang?lang=it&referrer=https%3A%2F%2Fwebthesis.biblio.polito.it%2F17869%2F)
L Fiore - 2021
> 由辐射引起的电子设备故障是过去几十年中出现的最具挑战性的问题之一。如今，辐射影响不仅在空间环境中至关重要，而且在海平面上也是如此，因为晶体管的缩小正在影响集成电路的特性。当在恶劣的环境中工作时，固态设备和集成电路可能会被光子、电子、质子、中子、重离子或α粒子直接击中，导致其电气特性的改变。这使这些设备的可靠性和完整性面临风险，如果它们发生在安全关键应用中，还可能导致灾难性的后果。国际标准IEC 61508规定了与安全有关的系统必须满足的要求，以便根据其可靠性水平进行分类和认证。  就硬件设计而言，通过对系统中的所有部件应用冗余概念，可以减轻辐射影响。在本论文项目中，介绍了CV23E40P内核的指令解码（ID）阶段的容错设计，它是一个RISC-V内核，实现了RV32IMC指令集。本论文中的工作包括在一个更广泛的项目中，该项目旨在使整个CV32E40P内核具有容错性。拟议的设计利用了纠错码（ECC）和N-模数冗余（NMR）技术，确保了该阶段中所有组件对单一事件影响（SEE）的容错。具体来说，从硬件优化的角度来看，萧氏代码是最合适的ECC之一。因此，在设计中使用了单次纠错和两次错误检测（SECDED）功能。至于NMR技术，就本论文的目的而言，三乘法（TMR）在硬件开销和容错水平之间取得了最佳权衡。事实上，TMR使用最小级别的冗余，能够在不暂停程序执行的情况下检测和纠正单个错误。然而，在最先进的技术中，一些RISC-V内核已经使用这些技术来缓解瞬时错误。 本论文工作的创新之处在于，除了针对瞬时错误的传统技术外，还设计了一个针对永久错误的特定部分解决方案。具体来说，从辐射的角度来看，ID阶段最关键的部件是寄存器文件，它是整个内核中最扩展的部件。它的设计提供了额外的 "供应 "位置，以取代永久损坏的寄存器，并允许在任何最恶劣的情况下正确执行程序。ID阶段的容错设计的有效性是通过模拟的方法来评估的。在运行CoreMark基准程序的每个模拟核心中注入一个瞬态故障。实现故障注入的系统是基于TCL脚本，利用QuestaSim模拟器的特殊功能（即强制命令），对该阶段的存储器组件进行位翻转。在这些条件下，对软错误的容错水平达到了95%至100%，置信度为99%。

<a name="89f2182b7ad1aae6087b2239592a16d1"></a>
#### [Interfacing a Neuromorphic Coprocessor with a **RISC**-**V **Architecture](https://webthesis.biblio.polito.it/secure/cgi/set_lang?lang=en&referrer=https%3A%2F%2Fwebthesis.biblio.polito.it%2F18187%2F)
A Spitale - 2021
> 如今，神经网络的概念已经到处传播。神经网络通常是指提供所谓的人工智能，它是为数不多的最先进的技术之一，其复杂性和效率不断提高，以克服新的挑战，提供迷人的服务和功能，从图像分类和处理，到语音识别和更多仍在探索的功能。神经网络是一个从人脑中获得灵感的计算系统，利用其平行互连来解决复杂的数据问题，修改其内部参数（训练阶段），以便以更高的精度识别未知的输入数据（推理阶段）。 尖峰神经网络（SNN）代表了一类新兴的神经网络，来自于神经科学研究，旨在准确地再现人类大脑神经元的静态和动态行为。最初的设想是帮助神经科学家丰富他们关于人脑结构和工作原理的知识，SNN已经在计算机科学界引起了关注，原因有几个：电源效率、持续学习、对时空输入的自然支持。  面向物联网（IoT）的应用，运行在能够以有限的电力资源实时分析数据的智能设备上，预计将从采用基于SNN的解决方案中获益良多。 事实上，由于尖峰在时间和空间上是稀疏的，网络的特点是活动性非常低，因此，如果与其他解决方案（如卷积神经网络）相比，整体功耗会大大降低。 为了提高物联网设备在执行基于SNN的算法时的计算能力，论文描述了一种接口解决方案的设计，用于控制一个可重新配置的SNN加速协处理器，命名为ODIN，通过基于RISC-V的片上系统（SoC），不需要任何来自云的远程控制器。所设计的架构利用串行外设接口（SPI）让RISC-V内核配置加速器参数，从而在ODIN上卸载了SNN任务。该系统作为独立设备工作的能力已经通过在没有主机干预的情况下配置合成火链模拟得到了验证。同步火链是一种特殊的尖峰神经元安排，它们被连接在一个循环中，一旦有适当的输入刺激，就会串联起来发射。由于其可预测的行为，可以通过寄存器传输级（RTL）仿真进行监测，因此合成链被用来作为拟议架构的基准。 在一些情况下测试了网络行为，其中一些参数被改变，结果与预期的结果相匹配。因此，证明了拟议的解决方案可以避免使用主机来控制和配置基于SNN的加速器。



<a name="674397b60ae318a1604e424f7504b055"></a>
#### **[PDF]** [Side-Channel Attacks on **RISC**-**V **Processors: Current Progress, Challenges, and Opportunities](https://www.researchgate.net/profile/Mahya-Morid-Ahmadi/publication/351057634_Side-Channel_Attacks_on_RISC-V_Processors_Current_Progress_Challenges_and_Opportunities/links/6081bdea907dcf667bb99225/Side-Channel-Attacks-on-RISC-V-Processors-Current-Progress-Challenges-and-Opportunities.pdf)
MM Ahmadi, F Khalid, M Shafique
> 像RISC-V这样的微处理器的侧信道攻击表现出安全漏洞，导致了一些设计挑战。因此，全面研究和分析这些安全漏洞是非常必要的。在本文中，我们对现代微处理器在侧信道攻击方面的安全漏洞及其各自的缓解技术进行了简要而全面的研究。本文的重点是分析利用功耗的硬件可利用的侧信道攻击和操纵高速缓存的软件可利用的侧信道攻击。为此，我们对RISC-V微处理器上的缓存攻击的适用性和实际影响及其相关挑战进行了深入分析。最后，基于比较研究和我们的分析，我们强调了一些关键的研究方向，以开发能抵御侧信道攻击的强大的RISC-V微处理器。



<a name="113e2c25b82071c8b6501b27e6b4d54e"></a>
#### 


<a name="d4deebcc43e0002e80ecafcd32ffa80e"></a>
#### [ISA extensions in the Snitch Processor for Signal Processing](https://webthesis.biblio.polito.it/secure/cgi/set_lang?lang=en&referrer=https%3A%2F%2Fwebthesis.biblio.polito.it%2F18144%2F)
S Mazzola - 2021
> … Snitch is a tiny in-order, single-issue core based on the **RISC**-**V** open instruction set
> architecture (ISA) … The introduced DSP instructions have been carefully selected from
> the Xpulp custom **RISC**-**V** extension for DSP, based on their impact on software of interest …



<a name="52fd6e75a7b0493aff8a902ef876d99a"></a>
#### [On the design of a reliable current reference for systems‐on‐chip](https://onlinelibrary.wiley.com/doi/abs/10.1002/cta.2955)
R Torres, E Roa, LE Rueda G - International Journal of Circuit Theory and Applications
> 现代片上系统使用多个电流基准，以保证根据所需的性能和功耗进行适当的电路偏置。由于超低功耗系统的需要，新的参考拓扑结构已经出现了。然而，在微安培领域，由于其可靠性和准确性，最常见的架构在基于工业的应用中是首选。在与当前SoC的基准相关的有限文件和数据的激励下，这项工作根据可靠的电压-电流基准架构的固有元素的误差贡献，提出了设计决策中的关键观察。蒙特卡洛模拟显示了5μA的电流基准的结果，在没有修剪和有修剪的情况下，±3σ的误差分别为±4%和±1.3%。此外，在两个不同的SoC中，使用0.18微米CMOS标准技术制造了两个电流参考。跨越电压和温度变化的测量结果显示，50μA±0.24%和5μA±0.8%的电流基准没有修剪。

<a name="1b8754e775752af62b43ad93693d3cd6"></a>
#### **[PDF]** [Minimally Invasive HW/SW Co-debug Live Visualization on Architecture Level](http://www2.informatik.uni-freiburg.de/~wimmer/pubs/pieper-et-al-glsvlsi-2021.pdf)
P Pieper, R Wimmer, G Angst, R Drechsler - 2021
> 我们提出了一个工具，允许开发人员在早期设计阶段调试硬软件和它们之间的互动。我们将一个SystemC虚拟原型（VP）与一个易于配置和互动的图形用户界面和一个标准的软件调试器结合起来。图形化的用户界面将硬件的内部状态可视化。同时，软件调试器监控并允许操作软件的状态。这种共同可视化支持设计理解和硬件/软件交互的实时调试。我们通过一个案例研究来证明它的实用性，在这个案例中，我们调试了一个运行在RISC-V VP上的OLED显示驱动器。



<a name="e46f5d93dc4c600c82f6b9e5b10cf937"></a>
#### **[PDF]** [Towards Reliable Spatial Memory Safety for Embedded Software by Combining Checked C with Concolic Testing](http://www.informatik.uni-bremen.de/agra/doc/konf/DAC-2021-CheckedC-Concolic-Testing.pdf)
S Tempel, V Herdt, R Drechsler
> 在本文中，我们建议将安全的C语言Checked C与协程测试结合起来，以获得一种有效的方法来实现更安全的C代码。Checked C是对C编程语言的现代和向后兼容的扩展，它为编写内存安全的C代码提供便利。我们利用不安全的C语言软件向Checked C语言的增量转换。在每个增量之后，我们利用协程测试，一种有效的测试生成技术，通过搜索新引入的和现有的错误来支持转换过程。



<a name="92596db5c226cb23b58f4dec0c6cc3f5"></a>
#### **[PDF]** [Face-Mask Detection with Micro processor](https://www.koreascience.or.kr/article/JAKO202111236685898.pdf)
H Lim, S Ryoo, H Jung - Journal of the Korea Institute of Information and …, 2021
> 本文提出了一个基于微处理器而不是Nvidia Jetson Board（目前流行的开发套件）的嵌入式系统，用于检测面具和人脸识别。我们为移动和嵌入式视觉应用使用了一类名为Mobilenets的高效模型。MobileNets是基于一个精简的架构，使用深度可分离卷积来建立轻量级的深度神经网络。该设备使用了具有CNN硬件加速功能的Maix开发板，训练模型使用了为移动设备优化的基于MobileNet_V2的SSD（Single Shot Multibox Detector）。为了建立训练模型，我们使用了来自Kaggle的7553个人脸数据。作为测试数据集的结果，AUC（曲线下面积）值高达0.98。


<br />

<a name="3c203c3566dc20957a2ba358517dbb22"></a>
#### [Confidential computing for OpenPOWER](https://dl.acm.org/doi/abs/10.1145/3447786.3456243)
GDH Hunt, R Pai, MV Le, H Jamjoom, S Bhattiprolu… - Proceedings of the …, 2021
> 本文介绍了受保护的执行设施（PEF），一个基于虚拟机的可信执行环境（TEE），用于Power ISA上的保密计算。PEF实现了受保护的安全虚拟机（SVM）。像其他TEE一样，PEF在执行前对SVM进行验证。PEF利用了可信平台模块（TPM）、安全启动和可信启动，以及为Power ISA系统新引入的架构变化。利用这些架构变化需要新的固件，即受保护执行超导器。PEF在最新版本的POWER9芯片中得到了支持。PEF表明，用于隔离的访问控制和用于保密的密码学是保密计算的有效方法。我们特别关注我们的设计如何（i）平衡访问控制和密码学，（ii）最大限度地利用现有的安全组件，以及（iii）简化SVM生命周期的管理。最后，我们评估了SVM与OpenPOWER系统上正常虚拟机的性能对比。



<a name="9649aa848e9c17f0f84c6054d01c82d8"></a>
#### [An Energy-efficient Compressed Sensing Based Encryption Scheme for Wireless Neural Recording](https://ieeexplore.ieee.org/abstract/document/9410565/)
X Liu, AG Richardson, J Van der Spiegel - IEEE Journal on Emerging and Selected …, 2021
> 本文提出了一种基于压缩传感（CS）的无线神经记录的加密方案。通过利用CS同时进行数据压缩和加密，实现了超高的效率。CS通过利用神经信号固有的稀疏性，实现了神经信号的亚奈奎斯特采样，而CS过程同时对数据进行加密，采样矩阵是加密密钥。为了在不安全的无线信道上共享密钥，我们实施了一个基于椭圆曲线加密法（ECC）的密钥交换协议。我们采用了本地密钥洗牌和更新，以消除潜在的信息泄漏风险。CS是在180纳米CMOS技术制造的特定应用集成电路（ASIC）中执行的。混合信号电路被设计用来优化CS操作的矩阵向量乘法（MVM）的功率效率。ECC是在一个基于Cortex-M0的低功耗微控制器（MCU）中实现的。为了防止定时攻击，该实施方案避免了可能的数据依赖性分支。一个无线神经记录器原型已被开发出来，以证明所提出的方案。与传统的实现方式相比，该原型实现了8倍的数据率降低和35倍的功率节省。在加密的无线传输过程中，ASIC和MCU的总体功耗为442μW。重构信号和未压缩信号之间的平均相关系数为0.973，而在20万次攻击中，仅有密码文本的攻击（CoA）实现了不优于0.054。这项工作展示了一个有前途的数据压缩和加密方案，可用于具有安全要求的广泛的低功率信号记录系统。


<br />


---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

