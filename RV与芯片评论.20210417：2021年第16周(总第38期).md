本期概要

- 龙芯：完全自主指令集架构LoongArch
- 全志发布搭载玄铁906的D1处理器
- 乐鑫发布ESP32-C6，集成WiFi6和蓝牙5，RISC-V 32位单核处理器
- SiFive 流片5nm成功
- 机械硬盘涨价50%




---

<a name="7NzFL"></a>
## 重点聚焦


<a name="VoXVb"></a>
#### 相关周报

- <img src="https://images.gitee.com/uploads/images/2021/0403/095032_5076b6d0_5631341.png" alt="image.png" title="from RV4KIDs" width = "22" height = "16" />[semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅农亚历山大: [RISC-V双周报](https://www.rvmcu.com/article-show-id-557.html
)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：[第 29 期 ](https://zhuanlan.zhihu.com/p/364036258
)

---



<a name="gxOhD"></a>
## 技术动态
"[龙芯发布完全自主指令集架构LoongArch ](https://finance.sina.com.cn/tech/2021-04-15/doc-ikmyaawa9794569.shtml
)(From finance.sina.com.cn 2021.04.16)"
> 上月，拥有MIPS指令集的Wave Computing转投RISC-V阵营的消息引发关注。今天，MIPS生态的重要维护者龙芯也放弃MIPS指令系统，发布了完全自主指令集架构Loongson Architecture，简称龙芯架构或LoongArch。

"[乐鑫科技发布首款集成Wi-Fi 6 + Bluetooth 5的RISC-V SoC ](https://www.eet-china.com/info/63068.html
)(From www.eet-china.com 2021.04.16)"
> ESP32-C6 是乐鑫科技首款集成 Wi-Fi 6 + Bluetooth 5 (LE) 的 32 位 RISC-V SoC，具有极低功耗和高性价比，能够大幅提升物联网设备的 Wi-Fi 传输效率，提供安全可靠的连接性能。
> 相关新闻
>
>    - [2020.11.20 乐鑫ESP32-C3发布 ](https://www.yuque.com/riscv/rvnews/olvw6q)

"[首款5nm RISC-V架构处理器流片：ARM对手来了 ](https://finance.sina.com.cn/tech/2021-04-14/doc-ikmyaawa9570959.shtml
)(From finance.sina.com.cn 2021.04.16)"
> 本周二，最大的RISC-V架构厂商SiFive宣布，首款基于台积电5nm工艺的RISC-V架构的SoC芯片成功流片。
> 芯片基于SiFive E76 32bit CPU核心，该核心专为不需要全量精度的AI、微控制器、边缘计算等场景设计。
> - "[SiFive推出业内首颗5纳米RISC-V芯片 ](http://www.semiinsights.com/s/electronic_components/23/42063.shtml
)(From www.semiinsights.com 2021.04.16)"

"[晶心科技RISC-V向量处理器NX27V升级至RVV 1.0 ](http://www.andestech.com/cn/2021/04/09/andes-risc-v-vector-processor-nx27v-is-upgraded-to-rvv-1-0-3/
)(From www.andestech.com 2021.04.16)"
> AndesCore™ NX27V升级支持最新RISC-V向量(RVV)扩展指令1.0版以及支持更多的配置以满足不同市场的需求

"[全志科技发布首颗RISC-V应用处理器 ](https://kuaixun.stcn.com/egs/202104/t20210415_3043598.html
)(From kuaixun.stcn.com 2021.04.16)"
> 全志科技4月15日宣布推出「D1」处理器，其是全球首颗量产的搭载平头哥玄铁906RISC-V的应用处理器

"[Tengine 支持 RISC-V 模型部署-C910 ](https://zhuanlan.zhihu.com/p/363576974
)(From zhuanlan.zhihu.com 2021.04.16)"
> Tengine 开源社区已经初步实现 RV64 Vector 指令的 Convolution 模块，能够验证 Tengine example 中已有的模型。

"[基于网络搜索优化的RISC-V边缘人工智能加速器设计方法 ](https://sme.sustech.edu.cn/index/news/neiye/id/420.html
)(From sme.sustech.edu.cn 2021.04.16)"
> 由南方科技大学余浩教授人工智能芯片设计团队牵头研发、深圳市腾讯计算机系统有限公司、盯盯拍（深圳）技术股份有限公司共同完成。获得“吴文俊人工智能专项奖芯片项目”二等奖

[http://www.wuwenjunkejijiang.cn/upload/editor/20201202105040268.doc](http://www.wuwenjunkejijiang.cn/upload/editor/20201202105040268.doc)<br />

<a name="gHqP6"></a>
#### 一周问答
"[请问想在蜂鸟的risc-v架构下实现一些基本外设功能，比如以太网摄像头这些是否可以不移植操作系统直接添加verilog？ ](https://www.riscv-mcu.com/community-topic-id-122.html
)(From www.riscv-mcu.com 2021.04.16)"<br />

<a name="an5uU"></a>
#### 社区动态
"[RISC-V中国峰会2021大会主席确定！ ](https://zhuanlan.zhihu.com/p/363831631
)(From zhuanlan.zhihu.com 2021.04.16)"
> RISC-V国际基金会董事会成员、中科院计算所副所长包云岗老师将受邀担任本届RISC-V中国峰会的大会主席（学术界方向）




---

<a name="RLWsL"></a>
## 产业风云
"[乐鑫科技净利下滑逾30%众多基金减持 如何提升毛利率成未来关键 ](http://finance.eastmoney.com/a/202104141883018088.html
)(From finance.eastmoney.com 2021.04.16)"<br />
<br />"[晶心科技RISC-V向量处理器NX27V升级至RVV 1.0 ](https://moore.live/news/272732/detail/
)(From moore.live 2021.04.16)"<br />
<br />"[台媒：台积电停接有疑虑的客户订单，飞腾5nm投片喊停 ](https://www.eet-china.com/news/202104121721.html
)(From www.eet-china.com 2021.04.16)"
> 4月8日，美国拜登政府宣布，新增飞腾、申威等七家中国大陆超算实体列入管制名单 。4月11日有台媒报道称，台积电因应美国最新“出口管制”实体清单变化，内部第一时间启动盘点机制，全面停接有疑虑的客户订单。供应链传出，世芯电子（Alchip）代替飞腾向台积电5纳米工艺投片量产的案子已经喊停……

"[2020年十大IP厂商排名，前五位年增长均为两位数 （IPNest 版） ](https://www.ednchina.com/news/a6703.html
)(From www.ednchina.com 2021.04.16)"
> 2021年4月，IPNest宣布了2020年IP市场的报告，该报告显示，全球设计IP销售额在2020年增长16.7％，达到46亿美元，这是自2000年以来最好的增长，也是整个IP市场在去年最大的亮点之一。

<a name="us08D"></a>
#### 评论文集
"[国产芯片工具链机会展望 ](https://www.163.com/tech/article/G7FTLRVE00099A7M.html
)(From www.163.com 2021.04.16)"<br />


---



<a name="CVPaP"></a>
## 其他动态
<a name="xfDo7"></a>
#### 博文推荐

- "[RISC-V入门科普 ](https://posts.careerengine.us/p/60739fef97bdcc4d71bbaa1d?from=latest-posts-panel&type=title
)(From posts.careerengine.us 2021.04.16)"
- "[自制机器人系列（四）：巡线解迷宫机器人（上） ](https://zhuanlan.zhihu.com/p/354239065
)(From zhuanlan.zhihu.com 2021.04.16)"
> 芯来科技举办的《RISC-V处理器嵌入式开发》在线课程，该课程基于全球首颗RISC-V架构的通用量产微控制器GD32VF103，采用理论学习与动手实践相结合的方式，详细介绍了RISC-V嵌入式处理器的基础知识与实战开发等内容。

- "[RISC-V 小白优化学习 ](https://zhuanlan.zhihu.com/p/363901274
)(From zhuanlan.zhihu.com 2021.04.16)"




---

<a name="MyRXD"></a>
## 一周论文


<a name="QwZF4"></a>
#### [Design and Implementation of Arbitrary Point FFT Based on RISC-V SoC](http://scholar.google.co.uk/scholar_url?url=https://ieeexplore.ieee.org/abstract/document/9390787/&hl=en&sa=X&d=4749667653353649362&ei=qYlxYNvcMIjWmgHsiLHQCA&scisig=AAGBfm2v1eP7YRxDZNM9Rie4thKUUDgXwA&nossl=1&oi=scholaralrt&hist=AQxAsJ0AAAAJ:7725229067016820683:AAGBfm0SNCiCPil-K62CoBDZ6Si4YLOjtw&html=&folt=rel)
Z Zheng, X Zhu, H Qian - 2021 IEEE 5th Advanced Information Technology …, 2021
> 本文设计并实现了一种基于RISC-V(第五代减少指令集计算)的专用微处理器架构，仅有20条指令，用于任意点FFT(Fast Fourier Transform)算法。此外，为了实现可扩展性和可重构性，还构建了相应的SoC（System-on-Chip）。采用软硬件协同验证的方法，通过对比MATLAB、Visual Studio 2019和VIVADO 2019.1的仿真结果，验证系统功能的正确性。最后，采用Xilinx Artix-7(XC7A100TFGG484-2)FPGA(现场可编程门阵列)平台对所提出的硬件系统进行实现和原型设计，该系统完全使用了1897个LUT(查找表)、361个FF(触发器)和25个BRAM(块随机存取存储器)，在100MHz下消耗2.016W。实验结果表明，所提出的系统可以通过重新配置软件的参数和扩大存储器的容量来实现任意点的FFT算法，由于面积小、功耗低，适合嵌入式应用。



<a name="9wsFp"></a>
#### [Adaptive simulation with Virtual Prototypes in an open-source **RISC**-**V **evaluation platform](https://www.sciencedirect.com/science/article/pii/S1383762121001016)
V Herdt, D Große, S Tempel, R Drechsler - Journal of Systems Architecture, 2021
> 最近，针对新兴的RISC-V指令集架构(ISA)推出了虚拟原型(VP)，并成为不断发展的RISC-V生态系统的重要组成部分。VP的一个核心组件是指令集模拟器（ISS）。VP应该提供高仿真性能，同时产生准确的结果，这是两个相互冲突的要求。为了解决这个问题，我们提出了一个高效的基于VP的自适应仿真，它是为RISC-V ISA量身定做的，并允许在运行时无缝切换ISS的精度设置。这样就可以有选择地对应用进行尽可能快、尽可能准确的仿真。在本文中，我们重点讨论不同精度设置对性能的影响，并将精度结果的评估留待以后的工作。我们的RISC-V实验，使用裸机和基于操作系统的基准，证明了在ISS中基于JIT的设置可以实现高达543倍的速度提升。



<a name="3mIRl"></a>
#### [**RISC**-**V **Online Tutor and Lab](https://hal.archives-ouvertes.fr/hal-03194370/)
F Morgan, A Beretta, I Gallivan, J Clancy, F Rousseau… - International Conference on …, 2021
> 学习编程的在线培训和实践举措已被工程和计算机科学界所接受。编程培训平台在提供动手操作、自定进度的独立学习方面非常有效，使用一系列浏览器技术、仿真工具、结构化学习和评估。连接到远程集成电路硬件的在线学习，实时探测内部信号，可以促进类似的丰富的、边做边学的体验。本文介绍了RISC-V在线辅导课程，该课程利用所报道的vicilogic平台，提供结构化的、自定进度的、交互式的RISC-V架构和应用培训和练习。本文介绍了课程结构、交互式自主学习教学法以及vicilogic学习、原型设计和课程构建平台中的资源。该课程以数字设计（RISC-V处理器）的应用为切入点和热点，桥 的低级处理器硬件去设计和高级C语言编程之间的差距。本文介绍了用户实践、用户意见和分析的信息。反馈表明，用户满意度很高，自主学习和成绩很有效，验证了技术增强的学习策略，并肯定了平台提供的视觉丰富的交互体验。随着人们对RISC-V开源指令集体系结构的兴趣越来越浓厚，RISC-V的现有书籍数量较少，以及RISC-V基金会鼓励高校和行业培训机构开发培训教材，RISC-V在线辅导课程的推出非常及时。



<a name="I3xzZ"></a>
#### [Constrained Random Verification for **RISC**-**V**: Overview, Evaluation and Discussion](https://ieeexplore.ieee.org/abstract/document/9399722/)
S Ahmadi-Pour, V Herdt, R Drechsler - MBMV 2021; 24th Workshop, 2021
> RISC-V是一个现代开放和免费的指令集架构(ISA)，它是以非常模块化的方式设计的，能够集成自定义指令扩展，以构建高度应用特定的解决方案。广泛的验证和确认对于确保设计满足规范的所有要求至关重要。限制性随机验证（CRV）已被证明是一种非常有效的技术。RISC-V DV是一个强大的CRV框架，它是为RISC-V量身定做的，并由Google积极开发。在本文中，我们基于RISC-V DV框架，对RISC-V的CRV进行了概述、评估和讨论。在评估中，我们通过突变样本来评估RISC-V DV的bug猎取能力，并且我们提供了该框架的额外执行指标。此外，我们还补充了对该方法的讨论，并勾画了该领域未来研究方向的思路，以进一步推动该方法的发展。



<a name="ZsNja"></a>
#### [Register and Instruction Coverage Analysis for Different **RISC**-**V **ISA Modules](https://ieeexplore.ieee.org/abstract/document/9399723/)
P Adelt, B Koppelmann, W Mueller, C Scheytt - MBMV 2021; 24th Workshop, 2021
> 故障覆盖率分析和故障仿真是硬件设计中测试向量鉴定的成熟方法。然而，它们在虚拟原型设计中的作用以及与设计过程中后期步骤的相关性需要进一步研究。我们为二进制软件引入了一个RISC-V指令和寄存器覆盖率的度量标准。该度量标准衡量RISC-V指令类型是否被执行，以及GPR、CSR和FPR是否被访问。分析是通过虚拟原型的方式应用的，虚拟原型是基于一个抽象的指令和寄存器模型，直接对应于它们的位级表示。在这种情况下，我们分析了三种不同的公开可用的测试套件：RISC-V架构测试框架、RISC-V单元测试和由RISC-V Torture测试生成器自动生成的程序。我们讨论了它们的权衡，并表明通过将它们组合成一个统一的测试套件，我们可以得出100%的GPR和FPR寄存器覆盖率和98.7%的指令类型覆盖率。

<a name="ce933dbf"></a>
#### [Faster deep neural network image processing by using vectorized posit operations on a **RISC**-**V **processor](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/11736/1173604/Faster-deep-neural-network-image-processing-by-using-vectorized-posit/10.1117/12.2586565.full)
M Cococcioni, F Rossi, E Ruffaldi, S Saponara - Real-Time Image Processing and …, 2021
> 在机器学习（ML）和深度神经网络的现代应用中，图像和视频的实时处理正变得相当关键。拥有更快的压缩浮点算术可以显著提高此类应用的性能，优化内存占用和信息传输。在这个领域，新颖的posit数系统非常有前途。在本文中，我们利用posit数来评估几种机器学习算法在实时图像和视频处理应用中的性能。未来的步骤将涉及原生posit操作的进一步硬件加速。



<a name="3c4e3d10"></a>
#### **[PDF]** [**RISC**-**V **implementation of the NaCl-library](https://pure.tue.nl/ws/portalfiles/portal/169647601/Berg_S._ES_CSE.pdf)
S van den Berg
> 在本论文中，介绍了由Bernstein、Lange和Schwabe在RISC-V中创建的网络和密码学库（NaCl）的两个本地实现。这些实现是为了让使用RISC-V指令集架构(ISA)的物联网设备更容易安全而创建的。RISC-V是一个开放源码的ISA，具有不同功能的扩展，使公司能够只用必要的指令生产处理器。为了向所有RISC-V处理器提供NaCl，一个实现只使用32位基本指令集。另一个实现则额外使用乘法扩展，这在大多数处理器上都能找到。没有乘法扩展的实现对1024字节xSalsa20流需要121 247个周期，对Curve25519标量乘法需要42 325 262个周期，对1024字节Poly1305计算需要492 454个周期。带乘法扩展的实现分别需要121 910、5 389 988和38 530个周期。这些实现没有依赖于秘密数据的分支条件或负载。这意味着没有来自分支和加载指令的定时攻击直接或间接依赖于秘密数据。

<a name="QkptX"></a>
#### [Efficient LLVM-based dynamic binary translation](https://dl.acm.org/doi/abs/10.1145/3453933.3454022)
A Engelke, D Okwieka, M Schulz - Proceedings of the 17th ACM SIGPLAN/SIGOPS …, 2021
> 仿真其他或较新的处理器架构对于各种用例都是必要的，从确保兼容性到为计算机架构研究提供工具。这个问题通常使用动态二进制翻译来解决，即在程序执行过程中，机器代码被即时翻译到主机架构上。现有的系统，如QEMU，通常关注的是翻译性能，而不是整体程序的执行情况，而像HQEMU这样的扩展，则受限于其底层实现。相反，注重性能的系统通常用于二进制仪表。例如，DynamoRIO尽可能重用原始指令，而Instrew利用LLVM编译器基础架构，但只支持同架构代码生成。在这篇简短的论文中，我们通过重构升降器和实现目标无关的优化，将Instrew泛化为支持不同的客体和主机架构，以便为仿真代码重用主机硬件功能。我们通过添加对RISC-V作为客体架构和AArch64作为主机架构的支持来证明这种灵活性。我们在SPEC CPU2017上的性能结果显示，与QEMU、HQEMU以及原始的Instrew相比有显著的改进。

<a name="bQRnS"></a>
#### [Extending Verilator to Enable Fault Simulation](https://ieeexplore.ieee.org/abstract/document/9399725/)
E Kaja, NO Leon, M Werner, B Andrei-Tabacaru… - MBMV 2021; 24th Workshop, 2021
> 故障模拟是一种用于评估安全关键系统稳健性的技术。该技术的目标是将故障注入系统并观察其行为。为了处理大型复杂的设计，对快速有效的故障仿真技术要求很高。为此，使用了故障仿真器。故障仿真器是一种便于在设计模型上进行故障注入的软件程序，它可以捕捉设计对不同故障类型的响应。本文探讨了扩展具有故障注入功能的硬件模拟器的方法。我们考虑了Verilator，一个开源的硬件仿真器，用于复杂设计的故障仿真。为此，我们对Verilator进行了具有故障注入功能的扩展。Verilator的高性能与增加的故障建模功能相结合，可以提供准确和快速的结果来测量设计的可靠性和鲁棒性。为了评估和验证该方法，我们在几个设计中注入了不同的故障模型。实验结果显示，平均减缓了23%的模拟器运行时间。此外，该技术还被用于评估与安全相关的软件流监控算法的SoC的可靠性。

<a name="SNnpC"></a>
#### [(No) Compromis: paging virtualization is not a fatality](https://dl.acm.org/doi/abs/10.1145/3453933.3454013)
B Teabe, P Yuhala, A Tchana, F Hermenier… - Proceedings of the 17th …, 2021
> 嵌套/扩展页表（EPT）是目前虚拟化系统中内存虚拟化的硬件解决方案。由于它需要进行2D页走动，因此在TLB失误时需要进行24次内存访问（而不是原生系统中的4次内存访问），因此会引起巨大的性能开销。这种二维走页约束来自于利用分页来管理虚拟机（VM）内存。本文表明，在管理程序中，分页是不必要的。我们的解决方案Compromis，一个新颖的内存管理单元，使用直接段来管理虚拟机内存，并结合分页来管理虚拟机的进程。这是第一次证明基于直接段的解决方案适用于整个VM内存，同时保持应用程序不变。依靠310个被研究的数据中心痕迹，论文表明，使用一个内存段可以为高达99.99%的虚拟机提供服务。论文介绍了在硬件、管理程序和数据中心调度器中实现Compromis的系统方法。评估结果显示，Compromis比两种流行的内存虚拟化解决方案：影子分页和EPT的性能分别高出30%和370%。

<a name="j23E8"></a>
#### **[PDF]** [Memory-Efficient Hardware Performance Counters with Approximate-Counting Algorithms](https://people.eecs.berkeley.edu/~ysshao/assets/papers/xu2021-ispass.pdf)
J Xu, S Kim, B Nikolic, YS Shao
> 硬件性能计数器是处理器上用于跟踪硬件活动的特殊寄存器。虽然性能计数器的数据对许多应用都很有用，但由于芯片上的性能计数器数量有限，在有效地同时收集许多事件统计数据方面存在挑战。我们提出了一种有效的硬件性能计数器设计，使用近似计数算法来提高片上跟踪事件的数量，而不会产生明显的内存开销。这些计数器更节省内存，因为它们根据动态概率递增计数并近似于精确计数。与多路复用的硬件性能计数器相比，我们的近似硬件计数器具有统计学上可证明的内存精度权衡，并且完全由硬件管理。

<a name="tBb5r"></a>
#### **[PDF]** [Vertically Integrated Computing Labs Using Open-Source Hardware Generators and Cloud-Hosted FPGAs](https://people.eecs.berkeley.edu/~ysshao/assets/papers/amid2021-iscas.pdf)
A Amid, A Ou, K Asanovic, YS Shao, B Nikolic
> 在过去的十年里，计算系统的设计发生了巨大的变化，但大多数高级计算机体系结构的课程仍然没有改变。计算机体系结构教育位于计算机科学和电子工程之间的交叉点，基于计算系统设计堆栈中适当的抽象层次，在课堂上进行实践练习。以硬件为中心的实验练习往往需要广泛的基础设施资源，而且往往在繁琐的实际实现概念中游刃有余，而以软件为中心的练习则在建模和系统实现意义之间留下了空白，这也是学生以后在专业环境中需要克服的问题。领域特定计算系统的垂直整合趋势，以及软硬件联合设计，通常在课堂讲授中被覆盖，但由于复杂的工具和仿真基础设施，在实验室练习中没有被重新发现。我们描述了我们的经验，通过使用开源处理器硬件实现、基于生成器的硬件设计方法和云托管FPGA，在课堂练习中探索计算机架构概念的软硬件联合方法。这种方法进一步实现了规模化的课程招生、远程学习和跨班级协作的实验室生态系统，在计算机科学和电子工程体验式课程之间建立了一条连接线。


<br />
<br />


---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)



| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| --- | --- | --- | --- | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![image.png](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |


<br />
<br />

