![输入图片说明](https://images.gitee.com/uploads/images/2021/0906/101011_f82822b0_5631341.jpeg "DIFFTEST 3-999.jpg")

[差分测试学习法](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md#%E4%B8%80%E5%B7%AE%E5%88%86%E6%B5%8B%E8%AF%95%E5%AD%A6%E4%B9%A0%E6%B3%95)证明是有效的，才[发布MD](https://gitee.com/RV4Kids/RVWeekly/edit/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md)，晚上觉得可以[写新作业啦](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H)，没想到上节课的各种疑问，这节课一重温都明白啦。实验也顺利，直接开挂，[38 Replies Issue](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H) [搞定 Verilator](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549306_link) 例子工程 [rvcpu 也顺利编译成功](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549310_link)，[VCD 也在 GtkWave 正确显示](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549301_link)。完美 :pray: ，主要是为新作业，还准备了[新封面](https://images.gitee.com/uploads/images/2021/0906/101011_f82822b0_5631341.jpeg)，先睹为快。

> 这是最 "偷懒" 的一期期刊封面了，一看就是用现成的 前期 “乱如麻的人脑” 配上 树莓派 顶 + 标题图 用心构建 ( UBUNTU + CORE + RASPBERRY pi) 而 本期 SIGer 编委上的 RV4kids 标志 增加了 [RV 少年 正青春](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209%20-%20RV%20%E5%B0%91%E5%B9%B4.md) 的新标。没有更多辞藻，用 石榴派国际漂流的寄语，作为致敬。

  - [用 “心” 构建树莓派桌面](https://gitee.com/flame-ai/siger#note_6545828)  
    [Build a Raspberry Pi Desktop with an Ubuntu heart sincerely](https://gitee.com/flame-ai/siger#note_6545832)

  - [用 “心” 构建石榴派桌面](https://gitee.com/flame-ai/siger#note_6545835)  
     _Build a Shiliu Pi 99 Desktop with a Shiliu Si heart sincerely_ 

> 下面是标题文章节选，后面附上本期清晰明了的 “[学习笔记](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6644871_link)” 5 Issues and 82 replies。

# [Ubuntu 用 “心” 构建树莓派桌面](https://gitee.com/RV4Kids/NutShell/issues/I48LTS#note_6538015_link)

2020年十月22日，Canonical发布了专为树莓派优化的Ubuntu桌面系统镜像。树莓派基金会的4GB和8GB开发板将获得用户所期望的完整的Ubuntu桌面，同时使得与世界上最流行的开发板联系更紧密。我们也很荣幸通过优化树莓派上的Ubuntu镜像以支持树莓派基金会将计算的力量带给全球用户的使命。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0904/011654_6376c0fd_5631341.png "在这里输入图片标题")

### 结语

完整版Ubuntu桌面现在树莓派上已可用。有了它，用户可在世界上最流行和通用的单板计算机上对完整的Linux工作站访问。该开发不仅为更实用的Raspberry Pi桌面体验铺平了道路，而且为在ARM上运行的云计算和应用程序的新世界铺平了道路。我们对Raspberry Pi基金会深表钦佩，并希望将来与他们及其技术合作。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0904/011800_21a83b38_5631341.png "在这里输入图片标题")

### [学习笔记](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6644871_link)

1. [OpenSSH Server](https://gitee.com/RV4Kids/NutShell/issues/I48MAE)

   由于 UBUNTU 的键盘失效的问题，我选择安装后直接启动 OPENSSH 的方案以实现可以安装和更新包的能力，所以锁定了 OpenSSH.

   - [sudo apt install openssh-server](https://gitee.com/RV4Kids/NutShell/issues/I48MAE#note_6541259_link)

     - 在 PC 上链接下，成功：
     - 安装个 JAVA 试试, 相当地 NICE ，welldone.

   - [sudo: git: command not found](https://gitee.com/RV4Kids/NutShell/issues/I48MAE#note_6541424_link)

   - [git clone : LuckyLudii & luckystar & shiliupi99](https://gitee.com/RV4Kids/NutShell/issues/I48MAE#note_6541441_link)

   - [Build a LAMP Web Server with WordPress (Wiki)](https://gitee.com/RV4Kids/NutShell/issues/I48MAE#note_6541477_link)

   至此，shiliupi99 全部应用都安装到 Ubuntu 啦，意外收获一个发行版。:D

2. [How to Fix Keyboard Not Working After Ubuntu Update (2020)](https://gitee.com/RV4Kids/NutShell/issues/I48M0U)

   > 这个帖子，写了各种情况，最后依然无法解决 UBUNTU 安装后 键盘失灵的问题。奇怪的是，只有部分功能失灵，而非全部，用软键盘，居然输入不了回车，太闹心啦。

3. [UBUNTU 21.04 FOR IOT - RPI4](https://gitee.com/RV4Kids/NutShell/issues/I48LTS)

   地球人都选择树莓派，不是没有道理的，UBUNTU 的工程师都期待未来进行技术合作。石榴派自然也没有必要回避，上树莓派。(看到了 NUC 的名字，这是我们的子安同学的装备呢。IOT 是未来。)

   - https://canonical.com/ 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48LTS#note_6537600_link)】

     > 顺便调研了的 母公司，提供企业服务是必须的。BEST sevices 9+6 都是如雷贯耳的品牌呢。Partners 自然也都响当当。

   - [Ubuntu 用 “心” 构建树莓派桌面](https://cn.ubuntu.com/blog/build-raspberry-pi-desktop-ubuntu) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48LTS#note_6538015_link)】

     > 这篇写与去年十月22的文章，是正赶着 RPI4 发布，这个全球品牌的示好文，自然成为了本期的头条和封面。核心观点，也就是 UBUNTU CORE 对 IOT 设备的适配能力

       - 刚刚好的硬件
       - 工程协作
       - 开源的工作站
       - 专为树莓派定制的桌面
       -  **结语就是标题** ，也是封面素材的源头。

   - [Canonical 和 DFI 发布第一款 Ubuntu 认证的基于 AMD 的 “工业 Pi”](https://gitee.com/RV4Kids/NutShell/issues/I48LTS#note_6549119_link)

      > 小尺寸，高性能，领略了工业级别的开发板的风采。也是UBUNTU的所爱。

   - 延伸阅读 | [获取Ubuntu Core的最新技术和产品信息](https://gitee.com/RV4Kids/NutShell/issues/I48LTS#note_6548835_link)

     > BLOG 和 认证硬件，全面展现了 UBUNTU 的服务能力，我特地精选了 39 个认证的开发板，包括 XILINX ZYNQ, 还有全部 1605 个认证信息中，desktop, device, soc 三类。

4. [让人又爱又恨的 UBUNTU 20.04 我来了](https://gitee.com/RV4Kids/NutShell/issues/I48DW8)

   这是痛苦的回忆，再完美的操作系统，也依然有很多的坑（不能满足适配）。这是 安装 NUTSHELL 学习环境必须交的学费。—— 牺牲了两块老硬盘，就是因为没有网线（这个梗会在后面深埋）

   - [装无线驱动（WIN 免驱，LINUX 没有驱动。）](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6514335_link)
   - 免费下载 Realtek 8188GU Wireless LAN 802.11n USB NIC Wi-Fi 设备 驱动程序（[不是LINUX的](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6514350_link)。）
   - [No wifi connection](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6514414_link) 人家是先有线，再装无线啊，我只有无线。
   - [SOLVED] Realtek 8811CU Wireless LAN 802.11ac USB NIC （[还是一样，俺妹没有有线。](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6514471_link)）
   - https://gitee.com/RV4Kids/rtl8821cu_wifi_driver （被我删除了）【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6514768_link)】
   - [Ubuntu Windows 双系统和USB无线网卡安装的正确方法](https://www.zhihu.com/tardis/sogou/art/157825578) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6516223_link)】
   - [腾达(U12)USB无线网卡Linux驱动安装笔记](https://www.cnblogs.com/idorax/p/12369624.html) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6516339_link)】
   - https://gitee.com/RV4Kids/rtl8812au 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6516512_link)】
   - [无网环境下在下面这个网址进行下载，在通过xftp上传到linux系统中](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6516930_link)
   - [因为没有网路，不只缺一个包，很多包都缺，最后放弃啦。 **没有网线的日子实在是太难啦** 。](https://gitee.com/RV4Kids/NutShell/issues/I48DW8#note_6537513_link) :sob:
  
5. [Building Verilotar & Chisel 3 @ UBUNTU 21.04 FOR IOT - RPI4](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H)

   依据刚发布的 [Differential testing to LEARNING](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md) [第五章](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md#%E4%BA%94%E5%86%8D%E8%AF%BB-nutshell-doc%E4%B8%89%E6%9C%9F%E8%A1%A5%E8%AF%BE-1-6)  **补课2 提交的作业** ，遗留了一个未完成开发环境，以及 构建 NutShell 在石榴派上的大目标，今天开始 正式在 RP4 上的 UBUNTU 21.04 构建 开发环境。

   - [笔记截图](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6435064_link)：对课程中的细节都做了重点标注，并截图。是个好记性不如烂笔头的方法。

     >  **VERILATOR 是编译器可以把 硬件描述语言编译成 C++ 用你的 C++ 语言调用它的库进行调试** 

   - 4. 增加 "一生一芯" [开发环境一键搭建脚本](https://gitee.com/oscpu/oscpu-framework#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)

   1. [开发环境一键搭建脚本](https://gitee.com/oscpu/oscpu-framework#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549144_link)】

      - [Your Linux branch does not meet the requirements, please use Ubuntu 20.04.](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549145_link)

        > 手工修改下版本号到 21.04

      - nano oscpu-env-setup.sh
        ./oscpu-env-setup.sh 
        [finished](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549161_link).

      - [选择使用chisel语言开发](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549192_link)

        > `./oscpu-env-setup.sh -g -c` 两个环境就差两个参数, 探究下原因，结果发现 命令结尾是删除，快去看看还在吗？

        - 解读下这个一键安装，分4个步骤：

          1. WGET 刚才已经先运行了，结果是20.04限制，我们已经改好了
          1. chmod +x oscpu-env-setup.sh， 这是一个 使能命令，就是让 .sh 拥有执行的权力【笔记】
          1. 就是安装，两个参数的区别，下帖再说。
          1. 删除掉这个安装程序（这么好的学习资源，怎么能删掉呢）

      - chmod +x，赋予“可执行”权限([学会一招儿](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549203_link))

      - [发现一个 GITEE 回帖顺序的技巧，目前GITEE 只支持三级回帖。](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549205_link)

      - [cat oscpu-env-setup.sh 为了研究 -g -c 两个参数的作用。](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549206_link)

      - `./oscpu-env-setup.sh -g` [看反馈信息 GITWAVE 和 MILL 还有 VERILATOR 一个都没装上](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549212_link)。

      - [install_package gtkwave libcanberra-gtk-module 这个容易](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549226_link)

      - [install_mill()](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549233_link)  linux查看软件的安装位置简单方法（which whereis mill）找到了。
  
   2. [verilator 安装教程 还是要自己构建啊：VERILATOR](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549241_link)  

      是编译器可以把 硬件描述语言编译成 C++ 用你的 C++ 语言调用它的库进行调试

       **1.1.2 方式二 编译安装 Verilator** 

      - [Install Prerequisites](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549252_link)
      - sudo apt-get install git perl python3 make 安装基础包 【[笔记1](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549253_link)】【[笔记2](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549256_link)】
      - sudo apt-get install autoconf ... 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549265_link)】
      - sudo apt-get all ... （[可以通过一条指令安装所有](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549266_link)）
        > 发现无法定位的，一定要删除，再来一遍，保证所有包都安装好。
      - [Obtain Sources](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549269_link) 
 `git clone`
      - [Every time you need to build: autoconf](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549270_link)
      - 前两句 [环境变量 VERILATOR_ROOT](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549274_link)
      - get pull / git checkout v4.204 [锁定 VERILATOR 版本号](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549275_link)
      - [4. Install System Globally](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549277_link)  autoconf make make install.
      - [./configure](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549278_link)
         _Now type 'make' (or sometimes 'gmake') to build Verilator._ 
      - [Compile (make j)](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549280_link)
      - make j error 【[笔记1](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549283_link)】【[笔记2](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549285_link)】
      - make命令参数和选项大汇总
        > -j [JOBS]，--jobs[=JOBS] 可指定同时执行的命令数目，爱没有 "-j" 的情况下，执行的命令数目将是系统允许的最大可能数目，存在多个 "-j" 目标时，最后一个目标指定的 JOBS 数有效。
        > 消耗内存。make: 【[笔记1](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549286_link)】【[笔记2](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549291_link)】【[笔记3](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549292_link)】
      - [make test](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549293_link) 运行了四五遍，第一遍确实查出几处需要重BUILD的地方。最后两边就快多了。
      - make install / sudo [make install](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549294_link)
        > For documentation see 'man verilator' or 'verilator --help'

   3. [获取代码](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549296_link)

      `sudo git clone --recursive -b 2021 https://gitee.com/oscpu/oscpu-framework.git oscpu`

      - [参与一生一芯还需要设置git信息。](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549297_link)
      - [./build.sh -e counter -b -s](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549298_link)
      - [sudo ./build.sh -e counter -b -s](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549300_link)
      - [ls projects/counter/build_test/](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549301_link)
      - [正确安装好环境啦](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549306_link)。:pray:

      ![输入图片说明](https://images.gitee.com/uploads/images/2021/0906/063626_9f33b893_5631341.png "在这里输入图片标题")  

   4. [sudo ./build.sh -b -t rvcpu.v -s](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549310_link)

      - [GTKWave Analyzer v3.3.104 (w)1999-2020 BSI](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549311_link)
  
   5. [封面](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6551337_link)

      - [刚看封面，特别像果汁的颜色](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6564775_link) :stuck_out_tongue_closed_eyes:

   6. [Issue 14: Difftest Learning.md](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md) [成刊啦](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6564774_link)。

