目录：

- [RISC-V World Conference China](#risc-v-world-conference-china)
- [Shiliu PI, Shiliu Silicon, Shiliu Si, 石榴核](#shiliu-pi-shiliu-silicon-shiliu-si-石榴核)
- [RP_Silicon](#rp_silicon)

  - The journey to Raspberry Silicon
  - 认识 Raspberry Silicon：Raspberry Pi Pico 现在售价 4 美元
  - Raspberry Silicon 更新：RP2040 现已发售，价格为 1 美元

- [RV与芯片评论.20210619：2021年第25周(总第47期) preversion](#rv%E4%B8%8E%E8%8A%AF%E7%89%87%E8%AF%84%E8%AE%BA202106192021%E5%B9%B4%E7%AC%AC25%E5%91%A8%E6%80%BB%E7%AC%AC47%E6%9C%9F-preversion)
- 学习小组 [论坛](#%E5%AD%A6%E4%B9%A0%E5%B0%8F%E7%BB%84-%E8%AE%BA%E5%9D%9B) [点评](#%E5%AD%A6%E4%B9%A0%E5%B0%8F%E7%BB%84-%E7%82%B9%E8%AF%84)
- [星辰，机遇，使命](#%E6%98%9F%E8%BE%B0%E6%9C%BA%E9%81%87%E4%BD%BF%E5%91%BD)

附：
- [学习 OPENWRT 的仓](#%E5%AD%A6%E4%B9%A0-openwrt-%E7%9A%84%E4%BB%93)

---

###  **RISC-V World Conference China**   
https://riscv.org/event/risc-v-world-conference-china/

June 22 @ 12:00 am - June 24 @ 12:00 am

> The first RISC-V World Conference in China will be hosted at ShanghaiTech University from June 22 to 24, 2021. This conference is a major international event for promoting RISC-V, bringing together the community for a multi-track conference, tutorials, exhibitions and more. The RISC-V Shanghai Day 2018 had more than 800 attendees around China. This year’s International Conference is expected to have more than 1000 attendees, more than 100 exhibitors and 90 presentations. 中国首届RISC-V世界大会将于2021年6月22日至24日在上海科技大学举办。本次大会是推动RISC-V的重大国际盛会，汇聚社区进行多轨道会议、教程、展览和更多。2018 年 RISC-V 上海日在中国有超过 800 名与会者。今年的国际会议预计将有1000多名与会者、100多家参展商和90场演讲。

![Image description](https://images.gitee.com/uploads/images/2021/0622/035826_67569f85_5631341.png "屏幕截图.png")

昨天中午，乘坐国航飞抵上海，正午阳光不能抵挡好心情，因为这份海报就是为这次大会准备的，凌晨在闷热的午夜还在赶稿绘制的我，丝毫没有倦意，直接迎来了21日北京的日出，4:00am. 顺利完成满意的海报后，心情自然不用说。

迎接我的是地铁站口的上海科技大学的志愿者，将近14点，站好最后一班岗的两位小伙子，陪同我一同来到校门口，在我的要求下高兴的与我合影留念。

了解了会场的布置，我决定在 POST 展区的公共展台，为石榴派增加布展，将这份海报和今天将进行的 POSTER 演讲时介绍的 A0 海报合并一起，为石榴派RV版本的发布站台。

![Image description](https://images.gitee.com/uploads/images/2021/0622/040431_6d6b29d5_5631341.png "屏幕截图.png")

而这份 SIGer 新期刊的封面，就作为我们的定海神针，在180-80 X展架的最下面平铺开来，支持今天的演讲，这是石榴派的缘起，树莓派最新的 RP 2040 的技术手册的封面改进来的。缘起，故事，将成为今天与来展台前小伙伴分享的全部内容。这里的日记体，就是开端。

# Shiliu PI, Shiliu Silicon, Shiliu Si, 石榴核

这就是今天的头条，[石榴核](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VF6N)，也是当期 SIGer 期刊 RV4kids 的第2期的封面故事。让我们共同期待吧...

> 这是自然的发展过程，学习树莓派从 PI400 到 RP silicon 在自由电脑的道路上，我们又前进了一步。  
> 我将借助这个项目学习，开始相关知识的汇集。
>
> 石榴核的条件
>
> - 石榴派的 北向方案
> - Linux Kernel 的替代方案
> - 国产 FPGA 的能力
> - 直到最终 流片成功。Shiliu SI

### RP_Silicon

- 树莓派硅之旅 [The journey to Raspberry Silicon](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3UI05)
  - When I first joined Raspberry Pi as a software engineer four and a half years ago, I didn’t know anything about chip design. I thought it was magic. This blog post looks at the journey to Raspberry Silicon and the design process of RP2040. 四年半前，当我第一次以软件工程师的身份加入 Raspberry Pi 时，我对芯片设计一无所知。我以为这是魔术。这篇博文着眼于 Raspberry Silicon 之旅和 RP2040 的设计过程。
  - 树莓派 Pico 上的 RP2040
  - RP2040 – Raspberry Pi Pico 的核心
  - RP2040 – the heart of Raspberry Pi Pico

- 认识 [Raspberry Silicon：Raspberry Pi Pico](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3UHV1) 现在售价 4 美元
  - Today, we’re launching our first microcontroller-class product: Raspberry Pi Pico. Priced at just $4, it is built on RP2040, a brand-new chip developed right here at Raspberry Pi. Whether you’re looking for a standalone board for deep-embedded development or a companion to your Raspberry Pi computer, or you’re taking your first steps with a microcontroller, this is the board for you. 今天，我们将推出我们的第一个微控制器级产品：Raspberry Pi Pico。它的价格仅为 4 美元，基于 RP2040，这是一种在 Raspberry Pi 开发的全新芯片。无论您是在寻找用于深度嵌入式开发的独立开发板还是与 Raspberry Pi 计算机配套使用，或者您正在使用微控制器迈出第一步，这都是适合您的开发板。

- [Raspberry Silicon](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3UHU4) 更新：RP2040 现已发售，价格为 1 美元
  - Back in January, we launched Raspberry Pi Pico. This was a new kind of product for us: our first microcontroller-class board, and the first to be built on RP2040, a chip designed here at Raspberry Pi. At the same time, we announced RP2040-based products from our friends at Adafruit, Arduino, Sparkfun, and Pimoroni. 早在 1 月份，我们就推出了Raspberry Pi Pico。这对我们来说是一种新产品：我们的第一个微控制器级电路板，也是第一个构建在RP2040上的产品，RP2040是在 Raspberry Pi 设计的芯片。同时，我们从Adafruit、Arduino、Sparkfun和Pimoroni 的朋友那里宣布了基于 RP2040 的产品。
  - Today, we’re announcing the logical next step: RP2040 chips are now available from our Approved Reseller partners in single-unit quantities, allowing you to build your own projects and products on Raspberry Silicon. 今天，我们宣布了合乎逻辑的下一步：RP2040 芯片现在可以从我们的授权经销商合作伙伴处以单件数量提供，让您可以在 Raspberry 芯片上构建自己的项目和产品。

---

### RV与芯片评论.20210619：2021年第25周(总第47期) preversion

以预刊的名义撰写，是没有等到 @[lj-k](https://gitee.com/lj-k) 小孔老师的新刊，祝他工作顺利，完成月底的里程碑，和同学们一起度过即将开始的暑期，2021的假期，注定与众不同，意义非凡。

> 关于本刊: 
> 
> - 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
> - [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)
> 
> 
> 
> | 标题 | 内容 |
> |---|---|
> | 语雀 | [RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br > https://www.yuque.com/riscv/rvnews |
> | Inspur | http://open.inspur.com/riscv/RVWeekly |
> | 微信公众号 | 高效服务器和存储技术国家重点实验室<br /> http://www.hsslab.com <br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "屏幕截图.png") |
> | Github | https://github.com/inspur-risc-v/RVWeekly |
> | Gitee | https://gitee.com/inspur-risc-v/RVWeekly |
> | [RV4Kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) | [(1)](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md) [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md) |

这就是头条了，
- [RV4Kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids)
- [(1)](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)
- [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md)

[浪潮RISC-V研究小组](https://gitee.com/inspur-risc-v) / [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) 接受了我的 PR 
[RV4kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) 正式出刊了。
from 2021-03-25 10:54 https://gitee.com/RV4Kids/RVWeekly/issues/I3D4UK
to Jun 12 09:32 [Lightweight PR]: [update README.md. adding RV4Kids](https://gitee.com/inspur-risc-v/RVWeekly/commit/c5d4f1eebab515073d2f2546fed7d6ff75e58cd1)

这记录下了 [RV4kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) 的诞生，将开启一段全新的学习之旅。
[SIGer](https://gitee.com/flame-ai/siger) [学习小组](https://gitee.com/RV4Kids/RVWeekly/issues)。今天下午的 POSTER TIME 我将用 “ **同学们迎来了最好的时代 RISCV的时代** ” 纪念这个时刻。

### [学习小组](https://gitee.com/RV4Kids/RVWeekly/issues) 论坛：
| | [issues](https://gitee.com/RV4Kids/RVWeekly/issues) | post time |
|---|---|---|
| | #[I3D4UK](https://gitee.com/RV4Kids/RVWeekly/issues/I3D4UK):我们的目标是 RISCV-CHINA WEEKLY :D | 2021-03-25 10:54 |
| | #[I3D8SW](https://gitee.com/RV4Kids/RVWeekly/issues/I3D8SW):RVNews RV WIKI 类 贡献一个PR。 | 2021-03-25 17:24 |
| | #[I3ECMG](https://gitee.com/RV4Kids/RVWeekly/issues/I3ECMG):国产芯片再突破：全球首款RISC-V AI单板PC发布 | 2021-03-31 11:09 |
| top | #[I3EVDH](https://gitee.com/RV4Kids/RVWeekly/issues/I3EVDH):RV4Kids Weekly Select (1) 每周精选1 《计算机组成与设计：RISC-V》 | 2021-04-03 10:09 |
| | #[I3GJDE](https://gitee.com/RV4Kids/RVWeekly/issues/I3GJDE):UCTECHIP寒假训练营活动回顾 Winter Trainning Camp 2018 | 2021-04-05 23:58 |
| top | #[I3HQOO](https://gitee.com/RV4Kids/RVWeekly/issues/I3HQOO):操作系统(RISC-V) ：电子工程世界（大学堂） | 2021-04-07 15:06 |
| | #[I3HW35](https://gitee.com/RV4Kids/RVWeekly/issues/I3HW35):​iPhone芯片前传：苹果与Arm的那段过往 | 2021-04-08 12:59 |
| top | #[I3HY6Q](https://gitee.com/RV4Kids/RVWeekly/issues/I3HY6Q):PLCT实验室招募实习生啦！做编译器哦！ | 2021-04-08 16:43 |
| top | #[I3I74V](https://gitee.com/RV4Kids/RVWeekly/issues/I3I74V):RV4Kids Weekly Select (2) 第37期精选 《开发一个RISC-V上的操作系统》 | 2021-04-10 14:27 |
| | #[I3I8F9](https://gitee.com/RV4Kids/RVWeekly/issues/I3I8F9):riscv soc 设计 | 2021-04-11 02:02 |
| | #[I3I8ZE](https://gitee.com/RV4Kids/RVWeekly/issues/I3I8ZE):外国小哥科普开源指令集RISC-V处理器 | 2021-04-11 13:15 |
| | #[I3IA6B](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B):[Verilog入門教學]与背景知识 | 2021-04-12 04:02 |
| end | #[I3IHBN](https://gitee.com/RV4Kids/RVWeekly/issues/I3IHBN):PDF 转 MARKDOWN 小鹤的文章可以接受 PR 啦！ | 2021-04-13 08:43 |
| | #[I3IUMG](https://gitee.com/RV4Kids/RVWeekly/issues/I3IUMG):IBM J9 Java 虚拟机正式开源，贡献给 Eclipse 基金会管理 | 2021-04-14 23:52 |
| | #[I3J1XA](https://gitee.com/RV4Kids/RVWeekly/issues/I3J1XA):【专访】张先轶博士：手持开源“大刀”，RISC-V如何从“低端量变”到“高端质变”？ | 2021-04-16 01:47 |
| | #[I3JMJZ](https://gitee.com/RV4Kids/RVWeekly/issues/I3JMJZ):RV4Kids Weekly Select (3) 第38期精选 《全志科技发布首颗RISC-V应用处理器》 | 2021-04-17 20:42 |
| | #[I3KP0S](https://gitee.com/RV4Kids/RVWeekly/issues/I3KP0S):【RISC-V学习】我是怎么拿到平头哥半导体的开发板的 | 2021-04-18 11:33 |
| | #[I3MSS7](https://gitee.com/RV4Kids/RVWeekly/issues/I3MSS7):单片机可以运行java吗？java me. 就是做这个的, 不过是直接跑虚拟机 | 2021-04-19 20:52 |
| ing | #[I3MYFE](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE):RV4Kids FPGA 课程介绍： | 2021-04-20 11:05 |
| ing | #[I3PQY6](https://gitee.com/RV4Kids/RVWeekly/issues/I3PQY6):为 RVWeekly 建立 RSS 聚合 | 2021-05-06 01:37 |
| | #[I3PXBU](https://gitee.com/RV4Kids/RVWeekly/issues/I3PXBU):硅谷教父John Hennessy：我们正站在计算机架构第五时代的门槛上 | 2021-05-06 20:50 |
| | #[I3PXC1](https://gitee.com/RV4Kids/RVWeekly/issues/I3PXC1):过程记录：编译 RISC-V 工具链 | 2021-05-06 20:53 |
| | #[I3Q84O](https://gitee.com/RV4Kids/RVWeekly/issues/I3Q84O):Sipeed+平头哥：玄铁906是K210之后的下一块开发板 | 2021-05-07 19:44 |
| | #[I3R2GD](https://gitee.com/RV4Kids/RVWeekly/issues/I3R2GD):ZYNQ7020折腾之路 - xddcore - ee专业 - chiselos.com | 2021-05-12 22:13 |
| | #[I3RNRN](https://gitee.com/RV4Kids/RVWeekly/issues/I3RNRN):懂得分享的人，往往能收获更多 | 2021-05-17 16:33 |
| | #[I3SIEV](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEV):人工智能芯片观察：FPGA的过去，现在和未来 | 2021-05-23 05:12 |
| | #[I3SIEZ](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEZ):CPU、GPU、FPGA、ASIC等AI芯片特性及对比 | 2021-05-23 05:53 |
| | #[I3SIF0](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIF0):人工智能时代传统CPU已经落伍了吗？ | 2021-05-23 06:04 |
| | #[I3SISM](https://gitee.com/RV4Kids/RVWeekly/issues/I3SISM):采用了大量FPGA，即将发射的美国火星漫游车技术揭秘 | 2021-05-23 12:49 |
| | #[I3SJ77](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ77):护航“毅力号”火星探索之路 | 2021-05-23 16:01 |
| | #[I3SJ8R](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ8R):一仗花掉350亿美金，她是芯片争霸战里最能打的华裔女性 | 2021-05-23 16:26 |
| | #[I3SJCD](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJCD):赛灵思收购峰科 Falcon 计算 | 2021-05-23 17:31 |
| | #[I3STL5](https://gitee.com/RV4Kids/RVWeekly/issues/I3STL5):RV与芯片评论.20210522：2021年第21周(总第43期) | 2021-05-25 13:18 |
| | #[I3TUEU](https://gitee.com/RV4Kids/RVWeekly/issues/I3TUEU):RV4Kids Weekly Select (3) 第44期精选 《RVfpga直播: 首个RISC-V中文课》 | 2021-06-01 16:44 |
| | #[I3UEA7](https://gitee.com/RV4Kids/RVWeekly/issues/I3UEA7):FPGA based Any | 2021-06-04 17:24 |
| | #[I3VG2T](https://gitee.com/RV4Kids/RVWeekly/issues/I3VG2T):RV4Kids Weekly Select (4) 第46期精选 《Intel收购SiFive》 | 2021-06-12 23:38 |
| | #[I3WPML](https://gitee.com/RV4Kids/RVWeekly/issues/I3WPML):RISC-V World Conference in China June 21. - 26. | 2021-06-22 03:58 |

截至发稿 2021-6-22 5:15am 从学习论坛的发帖时间和标题分类可以清晰看出 RV4kids 的学习曲线。
首先是置顶的4篇，为 慕课学习笔记 包括 PLCT 实验室的视频课以及 RVWeekly 早期推荐的几篇视频教程
其中 《[操作系统(RISC-V) ：电子工程世界（大学堂）](https://gitee.com/RV4Kids/RVWeekly/issues/I3HQOO)》 是我第一次精读的课程，看了两遍，并认真做了课堂笔记分享在下面，是第一次借助 RISC-V 对自己过往知识库缺失的一次补充，也为后面阅读其他慕课打下了基础，可以更加重点的查漏补缺。相当于 “预习” 篇。在预习阶段，查阅了相当的学习资料，包括 [“芯来科技” 的蜂鸟开发板](https://gitee.com/RV4Kids/RVWeekly/issues/I3WPML#note_5533025_link)。在今天中午下午会有两场 新版教材 的签名赠书环节。

置顶的 《[PLCT实验室招募实习生啦！做编译器哦！](https://gitee.com/RV4Kids/RVWeekly/issues/I3HY6Q)》 是本次 RISCV 2021 中国峰会主办方 中科院软件所 wuwei 老师的帖子，是全面了解 RISCV 的重要窗口，也是在精读了前半部分的 《[开发一个RISC-V上的操作系统](https://gitee.com/RV4Kids/RVWeekly/issues/I3I74V)》 同时也是 RVWeekly 推荐的课程后，结合前面的预习，让 RV4kids 的第一个学员 我本人 的知识地图更饱满了。而且在之后，和主讲人汪老师达成默契，将完成一本教参的编写，出书的节奏了:D 补一个预习阶段的课程介绍，是 《[RV4Kids Weekly Select (1) 每周精选1](https://gitee.com/RV4Kids/RVWeekly/issues/I3EVDH)》 这是 RV4kids 的缘起啦。

在看 ing... 的两篇 《[RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE)》 《[为 RVWeekly 建立 RSS 聚合](https://gitee.com/RV4Kids/RVWeekly/issues/I3PQY6)》是 RV4kids 的正式起点，这得益于之前的学习，也是沉淀的结果。这期间发了一篇 end 《[PDF 转 MARKDOWN 小鹤的文章可以接受 PR 啦！](https://gitee.com/RV4Kids/RVWeekly/issues/I3IHBN)》 是我的一位 SIGer 编委，利用高考前备战的间隙，发挥其语言特长（三门外语）完成了这篇 《[当今的计算挑战：计算机硬件设计的机遇](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E5%BD%93%E4%BB%8A%E7%9A%84%E8%AE%A1%E7%AE%97%E6%8C%91%E6%88%98%EF%BC%9A%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A1%AC%E4%BB%B6%E8%AE%BE%E8%AE%A1%E7%9A%84%E6%9C%BA%E9%81%87.pdf)》 这也是第一份答卷，之于 RVWeekly -> RV4kids. (PS:这期间有一个特别的故事，正好是我本人的还愿之旅，这个故事一定要面对面分享，期待您我的一面之缘。 :pray: )

在往后就开启了石榴派的星辰大海之旅了，这对于石榴派还有我本人乃至全人类都是一个 去远方 的命题啦，它就是关于未来，关于希望的故事的起点啦。上一期 SIGer RV4kids 创刊封面，也是今天正式发布的 POSTER TIME 的演讲主题的缘起了。

### 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)》

[![Image description](https://images.gitee.com/uploads/images/2021/0605/131157_784b56bd_5631341.jpeg "Enter image title here")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)

让我们共同迎接最好的时代的到来吧！祝 RISC-V 生态繁荣昌盛，成为人类共建美好明天的新起点！

---
(感谢  @[lj-k](https://gitee.com/lj-k) 小孔老师，让我们相识在开源社区  :pray: )

### 《[学习 OPENWRT 的仓](https://gitee.com/THCGC/shiliupi99/issues/I3XZOY?from=project-issue)》

附：为了本次峰会，石榴派开发组已经开始筹备 石榴派上线的准备工作：  
请加入学习小组，进入学习内部仓，所有学习小组最终都会发布一个开源仓给更多同学学习。这可以形成一个工作制度。

---
### [学习小组](https://gitee.com/RV4Kids/RVWeekly/issues) 点评：




1. RISCV-CHINA WEEKLY
目标正在进行中。。。
将以 RV4kids 学员 每周的学习笔记的方式呈现一个欣欣向荣的 RISCV 生态建设全景。
2. RV4kids 已经成为 RVWeekly 的一个组成部分。
3. StarFive CNRV 2021 峰会已经与石榴派同台展出！ :bowtie:
4. RV4Kids Weekly Select (1) 《计算机组成与设计：RISC-V》
将被 PLCT 实验室 汪辰老师的“从0”计算机操作系统课替代，并以石榴派为实验系统。
这是第一份 RV4kids 的学习笔记，详实完整，对整个计算机体系结构有了全面的了解。
5. UCTECHIP寒假训练营, 早期的 RISCV 教育实践，现将被更新的 石榴派开发板所替代！
6. 电子工程世界（大学堂）
清华大学的课程同台，也是初识蜂鸟的时刻，如今获得芯来的祝福，适配蜂鸟并实现 民族棋 的机器博弈示范，成为下一个实验课题材。
7. 苹果M1 的追赶还将是任重道远之路！
8. 《PLCT 实习生招募》
感谢 CNRV 2021 大会的服务， PLCT 从此近在眼前。
其实习生制度，将作为 石榴派 的石榴核 的组成架构，开发者，核心团队，社区，无界融合，只为一个教育生态的建设。
这符合社区替代组织的趋势，也是地球村到地球之家的转变，大家长时代的终结，一定会由高度自觉的家庭成员之间的默契所替代，石榴派，就是这个时代的见证人！一个最好的时代 RISC-V 的新时代。
9. 《开发一个RISC-V上的操作系统》
与汪辰老师将携手完成，石榴派为载体的 全新的实验课，RV4kids 已经成为了 PLCT 实验室的1员，而不只是外延。
感谢 CNRV 峰会的举办，让 RISC-V CHINA 更加凝聚的像一颗石榴，我们每一个人都是一颗紧密相连的石榴籽。
10. RISCV SOC 设计，其中 YARVI 曾经是 石榴派 RV 的第一个软核设定，如今已经被 石榴派 ZYNQ7020 替代为 ROCKETCHIP.
11. 在做石榴派 RV 论证的初期，找到一个可以运行的硬件平台是当务之急，而3个月后的 CNRV 2021 峰会，所有答案都有了，只是比预期的大规模还有一定差距，这一刻一定能到来。
12. [Verilog入門教學]与背景知识，将会继续完善，配合上接口，发展成为一个石榴派的特色实验课。
13. 小鹤PR，较早前完成，是 RV4KIDS 的实质性进展。
14. JAVA 是石榴派的上层中间件，是标配。RV的 PORTING 指日可待！
15. RV4Kids FPGA 课程完成了创刊，将开启实验课横向发展！
16. RVWeekly 手工搬移，由实习生完成工作，既是学习，又是分享。
17. John Hennessy：巨佬的鼓励永远不缺席！唯有后浪的努力才是最好的回答！
18. 构建RISCV 工具链：此 hello world 和石榴派的 hello world 有什么区别吗？
19. 芯来科技是第一个送祝福的 CNRV 展商，共同迎接未来。
20. 苏姿丰坚定了 FPGA 的石榴派方向。
21. FPGA 算法为王，方法论，工具链才是王道。
22. RVfpga 邂逅于 CNRV 2021 峰会，同样的 POSTER 1MIN 的伙伴！
23. FPGA 才是星辰大海的载体！