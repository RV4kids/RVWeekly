[![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/230428_81838740_5631341.jpeg "浪潮超算！999.jpg")](https://images.gitee.com/uploads/images/2021/0728/230209_2ea95f99_5631341.jpeg)

（点进去可看大图，设计稿）

标记完 p3,p4,p5,p6 我心里踏实了，这期高效地期刊，相当流畅。想着给 RVWeekly 做一篇总结当作礼物 PR 给 @lj-k 小孔老师，现在如愿了。都出到第10期的学习笔记，加上 SIGer RV 的前两期，相伴4个月的 RVWeekly 还有好多内容没有读过。这么好的学习资料怎么能错过。这期回顾，又挖出不少宝藏。

- 《RV少年》之未来：RVWeekly 停更2月 Review p1.p2
- 高效能服务器和存储技术国家重点实验室 HSSLAB p3
- "浪潮杯" 首届中国计算机博弈锦标赛 p5
- ASC 世界大学生超级计算机竞赛 p6
- 云会议云课堂爆红背后：“新基建” 火了，国产服务器赚疯 p4

除了头条 “新基建” 的热词，在 [Hello, openEuler!](https://gitee.com/flame-ai/hello-openEuler/wikis/Hello,%20OpenEuler!#9-%E5%8D%8E%E4%B8%BA) 创刊号中有遇到（oE也在新基建中有位置）。最感慨的是，竟然在 机器博弈 首届赞助的名字中看到了 “浪潮”。燃鹅，同期创建的 ASC 超算大赛也在如火如荼地举办。选用的一张照片，站在人群正中的四个小娃娃相当惹眼。

下面就是各趴，同学们慢慢品。  
（内容不少，就不一一注释啦，看标题吧）

---

RV 少年：RVWeekly 停更2月
https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4

自 6月 CNRV 峰会归来，盼望已久的 RVweekly 已经停更 2个月了。@lj-k 小孔老师成了名副其实的鸽王。而这份尴尬 在完成了 RV少年 | RV4Kids FPGA 课程 ([1](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)/[2](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)/[3](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)/[4](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)/[5](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)/[6](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1:%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)/[7](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P)/[8](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E7%9F%B3%E6%A6%B4%E6%B4%BE%E3%80%91SIGer%20%E7%BC%96%E8%BE%91%E6%89%8B%E5%86%8C.md)/[9](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209:%20RV%20%E5%B0%91%E5%B9%B4.md)) 后更加明显了。在 [PR 了 RVWeekly readme](https://gitee.com/inspur-risc-v/RVWeekly/pulls/17) 后，我开始重温 RVweekly 未读完的部分。

- [A+R：双架构产品研究](https://www.yuque.com/riscv/rvnews/snivsf)

  - Arm for application，RISC-V for extended and embedded
  - EPI与法国厂商SiPearl一起，正在开发一款原型机，将ARM CPU与基于RISC-V的加速器捆绑在一起。
  - MAX78000：ARM主核+RISC-V协处理器
    2020.10.08 MAX78000：ARM主核+RISC-V协处理器 · 语雀

停更的文章目录：

- RV与芯片评论.202107xx：2021年第2x周(总第4x期)
  > 博文推荐
  > "[从历届HotChips 检视RISC-V 的发展](https://zhuanlan.zhihu.com/p/389574143) (From zhuanlan.zhihu.com 2021.07.14)"
  > 来源：technews(台） 作者：痴汉水球
- RV与芯片评论.20210710：2021年第28周(总第50期)
  > 抱歉，页面无法访问…
  > 页面链接可能已失效或被删除
- 未完成：RV与芯片评论.20210703：2021年第27周(总第49期)
- 未完成：RV与芯片评论.20210624：2021年第26周(总第48期)
- RV与芯片评论.20210619：2021年第25周(总第47期) [RV4kids](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV%E4%B8%8E%E8%8A%AF%E7%89%87%E8%AF%84%E8%AE%BA.20210619%EF%BC%9A2021%E5%B9%B4%E7%AC%AC25%E5%91%A8(%E6%80%BB%E7%AC%AC47%E6%9C%9F).md)
  https://www.yuque.com/riscv/rvnews/uoo8gk

---


- RV与芯片评论.20210703：2021年第27周(总第49期)
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008222_link

- 未完成：RV与芯片评论.20210624：2021年第26周(总第48期)
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008228_link

  - 清华-伯克利深圳学院RISC-V国际开源实验室2021夏令
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013092_link

    - PicoRio 项目

      全球首个可运行 Linux 的芯片级全开源、低功耗、体积小的 RISC-V 微型电脑系统。  
      PicoRio 版本1.0 [基于本地化开源64位 RISC-V 多核处理器芯片]

    - Chrome OS & V8 Engine

      针对 RISC-V 架构的 Chrome 操作系统以及 V8 引擎开源移植；  
      PicoRio 项目现阶段的核心应用层开发；  
      解决 RISC-V 架构的移植性问题，并结合到二进制翻译等新的应用领域。

    - RISC-V Binary Translation & Optimizations

      将现有 x86, ARM 等架构的应用程序的二进制文件进行转换，使之可以在 RISC-V 的处理器执行。并针对 RISC-V 架构进行优化，通过软硬件协同设计的方式提升程序的性能。

  - 【人物】包云岗的四十不惑
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6012875_link

  - 中科院包云岗：RISC-V可支撑未来服务器定制化的需求
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013135_link

- 基于FPGA实现的RISC-V处理器、FPGA芯片及片上系统
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008254_link

  - 广东高云半导体科技股份有限公司
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008267_link

  - 新闻中心
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008273_link

    - 高云半导体宣布发布USB 2.0接口解决方案
      https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008275_link
    - 高云半导体近20套教学视频全网发布啦！！！
      https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6008274_link

- RV与芯片评论.20210619：2021年第25周(总第47期).md
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6012782_link

  - 国产嵌入式CPU实现关键技术突破！阿里平头哥获浙江省技术发明一等奖
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013186_link

  - 华为、英特尔都来“搅局”，RISC-V的机会越来越大了？
    https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013301_link

- RISC-V的杀手级应用即将到来
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013715_link

---

![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/212115_6799d91f_5631341.png "屏幕截图.png")

www.hsslab.com

- 高效能服务器和存储技术国家重点实验室

  高效能服务器和存储技术国家重点实验室始建于1999年，2002年9月正式对外开放，2007年国家科技部正式批准建设高效能服务器和存储技术国家重点实验室，2010年12月通过专家组建设验收，2011年5月科技部授牌。实验室是国家首批依托企业设立的国家重点实验室之一，并于2018年被科技部评为“优秀类国家重点实验室”。

  实验室以国家战略需求和产业发展为导向，以应用基础研究、关键技术研究和共性技术研究为主要研究重点，以解决高速、高效、海量、高可用性等高效能计算与存储问题为主要研究内容，设立了体系结构、系统设计、软件系统、测试评估4个研究部。

  实验室采用开放、流动、联合、竞争的机制，现有院士1人，国家重大专项和重点专项专家7人，特殊津贴专家10人，省部级人才认定25人。实验室建设面积6500平方米，具备先进的研发信息网络和安全防护措施，同时可以提供系统压力测试、网络节点故障检测、逻辑仿真测试等大型综合检测环境。

  实验室成立以来，在科学研究、人才培养、学术交流、科研成果转化等方面取得了丰硕成果。已承担国家973、863、支撑计划、重大专项等课题40余项，发明专利2500余项，获得科技奖励40余项，其中国家科技进步奖7项，中国电子学会科学技术奖5项，省部级奖励11项。自主研制出高端容错计算机、海量存储系统、云服务器等产品，其中高端容错计算机荣获2014年国家科技进步一等奖；2020年的SPC-1测试中，实验室成果以超300万 IOPS位居8控存储产品性能全球第一。同时，实验室十分重视产学研合作及对外交流，累计组织国内外学术活动一百余次，已成为国际先进的服务器和存储技术研究平台，开放服务中心和国内外交流窗口。

  未来，实验室将围绕人工智能、智慧计算等开展应用基础研究和竞争前共性技术研究，建设成为世界一流的科研平台，引领智慧计算未来发展！

- 实验室建设历程

  - 1993年，研制出中国第一台基于10颗处理器的服务器SMP2000
  - 1994年，研制出国产第二代服务器SMP3000
  - 1996年，SMP2000获得国家科技进步二等奖
  - 1997年，承担国家863计划“国产服务器推广应用一期”
  - 1998年，SMP3000获得国家科技进步三等奖
  - 1999年，①山东省科技厅批准浪潮建设“山东省高性能服务器重点实验室”
             ②承担国家863计划“国产服务器推广应用二期”
  - 2000年，承担国家863计划应急项目“浪潮信息安全服务器”
  - 2002年，承担国家863计划重点工程“新型网络服务器系统”
  - 2004年，①IA64位服务器获得国家科技进步二等奖
             ②承担国家863计划“新型网络服务器系统（一）”
  - 2007年，①科技部批准建设“高效能服务器和存储技术国家重点实验室”
             ②承担国家863计划重大项目“浪潮天梭高端容错计算机”
  - 2008年，①承担国家863计划“浪潮海量信息存储系统应用与示范”
             ②承担国家863计划“多协议混合存储系统就与实现”
  - 2009年，①“高效能服务器与存储技术创新工程”获得国家科技进步二等奖
             ②TS30000获得国家科技进步二等奖
  - 2010年，①国家重点实验室建设通过科技部验收
             ②承担国家973计划“千核级分布式内存计算机系统支撑技术研究”
  - 2011年，①承担国家973计划“云数据中心关键支撑技术研究
  - 2012年，①承担国家863计划“中国云总体技术研究”
             ②承担核高基专项“基于国产CPU/OS的服务器研发与应用推广”
  - 2013年，①承担863计划“浪潮亿级并发云服务器系统研制”
             ②承担863计划“浪潮EB级云存储系统研制”
  - 2014年，高端容错计算机获得国家科技进步一等奖
  - 2015年，承担863计划“异构混合内存体系结构研究与开发”
  - 2016年，研制出64路高端容错计算机M13和人工智能系列服务器
  - 2017年，承担国家重点研发计划“高效能云计算数据中心关键技术与装备”
  - 2018年，被科技部评为“优秀类国家重点实验室”
  - 2019年，荣获山东省科技进步一等奖
  - 2020年，AIStation人工智能开发平台获评CITE2020人工智能最佳创新奖
  
【活动通知】

- “FPGA异构加速科技创新”研讨会暨走进香港城市大学活动
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013921_link

- 【公开课】人工智能计算平台进化神器：下一代分布式加速技术解析
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013931_link

- “下一代人工智能分布式加速技术研究”讲座暨实验室进高校活动将于9月17日下午举行
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6013987_link

---

![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/212219_4df61e3f_5631341.png "屏幕截图.png")

云会议云课堂爆红背后：“新基建”火了，国产服务器赚疯
https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6015616_link

> 但如果回到上世纪90年代，很难想象国产服务器厂商今日之成绩。
彼时，全球高端服务器市场，鲜有中国服务器厂商的一席之地。国产和进口，成为基本判断服务器质量的固化“标签”。
而浪潮27年前的转型决定，国产服务器崛起之路，埋下了至关重要的伏笔。
1993年、2009年、2017年，三个关键节点将浪潮推到了中国服务器市场的潮头。
1993年，华为第一颗有名字的ASIC芯片SD509问世，并刚刚用自研交换机敲开通信世界的大门；联想刚跨入“奔腾”时代，推出的中国第一个家用电脑品牌“联想1+1”风靡全国。

> 那一年，也是浪潮转舵驶向服务器的起点。▲浪潮集团董事长孙丕恕
如今的浪潮集团董事长孙丕恕，当时还是浪潮集团技术副总工程师。公司采纳他的建议，转型研发服务器，造出
市场推广之路异常坎坷。当时，只有银行之类的高端客户有实力使用服务器，偏偏这些客户担心国产不可靠，宁可多花钱买进口的，也不肯试用免费的国产服务器。那会儿孙丕恕带着几个同事，扛着服务器天天跑银行，软磨硬泡地求一个试用机会，终于用产品和服务“磨”到了第一笔订单。

> ▲2007年，科技部批准建设浪潮集团“高效能服务器和存储技术国家重点实验室”
浪潮率先打开了国产服务器的大门，随之而来的就是中国互联网创业大潮，1996-1998年，搜狐、网易、新浪接连登上历史舞台，互联网为服务器行业带来新的需求增长。这一时期，除了浪潮、中科曙光、联想外，紫光、方正、长城、宝德等一批企业都涌入服务器产业，并初具规模。但此后数年间，因核心技术受限，国产品牌主要凭借价格优势活跃在中低端市场。截至2009年，70%的中国服务器市场份额仍被三大国外品牌占据，而更是被 外企占去了超过90%的市场份额 。

> 第二个关键节点是2009年，云计算开始在中国冒出萌芽。浪潮再次展示出敏锐的市场嗅觉——投身云计算软硬件，率先提出“行业云”概念，并在2010年成功研制出中国第一代融合架构云服务器。浪潮对未来IT趋势做出预判，相信互联网市场才是未来服务器市场的主要阵地，并认为整个IT是依靠软件定义的融合架构，具有计算、存储和网络的吞吐能力，因此浪潮也更加重视软硬一体化交付的云服务器形态。2011年，浪潮全面实施向云计算领域的战略转型，具备涵盖IaaS、PaaS、SaaS的云计算解决方案服务能力，其32路高端服务器填补了国内空白。

> 不止是浪潮，华为和联想也在服务器领域开始快速奔跑。2010年，华为提出“云-管-端”战略，其中承载着计算和存储任务的服务器，成为华为云计算战略的基石。2013年，浪潮推出中国第一款具备完全自主知识产权的高端容错服务器——天梭K1。2014年，联想收购IBM部分服务器业务，一跃跻身全球前三、中国第一大x86服务器厂商。▲浪潮天梭K1

![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/222832_caee85a0_5631341.png "屏幕截图.png")

> 这一阶段，国产服务器厂商开始在国内市场表现强劲，并逐渐向高端服务器市场渗透。2015年，浪潮提出到2020年实现“从中国第一到全球前三”的小目标，结果这一目标提前在2017年实现了。

- 浪潮天梭K1
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6021479_link

- TS K1 System
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6021665_link

---

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/213015_730a4d7d_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/0728/143032_5fa72577_5631341.jpeg)  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/212916_3e4970e2_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/0728/143112_6b239b61_5631341.jpeg)  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/212618_571f52b4_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/0728/143142_3b95b640_5631341.jpeg)

- “浪潮杯”首届中国计算机博弈锦标赛(会议秩序册）
- 人工智能烽火点燃中国象棋
  ——记“浪潮杯”首届中国象棋计算机博奕锦标赛暨2006中国机器博奕学术研讨会、“浪潮杯”首届中国象棋人机大战
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6014781_link

- 2006“浪潮杯”首届中国计算机博弈锦标赛暨2006机器博弈学术研讨会
  http://computergames.caai.cn/main_cg2006.html
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6022902_link

  今年是人工智能学科创立50 周年。在北京举行的“庆祝人工智能诞生50周年科技活动周”和“首届中国智能产品与科技成果博览会”是国内的两个重要庆祝活动。由中国人工智能学会、浪潮集团、中国科学技术馆主办，东北大学、清华大学、北京理工大学承办的“首届中国计算机博弈锦标赛暨2006中国机器博弈学术研讨会”和“中国象棋人机大战”是科技活动周的重要内容。

  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/223140_a9834b0f_5631341.png "屏幕截图.png")  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/223356_efca63ac_5631341.png "屏幕截图.png")  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/223249_7a7450d9_5631341.png "屏幕截图.png")](http://computergames.caai.cn/main_jc2006.html)  

  8月3日上午9点，中国象棋计算机博弈锦标赛在北京中国科学技术馆B馆拉开了序幕。来自海内外的18支队伍（宝岛一号（台湾）、兵芯（台湾）、将神传说、理治棋壮、落花、梦入神机（美国）、棋乐无穷、棋天大圣、棋之梦、青羽堂、深象（台湾）、神乎棋技（美国）、天机、象棋ABC、象棋奇兵、象棋旋风、象眼竞技、谢谢大师（法国））开始了小组赛的角逐。小组赛分3个小组，采用双循环制，按积分每个小组的前三名进入决赛。经过一天半5轮90盘的激战，宝岛一号、将神传说，梦入神机、棋天大圣、天机、象棋ABC、象棋奇兵、象棋旋风、象眼竞技进入了决赛。

  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/223629_fc546d0a_5631341.png "屏幕截图.png")  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/223821_143a62a4_5631341.png "屏幕截图.png")  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/224007_74b8add6_5631341.png "屏幕截图.png")](http://computergames.caai.cn/main_jc2006.html)  

  锦标赛小组赛的战火刚刚熄灭，8月4日下午2点，2006中国机器博弈学术研讨会又开幕啦。此次研讨会是国内在机器博弈领域的首次学术会议，受到了各方面的重视，中国人工智能学会理事长钟义信、中国科技馆党委书记赵有利、浪潮集团副总裁王恩东、东北大学徐心和教授出席了开幕式，并作重要讲话。本次研讨会征集了海内外各地的机器博弈方面的论文20余篇，经过认真审查从中挑选出了部分论文并编辑成册，出版了《2006中国机器博弈学术研讨会论文集》。本次研讨会还邀请了微软亚洲研究院许峰雄博士、美国惠普实验室吴韧博士、台湾交通大学吴毅成教授、东北大学徐心和教授、象棋百科全书站长黄晨先生作大会报告。

---

![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/215817_6757b9e4_5631341.png "屏幕截图.png")

ASC世界大学生超级计算机竞赛
https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6015842_link

> ASC世界大学生超级计算机竞赛（ASC Student Supercomputer Challenge，简称ASC超算竞赛），肇始发轫于2012年，是世界最大规模的超算竞赛，与德国ISC、美国SC并称世界三大超算竞赛。 [1]

> 该竞赛由中国倡议成立，与日本、俄罗斯、韩国、新加坡、泰国、中国TW、中国HK等国家和地区的超算专家和机构共同发起并组织，并得到美国、欧洲等国家地区超算学者和组织的积极响应支持。ASC旨在通过大赛的平台，推动各国及地区间超算青年人才交流和培养，提升超算应用水平和研发能力，发挥超算的科技驱动力，促进科技与产业创新。[2]

> - 中文名：ASC世界大学生超级计算机竞赛
> - 外文名：ASC Student Supercomputer Challenge
> - 简 称：ASC超算竞赛

- 历年赛事盘点
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6017056_link

- 2020-2021 ASC世界大学生超级计算机竞赛（ASC20-21）报名通知
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6018207_link

- 中国网：电子科大学子在ASC世界大学生超级计算机竞赛中斩获一等奖
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6018323_link

- 关注超算 首届中国大学生超级计算机竞赛
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6018437_link

- 参考资料
  https://gitee.com/RV4Kids/RVWeekly/issues/I42OF4#note_6017082_link

  1.  [世界三大超算竞赛结成“联盟”](https://images.gitee.com/uploads/images/2021/0728/154543_6d867ed0_5631341.jpeg)  ．科学网[引用日期2016-12-20]
  2.  [ASC16世界超算大赛华中科技大学夺冠](https://images.gitee.com/uploads/images/2021/0728/160120_5b3d2961_5631341.jpeg)  ．人民网[引用日期2016-12-20]
  3.  [ASC15决赛开幕 顶尖难题将等待16支世界强队](https://images.gitee.com/uploads/images/2021/0728/164843_3c41b225_5631341.jpeg)  ．比特网[引用日期2016-12-20]
  4.  [2017 ASC世界大学生超级计算机竞赛清华大学夺冠](https://images.gitee.com/uploads/images/2021/0728/164938_ebc737d5_5631341.jpeg)  ．新浪[引用日期2018-10-25]
  5.  [ASC18世界超算总决赛清华成功卫冕 黑马上海科大斩获AI大奖](https://images.gitee.com/uploads/images/2021/0728/163751_d596ff40_5631341.jpeg)  ．中新网[引用日期2018-10-25]
  6.  [ASC19超算大赛20强名单出炉](https://images.gitee.com/uploads/images/2021/0728/163843_e161d424_5631341.jpeg)  ．中国经济网[引用日期2019-06-25]
  7.  首届中国大学生超算大赛落幕 清华、国防科大获胜  ．中国教育网[引用日期2016-12-21]
  8.  [从清华到华科，名校为啥要主办超算大赛？](https://tech.huanqiu.com/article/9CaKrnJUndZ)  ．环球网[引用日期2016-12-21]
  9.  世界最大规模超算竞赛将在中国举行  ．中新网[引用日期2016-12-21]
  10.  ASC13初赛落幕 应用优化或成决赛制胜关键  ．中国经济网[引用日期2016-12-21]
  11.  [ASC13总决赛背后探秘 清华大学如何夺得HPL冠军](https://images.gitee.com/uploads/images/2021/0728/163923_718c3d0f_5631341.jpeg)  ．光明网[引用日期2016-12-21]
  12.  [ASC14世界超算大赛16强出线 中外名校4月决战广州](https://images.gitee.com/uploads/images/2021/0728/165441_4d3a295d_5631341.jpeg)  ．中新闻[引用日期2016-12-21]
  13.  [上海交通大学获ASC14总冠军 中国超算未来可期](https://images.gitee.com/uploads/images/2021/0728/165527_1a028b4e_5631341.jpeg)  ．光明网[引用日期2016-12-21]
  14.  [ASC15世界超算大赛落幕 清华大学获冠军](https://tech.huanqiu.com/article/9CaKrnJLiuy)  ．环球网[引用日期2016-12-21]
  15.  [2016世界超算大赛决赛4月在汉举行](https://images.gitee.com/uploads/images/2021/0728/160558_11385a4e_5631341.jpeg)  ．中国日报[引用日期2016-12-22]
  16.  ASC16世界超算大赛华中科技大学夺冠  ．新华网[引用日期2016-12-22]
  17.  [世界超算高手太湖之光决出高下 人工智能受关注](https://images.gitee.com/uploads/images/2021/0728/160341_c75c4d12_5631341.jpeg)  ．人民网[引用日期2017-07-25]
  18.  [世界最大规模超算竞赛ASC17在美启动](https://smart.huanqiu.com/article/9CaKrnJYFPI)  ．环球网[引用日期2016-12-22]
  19.  [2017 ASC世界大学生超级计算机竞赛清华大学夺冠](https://images.gitee.com/uploads/images/2021/0728/164224_ba36ea89_5631341.jpeg)  ．新浪[引用日期2017-07-25]
  20.  [ASC19世界超算大赛落幕 两岸清华分获冠亚军](https://images.gitee.com/uploads/images/2021/0728/160822_3161fdbd_5631341.jpeg)  ．中国日报[引用日期2019-06-25]
  21.  [世界大学生超算竞赛落幕，清华大学夺冠！](https://images.gitee.com/uploads/images/2021/0728/161235_45a61b98_5631341.jpeg)  ．中青在线[引用日期2018-11-08]
  22.  [ASC18世界超算大赛启动 包含AI与"冷冻电镜"赛题](https://images.gitee.com/uploads/images/2021/0728/160018_0f28cfb9_5631341.jpeg)  ．网易[引用日期2018-11-08]
  23.  [TOP500发起人：ASC竞赛让学生思考超算与人工智能融合](https://tech.gmw.cn/2019-04/24/content_32772808.htm)  ．光明网[引用日期2019-06-25]