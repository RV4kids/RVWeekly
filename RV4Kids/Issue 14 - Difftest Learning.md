# Differential testing to LEARNING

- Processor verification by differential testing
- Personalized vs. differentiated vs. individualized learning
- Differential Learning System

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0905/184846_151ba967_5631341.jpeg "DIFFTEST999.jpg")](https://images.gitee.com/uploads/images/2021/0831/022223_bc535e0c_5631341.jpeg)

> 这是想介绍 [Difftest](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415486_link) 吗？~ 这个验证架构确实非常方便

> @yuandj （本期主题）初步框架是，借由 [DIFFTEST](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU) 引申到学习方法论，作为 “[一生一芯](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247346_link)” 的科学文化的延展进行介绍。对青少年，以及我这样的科学老师，会有很直观的代入感，好理解，“北向文化是开源的要义”，也是开源科普的核心。太技术的题目则转自专家。

> 您这样想确实很棒，方法论比具体的技术对同学们来说适用范围更广。期待您引申的学习方法论 :+1: 

### 一、差分测试学习法

的特点：

- 任意兴趣点出发，建立基础知识点；
- 选择不同路径，补充建立知识树；
-  **多学习样本比较，强化知识点理解** ，并通过实验验证；
- 差异化的自主学习方法；

是我总结的，下面是我分享给 [DIFFERENTIATED LEARNING](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445442_link) 的一位 [教育专家](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445446_link) 的内容：

> 芯片设计中的“差分测试”是比照两个实现方法完全相同的模拟器输出，以单条指令的细度比较执行差异，反推BUG调试芯片的方法。课程中有三个有关视频和资料，不同角度论述了其原理和应用实现，对学习起到了相互补充的作用。
>
> 就学生的学习笔记反馈，记录下的学习要点，基本都是三篇文章中论述角度不同的部分，我认为这是一个 **差异点，引起学生学习探究** 的案例。
>
> 因而引发关于 **学习好奇心** 的思考，也就是当学生第一遍学习时，记下的知识点一定是不全的，原因不同，不同学生记下的知识点各有差别（学生笔记就能看出），通过预习，复习等方式，反复补充不足的知识点，达到巩固学习的目的。
>
> 从学习过程，则以同一主题不同老师的讲解，以及 **不同同学学习笔记的分享，为学习素材，实现学习资源的丰富** ，以随机选择的方式，由学生习得，在一定量的基础上，完成知识和技能的掌握。
>
> 尤其计算机学科，动手实践中积累经验看，对于巨大海量的芯片设计学科，非常难上手的情况看，差异化学习法，可以实现循序渐进的学习目标，能够逐步鼓励同学们进入到这个领域。

### 二、我的学习实践

本期我选了一个不一样的题目，“[在 debian 上安装 工具链](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247410_link)”，[Flag 在此](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415378_link)，下面是工具启动画面一角：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0830/011309_808efab6_5631341.png "在这里输入图片标题")

随着 “[gtkwave 安装正常！](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450644_link)” 的 Issue 完成，上百条学习笔记（[本条 Issue](https://gitee.com/RV4Kids/NutShell/issues/I47E8R) 就有 50 条回复）终于截稿。这是整个假期开启 [RV 学习小组](https://gitee.com/RV4Kids) 模式后，最投入的一次，前后历时5天。它将作为 《开学第一课》 的重要素材，分享给同学们。最后一句我尤其喜欢：

“ **_尤其计算机学科，动手实践中积累经验看，对于巨大海量的芯片设计学科，非常难上手的情况看，差异化学习法，可以实现循序渐进的学习目标，能够逐步鼓励同学们进入到这个领域。_** ”

不论同学们对什么感兴趣，学什么，怎么学，只要爱分享，投身到开源社区当中，哪怕是星辰大海，你们一样可以征服。刚刚收到 @zhou-zian905906 的一篇分享，今天是[祝融号火星车登陆第100天](https://gitee.com/zhou-zian905906/siger/issues/I47VUE)，本期主题的意义就更重啦。

学习轨迹分享，正式开始！

--- 

### 三、封面故事：回归学习 to LEARNING

制作封面，是学习小组期刊的重要工作内容，它便于我们整理一周来甚至关联的前几周搜集的学习资料，以统一的整体，呈现给您。这期封面，源于一个 IDEA “将人脑和电脑AI” 进行比对，差分学习一把，于是呼，bing 搜索出了这篇 [Google 的深度学习文章](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445512_link) 的封面，非常契合当时的心境，浩如烟海，混乱如麻的连线，这不就是 Verilator 要做的事情吗？—— 把我们人脑构思的 组件，通过语法定义的 IO 连线起来，直到成为一个芯片的电路，并最终覆盖掉人脑，多少有一些悲观。但最终人类找到了方法，去驾驭更复杂的系统，去挑战更高难度的目标。就如，标题最后，从技术方法，上升到了学习方法论，这更适合 我们的科普目标，也能激励更多的同学，进入更多的领域。（一个小插曲，您可以凝视着这些线宽，会突然看到线路的层次感  :sunglasses: ）

 **㊀ 这个学习方法如何命名？** 

Difftest 英文全称  **Diffrentail Testing**  ，首先是从 “[NutShell: A Linux-Compatible RISC-V Processor Designed by Undergraduates](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_6411662_link)” 的英文报告中提取的，我们首先阅读的是 [中文版](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU)。按图索骥，我们收获了不少相关内容：

- [Diffrentail Testing](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445261_link) by [Mayur Naik](https://www.cis.upenn.edu/~mhnaik/edu/cis700/lessons/differential_testing.pdf)
  
  一个国外计算机学院同学，用不同的 C Compiler 比较编译结果捉虫的方式，相同就没BUG，不同就有可能是BUG。这里不做评述，也没有实践验证，这是最接近 Difftest 的同行解答。（先介绍这个，符合期刊的学习过程倒叙的习惯。）

 **㊁ 两个教育相关的主题** 

- 体育运动心理学：通过差异化运动，提升运动成绩的心理学理论，可以激发人体学习新运动技能。
- 差异化教学：最直接的教育领域的探讨，无一定论，也是本期期刊锁定题目的直接原因。
- 神经网络相关： _增加学习样本数量的论文，没有收录，不想过多学术延伸。_ 

 **㊂ 体育运动心理学** 

- [What is differential learning?](https://skillacqscience.com/2018/07/29/what-is-differential-learning/) [什么是差异学习?](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445353_link)

  差异学习是一种强调动作模式探索的训练方法。差异学习最早由沃尔夫冈·肖尔霍恩(Wolfgang Schollhorn)及其同事于2006年提出，它表明，以随机的方式练习各种动作解决方案有助于形成适应新情况的动作模式。事实上，它认为动作的变化是学习的基础，而不是动作的重复。通过不断(和随机)改变用于执行一项技能的技术，表演者将: 发现什么对他们最有效，然后学会用多种方式来完成这项技能。... 

  - [About](https://skillacqscience.com/about/) [Skill Acq Science.com](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445373_link) 网站
  - [Contributors](https://skillacqscience.com/contributors/) 贡献者（有老师，有学生，一起分享他们的研究成果。）

- DIS https://dls-sports.com/ 

  是相对完善的 [运动心理学理论](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445463_link)，术业有专攻，ABOUT THE CREATOR：Wolfgang Schöllhorn

  > 沃尔夫冈博士Schöllhorn是差异学习系统(Differential Learning System)的创始人。多年来，他在不同的运动项目中训练国内和国际运动员。1990年，他获得了法兰克福大学生物力学博士学位。在世界各地担任教授和客座教授后，他于2007年成为美因茨约翰内斯古腾堡大学(Johannes Gutenberg University)的训练和运动科学教授。他在田径、生物力学和运动学习领域的工作获得了多个国家和国际奖项，包括国际生物力学差异学习协会(DL)的表演奖和2006年的“德国Werkbund Label”。他证明了 “ **多样化的运动对运动员是必要的。他进行了一些不寻常的练习:拿着一个物体进行训练比赛，或者用网球杂耍。** ”

  - 关于DLS

    DLS启动最有效的学习。DLS带来最高的性能和乐趣。DLS最有效地防止伤害。DLS不仅会影响你的身体，还会影响你的大脑。DLS是基于最新的科学发现。DLS利用了只有亚洲僧侣才知道的大脑的潜力。DLS不仅能提高你的表现，还能改变你的性格。(DLS是许多即将到来的培训和教学概念的基础)。

  这其中 “ **_亚洲僧侣才知道的大脑的潜力_** ” 是什么引起了我的好奇心，或许可以成为一期学习专题。

 **㊃ 差异化教学** 

- [如何使用差异化教学](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445403_link) [Differentiated Instruction](https://zhuanlan.zhihu.com/p/351931704)

  > 老师在讲台统一讲解的教学方式，只能契合一小部分学生的学习需求。因此，差异化教学（differentiated instruction) 的观念应运而生，目的是使教学契合更多学生的学习需求，提高课堂教学的整体效果。

  其实这个场景并不在我们的关注范畴，或者这只是学习的一个场所，被我们涵盖在内啦。:+1 多元智能理论的引用是有益的，只是告诉我们，学习的过程性，和一个人的学习力的构成。但学习目标，我们认为更重要。

  - 言语语言智能 linguistic intelligence
  - 数理逻辑智能 logical intelligence
  - 视觉空间智能 visual intelligence
  - 音乐韵律智能 musical intelligence
  - 身体运动智能 kinesthetic intelligence
  - 人际沟通智能 interpersonal intelligence
  - 自我认识智能 interpersonal intelligence
  - 自然观察智能 naturalistic intelligence

  学习风格理论，我们认为可以被个人的学习习惯囊括： **视觉型Visual**  、  **听觉型auditory**  、  **动觉型Kinesthetic**  以及  **读写型Reading/Writing**  学习者。整体上还是一篇学习笔记，至少代表了当前自媒体时代流行的一个语境，为了个性化而个性化。对于解决方案，尤其不能认同，这未免太劳民伤财啦：

  > 老师可以根据学生的 学习准备程度（如前提知识的掌握、认知水平的发展、社会情感能力、行为管理技巧等） 、兴趣、以及 学习者特征（如学习者风格、智力偏好、环境偏好、性别、文化、喜欢独立任务还是小组合作等） ，在学习的 内容、过程以及成果 方面进行差异化教学。

- [个性化、个别化和差异化学习](https://www.jiaojianli.com/11920.html) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445442_link)】

  1. 个别化教学（Individualized instruction）指的是按照不同学习者的学习需要来调整教学的进度。
  2. 差异化教学（Differentiated instruction）指的是按照不同学习者的学习偏好来定制教学。
  3. 个性化教学（Personalized instruction）是根据学习者的需要和特点，安排不同的教学进度、教学内容和教学方式。

  至少这样的学术定义，让我们更清楚自己的定位，而谁再说更为重要：关于[焦建利](https://www.jiaojianli.com/aboutus-2)，他就是本期开篇引用的 [教育专家](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445443_link)。我通过教授留的联系方式，分享了我们的《[差分测试学习法](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445446_link)》

---

### 四、学习 [NUTSHELL](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU)，补课 “[一生一芯](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247400_link)”

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0831/223803_bff65ff9_5631341.png "014358_d6d3daa5_5631341.png")](https://oscpu.github.io/ysyx/) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0730/142812_deac95ef_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)

最初还没有明确本刊的题目和聚焦在学习方法论，立本 Issue 是学习笔记的例行公事。因为有了之前的学习成果打底，才有了信心重拾 “[一生一芯](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247400_link)” 课程，用补课来形容是最真实的情况。上周助教团队还发布公告，征询学习困难的同学反馈问题，这篇学习笔记就作为我补交的作业吧。在时序上，也是符合补课顺序的。选择 [NUTSHELL](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU) 要补上之前的先验知识。

1. [oE](https://openeuler.org/zh/) [社区](https://gitee.com/openeuler/community/issues)的 [sig-RISC-V](https://gitee.com/RV4Kids/RISC-V) 是我的入门知识的积累，不只打开了 [RISC-V](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC4%E6%9C%9F%20Better%20RISC-V%20For%20Better%20Life.md) 的大门，还收获了 RV开发板——[石榴派](https://gitee.com/shiliupi/shiliupi99/issues/I3TIA9)
2. 最初的移植目标，就是用 oE 社区的成果，选用 NUTSHELL 为软核的，可能是机缘未到，我们选用了 RocketChip
3. 经过 6/10 日 oE 开发者日的自信，我们将 [石榴派](https://www.bilibili.com/video/BV1qV411H7j9) 以 [poster](https://www.bilibili.com/video/BV1qV411H7j9) 身份投稿 [CNRV](https://space.bilibili.com/1121469705)，赶上了末班车
4. 现场众多分享，唯独在大会首日傍晚，看到了朝气蓬勃的 [一生一芯 团队的彩排](https://www.bilibili.com/video/BV1Zb4y1k7RJ)，他们是来分享[香山](https://www.bilibili.com/video/BV19X4y1w7EB)的
5. 虽然之后几天，零星听了一些报告，会务缠身并未深入。（ _其实 一生一芯 团队是最早造访石榴派展位的_ ）
6. 带着遗憾（ _人情债_ ），[RV 学习小组](https://gitee.com/RV4Kids)一成立，[RVOS](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I43DNU) 和 [一生一芯](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL) 即被我列为了实验课目标，笔记为证：[3](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)/[4](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)
7. 正好是暑假，[RV 学习小组](https://gitee.com/organizations/RV4Kids/issues)和 [三期计划](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5754527_link)同时开始，我还在 [三期课程动员的 B站](https://www.bilibili.com/video/BV1PU4y1V7X3)下，留下了一个25赞的顶帖 :pray: 
8. 后面的故事，就用本期学习笔记分享吧...

于是下面这篇编辑贴（ _[SIGer学习小组](https://gitee.com/flame-ai/siger)的学习期刊都以杂志编辑部的运作流程，通过立Issue确定内容，并梳理思路_ ）诞生了，好在赶上了《开学第一课》。

- 差分测试学习法 - 学习 NUTSHELL
  https://gitee.com/RV4Kids/NutShell/issues/I47E8R

  本仓 已经完成了 NUTSHELL 相关的 B站视频的学习，包括昨天的 [王华强同学分享的 CHISEL3 在NUTSHELL的经验](https://gitee.com/RV4Kids/NutShell/issues/I47934)。下面的学习，从锁定其经验分享中指定的代码位置开始。 **以缓解浩如烟海的 RV CHIP 学习的焦虑** ，一盏油灯也是点亮吗。

  1. [石榴派开发板实验课1st](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL)

     第三期一生一芯计划后，启动的学习笔记，包括了中期介绍，以及包云岗老师的鼓励，决定全身投入赶队伍。

     - 前期：[同学互动](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5715372_link)，[解壁伟老师整理的QA](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5721447_link) 并整理了 [微信群同学们最集中的 18 个问题](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5721537_link)，[“一生一芯” 时间计划](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5754527_link)（ _[这部分笔记，在之前的学习期刊中有收录](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5793452_link)_ 。）[为了撸代码](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5793456_link)，还做了[网摘](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5793499_link)，

     - 中期：就是 《[第三期“一生一芯” 二次启动会](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247346_link)》，记录了[包老师的鼓励](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247371_link)，和[其他同学的问题](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247380_link)，之后，重温了 [项目进度](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247394_link)，[一生一芯官网](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247400_link)（[首页](https://oscpu.github.io/ysyx/)，[学习记录](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247405_link)，[讨论区](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247406_link)，[项目WIKI](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247407_link)，[参考资料](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247560_link)，[示例项目 riscv-sodor](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247601_link)）；确定了具体实施目标：查看了 Verilator 的 [OS 要求](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247410_link)，[Debian -- buster 软件包](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247411_link)，第一次造访 [debian - The Universal operating system 首页](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247541_link)。

     - 2 天前：真正开始安装实践：[安装/卸载. deb 文件](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6446932_link)；verilator_4.010-1_arm64.deb [安装失败](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6446953_link)；然后，按照 [一生一芯 实验课 尝试构建](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6447444_link)，并详细记录：

       1. [git clone](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6447483_link) ；
       2. [g++: internal compiler error: Killed (program cc1plus) 解决办法](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6447520_link)；
       3. [bs=64M count=16 1.0 GiB](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6447560_link) 失败；[V3PreProc.cpp](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6447618_link) 报错；
       4. [加到 8.0 GiB](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6448103_link) 依然报错；截图 [SSH 窗口](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6449114_link) 封存；[最后的错误提示：g++: fatal error: 已杀死](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6449235_link) ；

  2. [ysyx-片上功能展示-录屏+展示](https://gitee.com/RV4Kids/NutShell/issues/I3R6HJ)

     补课：去年的一生一芯汇报的视频，用NUTCORE制作的芯片成品，跑LINUX的场景。有了[石榴派打底](https://gitee.com/shiliupi/shiliupi99/issues/I3TIA9)，这些启动界面已经相当熟悉，一带而过，算是补作业。( _包老师：发布于 2020-07-27 · 1.4 万次播放 · 61 赞，本 Issue 收录了11个网友的典型回帖，表示支持_  :+1: )

  3. [1-2 NutShell-本科生设计的可运行Linux的RISC-V芯片 王华强](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU)

     [补课](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_6401033_link)：去年[王华强同学的毕业汇报](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_6401033_link)以及[英文的 RISC-V GLOBAL 2020 ](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_6411662_link)汇报，重点说明了差分测试的威力，以及CHISEL3敏捷开发在RV核开发的作用。(本 [Issue 早有收录](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_5661601_link)，重温 [Nutshell-doc 补上了清晰的框图](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_6414847_link)，在 [Difftest & NEMU 等的案例中](https://gitee.com/RV4Kids/NutShell/issues/I3R6GU#note_6424510_link)均有推荐。)

     其中两个细节印象非常深刻：
     - 主持人问询如何保证差分测试比对的目标就是正确的，答案是用 QEMU,前人的成果。
     - 然后就是 外设缺陷未能在芯片上跑成功 DEBAIN , 并对外设调试做了强调。

  4. [NUTSHELL using CHISEL](https://gitee.com/RV4Kids/NutShell/issues/I47934)

     昨天的课程，干货分享，重申了CHISEL3和差分测试。这也是我用差分测试来描述学习的原因。

  锁定目标是 NUTSHELL 源码的 FPGA 移植。  
  两个仓很重要：

  - https://gitee.com/RV4Kids/NutShell-doc
  - https://gitee.com/RV4Kids/NutShell

---

### 五、[再读 NutShell-DOC](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6414830_link)，三期补课 1-6

- 基于 9 级流水线顺序
- Zicsr 与 Zifencei 指令扩展(可以跑Linux)
- 支持 Sv39 分页方案

这和前面的 7 级不同，是2021又升级了吗？回想 一生一芯3期 同学们的学习要求，将任务定为 FPGA 移植，对于石榴派有意义更大。两张图系统框图，也补了之前作业。

- [NutShell处理器核快速上手教程](https://oscpu.github.io/NutShell-doc/%E4%BB%8B%E7%BB%8D/NutShell-tutorial.html) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415158_link)】

  - .tree 在 后面的课程实践里，被多次使用。
  - 安装 Verilator ： Ubuntu 18.04 或者 Debian 10 以上：
  - `sudo apt-get install verilator`
  - 然后就是“耳熟能详”的 NEMU AM 在 三期课程资料里算“热词”

- [参数化配置](https://oscpu.github.io/NutShell-doc/%E5%85%B6%E4%BB%96/setting.html) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415092_link)】

  > 这是移植工作的重要参考，这时 pynq 就更亮眼了。突发奇想，能否看到整个项目，从零开始演变过程，一定是很好的学习体验。在 oE 社区就是这个体验，RISC-V sig 组的软件包的数量，远远少于 X86 ARM 的版本，可以用 “清爽”“清新可人”来形容，这才有了 RISC-V 学习的起源。

  - 标记下：[一生一芯三期，的 WIKI 资料中的部分](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415230_link)，有重叠
  - 列出了 “[用Chisel开发的RISC-V处理器示例项目](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415230_link)”
  - [FLAG: 尝试使用 DEBIAN10 的衍生版本，RASPBIAN 进行构建。](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415378_link)
  - [AM 在 NUTSHELL 中的推介](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415408_link)。

----

- [补课1](【笔记】https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415464_link)：7月9日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=5)] [Verilator 的介绍](https://oscpu.github.io/ysyx/events/events.html?EID=2021-07-09_Verilator) 负责人: [洪志博](https://gitee.com/a5265325) 21:00 CST 【[笔记](【笔记】https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415464_link)】

  > 这是正式补课的实验课第一课，本期唯一实验记录：作为第一节课，示范了OS-FRAMEWORK, 介绍了开发框架和环境。【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6435056_link)】完整的转载，并依照说明，多角度实验的，第一份正式的实验课作业。

    - [笔记截图](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6435064_link)：对课程中的细节都做了重点标注，并截图。是个好记性不如烂笔头的方法。

      >  **VERILATOR 是编译器可以把 硬件描述语言编译成 C++ 用你的 C++ 语言调用它的库进行调试** 

    - 是本堂的核心，全部实验都围绕这个过程展开。而安装实验环境的各种尝试，也是这个过程中涉及的工具链和环境的配置。这些截图，在之后的实验，有重要的参照作用。

      > .v -> .cpp + main.cpp -> bin -> output .vcd -> gtkwave show 波形  
      > Difftest 则可以减少波形查看的册数，用 printf 查看 错误 LOG ，就能实现。

- Verilator  **实验作业** ：

  1. [尝试在 raspbian 构建 verilator](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6449360_link):

  2. 做了学习笔记的搬移自：[操作系统要求 OS Requirements](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_6247410_link) 

     > 发现：单独运行 可以生成 .o  
     > `g++ -O2 -MMD -I. -I.. -I.. -I../../include -I../../include -MP -faligned-new YSTEMC_ARCH="" -DDEFENV_SYSTEMC_INCLUDE="" -DDEFENV_SYSTEMC_LIBDIR="" -DDEripts -Wno-unused -c ../V3PreProc.cpp -o V3PreProc.o`  
     > 怀疑是开发环境内存限制导致的。还未考虑交叉编译环境的构建。

  3. [直接使用 APT ,发现更新的版本，就是 DEBIAN 官网发布的。 4.010-1](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6449365_link)
  
     - `sudo apt-get install verilator`
     - [gtkwave &  libssl-dev ( libsystemc-dev ) & 被建议，安装了 libssl-doc](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6449393_link).
     - sudo git clone oscpu-framework | [第一个例子 counter](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450575_link).
       `%Error: Invalid Option: --build`
     - 版本号不对， 4.010.1; [比照 make_hello_c](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450602_link); 查看 讲义的截图，发现了 **编译信息就是 G++** 
  
  4. [ubuntu16.04安装verilator+systemc并运行测试程序](https://blog.csdn.net/weixin_39548025/article/details/96967429) 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450604_link)】

     > 明白了 obj_dir 目录的作用 .v -> .cpp 的工作目录

     - [make -j -C obj_dir -f Vtop.mk Vtop](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450610_link)  **HELLO-C 成功了** 
     - [照猫画虎，在 oscpu-framework / projects / counter 目录里尝试](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450614_link) 
       `%Error: Cannot write obj_dir/Vtop__Syms.h`
     - [替换 HELLO-C 目录](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450628_link)，测试 `make -j -C obj_dir -f Vtop.mk Vtop`
       - 修改源文件，隐含掉, 
       - include 路径问题。
       - 缺少 4.2 的 include 补齐。
     - 但是， **找不到 vcd 文件** 。（需要再找原因）

  5. [gtkwave 安装正常](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6450644_link)！

      **交作业!**  :pray: :pray: :pray: 

  6. 课后阅读：[编译器技术在 RISC-V 前端后端的应用](https://gitee.com/RV4Kids/PLCT-Weekly/issues/I47QMY#note_6455469_link)，产生以下三个命题：

     - C 语言作为 Verilator 输出调试的中间语言
     - 现有 RISC-V 前端资源与 C 语言的对接
     - 结合 Blockly 将 C 语言规范作为图形化开发的基础

  7. @a5265325 各位同学，[oscpu-framework](https://gitee.com/oscpu/oscpu-framework) 有更新如下：【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6501176_link)】

     1. 增加 chisel 版本的 [chisel_cpu_diff](https://gitee.com/oscpu/oscpu-framework#chisel_cpu_diff) 例程
     2. 集成依赖AM编译的测试 [bin](https://gitee.com/oscpu/oscpu-framework/tree/2021/bin) 文件
     3. 回归测试支持指定测试用例路径
     3. 增加 "一生一芯" [开发环境一键搭建脚本](https://gitee.com/oscpu/oscpu-framework#%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83)
     4. 增加 "difftest" 接口说明文档

  8. [UBUNTU 20.04 我来了](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6547783_link)（爱 :heart: 了）10条【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I48DW8)】[2](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6547802_link)

     - 不支持WIFI，默认有线网卡是所有 Linux 系统的标配，因为安装环境所需的包都需要源下载
     - 建议使用虚拟机安装 Ubuntu ISO 镜像，至少你还有一个熟悉的桌面可用。
     - Ubuntu For IOT(raspbian4 CM) 最新的 [RP4 Desktop](https://gitee.com/RV4Kids/NutShell/issues/I48LTS) 是我们的选择。 :pray: 

  9. [Verilotar 正确安装](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549306_link)到 [UBUNTU 21.04 FOR IOT - RPI4](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H)

     > [差分测试学习法](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md#%E4%B8%80%E5%B7%AE%E5%88%86%E6%B5%8B%E8%AF%95%E5%AD%A6%E4%B9%A0%E6%B3%95)证明是有效的，才[发布MD](https://gitee.com/RV4Kids/RVWeekly/edit/master/RV4Kids/Issue%2014:%20Difftest%20Learning.md)，晚上觉得可以[写新作业啦](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H)，没想到上节课的各种疑问，这节课一重温都明白啦。实验也顺利，直接开挂，[38 Replies Issue](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H) [搞定 Verilator](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549306_link) 例子工程 [rvcpu 也顺利编译成功](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549310_link)，[VCD 也在 GtkWave 正确显示](https://gitee.com/RV4Kids/NutShell/issues/I48Q9H#note_6549301_link)。完美 :pray: ，主要是为新作业，还准备了[新封面](https://images.gitee.com/uploads/images/2021/0906/101011_f82822b0_5631341.jpeg)，先睹为快。

- [补课2](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415480_link)：7月13日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=7)] AM 运行环境介绍 负责人: 余子濠, 陈璐 19:00 CST 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415480_link)】【[大纲](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6428654_link)】

  > [试图和 DIFFTEST 捆绑](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6425599_link)： _AM 学习过，作为 DIFFTEST 前身的基础设施，由南京大学授课。作为补课的内容，接到 补课3（7月17日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=9)] Difftest 处理器验证方法介绍 负责人: 余子濠, 王华强 19:00 CST）后。阅读完，AM 和差分测试还是有很大差别的，它是一个平台架构和生态资源。学习笔记：回归本课。_ 

  - 其中的超级玛丽，是 一生一芯 NUTSHELL 中的彩蛋，是同一个平台架构
  - AM 花絮，介绍了缘起，让南京大学成为了北向之源。
  - DIFFTEST 则是 [2018 龙芯杯](https://nju-projectn.github.io/ics-pa-gitbook/ics2020/2.4.html#differential-testing)的成果，这在 一生一芯 三期 资料中都有收录。
  - [AM 代码导读](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6430795_link)（陈璐）单独整理

  参考资料：[AM repo](https://github.com/NJU-ProjectN/abstract-machine) | AM 概述（ [计算机系统基础课程视角](https://nju-projectn.github.io/ics-pa-gitbook/ics2020/2.3.html) | [操作系统课程视角](http://jyywiki.cn/OS/AbstractMachine/AM_Design) ）| AM [接口规范](http://jyywiki.cn/OS/AbstractMachine/AM_Spec) | AM 选讲（习题课）[ppt](http://jyywiki.cn/ICS/2020/slides/9.slides#/)

  - [八卦 (1): PA 的由来](http://jyywiki.cn/ICS/2020/slides/9.slides#/5/1) yzh 觉得……好像只有 OS 不够劲啊，OSLab 上直接把 x86 的手册丢给你也不像话。
  - [八卦 (2): PA 的后续](http://jyywiki.cn/ICS/2020/slides/9.slides#/5/2)，但用 Verilog (Chisel) 写个 x86 的 CPU 好像夸张了点……
  - B站录播（[蒋炎岩](https://www.bilibili.com/video/BV1qa4y1j7xk?p=8)）课源：[南京大学 计算机系统基础课](https://www.bilibili.com/video/BV1qa4y1j7xk)

- [补课3](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415486_link)：7月17日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=9)] Difftest 处理器验证方法介绍 负责人: [余子濠](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6424274_link), 王华强 19:00 CST 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415486_link)】

  > 1:00:00 香山 Difftest 框架简介 - 王华强（中科院计算技术研究所）2021-7-17 属于代码导读部分。前面想把 AM 归并在一起，并不是因为内容相近，而是学长学弟情深的表现。AM 作为 基础设施，源自 [龙芯杯获胜的秘诀](https://nju-projectn.github.io/ics-pa-gitbook/ics2020/2.4.html#differential-testing)，这是【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6424384_link)】
  > - 反复强调，反复学习的，缘起，团队文化。
  > - 其中就收录了 NUTSHELL 的案例（师兄师弟情谊，就体现在这里啦）

  - [调BUG是码农的宿命](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6424274_link) 由余子豪主讲，详细介绍了 DIFFTEST 理论，实践，案例。其中：

    - 其他领域的 DiffTest

      - 对于根据同一规范的两种实现，给定相同的有定义的输入，它们的行为应当一致
      - 编译器 - gcc vs. clang
      - 规范 = C语言标准
      - 工具链生成的调试信息 - gcc/gdb vs. clang/lldb vs. rustc/lldb
      - 规范 = 标准 UNIX 调试格式（DWARF）
      - 文件系统 - ext4, btrfs, NFS ...
      - 规范 = VFS (虚拟文件系统)
      - 航空航天容错 - 处理器三副本同时执行，少数服从多数

    - [DiffTest 概述](https://nju-projectn.github.io/ics-pa-gitbook/ics2020/2.4.html)，DiffTest 选讲（习题课）[ppt](http://jyywiki.cn/ICS/2020/slides/12.slides#/)

      - 基础设施：帮助你更快更好地生产代码 P24
      - 终极梦想：让计算机自动帮我们写程序
        - 告诉计算机需求 → 计算机输出正确的代码
        - 计算机科学的 holy grail 之一
        - 现在我们还在软件自动化的初级阶段
      - 计算机只能提供有限的自动化 (基础设施)
        - 集成开发环境 (IDE)
        - 静态分析
        - 动态分析
      - 但这些基础设施已经从本质上改变了我们的开发效率
      - 你绝对不会愿意用记事本写程序的
      - 基础设施：小结 P25

    - 当轮子都不够用的时候，我们就去造轮子

      - 调试困难，有参考实现 → diff-test
      - 难以贯通多门实验课 → AbstractMachine
      - 轮子还不够用呢？

    - diff-test 每秒只能检查 ~5000 条指令
      - 中断和 I/O 具有不确定性
      - 没有参考实现呢 → 一个新的研究问题在等着你
      - B 站录播 - https://www.bilibili.com/video/BV1qa4y1j7xk?p=11

  - 导读：[香山 DIFFTEST 架构](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6424742_link) 是很好的补充，摘要如下：

    - 香山 difftest 仿真框架：文档导读
    - [Difftest 自带的文档介绍了接入的过程](https://github.com/OpenXiangShan/difftest/blob/master/doc/usage.md)
    - [Step by step](https://github.com/OSCPU/NutShell/commits/3991293b3af2026231d9d071635fb7a1b8252cd7) 参考
    - 可以从 git commit 中看到如何将 NutShell 接入这个 difftest 框架
    - 香山项目， NutShell 项目都有 difftest 支持，可以作为参考
      - https://github.com/OpenXiangShan/XiangShan
      - https://github.com/OSCPU/NutShell
      - RTFSC (Read The Friendly Source Code) is important

  - [P30](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6445218_link) 高度凝练了 DIFFtest 的定义，是参考资料。

    - 对于根据同一规范的两种实现，给定相同的有定义的输入，它们的行为应当一致

    - 《[EasyDiff](https://crvf2019.github.io/pdf/14.pdf) – 一个高效实用的处理器验证框架》

       _实用的秘诀-差分测试API 差分测试思想: 对于根据同一规范的两种实现, 给定相同的有定义的输入, 它们的行为应当一致 DUT = 测试对象, REF = 参考实现 在NEMU教学实验中, DUT = 学生写的NEMU, REF = QEMU 在EasyDiff中, DUT = RTL码 , REF = NEMU 6 API 说明 difftest_memcpy(dest, src, size) 从src复制size字节到参考实现_ 

- [补课4](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415499_link)：CHISEL 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415499_link)】

  - 7月14日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=8)] Chisel 入门 负责人: 杨烨 15:00 CST 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6435113_link)】  
    学习资料：[bootcamp 在线学习环境](https://github.com/freechipsproject/chisel-bootcamp) | [Chisel-book](https://github.com/schoeberl/chisel-book) | [特性小抄](https://github.com/freechipsproject/chiselcheatsheet/releases/latest/download/chisel_cheatsheet.pdf) | [Users Guide](https://github.com/freechipsproject/chisel3/wiki/Short-Users-Guide-toChisel) | [FAQ](https://github.com/freechipsproject/chisel3/wiki) | [API](https://www.chisel-lang.org/api/latest/#package) | [本地开发环境](https://github.com/freechipsproject/chisel-template)

    > CHISEL 的优势，用高级语言描述硬件，代码量是 VERILOG 的 1/4 ，生成的 VERILOG 是后端工程的接口。

  - 7月20日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=11)] Chisel 使用经验 负责人: 陈春昀 19:00 CST 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6435126_link)】

    > 结合 Adder 与 进阶 Adder Tree 实例，深入浅出巩固了很多基础概念。干货分享：优化命令、个人经验。

- [补课5](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415513_link)：体系结构 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415513_link)】

  > 未完成任务，准备移植环境搭建好后，再补课。

  - 7月12日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=6)] 单周期处理器介绍 负责人: 薛臻 19:00 CST
  - 7月31日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=13)] 总线和输入输出 负责人: 唐浩晋 19:00 CST
  - 8月14日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=15)] 中断异常 负责人: 高泽宇 19:00 CST

- [补课6](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415528_link)：7月18日 [[讲座](https://www.bilibili.com/video/BV1PU4y1V7X3?p=10)] Git 介绍 负责人: 唐浩晋 19:00 CST 【[笔记](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6415528_link)】【[讲义](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6418636_link)】

  > 这是补课中最先完成的，先易后难，之后的知识学习，可没有这么细致，因为知识点密度太大，难啃。GIT 相对轻松。这也是重要的学习方式，集中优势兵力，各个击破，不断扩大战果，感觉写着写着又回想起《[南征北战](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)》啦。也是因为学习开源社区半年多，一直没有真正 GIT 过，这次系统补课很有必要。

  - 基础：学习目的，课程组检查作业的要求，用以查看作品是否自己独立完成。

    - 基本操作汇总的每一条，老师都实际演示了一遍，实操级课程。
    - 开发流程和规范，描述了协同开发的真实场景
    - 细致到，提交信息的规范。信息粒度。
    - [如何安装 Git](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6418927_link) 实操截屏，都在这里啦。
    - [sudo apt install git](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6418955_link) 全部输出。code.
  
  - 进阶1：老师之前总结的笔记 - [Git 使用规范手册](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6418648_link)
  
  - 进阶2：老师推荐了一本白皮书 - The entire Pro Git book 【[中文版](https://gitee.com/RV4Kids/NutShell/issues/I47E8R#note_6418666_link)】pdf [download](https://github.com/progit/progit2-zh/releases/download/2.1.55/progit_v2.1.55.pdf).
  
---

### 六、在广大的 “[新青年](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC18%E6%9C%9F%20%E6%96%B0%E9%9D%92%E5%B9%B4.md)” 中实践

这期期刊是有一位期待的读者的，他就是尊敬的包云岗老师。是作业，也是分享。是一个假期学习成果的汇报，虽然没有完美的实验课成绩，但这份用心是可以看到的。这是 SIGer 兴趣小组的积累，是开源社区的激励下诞生的。自寒假创刊，到暑假前完成示范开源项目 石榴派 落地，暑假中集中火力，周周分享的 RV 学习小组笔记。一个自主学习的平台已经形成了。就在这个月初，刚刚完成的AI全国赛（火焰棋实验室的重要科普活动），由志愿者团队担纲的新一期 SIGer 期刊也在上周完成了，目标也是《开学第一课》，我将序言，转载于此，能够说明他们是一脉相承的。

- 《[ **正心，正念，正果 之 愿力** ](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC18%E6%9C%9F%20%E6%96%B0%E9%9D%92%E5%B9%B4.md#%E6%A0%A1%E5%9B%AD%E6%99%BA%E9%81%872021-%E7%89%B9%E5%88%8A-%E7%88%B1%E6%B0%91%E6%97%8F%E6%A3%8B)》
 
   _最初发愿，“勇攀科学高峰”，是为了“传承民族文化”。如今两个愿不分伯仲，科学精神是民族文化的优秀基因的一部分。民族的就是世界的。我期待 @zhou-zian905906 和同学们，有这份担当，以屹立世界民族之林为傲。这是全体 SIGer 编委的使命，也是广大青少年们的使命，我们用 SIGer 2.0 来形容。加油！_ 

我再审视本期，整个 “差异化自主学习” 的过程中，
我更善于前端，激励青少年，团结更多的优秀老师，
运用文化的魅力，开源社区的力量，将一座座科学高峰展现给年轻人，
通过他们的聪明才智，和勤奋努力，从一个个兴奋点出发，积累第一个知识点开始，
最终，实现攀登高峰的知识体系，并在攀登过程中，以坚定的意志，克服困难，补足不足，最终登顶。

这就是此文的意义啦，
- 期待更多老师支持我，
- 期待更多同学加入我们，

实现我们最初的心愿： **褀福天下孩童，健康快乐** ！ **_Pray health and happiness for the whole world kids._** 