[![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/082851_37d4db54_5631341.jpeg "少年3副本999.jpg")](https://images.gitee.com/uploads/images/2021/0726/082832_929271c1_5631341.jpeg)

RV4kids 中文名：RV少年（正青春）
- RV：Risc-V
- 少年：象征朝气蓬勃的事业，以及一群奋斗的RV人（与年龄无关）
- 正青春是最IN的SLOGAN，作为中文副题非常契合，等待RV社区创造奇迹

（点击显示A4大图，作为海报稿底图）

---

制作封面早已经是梳理思路，明确目标的方法，这是 《SIGer 编辑手册》 中反复说明了的。RV 少年是对 石榴派 RISCV 版学习社群的一个勉励，作为实验课的分享，每周的学习笔记，将成为他的主体，引用相关学习资源也是常态。秉承 RVweekly 的摘要体，是分享学习笔记的形式，本刊第9期起，给同学们呈现：

（分享不分先后，由编辑依据供稿顺序整理）

### 百年主题《少年》字幕

  http://tv.people.com.cn/n1/2021/0310/c61600-32047792.html
  百年主题MV《少年》，一起出发，见证更多奇迹实现。

  > 演唱：梦然
  > 做曲：梦然
  > 歌词改变：罗高丞

  https://gitee.com/RV4Kids/RVWeekly/issues/I42AKL
  作为本期专题的起点，歌词改变，振奋人心，符合本期立意。

  - [《少年》歌词（原作）](https://gitee.com/RV4Kids/RVWeekly/issues/I42AKL#note_5974912_link)
    原作也靓丽清新，分享了青春不悔，一片赤子之心的情怀，积极向上，符合正青春的主题。

  - [少年 书法作品赏析，](https://gitee.com/RV4Kids/RVWeekly/issues/I42AKL#note_5975254_link)
    是本期封面的灵感源，少年主题，是最催人奋进的主题，将保持下去。

  - [RISCV 进社区，少年吴老师 是重点](https://gitee.com/RV4Kids/RVWeekly/issues/I42AKL#note_5974687_link)
    介绍了 RV 少年 的命名缘起，分享了 oE 社区在 PLCT 实验室吴伟老师接管后的欣欣向荣  :pray: 
    特以 石榴派 实验课的内容，汪辰老师的 操作系统课 porting 致敬 PLCT。

---

### [openEuler](https://gitee.com/openeuler) / [RISC-V](https://gitee.com/openeuler/RISC-V) （[RV4Kids forks](https://gitee.com/RV4Kids/RISC-V)）

oE 是 openEuler 的简称，成为本期 SIGer 鲤鱼 收入。RISC-V 以清爽的仓体，为进入 oE 社区的新人提供了很好的便利条件，在 sig 组会，我也会经常分享这个 [旭周](https://gitee.com/whoisxxx) 老师秒回的故事。才有了 石榴派 RV 版 的诞生。

- [fork RISC-V 到 RV4kids](https://gitee.com/RV4Kids/RISC-V)
  是一种收藏的功能，也作为 石榴派 oE 任务的仓存在，本期暂时为空。

本节以 sig-RISC-V 的 [ISSUES](https://gitee.com/openeuler/RISC-V/issues) 注释，做为分享内容：

- #[I1U0YD](https://gitee.com/openeuler/RISC-V/issues/I1U0YD) [meta] 解决RISC-V的构建失败问题 -  **置顶**   **严重**  11个月前 @whoisxxx
- #[I429HP](https://gitee.com/openeuler/RISC-V/issues/I429HP) 给吴伟和席静开放 rv_spare 的maintainer权限 - 1天前 @whoisxxx
- #[I41U7F](https://gitee.com/openeuler/RISC-V/issues/I41U7F) 文档建议收集 - 5天前 @xijing666
- #[I41U38](https://gitee.com/openeuler/RISC-V/issues/I41U38) 将openEuler:Mainline:RISC-V构建的结果通过自动化的方式实时发布展示 - 5天前 
@xijing666 负责人：@wuwei_plct
- #[I41QXI](https://gitee.com/openeuler/RISC-V/issues/I41QXI) openEuler RISCV 软件包 构建清单 征集 - 5天前 @yuandj 
- #[I4178R](https://gitee.com/openeuler/RISC-V/issues/I4178R) openEuler 在 石榴派 FPGA 开发板上的运行。 - 8天前 @yuandj 
- #[I40RVK](https://gitee.com/openeuler/RISC-V/issues/I40RVK) 在软件仓库中维护一个支持 RISC-V 的软件包的状态列表 - 11天前 @xijing666 负责人：@xijing666
- #[I40RUI](https://gitee.com/openeuler/RISC-V/issues/I40RUI) openEuler 针对香山处理器的适配工作 - 11天前 @xijing666
- #[I40RL6](https://gitee.com/openeuler/RISC-V/issues/I40RL6) 搭建 oE/RV 的 CI - 11天前 @xijing666 负责人：@wuwei_plct
- #[I40RBE](https://gitee.com/openeuler/RISC-V/issues/I40RBE) 在openEuler x86 和aarch64 的repo源中提供预编译包的qemu-system-riscv64 安装包 - 11天前 @whoisxxx
- #[I40R6J](https://gitee.com/openeuler/RISC-V/issues/I40R6J) 在arm64架构的openEuler中增加RISC-V的交叉工具链 - 11天前 @xijing666
- #[I40R6C](https://gitee.com/openeuler/RISC-V/issues/I40R6C) 在x86_64架构的openEuler中增加RISC-V的交叉工具链 - 11天前 @xijing666
- #[I40R3J](https://gitee.com/openeuler/RISC-V/issues/I40R3J) 让openEuler在全志哪吒D1上跑起来，提供文档和教程 - 11天前 @xijing666 负责人：@wuwei_plct
- #[I40R32](https://gitee.com/openeuler/RISC-V/issues/I40R32) 让openEuler在星光开发板（BeagleV）跑起来，提供文档和教程 - 11天前 @xijing666 负责人：@whoisxxx
- #[I40Q9C](https://gitee.com/openeuler/RISC-V/issues/I40Q9C) 翻译中文版README过程中：关于内容及格式的问题 - 11天前 @KaylaLi
- #[I40Q8I](https://gitee.com/openeuler/RISC-V/issues/I40Q8I) 翻译中文版README过程中：句意理解时遇到的问题 - 11天前 @KaylaLi
- #[I40Q5H](https://gitee.com/openeuler/RISC-V/issues/I40Q5H) 中文版README中：部分url与文字描述不匹配 - 11天前 @KaylaLi
- #[I40Q4L](https://gitee.com/openeuler/RISC-V/issues/I40Q4L) [meta] 在其他架构的openEuler中增加RISC-V的交叉工具链 - 11天前 @whoisxxx
- #[I40OKN](https://gitee.com/openeuler/RISC-V/issues/I40OKN) [meta] 为多种芯片支持的固件、文档等 - 11天前 @whoisxxx


| 活跃开发者 | 创建者 | 负责人 |
|---|---|---|
| @whoisxxx | 5 | 1 |
| @xijing666 | 9 | 1 |
| @wuwei_plct | | 3 |
| @yuandj | 2 | |
| @KaylaLi | 3 | |
| total newest issues in 11 days | 19 | 5 |

> 最近5天，主要围绕 OBS 服务和文档完善展开，并增加了两个 maintainer @wuwei_plct @xijing666 。
> 上周围绕的焦点是：交叉工具链，以及三个要适配的硬件，星光，全志，石榴派。
> 其中，文档管理相关的 issues 5条的点评，单独列在了下面。

- #[I34VMV](https://gitee.com/openeuler/RISC-V/issues/I34VMV) [easy] 修改更新文档，更加清晰的指出如何下载RISC-V镜像，解决【镜像名称难寻找】的问题。 - 6个月前 @yang_yanchao
  - @whoisxxx 这回复速度，当真是快啊 :+1: - 4个月前 @yuandj 
- #[I42HGZ](https://gitee.com/openeuler/RISC-V/issues/I42HGZ) [easy-task] 完善英文的 README 页面，去除中文内容，以及让内容更加通顺 - 2小时前 @wuwei_plct 
- #[I40Q9C](https://gitee.com/openeuler/RISC-V/issues/I40Q9C) 翻译中文版README过程中：关于内容及格式的问题 - 11天前 @KaylaLi
  - 2021年3月 《SIGer》青少年开源文化期刊 第#4期、[第#10期 《南征北战》]
问题描述：在我的英文翻译中，我将第四期的题目名进行了补充。主要目的是为了和第 10 期格式对齐。不知道
是否合适？ 以上内容的目前英文翻译内容如下所示。
  - March, 2021 SIGer, a journal of open source culture for teens,  _[Issue 4: Better RISC-V For Better Life and Issue 10: From Victory To Victory](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC4%E6%9C%9F%20Better%20RISC-V%20For%20Better%20Life.md)_ 
- #[I40Q8I](https://gitee.com/openeuler/RISC-V/issues/I40Q8I) 翻译中文版README过程中：句意理解时遇到的问题 - 11天前 @KaylaLi
- #[I40Q5H](https://gitee.com/openeuler/RISC-V/issues/I40Q5H) 中文版README中：部分url与文字描述不匹配 - 11天前 @KaylaLi

这五条被标记为 Easy-task 是 @wuwei_plct 吴伟老师，为实习生设置的练习任务。与本期注释 Issues 不谋而合，在 SIG 组公开讨论中，获得了 [PLCT 实习生生存手册](https://github.com/plctlab/weloveinterns/blob/master/how-do-we-rank-interns.md) ...
> @wuwei_plct 以上几个都是适合新人 /LV1 进行 MR 练习的 tasks, 欢迎认领和提交 MR 修复上述问题。

- #[I28H7L](https://gitee.com/openeuler/RISC-V/issues/I28H7L) [LV2+] 集成bishengJDK 进openEuler RISC-V - 8个月前 @whoisxxx
> 适合 LV2+ 实习生挑战一下

从吴伟老师的任务分派，文档整理属于 LV1 的入门任务，这与 SIGer 编辑手册的定位是完全一直的。下面是 [PLCT 实习生生存手册](https://github.com/plctlab/weloveinterns/blob/master/how-do-we-rank-interns.md) 的摘要： **我们如何对实习生进行能力分级评定** 

> 我们模仿常盘台中学经验将能力者分为LV0到LV5六种不同的程度。达到不同的能力等级之后，具备对应的能力，承担相应的责任。能力等级评定采用仁慈独裁者模式，由 ww 评定。能力等级评定会参考 mentors 和 LV4+ 的意见。评价标准均以能否完成机器人公司日常开发任务为评价标准，公司用不到的能力，不在评价范围。

  - 常盘台中学
    常盘台中学是轻小说《魔法禁书目录》及其漫画《科学超电磁炮》中的私立贵族女校，位于学园都市第七学区的学舍之园，御坂美琴、食蜂操祈、白井黑子就读的学校。常盘台中学是学园都市的五大名校之…
  - 中文名： 常盘台中学
  - 外文名： 常盤台中学(ときわだい ちゅうがっこう)
  - 办学性质： 私立中学
  - 知名校友： 御坂美琴、食蜂操祈、白井黑子
  - 所属地区： 学园都市第七学区
  - 常盘台中学_百度百科
https://baike.baidu.com/item/常盘台中学/4220919

当看到常盘台中学的百科后，我更加坚信 @wuwei_plct 就是广大实习生口中的 [少年](https://gitee.com/RV4Kids/RVWeekly/issues/I42AKL#note_5976061_link)吴老师 。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/083617_2c5dfeb8_5631341.png "屏幕截图.png")

#[I41U38](https://gitee.com/openeuler/RISC-V/issues/I41U38) 将openEuler:Mainline:RISC-V构建的结果通过自动化的方式实时发布展示 - 5天前
@xijing666 负责人：@wuwei_plct

- 当前 obs server：https://build.openeuler.org/project/show/openEuler:Mainline:RISC-V 在构建openEuler 的 packages 的过程中，有很多失败的包。
当前我们正在持续不断，逐一去解决失败的 packages 的问题，为了能够及时的，更加友好的展示构建结果，拿到类似 https://gitee.com/openeuler/RISC-V/blob/master/configuration/rv_cfg.list （历史某一次构建的结果） 的结果，希望能够通过自动化的手段去完成该工作。

  @whoisxxx
  - OBS构建系统提供了一个osc工具，可以通过命令行控制构建，获取结果等。我当前的做法是

    在本地环境配置osc环境，以OBS系统的账号登陆
用 `osc results -l openEuler:Mainline:RISC-V` 来获取构建结果

    相关文档：[osc 工具说明](https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.osc.html)

#### OBS 服务之昨日讨论

- 其实 obs worker 是什么 os 不重要，只要是 risc-v 的环境就 OK。因为每一次构建动作都是在 chroot 环境里边用依赖仓的软件包重新做的，依赖仓配置成 obs 一致的就可以。我这边用的就是 openEuler，为了使用和多发现问题可以多用 openEuler 哈

- 个人操作obs可以分成两种方式，一个是修改代码触发构建啥的，可以通过界面操作也可以通过命令行的osc环境操作，这两个动作本质上是一样的，视频中应该指的这种，这时实际执行build的任务是在obs集群中的openeuler risc-v worker上。另外一个是osc本地构建，就是可以通过命令行在本地进行build任务，在需要改代码调试的时候相对方便一些，这就需要本地是risc-v环境。

- 在需要改代码调试的时候相对方便一些，
  目前还有需要我们直接去维护软件包本身代码的情况么？

  我以为如果是软件包本身不支持riscv，或者在riscv上运行有问题，是反馈到上游社区让软件包对应社区去支持。。看起来我的理解有误

  - 有呀。。上游社区最多就只负责一个软件的适配(有时候还要看心情)
  - 比如吴老师这边做的v8的工作。  
    即使源码支持了，也会存在因为spec 没适配而失败的情况，以及一个os发行版集成软件包的版本匹配的问题，这些可能都需要修改调试

- 不应该相互引用吧，这种情况是不是把同一个包多次构建比较好？每次都在使用不同的配置，避免循环依赖
有具体的例子吗，我的直观的感受是构建系统应该能避免的这一点。也有可能是问题我理解的不准确。

  - OBS就是健哥参与开发的，这件事上最权威[哇]
  - 现在openeuler也在做这方面的优化，现在有些包的依赖写的不规范，造成循环依赖，光是安装环境就要长时间

- 基于obs的build的发行版，有rpmbuild，本地osc build，远程obs build三个层次的build方式。调试的时候，可以本地用osc build，站的包的角度看，和远端处理的包的依赖都一样的，只是当前包在本地。如果osc build都有看不清的问题，可以在用rpmbuild看下。

  三个层次的build方式：
  - 有rpmbuild，
  - 本地osc build，
  - 远程obs build

- osc会下载所有指定的依赖的包。如果和远程build不一样，可能是build-type的问题。
  osc命令：https://en.opensuse.org/openSUSE:OSC
  kiwi（build发行版的工具）：[来自suse的猕猴桃(KIWI)（Linux自动化部署工具之三）](http://mp.weixin.qq.com/s?__biz=MzI5MzcwODYxMQ==&mid=2247483760&idx=1&sn=0785ed74878b5ef27943bda7fc6f2c9f)

- 是的哈，现在的流程就是本地osc build验证通过之后，再传到obs上。对了想到一个现象，就是OBS 的工程，有的包似乎会重复被构建好几遍，当前是开启了risc-v-kernel的use for build，结果就会构建好几遍，感觉像是和循环依赖有关系。我一直挺好奇的是 OBS在处理这种循环依赖的自依赖构建的判定终止标准是啥呢？是看前后两次构建二进制是否一致？

  - 不太确定。和stage有关么？现在有没有用stage？
  - 指的是 staging project吗？目前还没呢
  - 现在是那些包会编译多次？我现在能想到的是这个包依赖的包有变化，所以要重新build。如果是这个原因，可以用staging看看，等基础包都构建完成再继续构建其它的包。
  - 昂，我去找下staging 的workflow是啥。有挺多的感觉编了多遍，包括kernel。我的想法也是基础包比如glibc gcc 这种编好了就归档不要动了，不然单包构建时间长而且还会引起大范围的重编

- 对新加入的同学的一个建议：
  1. 个人觉得接触一个项目，首先一定要明确项目目标，已经当前的状态，工作的重点：（这个之前的会议有提到，但是缺文档介绍，待整理）可以先看看会议纪要： https://etherpad.openeuler.org/p/sig-RISC-V-meetings
  2. repo的readme是必读的，了解下项目的整体情况：https://gitee.com/openeuler/RISC-V/blob/master/README.md
  3. 【最重要】工作重点：参考https://gitee.com/openeuler/RISC-V/issues/I1U0YD?from=project-issue（一定要认真的去读并理解每一步做什么，怎么做）
  4. 【重要】基础知识：obs的基础知识，推荐视频：https://www.bilibili.com/video/BV1YK411H7E2  （大致了解3之后就先学习该视频，然后再反过头去看3的任务）

  @xijing666 目前也依然是围绕着上述第3点这个issue，张老师 @whoisxxx 描述的步骤，逐步推进。

---

  ### [shiliupi](https://gitee.com/shiliupi/) / [shiliupi99](https://gitee.com/shiliupi/shiliupi99) / [石榴派漂流计划](https://gitee.com/shiliupi/shiliupi99/issues) | [SIGer 寄语](https://gitee.com/flame-ai/siger) | [MagSi 双漂流](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1:%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md#%E7%9F%B3%E6%A6%B4%E6%B4%BE-%E5%8D%97%E5%8C%97%E5%90%91%E5%88%86%E6%9E%90)

- #[I42A7W](https://gitee.com/shiliupi/shiliupi99/issues/I42A7W) 学习 OPENWRT 的仓【转】
- #[I427WW](https://gitee.com/shiliupi/shiliupi99/issues/I427WW) shiliupi漂流日记 No.1 - 3天前 @wolftree

迎来了第一篇 开发者 日记，标志着石榴派漂流第一波的成功。而第二波已经启动，并收到了 PLCT 实验室的汪辰老师的 RVOS 课上的学生，我的 "同学" 的申请。借此，将石榴派的主要内容也同时发布，OPENWRT 是石榴派 7020 FPGA 的上游系统，也是可以重建的。欢迎同学们申请，撸软核，撸板子。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/111325_03dc9e4a_5631341.png "屏幕截图.png")

身着 CNRV 峰会的战袍，和袁老师同款，擎着石榴派的我们，共同发愿，团结一芯，为 RISCV 社区贡献绵薄力量！（就在上周六，社区漂流也再现了战袍加身的威力，我们创造了新的人偶棋子，集体发愿的游戏！ :pray: ）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/111031_311eee88_5631341.png "屏幕截图.png")

- #[I41X64](https://gitee.com/shiliupi/shiliupi99/issues/I41X64) 蒲公英中学，迎接教育未来，拥抱开源社区！石榴派漂流日记

文化漂流，是石榴派双漂流的重点，北向。蒲公英中学漂流日记的花絮，为石榴派赋予了特殊的意义。这是这所特殊中学的缘起有关，十年前的一次志愿服务经历后，每年都会收到一封新年贺卡，突然某年没有收到，心中的惦念就更加强烈了。年初，一位志愿者好友分享了一则蒲公英新校的朋友圈，重燃了我的希望。再次联系，确定了 民族棋漂流 活动后，再这个暑期夏令营的最后两日，完成了漂流任务。这时，民族棋的使命已经交给了 石榴派。而 开源文化 的分享精神 是与 民族棋的缘起 火焰棋 的分享同源的。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0728/001212_918ebf7e_5631341.png "屏幕截图.png")

更多 关于 石榴派漂流计划的内容，可以参看 MagSi 的 石榴派南北向分析。
（下面是我们的漂流目标，一副满载寄语的民族棋）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/184912_a0c3821a_5631341.jpeg "在这里输入图片标题")

- #[I3YL6P](https://gitee.com/shiliupi/shiliupi99/issues/I3YL6P) 石榴派 X86 镜像 U 盘需求发布！

石榴派的使命是分享开源精神，开源科普，而 X86 镜像的能力，是 oE 发行版的标配，酝酿了 近1个月，也终于发布啦。这名字就是故事，不在这里重述，上图：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180517_04349422_5631341.png "在这里输入图片标题") 