[目录](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6241448_link)：

- [封面头条](#%E5%BA%8F) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6237916_link)
- [以开源的精神和包容的文化](#%E4%BB%A5%E5%BC%80%E6%BA%90%E7%9A%84%E7%B2%BE%E7%A5%9E%E5%92%8C%E5%8C%85%E5%AE%B9%E7%9A%84%E6%96%87%E5%8C%96join-us)（Join us） 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6239839_link)
- [HowTo Running Linux 发行版](#howto-running-linux-%E5%8F%91%E8%A1%8C%E7%89%88) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6239909_link)
- [TurboLinux Enterprise Server 15](#turbolinux-enterprise-server-15) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6240618_link)
  - [学习笔记1：一个 Bing 全搞定](#%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B01%E4%B8%80%E4%B8%AA-bing-%E5%85%A8%E6%90%9E%E5%AE%9A)
  - [学习笔记2：最好的分享在公众号（干货满满）](#%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B02%E6%9C%80%E5%A5%BD%E7%9A%84%E5%88%86%E4%BA%AB%E5%9C%A8%E5%85%AC%E4%BC%97%E5%8F%B7%E5%B9%B2%E8%B4%A7%E6%BB%A1%E6%BB%A1)
- [罗宇哲：Linux 内核发展史 | Linux 内核系列](#%E7%BD%97%E5%AE%87%E5%93%B2linux-%E5%86%85%E6%A0%B8%E5%8F%91%E5%B1%95%E5%8F%B2--linux-%E5%86%85%E6%A0%B8%E7%B3%BB%E5%88%97) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6241172_link)
- [openEuler Website 0.1.2.](#openeuler-website-012) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6241389_link)

---

### 序

![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/003003_c071022b_5631341.png "2021-08-14-002902_814x723_scrot.png")

- [GIMP](https://baike.baidu.com/item/GIMP/226522)（[Photoshop](https://baike.baidu.com/item/Photoshop/133866)型程序） [How to Install on Raspberry PI?](https://gitee.com/shiliupi/shiliupi/issues/I45GMP)

看着 GIMP 界面，由衷的喜欢，尽管和 PS 的操作习惯不同，但它出身于 GNU 世家散发的气息，深深地吸引啦我。而这要感谢 拓林思 TURBOLINUX 的学习。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/003328_18766864_5631341.png "TurboLinux.png")

就是他了，TURBOLINUX 学习笔记，感谢丁文龙老师的拥抱。“每次学习，总能将之前的知识呼应起来，让 oE 的全景图在心中更加清晰一些，再清晰一些。” 这是写下 “拥抱” 二字的感受。想着有更多的同学，踏着我的脚步，进入到 oE 的学习（使用），我就更踏实了。因为 陈棋德 老师指出 衍生版的生态实际就是帮助 原生OS完善生态的，他认为 openEuler 有望像 Debian 和 Fedora 那样成为世界最主流的版本之一。这个愿景，我收到了，再一次将 “石榴派” 作为感谢 “拥抱” 的礼物，回馈给 oe 社区。

- [以开源的精神和包容的文化](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6239839_link)（[Join us](https://www.turbolinux.com.cn/index.php/pages/cat_id/37.html)）
- [丁文龙](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6231943_link)：HowTo Running Linux 发行版
- 罗宇哲：[Linux 内核发展史](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6232189_link) | Linux 内核系列

三条头条是本期的题眼，而 [TurboLinux Enterprise Server 15](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228520_link) 才是本期主题，因为他是积淀了21年的行业经验，拥抱 openEuler 后的再青春。[头条中 Linus 的原话](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6232189_link)，让我们重拾初心的感动：

> 我们简要回顾了Linux内核的发展历史。Linux的成功不但让Linus还清了其DIY个人计算机的欠款，而且还让他收获了爱情——一位叫朵芙（Tove）的姑娘向他发来邮件，邀请他去约会。不知道Linus赴约的时候是否知道，邀请他的这个女生曾六次获得芬兰空手道冠军。“朵芙是第一个通过互联网方式接近我的女人，而我干脆就把她娶回了家。”多年以后，Linus回忆这一段经历，十分得意[1]。最后献上一句Linus的话与大家共勉[2]：
  - “做自己喜欢的、并对其他人也有帮助的事情很重要。”

---

### 以开源的精神和包容的文化（[Join us](https://www.turbolinux.com.cn/index.php/pages/cat_id/37.html)）

放在开篇，本想抛砖引玉，而重温了所有[学习笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA)，也未能找到全面详细地阐述这句话的内容，这才将 Join us 作为索引，推荐给大家，好在我已经学习了一遍，又复习了一遍，现在开始写总结，思考了一遍，这里与同学们分享一二。

-  **拥抱开源** 的企业文化
  > TurboLinux作为中国Linux领域的开拓者，在早年间就家喻户晓。
在开源和培训领域更是吸收培养了中国最早一批的Linux使用者和开发者。
随着本世纪第二个年代的开始，TurboLinux选择OpenEuler作为我们全新的出发点，以拥抱开源的模式积极参与鲲鹏生态的建设和发展。
我们积极欢迎所有想学习，开发以及使用OpenEuler的小伙儿伴加入我们，一起为鲲鹏生态添砖加瓦。

为什么用 “拥抱” 做序，这要追述到 [SIGer 创刊号，第 0 期](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md)，作为新入 oE 的新人，我这个老师也是 “步履为艰” 小心翼翼，因为被贴上 “实习生” 标签后，问啥都不好意思，因为，“请注意提问的礼仪？”。这是课堂上的尴尬， _本就预习了的同学，老师的提问就成为了良好的互动课堂的氛围的工具，或者检查预习作业的手段，而标准答案式的寻找，浪费掉了很宝贵的课堂资源。_ 同样的尴尬在社区中同样存在，原本希望繁荣的社区，结果有限的资源被一众小白浪费掉啦，着实可惜。殊不知，一个小小的拥抱，能够为 “实习生” 点亮未来的道路，为社区留下一颗火种，不日，他会用加倍的努力回报社区。尽管我并不赞同 “立于不败之地” 作为开源的出发点，但我依然要用 “拥抱未来” 就必须 “拥抱开源”，只有拥抱开源，我们采用拥抱未来。而这是所有开源人必须思考的问题，开源之北是 “文化”。一个优秀的企业，首先要有全新的 “实习生文化” —— 全方位拥抱我们的年轻人。

- 求贤若渴 ( [join us](https://www.turbolinux.com.cn/index.php/pages/cat_id/37.html) )

  > 作为一家钻石型企业，我们认为高效的管理配合优秀的团队，才能提供出最为上乘的产品和服务。 同时作为一家完全市场化的厂商，我们高度认可鲲鹏生态的市场价值和前景， 如果你也想和我们一起为搭建鲲鹏生态而出一份力的话，欢迎早日加入我们。我们目前需要： 
    - Linux 内核工程师 10名， 
    - Linux 图形环境开发工程师 2名， 
    - 虚拟化/容器开发师 5名， 
    - 售前工程师 3名， 
    - 技术支持工程师 20名。

---

### HowTo Running Linux 发行版

Leo.Ding 2021-08-12 18:42

> 袁老师你好，昨晚你给我发了几段比较长的语音，有些问题可以帮你简单解答一下。

Leo.Ding 18:43

> 因为 linux 的开源特性，所以做发行版也不是什么太难的东西，很多做发行版的资料在网上都能找到，只要有时间和精力，无论是个人也好还是公司也好，都可以做自己特色的发行版，然后分发给别人体验和使用。

> 维持一个发行版是比较耗资源的，一般 OS 厂商都有自己的发行版规划，比如基于哪个开源发行版做自己的版本（或者完全基于源码），多久发布一个新的版本，新老版本的技术支持，发行版中的 bug 修复、CVE 漏洞修复，新版本的构建测试等，这些都需要投入大量的人力。另外你还得需要一些其他的资源来支撑你的发行版的对外发布，比如构建一个自己专属的网站来发布版本信息，提供镜像下载、软件安装、系统更新等服务。你说的提供一个母盘，通过我这边的渠道来对外提供系统下载、系统更新等，这个我可能没法帮你实现，毕竟我现在接触到的资源都是公司，我也无法不经过公司的同意直接拿过来用。

> openEuler 社区的人没有什么动力也是正常现象，因为社区虽然是开源的，但是社区最终的目标还是以利益为导向的，社区虽然人多，但是大部分人都是以公司的身份加入 openEuler 社区，他们在社区中的工作都是有公司利益相关的方面（组件），完全因为兴趣参与 openEuler 社区的人可以说是少之又少。

> 我在一个非官方的 Ubuntu 中文社区（ https://forum.ubuntu.org.cn/ ）看到过有人在已有 ubuntu 发行版上定制自己的发行版，基本操作就是将 ISO 解开，修改 ISO 镜像里的内容，删除不需要的软件包，添加自己需要的包（或者自己编译的），然后再封装成 ISO 重新发布，这是最简单的法子，也是成本比较低的一种方式。当然有些看似比较专业的发行版也用这种方式，比如 https://www.blueonyx.it/news/255/15/5210R-CentOS-8-ISO-building 
企业做发行版一般是从源码编译，编译自己需要的所有的源码包，然后再添加自己特色的一些组件（可能是开源的或闭源的或客针对不同客户的版本）构建出一个自己的发行版。企业之所以要重新编译源码，是因为 CentOS 等发行版编译出来的二进制安装包可能受当地法律限制，而开源代码是属于言论自由，企业可以直接拿过来，另外 openEuler 编译的二进制安装包企业是可以直接拿过来使用的。

这是一个大大的拥抱，正如我的提问，相当多的疑问在当下，“[石榴派](https://gitee.com/shiliupi/shiliupi99)” 急需一个比较完整系统的发行方案，不为别的，只希望能有更多的青少年，能第一时间体验和学习到 Linux —— 这一开源文化的结晶。下面是我的问题一箩筐：

@yuandj 2021-08-11 21:16

我有一个不成熟的想法，和丁老师交换。我用语音介绍下。您抽空听即可。

-  **发行，是个特别专业的活儿** 

丁老师，现在我们的石榴派 X86 版，是个U盘版。主要以赠送的方式，给学校做体验。实际这是个吃力不讨好的方式，因为U盘成本，经济一点的U盘，大概16G的那种，是  **USB2.0**  的接口，速度特别慢，制作一个（发行版）U盘，CLONE 一个得弄15分钟。还要折腾来折腾去，所以复制起来是蛮消耗时间的。然后如果是通过 **网络下载** ，如果是完全不熟悉的人的话，让用户来做的也是很挠头的，甚至，恨不得问半天下载地址在哪儿？所以这个发行版，是个特别专业的活儿，我们发了这个愿，现在发现真是个蛮复杂的事情。

-  **寻求专业的发行渠道** 

之前没有经验，应用软件光盘，安装都是交给客户的，那现在做 OS 的发行版，真不知道服务的效率怎么提升啊？这是个很专业的领域，也没地方可以问，所以我的一个不成熟的想法是说。可否做一个类似母盘，交给丁老师，用你们的渠道，是不是可行？不管是镜像下载也好，还是自动更新也好，让人看着就是一个专业的发行版。同学们真的是太业余了，我这个老师也是半瓶子。

这个愿景是够大的，所以我觉得还是得靠社区，真正能站在巨人的肩膀上。像 oE 社区包括华为，它们虽然很参与，但发行好像没有太多精力或动力，是把这个发展推给其他伙伴的。所以我才想，寻求（专业发行渠道）的合作方式有没有可能？

-  **学习就是我们的应用场景** 

我（[石榴派](https://gitee.com/shiliupi/shiliupi99)）的一个应用场景呢就是：由学生根据自己的学习需求，构建个人版的 openEuler 系统，上面挂接一些其他的专属应用。比如：[GNUgames](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md) 是我们的一个范例，其他的学习场景也会有。我们才构想用 U盘 做发行介质。现在很小众，如果数量多的话，就会产生一个公共的基础，用 oE + GNU + gnome桌面，作为一个基础，然后提供下载，其他软件，交由用户自己安装。但是这个过程还是希望有个正规的发行渠道，会更踏实些。现在就一个社区小伙子维护，开始有热情，被打动，有人需要，就给他发信息，后来报告说：“老师，我这来一个需求，就得忙活一晚上，什么都干不了，效率实在是太低了。我能写一个链接，让用户自己去下载，或自己构建啊。“ 这是一个这很实际的问题，完全没经验。发行版到底怎么发行的？可以说做一个镜像就叫发行版了吗？那这个渠道怎么弄就是太专业了，请老师给指点迷津。 :pray: 

- HOW TO RUNNING 一个发行版？[抱拳] @yuandj 21:22

@yuandj 2021-08-12 18:54

> 每个企业都有自己的积累。很好的学习资料。至少报名实习，学生们也不会一问三不知[合十]
> 目标就出一个石榴派的发行方案，以您的指导为线索

-  **SIGer 学习小组也是同学们的学习场景** 

  这是本刊的缘起，发起向 TurboLinux 学习的任务，以研究公司公开的分享，以习得一定的基础知识，之后再边实践边请教的学习过程，就是 SIGer 学习小组的形式了。而本刊从制作到完成，100% 是使用 Linux 操作系统（[树莓派](https://gitee.com/shiliupi/shiliupi/issues)），尤其是在 TurboLinux 的产品介绍中推介的 [GIMP](https://baike.baidu.com/item/GIMP/226522)。并由此，增加了三条学习笔记：

  - #I45C31 [在树莓派上截屏的方法](https://gitee.com/shiliupi/shiliupi/issues/I45C31)
  - #I459Q6 [树莓派如何安装中文输入法](https://gitee.com/shiliupi/shiliupi/issues/I459Q6)
  - #I459Q5 [在树莓派上使用麦克风](https://gitee.com/shiliupi/shiliupi/issues/I459Q5)

---

### [TurboLinux Enterprise Server 15](https://www.turbolinux.com.cn/x86/index.php/news/cat_id/7.html)

向 TurboLinux 学习必须要从 TLES15 入手，[封面故事](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6229130_link)就是本节了。全面特性如下：

#### 产品介绍

TurboLinux Enterprise Server 15 (TLES15) 是拓林思公司2020年3月基于Open Euler内核发布的一款Linux企业级服务器，与OpenEuler原生系统相比，TLES15为用户提供了更易使用的可视化操作界面。相较于市面上主流的其他Linux操作系统，TLES15基于ARM架构的鲲鹏芯片进行了更多深层次的内核级优化，从操作系统层面大幅度提升了服务器端的各项性能，其中对MySQL数据库的优化甚至能提升综合性能至30%以上。

#### 核心特性

- 性能全面超越CentOS 性能全面超越CentOS
全软件堆栈技术优化
通过对底层OS,虚拟化容器，加速库，和应用这四个层面的全面优化，
实现性能的全方位提升

- 针对MySQL特殊优化 针对MySQL特殊优化
通过感知硬件拓扑的多核调度，
发挥多核算力，
MySQL性能领先30%

- 虚拟化 虚拟化
增强KVM的虚拟化，性能开销<5%

- 轻量化容器 轻量化容器
轻量化容器引擎
兼容南北向标准
更快的启动与损耗

- 编译器 编译器
编译器优化发挥鲲鹏多核性能，性能提升15%~20%

- 智能自优化 智能自优化
A-Tune可以依据业务场景和系统信息自动优化具体参数

#### 特性详情

 **感知硬件拓扑的多核调度：发挥多核算力**

- 依据用户态进行差异化调度，时延缩短  50%
- 提供内存访问加速，满足关键路径  0 TLB miss
- ARM64用户态IO栈，IOPS提升  10倍
- 高性能锁重构，spinlock锁感知NUMA架构

 **针对鲲鹏芯片架构增强KVM的虚拟化**

- 鲲鹏虚拟化关键技术

  - SPDK高性能存储IO栈
  - 虚拟机Numa感知调度
  - 虚拟机CPU QoS确定性时延调度
  - 虚拟机热迁移内存压缩
  - GICv4虚拟设备中断direct deliver

- 虚拟化性能开销<5%

  - cpu性能损耗降低87%
  - 内存性能损耗降低88%
  - 存储性能损耗降低95%
  - 虚拟机平均响应时延降低77%

 **轻量化容器引擎，兼容南北向标准，更快的启动与损耗**

- 支持三种部署模型
  容器根据部署模型可以分成三种模型，三种模型均有适用场景，目前不存在统一的部署模型。

  1. 应用容器：应用最广泛的容器形态，容器中仅运行客户定义的应用程序。
  2. 安全容器：本质上是虚拟机，但是接口封装为容器接口；着重容器的隔离性缺陷。
  3. 系统容器：本质上是容器，但是使用方式和虚拟机相同；着重解决虚拟机的厚重问题。
 
- 相比传统Docker， 启动时间减少50%，内存开销减少58%
  三个优势特性：

  1. 一步到位兼容最新标准（OCI和CRI标准），相比Docker常驻内存进程从3个减少到1个。
  2. 调用层级从3级压缩到1级，提升启动速度。
  3. 扬弃go，改用C语言，删除标准外冗余代码，开销减小，实现代码精简化（30W->7W。

 **编译器优化发挥鲲鹏多核性能，性能提升15%~20%**

- Spec2006 提升  20%
- Spec2017 提升  15%

 **A-Tune场景智能自动优化** 

A-Tune是智能性能优化系统软件，  
即通过机器学习引擎对业务应用建立精准模型,  
再根据业务负载智能匹配最佳操作系统配置参数组合，  
实现系统整体运行效率提升。

#### 而浓缩的成果就是三份[文档](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228981_link)啦：

- [发行说明](https://www.turbolinux.com.cn/x86/index.php/pages/cat_id/22.html)：
- [管理员指南](https://www.turbolinux.com.cn/x86/index.php/pages/cat_id/39.html)：
- [容器用户指南](https://www.turbolinux.com.cn/x86/index.php/pages/cat_id/16.html)：

---

### 学习笔记1：一个 [Bing](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA) 全搞定

- [Microsoft Bing 国际 turbolinux](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA)

  Turbolinux was a Japanese Linux distribution targeting Asian users.

  - OS family： Unix-like
  - Developer： Turbolinux
  - Platforms： IA-32, x86-64
  - Latest release： 12.5J / August 29, 2012
  - [Turbolinux - Wikipedia](https://en.wikipedia.org/wiki/Turbolinux)  
    https://en.wikipedia.org/wiki/Turbolinux

   **Linux distribution** 

  The Turbolinux distribution was created as a rebranded Red Hat distribution by Pacific HiTech employee Scott Stone. Scott was the lead release engineer through version 3.6. Turbolinux was notable for including licensed copies of CyberLink PowerDVD and Windows Media binary codecs.[了解更多](https://en.wikipedia.org/wiki/Turbolinux)

   **Company** 

  Cliff Miller and Iris Miller started TurboLinux in 1992 under the name Pacific HiTech, Inc. in the basement of their home in Salt Lake City, Utah., PHT changed its name to Turbolinux in June 1999 to better identify with its flagship product. In January 2000, Turbolinux received $57 million in investment from Compaq Computer, Dell Computer, Intel, and other companies. The Millers were "terminated without cause" from Turbolinux in July 2000 after a disagreement with venture capitalists. In October[了解更多](https://en.wikipedia.org/wiki/Turbolinux)

   **Dissolution** 

  On December 31, 2019, TurboLinux announced that it was immediately ceasing operations.[了解更多](https://en.wikipedia.org/wiki/Turbolinux)

  摘要如下：

  - [TurboLinux - 维基百科，自由的百科全书](https://zh.wikipedia.org/zh-hans/TurboLinux) 笔记：[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6223206_link) （含简介）
  - [DistroWatch.com: Turbolinux](https://distrowatch.com/turbolinux) 
  - [TurboLinux简介_kougq819的博客-CSDN博客](https://blog.csdn.net/kougq819/article/details/6155199) TurboLinux的站点：
    - http://www.turbolinux.com （美国）
    - http://www.turbolinux.com.cn （中国）
    - http://www.turbolinux.co.jp （日本）
  - [【TURBOLINUX操作系统】报价_(TURBOLINUX ...](product.yesky.com/os/turbolinux) 笔记：[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6223250_link) 曾经是 5/6 位数。
  - [【Turbolinux MaxDB】报价_参数_图片_论坛_(Turbolinux)拓 ...](detail.zol.com.cn/os/index130809.shtml) 笔记：[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6223413_link)（￥4.62w）
  - [TurboLinux使用经验谈(转)_cuemes08808的博客-CSDN博客](https://blog.csdn.net/cuemes08808/article/details/100392387) 笔记：[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228430_link)
  - [Turbolinux 12.5 发布_Linux下载_Linux公社-Linux系统门户网站](https://www.linuxidc.com/Linux/2012-08/69248.htm) 笔记：[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228358_link) Turbolinux 12.5 发布 2012/08/30
  - [ターボリナックス株式会社 - Turbolinux, Inc.](www.turbolinux.com) 笔记：[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228080_link)

- http://www.turbolinux.com.cn （[拓林思中国](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228448_link)）

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/123740_3fa171bd_5631341.png "在这里输入图片标题")

  - [ABOUT US：](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228765_link) 公司介绍 | 发展历程 | 企业文化 | 人才招聘 | 商务合作 | 联系我们
  - [X86： 基于X86架构 深度优化的 TurboLinux](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228520_link)
    - [案例](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228749_link)：国家气象局 | 大唐集团 | 大有物联网 | 中石化 | 中铁快运 | 合作伙伴
  - [文档](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6228981_link)：发行说明 | 管理员指南 | 容器用户指南
  - [TurboLinux Enterprise Server 15](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6229130_link)
  - [ARM： 基于ARM架构 深度优化的 TurboLinux](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6229213_link)

---

### 学习笔记2：[最好的分享在公众号](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6231861_link)（干货满满）

1999-2020 这才是封面故事的底蕴，也是我学习 TurboLinux 后的致敬。用干货满满来形容，是因为它给于 ”石榴派“ 两个决定，将为 GNUgames 增加两个新游戏 blesschess & LuckyDoku ， 同时基于 TurboLinux 帮助构建的 oE + Gnome 精简版 构建 GNUgames99 民族棋发行版，之所有保留民族棋，是记住 我们 来 oE 的起点。同时，民族的就是世界的！是永远正确的北向文化。并以下面这段话为坐标，秉承专注的企业精神，将开源科普进行到底！

> TurboLinux创立于1999年，是最早在中国提供企业级Linux服务的知名开源厂商，总部位于北京。是国内领先的Linux操作系统服务商。

学习要点如下：学习笔记全文见（[这里](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6231861_link)）

#### 2021

- 06-11 [活动 | 拓林思助力openEuler Developer Day 2021 ，推动开源社区发展与内核创新](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484776&idx=1&sn=26228264a4ba4aef4ab30739607b798f)

- 06-09 [速看！！！openEuler Developer Day 2021 现场活动安排曝光](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484284&idx=1&sn=5a8b4ed465a523b3ad16826566c905a7)

- 05-28 [@全球开发者，openEuler Developer Day 2021邀请函来啦！](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484237&idx=1&sn=d8ce2ae1dcb17d1816620420fffd513c)

- 05-11 [openEuler Developer Day 启动大会招募环节，报名通道同步开启！](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484230&idx=1&sn=62af5bfd8a80f57831b20bbb4d2502d3)

- 04-28 [openEuler 21.03 内核创新版正式发布，拓林思邀您下载技术白皮书](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484214&idx=1&sn=cfa98b922eb6a036c37b557f2c1cadf4)

- 04-16 [技术干货 | 原来，硬件的对齐方式对性能影响这么大](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484067&idx=1&sn=df33f8a82c7bd787964889307e4d6857)

  1. 对齐的动作。程序设计者需要处理的是数据方面的对齐。
  2. 为什么要对齐？ 是因为CPU数据总线宽度和内存芯片数据总线宽度的不相等。
  3. 对齐反映到程序设计
  4. 鲲鹏CPU下的对齐

     ![输入图片说明](https://images.gitee.com/uploads/images/2021/0813/181921_c2d7d453_5631341.png "屏幕截图.png")
     采用NUMA架构的鲲鹏cpu

  5. 参考资料

     - 《微型计算机原理与应用》 李伯成等 著 西安电子科技大学出版社
     - CSDN：[CPU内存访问要求对齐的原因](https://blog.csdn.net/hehe199807/article/details/109091843)
     - 脚本之家：[解析内存对齐 Data alignment: Straighten up and fly right的详解](https://www.jb51.net/article/36726.htm)
     - 博客园：[GNU C - 关于8086的内存访问机制以及内存对齐](https://www.cnblogs.com/respawn/archive/2012/07/10/2585334.html)(memory alignment)


- 03-26 [技术干货——开源世界中的upstream](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484049&idx=1&sn=162b626b7923735698f9068faa53deae)

  > 开源世界中，upstream通常是指贡献和发布出来的软件源码仓库，一般从upstream流向downstream。当谈到某个软件或产品的upstream时，通常是指该项目或产品的源头。

  > 最典型的就是 Linux 中的 kernel ，它是许多 Linux 发行版的 upstream 项目。行业内的 Linux 发行版本（以下简称发行版），通常都是通过 upstream 获取到未经修改的内核源码（从官网 https://www.kernel.org/ 获取），然后添加一些patch以及个性化的配置，最终以他们想呈现给用户的选项构建该发行版的 kernel 。

  一个项目或产品可能有多个上游。以业内有名的Red Hat Enterprise Linux（RHEL）版本为例，RHEL基于Fedora 社区版本，Fedora社区又获取许多上游软件项目来创建Fedora linux，如：
  - Linux内核、
  - GNOME、
  - systemd、
  - Podman、
  - 各种GNU实用程序等。

   **为什么upstream很重要？** 

  - upstream是做出决策和贡献的地方，是项目的社区为了各方的利益而共同协商的地方。
  - upstream是相关贡献者协作工作的焦点。
  - upstream也是一个约定俗成的地方

   **上游优先** 

  - 树立良好的开源意识
  - 选择优先在上游工作
  - 上游不接受特性或修补程序，供应商可单独提供

   **从上游项目到下游产品** 

- 03-05 [拓林思出席openEuler 社区品牌宣传委员会第一次正式会议 共同确立2021上半年重点工作](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484036&idx=1&sn=1836159eddb2bfbb487197f7d8cc9d60)

- 02-12 [春节 | 所求皆如愿 相伴拓林思](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484026&idx=1&sn=6f0272f4ebb461884af89b888f76fd0f)

#### 2020 

- 12-25 [openEuler社区理事会正式成立，拓林思成为第一届理事会成员](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247484011&idx=1&sn=d38411d64e8522ceb988d73f3a3b705e)
- 12-17 [CentOS没了，Linux的新世界来了](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247483909&idx=1&sn=0039be3d11421f1692ffbd2838601d7d)
- 12-02 [拓林思邀您共赴openEuler Summit，解锁开源与操作系统的不解之缘](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247483692&idx=1&sn=6f4242504cc69db95e0df7bbfafc8e9f)
- 08-25 [openEuler 应用迁移大赛决赛开启，拓林思入选，决战东莞松山湖](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247483670&idx=1&sn=844aef7249f3dfdea5dbcac9252c31f8)
- 07-27 [Linux 内核系列 | Linux 内核发展史](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247483666&idx=1&sn=7060b10e61072595e83124505ecc0d71)
- 07-17 [openEuler 社区正式成立 Xfce SIG](http://mp.weixin.qq.com/s?__biz=MzI5MDE5NDc2MA==&mid=2247483657&idx=1&sn=d69932c6b639a118654253fad490ced3) 笔记：[转发](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6231943_link)

《[openEuler 社区正式成立 Xfce SIG](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6231943_link)》这是我未正式拜访丁老师前，见到的照片，还记得《南征北战》专题里收录的 sig-RISCV 组的 旭周老师，同样的 oE sig 组海报，这也是 oE 的画风啦。如同，我第一次进入开源启动 SIGer 一样，我记住了这个词 开源鲤鱼，并用太极图双鱼，作为 SIGer 创刊的封面。前不久，完成的一期主题：《开源治理的方向在哪里？》我给出的答案就是：北向文化。非常感谢 丁老师 的再次拥抱， ”石榴派“ 必将还社区一个惊喜！

之所以只保留了两篇 ”技术干货“，甚至没有保留 2021/6/10 oE 开发者大会的平行空间，也是为了凸显 拓林思 的开源精神和企业文化——分享之心。

---

### [罗宇哲：Linux 内核发展史 | Linux 内核系列](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6232189_link)

这是拓林思公号早期的一篇转发，通过它，按图所冀，迁出了 openEuler Website 的建设轨迹和版本发展。因为 罗宇哲的这个系列，曾经作为 oE 的 Blog 被引用，其全文详述了 入门 oE 所必须的基本概念，可以说是一套 oE 的 Gitee 生存攻略，非常有必要重温学习。本刊以学习笔记的叙述方式，有全文转发，也有摘要提纲，逐一介绍：

罗宇哲 TurboLinux 2020-07-27
[原文链接](https://gitee.com/liucw0716/blog/blob/master/content/blog/002%20%E7%AC%AC%E4%BA%8C%E6%9C%9F%20Linux%E5%86%85%E6%A0%B8%E5%8F%91%E5%B1%95%E5%8F%B2%EF%BC%881%EF%BC%89/%E7%AC%AC%E4%BA%8C%E6%9C%9F-Linux%E5%86%85%E6%A0%B8%E5%8F%91%E5%B1%95%E5%8F%B2%EF%BC%881%EF%BC%89.md)：http://suo.im/5R0kbN
作者：罗宇哲，中国科学院软件研究所智能软件研究中心


```
+++
title = "Sample Post"
date = "2020-03-03"
tags = ["Sample", "ABC", "cccc"]
archives = "2020-03"
author = "openEuler Blog Maintainer"
summary = "Just about everything you'll need to style in the theme: headings, paragraphs, blockquotes, tables, code blocks, and more."
+++
```


 _作者：罗宇哲，中国科学院软件研究所智能软件研究中心_ 

openEuler是基于Linux 内核的企业级Linux服务器操作系统平台的开源社区发行版。openEuler支持鲲鹏架构，可运行在TaiShan服务器上。本技术连载将会从理论基础、源码分析和实操方法三个方面来比较全面地介绍内核编程与应用编程的基础知识，到2020年8月之前主要介绍内核编程部分。通过本连载的介绍，您将对openEuler内核编程和应用编程的理论和实践知识有一个基本的了解。本小节将从Linux内核发展史出发，带您走进openEuler的世界，一起学习操作系统的基础知识和openEuler内核的技术细节。

- 一、Linux内核发展简史
- 二、openEuler 特性
- 三、巨人的肩膀
- 四、结语
  这是本刊开篇全文引用的部分，北向文化，才是我们的方向。切莫迷失！ :pray: 为表感谢，依然在此插播广告  :sunglasses: 

 **关于拓林思** 

> 拓林思于 1999 年 3 月发布了 TurboLinux 3.0.2，这是国内第一个 Linux 中文化版本，开创了 Linux 中文化先河。TurboLinux 是世界公认的 Linux 中文化方面的领导者和奠基者。

> 拓林思完成了众多企业级项目实施和服务，拥有丰富的成功范例和经验，客户遍布金融、电信、能源等各个行业。并且承担了中石化IC加油卡工程、 中国工商银行Linux项目、铁道部铁路行包营运管理系统、上海超级计算中心10万亿次高性能计算系统、国家质量监督检验检疫总局网络及三电工程等等项目。

> 2006年，万里开源（拓林思母公司）与MySQL AB共同成立MySQL中国研发中心，公司则逐渐转向数据库（GreatDB）与操作系统（Turbolinux）相结合的业务为主，致力于打造极致性能、极致稳定、极致易用的基础软件平台。

---

### openEuler Website 0.1.2.

原文链接：http://suo.im/5R0kbN
> 这就是学习线索，让本节能独立成文。且看我的学习笔记。

- [liucw0716 / blog / content / blog](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6232696_link)

  我要全文摘录，才能知道为什么它那么醒目：

  - 000 [openEuler开源社区开发流程](https://gitee.com/liucw0716/blog/blob/master/content/blog/000%20openEuler开源社区开发流程/openEuler开源社区开发流程.md)
  - 001 [第一期 自动搭建openEuler虚拟机运行QEMU运行环境](https://gitee.com/liucw0716/blog/blob/master/content/blog/001%20第一期%20自动搭建openEuler虚拟机运行QEMU运行环境/第一期-自动搭建openEuler虚拟机运行QEMU运行环境.md)
  - 002 [第二期 Linux内核发展史（1）](https://gitee.com/liucw0716/blog/blob/master/content/blog/002%20第二期%20Linux内核发展史（1）/第二期-Linux内核发展史（1）.md)
  - 003 第三期 Linux内核发展史（2）
  - 004 第四期 Linux内核发展史（3）
  - 005 第五期 Linux内核源码结构（1）
  - 006 第六期 Linux内核源码结构（2）
  - 007 第七期 Linux内核编程环境（1）
  - 008 第八期 Linux内核编程环境（2）
  - 009 第九期 Linux内核补丁源码分析（1）
  - 010 第十期 Linux内核补丁源码分析（2）
  - 011 第十一期 Linux内核模块依赖图绘制（1）
  - 012 第十二期 Linux内核模块依赖图绘制（2）
  - 013 第十三期 Linux内核的分布式编译（1）
  - 014 第十四期 Linux内核的分布式编译（2）
  - 015 第十五期 ARM体系结构基础（1）
  - 016 第十六期 ARM体系结构基础（2）
  - 017 第十七期 ARM体系结构基础（3）
  - 018 第十八期 ARM体系结构基础（4）
  - 019 第十九期 处理器存储模型概述（1）
  - 020 第二十期 处理器存储模型概述（2）
  - 021 第二十一期 ARMv8-A存储模型概述（1）
  - 022 第二十二期 ARMv8-A存储模型概述（2）
  - 023 第二十三期 ARMv8-A缓存一致性（1）
  - 024 第二十四期 ARMv8-A缓存一致性（2）
  - 025 第二十五期 openEuler汇编语言（1）
  - 026 第二十六期 openEuler汇编语言（2）
  - 027 第二十七期 ARM64体系的异常与中断（1）
  - 028 第二十八期 ARM64体系的异常与中断（2）
  - 029 第二十九期 Linux内核的异常（1）
  - 030 第三十期 Linux内核的异常（2）
  - 031 第三十一期 ARM Linux内核的中断（1）
  - 032 第三十二期 ARM Linux内核的中断（2）
  - 033 第三十三期 ARM Linux内核的中断（3）
  - 034 第三十四期 ARM Linux内核的中断（4）
  - 035 第三十五期 ARM Linux内核的中断（5）
  - 036 第三十六期 ARM Linux内核的中断（6）

- Forked:
  全部都是大咖，社区里有头面的人物都在里面啦。各种际遇此处掠过，只要混社区的，时间一定可以解决所有问题。

- [openEuler](https://gitee.com/openeuler) / [blog](https://gitee.com/openeuler/blog) 暂停
  > 该仓库已停用，相关博客迁移至website仓库（ https://gitee.com/openeuler/website ）， 烦请移步至website，给您带来不便敬请谅解。 Please use website repository ( https://gitee.com/openeuler/website ) to instead this one. Thank you.

-  **近期动态**
  依然是大咖云集，包括我收到的第一个拥抱，出自其中。

- https://gitee.com/openeuler/website Readme.md
  - Brief Introduction
  - Installation
  - Debug
  - Maintainers
  - Contribution
  - Get Help

- [website](https://gitee.com/openeuler/website) / content / zh / [blog](https://gitee.com/openeuler/website/tree/master/content/zh/blog)
  能发 BLOG 建立专栏的，一定都是社区的灵魂人物。

- openEuler / [website-v2](https://gitee.com/openeuler/website-v2)
  到这里，也就是最新版本的 WEB 有实站可查，就不需要额述啦。

- [https://openeuler.org/zh/interaction/blog-list/](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6234902_link)
  如何发博的指导，让我明白了，本节开篇，md 开头的一段字符样的格式文，是给 BLOG 提供内容索引的啊。

  - [开源社区实习记 - 我把 openEuler 移植到了 Windows WSL](https://openeuler.org/zh/blog/ouyanghaitao/2021-7-28-%E5%BC%80%E6%BA%90%E7%A4%BE%E5%8C%BA%E5%AE%9E%E4%B9%A0%E8%AE%B0%20-%20%E6%88%91%E6%8A%8A%20openEuler%20%E7%A7%BB%E6%A4%8D%E5%88%B0%E4%BA%86%20Windows%20WSL.html)

    > 中间还有个有趣的小插曲，在社区发邮件期间，民族棋 SIG 的 Maintainer，同时也是为中学生做树莓派科普的袁老师找到我，说看到了我热情洋溢的邮件，问能不能代表同学们采访一下 WSL 项目，说实话当时的我实在是受宠若惊但又内心狂喜。在采访中，首先介绍了openEuler移植到WSL的进展，以及一些Linux入门的知识，其次按照袁老师的建议分享了自己高考考上哈工大（深圳）的经验与心得，同时也分享了多姿多彩的大学生活（[采访链接](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%B8%80%E5%B0%81%E9%9D%A2%E6%95%85%E4%BA%8B%E9%98%B3%E5%85%89%E6%B5%B7%E6%B6%9B)）。

    用这段采访，结束本刊非常圆满，因为 ”石榴派“ oE 阳光版的需求，就是从 ”阳光海涛“ 这里发出的，他也是 ”石榴派“ 阳光版的 [Maintainer](https://gitee.com/shiliupi/shiliupi99/tree/master/Sunnies). 也是《南征北战》专题的主角，也是从那时起，石榴派发行版的愿被放大了。直到今天，请教丁老师 HOWTO。而这个际遇，则源自 一次 sig 组会的跳票，GNOME 醒目地在 TC 的 MAILLIST 里。我就来了，遇到了社区的每一个人。 :pray:  :pray:  :pray: 

---

- [封面头条](#%E5%BA%8F) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6237916_link)
- [以开源的精神和包容的文化](#%E4%BB%A5%E5%BC%80%E6%BA%90%E7%9A%84%E7%B2%BE%E7%A5%9E%E5%92%8C%E5%8C%85%E5%AE%B9%E7%9A%84%E6%96%87%E5%8C%96join-us)（Join us） 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6239839_link)
- [HowTo Running Linux 发行版](#howto-running-linux-%E5%8F%91%E8%A1%8C%E7%89%88) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6239909_link)
- [TurboLinux Enterprise Server 15](#turbolinux-enterprise-server-15) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6240618_link)
  - [学习笔记1：一个 Bing 全搞定](#%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B01%E4%B8%80%E4%B8%AA-bing-%E5%85%A8%E6%90%9E%E5%AE%9A)
  - [学习笔记2：最好的分享在公众号（干货满满）](#%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B02%E6%9C%80%E5%A5%BD%E7%9A%84%E5%88%86%E4%BA%AB%E5%9C%A8%E5%85%AC%E4%BC%97%E5%8F%B7%E5%B9%B2%E8%B4%A7%E6%BB%A1%E6%BB%A1)
- [罗宇哲：Linux 内核发展史 | Linux 内核系列](#%E7%BD%97%E5%AE%87%E5%93%B2linux-%E5%86%85%E6%A0%B8%E5%8F%91%E5%B1%95%E5%8F%B2--linux-%E5%86%85%E6%A0%B8%E7%B3%BB%E5%88%97) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6241172_link)
- [openEuler Website 0.1.2.](#openeuler-website-012) 笔记：[issue](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6241389_link)

[目录](https://gitee.com/RV4Kids/RVWeekly/issues/I459QA#note_6241448_link)：