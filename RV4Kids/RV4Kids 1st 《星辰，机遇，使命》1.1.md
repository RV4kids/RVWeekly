RV4Kids 1st 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link)》 目录：

- [1.1](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link) [Architecture, Algorithm, All ... based on FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md#architecture-algorithm-all--based-on-fpga)

- [1.2](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5318914_link) [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#%E5%85%A8%E6%A0%88%E5%AD%A6%E4%B9%A0%E5%85%AC%E7%A4%BE)

  - [RV一起学](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#rv%E4%B8%80%E8%B5%B7%E5%AD%A6)
  - [How to start 我们的研学之旅](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#how-to-start-%E6%88%91%E4%BB%AC%E7%9A%84%E7%A0%94%E5%AD%A6%E4%B9%8B%E6%97%85-) ?
  - [Shiliu Pi 99](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#shiliu-pi-99) 石榴派

- [1.3](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5330927_link) [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E4%BA%A6%E5%B8%88%E4%BA%A6%E5%8F%8B%E4%BA%A6%E5%90%8C%E5%AD%A6%E6%88%91%E6%98%AF%E4%BD%A0%E4%BB%AC%E7%9A%84%E5%8A%A9%E6%95%99%E6%9B%B4%E6%98%AF%E8%AF%BE%E4%BB%A3%E8%A1%A8)。

  - [两份学习笔记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E5%88%86%E4%BA%AB%E4%B8%A4%E4%BB%BD%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0%E7%BB%99%E5%90%8C%E5%AD%A6%E4%BB%AC)： FSF 的 GNU chess、FPGA 的高性能计算
  - [学习资料](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E6%9C%80%E5%90%8E%E4%B8%8A%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99)：GNU Chess - 1st RISCV shiliupi is building...

- [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md)

  - [课程安排，四大块](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%AE%89%E6%8E%92%E5%9B%9B%E5%A4%A7%E5%9D%97-)
  - [课程参考书：《基于FPGA与RISC-V的嵌入式系统设计》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%8F%82%E8%80%83%E4%B9%A6%E5%9F%BA%E4%BA%8Efpga%E4%B8%8Erisc-v%E7%9A%84%E5%B5%8C%E5%85%A5%E5%BC%8F%E7%B3%BB%E7%BB%9F%E8%AE%BE%E8%AE%A1)
  - [在线学习慕课：《计算机组成与设计：RISC-V》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E5%9C%A8%E7%BA%BF%E5%AD%A6%E4%B9%A0%E6%85%95%E8%AF%BE%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BB%84%E6%88%90%E4%B8%8E%E8%AE%BE%E8%AE%A1risc-v)
  - [RVWeekly 联合 SIGer 青少年开源期刊共推](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#rvweekly-%E8%81%94%E5%90%88-siger-%E9%9D%92%E5%B0%91%E5%B9%B4%E5%BC%80%E6%BA%90%E6%9C%9F%E5%88%8A%E5%85%B1%E6%8E%A8)

---

@[lingjunk](https://gitee.com/lj-k)
> 在刘慈欣的小说里，只要每个士兵手拿黑白两个小旗，那么大秦国的百万军队便能构成一个宏大的人基计算机。现在，我们正有机会指挥数十亿甚至数百亿的碳基晶体管，使其听从我们的指令，完成我们的动作，实现我们的宏大目标。  
我们将借助FPGA向大家介绍RISC-V，目前最伟大的开源处理器项目，全世界最富有智慧的芯片专家的伟大结晶；也会通过FPGA学习数字系统的设计，这一信息化时代的基石，也是人类开展第四次工业革命的起点。  
今后的一段时间，我们将会设计自己的处理器，完成一系列数字时代的挑战，将人类最复杂和精妙的算法和架构部署其上，希望我们可以打好基础，参与到中国乃至世界的科技浪潮之中，做时代弄潮儿，技术最先锋，去定义我们自己的工业4.0。

感谢[孔令军](https://gitee.com/lj-k)老师寄语：《星辰，机遇，使命》全文299字，是 RV4kids 下周 2 正式出刊的最有力保证。我相信同学们一定可以一起见证这个时刻的到来 “身负强国使命，与一众小伙伴一起...” 这是 RV4kids 诞生第一期 RVWeekly 专题的 SIGer 的卷首语。再一次，心潮澎湃，让我们共同期待 2021.6.8 到来吧！（ 让我们一起溜[666]吧 :muscle: ）

# [Architecture, Algorithm, All ... based on FPGA](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5299948_link)

- [CPU、GPU、FPGA、ASIC](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEZ)  
人工智能芯片的特性与比较  
[AI on FPGAs: Past, Present, and Future](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEV)

- [决战高性能计算，350亿美元收购FPGA](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ8R)  
[FPGA processors keep Mars Rovers moving](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ77)

- [懂得分享的人，往往能收获更多](https://gitee.com/RV4Kids/RVWeekly/issues/I3RNRN)  
[Full-stack Hacker and Company](https://gitee.com/RV4Kids/gd32vf103-vga-ps2keyboard/issues/I3MBIR#note_5316267_link)...

![输入图片说明](https://images.gitee.com/uploads/images/2021/0605/131157_784b56bd_5631341.jpeg "RV4KIDSiv99dpiA4.jpg")

这是成刊前的封面预览，记录下的标题头条。它传递的是一份信息：

> FPGA 是高性能计算的未来，[芯片巨头布局 FPGA 的大手笔](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ8R)，成为本期的题眼，RV 是未来，是因为我们有了一个赋能的工具，哪就是 [FPGA，当人类迈向星辰大海时](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ77)，一切的未知和挑战，都需要我们以不变应万变，而不变的是我们用于承担挑战，解决问题的决心，而万变的问题，需要我们万变的方案，利器在手，无往不利，它就是 FPGA 的保驾护航。

- AMD以350亿美元收购FPGA龙头企业赛灵思 （Xilinx）的提案获得两家公司股东一致通过，交易预计2021年底完成。
- 51岁的苏姿丰，AMD现任CEO，被《财富》杂志评为“2020年全球最有权势的女人”。
- “野心勃勃”：吞下FPGA （现场可编程门阵列）龙头Xilinx，是基于对未来五年风口的预判，这将帮助AMD夺取自动驾驶、人工智能、5G等高性能计算领域的话语权，掘金多个千亿美金赛道。
- Xilinx已是NASA火星探测任务的老搭档，承载着成像、遥感、通讯、图形处理等核心功能。
- 今年2月18日，NASA“毅力号”成功登陆火星，便搭载了它四个型号的FPGA芯片。
- 五年前，当时的剧本是风头正盛的Xilinx要收购低迷的AMD，以对抗英特尔+FPGA老二Altera的组合。

> AMD的未来聚焦于三大持续增长的高性能计算领域：游戏、大数据中心和沉浸式平台。

  1. 基于ZEN架构的旗舰产品Ryzen（锐龙）芯片项目
  2. AMD的服务器芯片EPYC（霄龙）也成功上市

以上的每条信息都足够“头条”的标题，这或许就是本刊能顺利出品的原因，一不小心再次踏上了时代的潮头。这次我更加清晰地明白“科普”是我们的使命，因为只有让青少年参与到时代浪潮的见证中，才能使他们在未来的任何一次时代浪潮中，抓住机会。就和本期封面的焦点 “苏姿丰” 女士一样。相信接下来的日子，关于她的过往定会有更多传记诞生，这里略过。封面文章《[决战高性能计算，350亿美元收购FPGA](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ8R)》的后半段有了一些描述。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/155701_bac56ebc_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ77)

相比 “毅力号” 自拍这样的新闻，上面的 FPGA 位置图更符合我们的科普风格，我们能够了解到 FPGA 是怎么保驾护航火星车的，来自她的雷达系统，也就是眼睛，依靠强大的运算能力，让火星车能够洞察未知环境的每一丝变化，在人类无法帮助它时，所有困难只能独自解决。“四个系列” 的 FPGA 产品应该成为，接下来我们要学习的内容之一。就和 AMD 选择的三大领域，推出的两个系列芯片一样。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/050924_cf3b449f_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEV)

这是出刊本期 RV4KIDS 的内部封面的选图，希望用它来突出 FPGA 的功能定位，万能连接器，SOC体系的中心，替代CPU的角色。这图原自一篇 某度 转型 AI 后的战略分析，里面说明了很多 [人工智能芯片观察：FPGA的过去，现在和未来](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEV) 。其中不能不说，2016 年 FPGA 二哥投靠 INTEL 的风云突变。这也出现在了封面故事 AMD 收购 Xilinx 的桥段。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0523/055240_f9422412_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEZ)

真正全面阐述各个人工智能芯片的地位的文章应该是《[CPU、GPU、FPGA、ASIC 人工智能芯片的特性与比较](https://gitee.com/RV4Kids/RVWeekly/issues/I3SIEZ)》因为文章中未展开讨论的 “类脑芯片” 在2019年，一个科学时报的科普活动，有过一个报道，是关于 北大的类脑课题，当时一票小记者，来到展台前，观看着长相没有什么分别的类脑芯片，“十万个为什么？” 的尽头，让站台的研究生大哥哥来了精神，从头讲起。这其中说到了 高制程 和算法方向，弯道超车的 “愿景”。反观 CPU + FPGA 两个传统阵营的交锋，以及 ARM 超越 X86 再移动低耗市场的风光。同学们，我们的差距还相当的大啊。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/085210_1f443114_5631341.png "在这里输入图片标题")](https://gitee.com/RV4Kids/gd32vf103-vga-ps2keyboard/issues/I3MBIR#note_5316267_link)

- [工程师的五个等级 图片来源：《浪潮之巅》](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ8R)  
  参考4.《浪潮之巅》吴军 电子工业出版社

这张引自《浪潮之巅》的人才层次的图片，同样出现在了封面文章中  
[Full-stack Hacker and Company](https://gitee.com/RV4Kids/gd32vf103-vga-ps2keyboard/issues/I3MBIR#note_5316267_link) 对全栈工程师的介绍，出处略有不同，同原自 吴军先生，这次是 54th《硅谷来信》。它都说明了全栈的意义，不像程序员的酸涩，正文更说明了人才的梯度建设的重要。尽管 封面文章的引用，是说明 CEO 们的高瞻远瞩。但就人才培养的原则 “不想当将军的士兵，不是好士兵”，人才培养的要求，就是我们科普的要求。这为本期引出 《全栈学习公社》做了很好的伏笔。

《[懂得分享的人，往往能收获更多](https://gitee.com/RV4Kids/RVWeekly/issues/I3RNRN)》的文不对题，是当时有感于 RV 教育前辈的一份宣传资料。师者自当 “知无不言，言不不尽”，它也是《全栈学习公社》的学习态度，分享本身更是社区建设的根本。

---

下一篇：1.2 [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md)