![输入图片说明](https://images.gitee.com/uploads/images/2021/0821/012755_1d5f6c01_5631341.jpeg "RV-Question-Codasip2-999.jpg")

# 《要选，就选 Codasip !》

写完CODASIP的三驾马车在封面，这期学习笔记就可以收笔啦。《要选，就选 Codasip !》成刊喽，祝老师同学们周末愉快！按照惯例，欣赏着刚出炉的封面，总要发一通感慨的，这不《要学，就学 Codasip !》的副标题也出来了。当真地干货洗脑，这些有过之无不及的溢美之词，怎么看，怎么像软广，只有认真地看进去，才能有所收获。这就是为什么这两期，开始以专业公司的 “宣传” 材料作为学习内容的原因。这也是 RISC-V 行业的深度技术市场的特点，真的不是白菜式地吆喝能带来效果的。

封面头条：《RISC-V即未来？这是一个值得深思的问题！》这样一段话深深地印在了我的脑海中...

> 在过去十年中，我经常听到工程师们谈论 "Arm疲劳症"，以及对关键市场的垄断地位和供应商锁定的不安情绪。然而，只要Arm能以其广泛的产品系列宣称所谓的 "瑞士中立"，就不会有人因为Arm授权模式而被解雇。随着ARM被软银收购，这种中立性被严重削弱，而可能的英伟达合并则意味着：Arm挑选了自己的前客户进行战略合作！最终没人会认为ARM一个安全的选择。

这要比通常的标题党的三趋势更打动我，这也是为什么被掐脖子后的大中华成为了 RISC-V 理事会的半壁江山的原因，真的怕了。都说未来的趋势在生态，抱团取暖将成常态。那么坐拥独有专利的 Codasip 在生态（江湖）的位置是怎样的呢？我第一次见 TINA （中国区总经理）时，她已经耐心给我这个小白科普过了，一家提供 CPU 核 IP 和高级语言开发工具的技术公司，并不能提供 SOC 的全部能力，当时我这个心气高涨要做 “全栈” 的 “石榴派” 小老师，多少有些失落。但今天再看 CODASIP，我们的合作又进了一步。SIGer ，就是这期期刊，回报了第一位捧场 “石榴派” 的 CODASIP。生态，不只是直接的上下游，互相帮助，同样也是生态的一部分。“Codasip 是个有个性的公司！”，这是通篇阅读了 公号，网站，并回顾了 RV 峰会后的结论。

### 封面头条：

1. Is RISC-V the Future
   - 趋势1：RISC-V在性能上的转变！
   - 趋势2：处理器类型之间的壁垒正被逐渐打破！
   - 趋势3：客户迫切希望避免垄断性的供应商！

2. 中英文字幕版-Codasip视频系列之一第四集：支持操作系统

   从本集视频中应该记住的最重要的事情是什么呢？

   - 操作系统带来了许多有用的功能，以及使用操作系统可以降低代码的复杂性, 但您必须自己完全实现！
   - 目前有很多操作系统是为不同的个例设计的，因此在现有的操作系统中没有通用的选择。
   - 操作系统同时也有自己的硬件要求，所以您不应该使用比自身需求更复杂的解决方案，这样您就不会付出太大的硬件代价！  

   本集精读：中英文字幕（全文）
   What Is Needed to Support an OS?
   - Overview: 
     - codasip STUDIO 
     - codasip RISC-V PROCESSORS
     - codasip SweRV CORE & SUPPORT PACKAGE
   - What Is an Operating System ?
   - Which Systems Are Available ?
     - Bare-metal(no OS)
     - Real-time OS
     - Rich OS
   - What Do The Systems Provide ?
   - Use Cases
   - What Do The Systems Require ?
   - Codasip Processor Offering:
   - The Key Takeaways

3. [What is the difference between processor configuration and customization](https://codasip.com/2021/08/12/processor-configuration-vs-customization/)  
   Published on August 12, 2021 Blogposts | Image by Igor Ovsyannykov from Pixabay.

   - [RISC-V ISA](https://riscv.org/about/)
   - [Codasip’s RISC-V cores](https://codasip.com/products/codasip-risc-v-processors/) 
   - [CodAL](https://codasip.com/products/codasip-studio/technology/#codal)
   - [Codasip Studio](https://codasip.com/products/codasip-studio/)
   - [Codasip’s white paper](https://news.codasip.com/whitepaper-dsp-custom-instructions)

   ![输入图片说明](https://images.gitee.com/uploads/images/2021/0821/105132_6d141691_5631341.png "屏幕截图.png")

   每一篇 Codasip 博客的文章，都会在公号中复制并翻译，原文是自动聚会的，可以带来更好的阅读体验，并且更丰富的引用连接，能够直达核心，这篇最新的推介，就无处不在地展示了 Codasip 三架马车！集成开发环境，IP核，CODAL描述语言。而 Codasip 的性格也是通过这一篇一篇的博客，与不断反复强调的核心竞争力，体现出来的。

   《[何为处理器配置和定制？它们之间又有什么区别？](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484410&idx=1&sn=7c13a43698f8bb89b3319888bc247bb9)》

   ![输入图片说明](https://images.gitee.com/uploads/images/2021/0821/111438_563aec62_5631341.png "屏幕截图.png")

   显然这个 KEY 中国公司也 Get 到了，要不怎么会浓缩成一张 POST 在文末。本着开源精神，这个 PR 内容就是，应该将 Codasip 的三家马车无处不在地显示出来，而口号从排比，变成递进：“要选，就选 Coda !” 尽管我还没有功课 Codasip 的名字由来，而中国人习惯给老外起名的毛病，在我这里也不能免俗  :sunglasses: 

4. [Domain-Specific Accelerators](https://codasip.com/2021/05/21/domain-specific-accelerators)  
   《[何为特定领域加速器？](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484344&idx=1&sn=ecdf8680fd5581f5cd93243545f6be15)》- 又被科普啦 @yuandj 2021-07-21 13:57

   - 革新的设计不可避免！随着旧的确定的硅几何尺寸的扩展永远消失，该行业已经在发生变化。如上图所示，内核的数量一直在增加，复杂的SoC，如手机处理器，将在不同的子系统中结合应用处理器、GPU、DSP和微控制器等。
   - 常用的格式比如被广泛使用的IEEE 754，在特定领域加速器中可能是多余的。
   - 面对众多不同的特定领域加速器（DSA），最大的挑战将是如何有效和经济地定义它们。

5. [Codasip Announces A71X RISC-V Application Core with Dual-Issue Capability](https://codasip.com/2021/06/22/codasip-announces-a71x-risc-v-application-core-with-dual-issue-capability/)

   [首届RISC-V中国峰会，Codasip®宣布即将发布具备双发射功能的A71X RISC-V应用核心](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484258&idx=1&sn=c63f159ba0bb7193bc8a5bc4e9b0d25f)

   - [德国SEGGER和Codasip在首届RISC-V中国峰会期间共同宣布围绕RISC-V方面深度合作](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484258&idx=2&sn=1c3185de3b2bf50d0d82c77bbc308702)
   - [首届RISC-V中国峰会今日开幕，Codasip作为赞助商出席并亮相新闻发布会！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484258&idx=3&sn=5e037f819ce7d5e283fda408fd251272)
   - [中国RISC-V产业联盟会员之夜高清图片纯享版，作为会员之夜赞助人的Codasip完美结束峰会主会日活动！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484258&idx=4&sn=619e168939f685f773aab93f8562b5ba)

   共同见证，石榴派同台展示期间，为 Codasip 的 TINA 老师全程录像，[见证了 A71X 双发射的发布](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6298735_link)。

   1. 双发射 RISC-V A71X 应用处理器 发布会
   2. 我们公司 CODASIP 作为 RISC-V CPU 和 CPU 设计的自动化工具的领先的提供商。
   3. 关于我们今天新发布的核 A71X
   4. 那么 A71X 因为是双发射，所以一次发射可以执行两条指令。
   5. 那么，关于另一个发布呢，就是我们 CODASIP 和德国 SEGGER 公司的合作
   6. 因为时间关系，我这边的发布就到这里，
      (  _相海英 --- Codasip 中国区总经理_  )

6. [Mythic Licenses Codasip’s L30 RISC-V Core for Next-Generation AI Processor](https://codasip.com/2021/08/10/mythic-licenses-codasip-risc-v-core/)

   德国慕尼黑 - 2021年8月10日 - 可定制RISC-V®嵌入式处理器IP的领先供应商Codasip今天宣布，美国著名人工智能处理器公司Mythic选择Codasip的L30（原Bk3）RISC-V核心，为其下一代模拟矩阵处理器（Mythic AMP™）保驾护航。

   - [Mythic授权使用Codasip L30 RISC-V核心，为其下一代AI处理器产品保驾护航！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=1&sn=a3c6e3b966651f01595433d682b7abd8)

     - [中英文字幕版-Codasip视频系列之一第一集：产品组合！](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1996284811367202817&__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=2&sn=f2bd84fc945c2a55c70276726cc5bb42)
     - [中英文字幕版-Codasip视频系列之一第二集：处理器性能！](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1996262319797878787&__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=3&sn=267e0228dcf56481fb1ad16c11d90fc6)
     - [中英文字幕版-Codasip视频系列之一第三集：处理器复杂性](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1996272242078302209&__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=4&sn=b2cd926df404f607700b78bb913d7e5b)
  
### 学习笔记：

- [What is an ASIP?](https://codasip.com/2021/04/16/what-is-an-asip/)
  [Codasip博客系列之：什么是ASIP？](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484029&idx=1&sn=6e54e520d3aca34273beb03f5f418a09) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6337201_link)】
  
  ASIP是 "application-specific instruction-setprocessor（专用指令集处理器） "的缩写。简单地说，它是为某一特定应用或领域进行优化设计的处理器。[History](https://codasip.com/company/#history) -  **2004** : [Karel Masařík](https://codasip.com/company/management#karel) starts working on his PhD thesis covering a unique hardware/software  **co-d** esign technology that will become the [core of Cod **asip** ](https://codasip.com/products/codasip-studio). 写到这里，Codasip 的名字就出来了，Cod - asip 的意思，协同设计-专用指令集处理器 的意思了。 :sunglasses: 这就是学习的过程，在 REVIEW 这个部分的学习笔记后，串起来的结果，这要请 TINA 老师求证下啦。 :sweat_smile: 原本截稿内，最后扫尾了一篇学习笔记（[其他同学的译文](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6341000_link)）标题的学术味道更符合 Cod 的解释：[ **Co** de- **D** ensity-Improvements-Beyond-The-C-Standard-Extension.pdf](https://content.riscv.org/wp-content/uploads/2019/12/12.10-17.10c-Code-Density-Improvements-Beyond-The-C-Standard-Extension.pdf) Codasip 全称：**Co** de- **D** ensity  **a** pplication- **s** pecific  **i** nstruction-set **p** rocessor 

  - [Codasip视频系列之：Codasip应用核心系列简介！](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_1854457217463140355&__biz=MzIyNjQyMTg3MA==&mid=2247484029&idx=2&sn=4931735ff7f49af92edce5be3cbc81cd)
  - [Codasip - May the 4th be with you! 以梦为马，不负韶华！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484029&idx=3&sn=6731060ca898c0cd13b5455e650737fc)

  [POWERFUL EDA TOOLSET](https://codasip.com/products/codasip-studio/): Do you need to tweak an existing processor design to better fit your needs? Optimize it to get better PPA? Add domain-specific features? Or create a brand new processor core? Meet Codasip Studio, a  **highly automated**  EDA toolset that covers every aspect of the design process and makes the path to your custom core supremely  **smooth** ,  **fast** , and  **risk-free** . We know it because we use it ourselves to create the [Codasip processor IP](https://codasip.com/products/codasip-risc-v-processors/)!

  > “Codasip gave us the flexibility to create a truly unique RISC-V processor, specific to our needs. This saved us the effort to build our own processor from scratch and allowed us to focus on other critical areas of the product development.”
    - Ty Garibay, VP of Hardware Engineering, [Mythic](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=1&sn=a3c6e3b966651f01595433d682b7abd8) （  _8/10 刚发的合作信息_  ）

  [CODASIP STUDIO工具集](https://codasip.com/products/codasip-studio/?lang=zh-hans)：您是否需要调整现有的处理器设计以更好地满足您的需求？优化它以获得更好的PPA？增加特定领域的功能？还是寻求创建一个全新的处理器核心？来认识Codasip Studio工具产品吧！这是一个高度自动化的EDA工具集，它涵盖了设计过程的各个方面，并使通往自定义核心的路径极为流畅，快速且风险自由。Codasip内部使用此工具集用以创建Codasip处理器IP，因此我们拥有绝对的发言权！

  > “ Codasip使我们能够灵活地创建真正独特的RISC-V处理器，以满足我们的需求。这大大节省了初始构建理想处理器的工作时间，并使我们能够专注于产品开发的其他关键领域。”
    - [Mythic](https://codasip.com/2021/08/10/mythic-licenses-codasip-risc-v-core/) 公司 硬件工程副总裁 Ty Garibay

- [要选，就选 Codasip !](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R)

  学习只旅从这个愿开始，前文已经多次表达了对 TINA 老师的感谢，这里不再额述，下面以该 Issue 的结构叙述我的学习之旅，这也是 SIGer 编委 通常的学习风，也是袁老师以身示范开源学习的方式。 **峰会**  与  **公号** 是我峰会之后对 Codasip 进一步了解的唯一渠道。封面头条，多半来自这里面标注为重点的公号文章，深深地吸引了我的。

  1. [RISC-V World Conference China 2021](https://www.segger.com/events/risc-v-world-conference-china-2021) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6298243_link)】

     笔记，就是 Issue 下的回帖，是笔记的主体内容，或转载，或点评，是必须认真阅读过的才能收录。这篇是峰会当天就搜索学习过的。当然是首贴！

     - [CODASIP 科达希普 全新新闻发布会](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6298735_link)
       是 RVCN poster 演讲后为 Tina 老师助阵，全称录制的发布会内容。

  2. [公众号 - Codasip 科达希普](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6298746_link) 
     重点内容源，前面有介绍，看摘要就知道这功课有多细致：

     49篇原创内容：
     - 消息 35：所有公号的 URL 均可以点击，方便查看细节，为本刊成文打下基础
     - 视频 12：画龙点睛的视频干货。很多都进行了精读（后面笔记中会有条目）
     - 服务：[解决方案](https://codasip.com/solutions/?lang=zh-hans) | [产品资讯](https://codasip.com/products/?lang=zh-hans) | [关于我们](https://codasip.com/company/?lang=zh-hans#about)

     （公号的结构详读，才引出后面 Codasip 全站详读的学习。）  
     文章分类：#Codasip 处理器 22 | #Codasip/RISC-V 25 | #Codasip Studio 工具集 17 | #特定于域处理器 3 （之做了记录，并未逐一比对，因为从49条公号标题，以及分组发布归类，公号运营还是相当周到，这都是引自全球站的内容，底子好，没办法 :pray: ）

  3. 官网请访问：[www.codasip.com](https://codasip.com/?lang=zh-hans)

     Menu | P1 | P2 | P3 | P4 | P5 | P6 | bottom   
     连页底的隐私信息的连接都加了 URL 和复刻 Codasip 全站无异，这可是比精读更深入的学习啦。

     > 作为全球首批商业RISC-V处理器IP供应商之一和RISC-V 国际基金会的创始成员，Codasip是行业领先的处理器定制专家，我们提供先进技术来帮助您准确地获得所需核心。 Codasip成立于2014年，总部位于欧洲，现已发展成为处理器设计领域的全球领先企业。

     如果是第一次看全站，并不能太深刻地理解这样安排的用意，甚至上面这段话，不如公号自述更精准，如果算 PR 的话，只有精读，详读的老友，才能提出这样的 建议。（谁叫袁老师答应了 TINA 老师多提意见呢。）

  4. [IS RISC-V THE FUTURE?](https://codasip.com/2021/07/27/is-risc-v-the-future/?lang=zh-hans) | [公号](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484366&idx=1&sn=c0f052057718ccae66c9a233ebd3b1c6) | 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6298885_link)】

     ![输入图片说明](https://images.gitee.com/uploads/images/2021/0819/051212_bc9f3052_5631341.png "在这里输入图片标题")

     我要用 HPC 作为这节笔记的介绍，因为，封面已经收录了下面这段金句。而 HPC 作为新兴领域，RISCV 无疑是可以打破壁垒以 ASIP 领域的作为促进 HPC 的发展的，未来各个领域的疆界均会被打破，如今难能的 全栈 能力必然会成为 未来 之能。（这是我的点评，是一份对先行者的致敬 :pray: ）

     > 在过去十年中，我经常听到工程师们谈论 "Arm疲劳症"，以及对关键市场的垄断地位和供应商锁定的不安情绪。然而，只要Arm能以其广泛的产品系列宣称所谓的 "瑞士中立"，就不会有人因为Arm授权模式而被解雇。随着ARM被软银收购，这种中立性被严重削弱，而可能的英伟达合并则意味着：Arm挑选了自己的前客户进行战略合作！最终没人会认为ARM一个安全的选择。

     - [Is RISC-V the future? Codasip发表署名文章，引起媒体广泛热议！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484366&idx=2&sn=6dbeba220999a5a424b2e14ded0e33f1)

       甚至将 Codasip 自己的推荐文都一并收录，归类啦，相当用心啦（必须自夸 :sunglasses: ）

  5. [选择我们，选择Codasip! Codasip官方视频简介新鲜出炉，欢迎围观！](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484348&idx=1&sn=bed1cb0385009d4845490f77a14ca44b) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6302941_link)】

     这是本刊的题眼，灵感之源，我突然发现，Codasip 的编辑，好喜欢高楼的顶楼之角（是希望我们仰视吗？）其中的内容就不重述，至少反复强调的 “三驾马车” 已经深入我心（从这篇开始），下面的公号配文更精准啦。

      **关于Codasip** 

     > Codasip提供领先的RISC-V处理器IP和高级处理器设计工具，为IC设计者提供RISC-V开放ISA的所有优势，以及定制处理器IP的独特能力。作为RISC-V国际基金会的创始成员和基于LLVM和GNU计划的处理器解决方案长期供应商，Codasip致力于为嵌入式和应用处理器提供开放标准。Codasip成立于2014年，总部位于德国慕尼黑，目前在欧洲设有研发中心，销售代表遍布全球。有关我们产品和服务请访问官网中文版：www.codasip.com 关于RISC-V的更多信息，请访问：www.riscv.org

       - 【笔记】[选择我们！选择 Codasip !](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6311337_link)  
         将中英文分别归类成了三段话，更直观投射出文章表达的意义：设计之重，赋能，选择我们（自动化）

  6. [选择处理器IP内核时应考虑的实际问题系列之一：简介！](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=1&sn=47b0cf6a1a79cc191d1cfe044f7d9134) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6311861_link)】

        **本系列共包含7个部分，妥妥的技术干货，我们在Codsip本周官微合辑中重新发布，以供参考！** 

       1. [简介！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=1&sn=47b0cf6a1a79cc191d1cfe044f7d9134)
       2. [指令集架构（ISA）](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=2&sn=d8c2820bac1c2e7d3fa41afaafc29170)
       3. [性能！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=3&sn=e42aceece9ec714313bfac55a0653b2e)
       4. [复杂性！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=4&sn=ed32cf83a0fc007d3d00a6141255dd9d)
       5. [操作系统！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=5&sn=80899da811e6fb24a490403e80bac20d)
       6. [子系统硅面积！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=6&sn=0801d9dc907c5345e9c6dd5b946a1387)
       7. [许可模式！](http://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=7&sn=1d87c1e5cbda9f483b6443c3493431d1)

       - [选择处理器IP内核时应考虑的实际问题之五：操作系统！](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484332&idx=5&sn=80899da811e6fb24a490403e80bac20d) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6313761_link)】

         全文转载 “[操作系统](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6313761_link)” 是为了和后面，[视频系列](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6311911_link)的 “[操作系统](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6320107_link)” 比较。

  7. [Codasip 视频](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=2&sn=f2bd84fc945c2a55c70276726cc5bb42): 处理器设计基础 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6311911_link)】

     刚更新到第4期，是为了后面发布 BK7 做铺垫的，自然和 前面的 “[操作系统](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6313761_link)” 不同。 

     - [So what are the most important things to remember from this video blog?](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6320246_link)  
       那么，从本集视频中应该记住的最重要的事情是什么呢？

     - 第四集：[支持操作系统](https://mp.weixin.qq.com/s?t=pages/video_detail_new&scene=1&vid=wxv_2007717005646151680&__biz=MzIyNjQyMTg3MA==&mid=2247484410&idx=2&sn=7d3bdd73e6a1e1e42771025b5441a00e) 2021-08-18 09:52 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6320107_link)】  
       这是头条全文，封面截图的主要内容，干货分享！ :pray: 我们从主持人口中记住了 “菲利普”

       ![输入图片说明](https://images.gitee.com/uploads/images/2021/0821/235207_cde152f6_5631341.png "屏幕截图.png")

       PR: 中文字幕受限于空间，很多页都被截掉了末尾几个关键字，多少有些厚此薄彼啦  :pray: 

  8. [MYTHIC LICENSES CODASIP’S L30 RISC-V CORE FOR NEXT-GENERATION AI PROCESSOR](https://codasip.com/2021/08/10/mythic-licenses-codasip-risc-v-core/?lang=zh-hans) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6312219_link)】

     最新合作信息，包括其 CTO 的重要评语，也被引用，在后面的调研中发现的。（[POWERFUL EDA TOOLSET](https://codasip.com/products/codasip-studio/) & [CODASIP STUDIO工具集](https://codasip.com/products/codasip-studio/?lang=zh-hans)）, 写道这里，我突然发现了中英文措辞的不同，为啥中文版去掉了 EDA 工具，而用了 STUDIO 呢？那 Cod-asip 的名称是不是也要用一个中文 LOCAL 替代呢？如 Codasip 的 CEO [马克仁](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6337682_link)先生一样  :sunglasses: 。当我看到他时，是否应该称呼他 马克 呢？还是 老马 ？(摘自：[管理团队](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6337682_link))

     - [Mythic授权使用Codasip L30 RISC-V核心，为其下一代AI处理器产品保驾护航！](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484390&idx=1&sn=a3c6e3b966651f01595433d682b7abd8) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6337630_link)】  
       (公号转载)

  9. [Codasip正式发布用于RISC-V处理器核心的FPGA评估平台](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484014&idx=1&sn=6889808a833486a3f64982d0ff3c00d1) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6315611_link)】

     2021-04-28, 也是我拿到 “石榴派” 的日子，也因为 石榴派是一个 FPGA 开发板，为我打开了 全栈学习之门，也因此见到了 TINA ，才有了今年的深度学习。这就是奇妙的愿力，也是缘。 :pray: 下面是两条相关。

     - [Menta eFPGA and Codasip Announce Technology Partnership](https://codasip.com/2020/12/07/menta-efpga-and-codasip-announce-technology-partnership/) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6315794_link)】
     - [Codasip Announces FPGA Evaluation Platforms for RISC-V Processor Cores](https://codasip.com/2021/04/21/codasip-announces-fpga-evaluation-platforms-for-risc-v-processor-cores/) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6315760_link)】

  10. 《要选，就选 Codasip !》[封面](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6334590_link) 就是本 Issue 啦  :pray: 

      - [Codasip博客系列之：什么是ASIP？](https://mp.weixin.qq.com/s?__biz=MzIyNjQyMTg3MA==&mid=2247484029&idx=1&sn=6e54e520d3aca34273beb03f5f418a09) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6337201_link)】
        转载于此，是记录下，猜测 Codasip 名字的 学习过程！  :sunglasses: 

        - MASAŘÍK Karel, UML in design of ASIP, IFACProceedings Volumes 39(17):209-214, September 2006.
          参考文献：调研如下
          - [DETAIL PROJEKTU](https://www.vut.cz/vav/projekty/detail/14729) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6339467_link)】
            - MASAŘÍK, K.; HRUŠKA, T. UML IN DESIGN OF ASIP.
            - VUT > 在 BUT 的生活 > 布尔诺
            - 布尔诺技术大学

  11. [MANAGEMENT](https://codasip.com/company/management/) 管理团队 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6337682_link)】

      在好奇心的驱使下，有进行了一波学习，整改是 每周6 的 SIGer 学习小组 学习时间和公开课，15:00-16:00 然后是 2小时 自由讨论时间。将 “深创投 Codasip” “Codasip 中国公司” 作为搜索目标，笔记如下：

      - [Valtrix和Codasip合作验证系统](https://www.csdn.net/article/2021-03-29/2827659) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6338341_link)】
      - [深创投参加中国（深圳）-德国（慕尼黑）经贸与技术合作交流会并与德国合作项目签约](http://www.szvc.com.cn/main/a/20190624/2787.shtml) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6338557_link)】
      - [Codasip相海英：用顶尖CPU IP技术和服务打开中国市场，RISC-V正在改变嵌入式处理世界](https://www.shangyexinzhi.com/article/236256.html) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6339807_link)】
      - [Codasip推出Bk7 64位 RISC-V内核](http://news.eeworld.com.cn/manufacture/ic504091.html) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6340116_link)】
      - [国产EDA扎堆、异构集成刷屏，世界半导体大会干货合集！](https://new.qq.com/omn/20210616/20210616A0B1SJ00.html) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6340210_link)】
      - [“C”扩展（压缩指令扩展）之外的代码体积优化 --- （Codasip GmbH）](https://zhuanlan.zhihu.com/p/158098744) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6341000_link)】
      - [Code Density Improvements Beyond The C Standard Extension.pdf](https://content.riscv.org/wp-content/uploads/2019/12/12.10-17.10c-Code-Density-Improvements-Beyond-The-C-Standard-Extension.pdf) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4691R#note_6341426_link)】
        正式锁定 “ **Co** de- **D** ensity  **a** pplication- **s** pecific  **i** nstruction-set **p** rocessor ” 的命名推测！

### 结语：

这只是研究 Codasip 的开始，在接下来的 RV 少年 石榴派学习小组的几周内，将交给同学们去精读 Codasip 分享的干货，以小组交流和分享的方式，形成一张 Codasip 知识地图，补充到我们 RV 小组的知识库中。

（ _真正封笔，又过去了一个充实的周末夜晚，小袁们在小区疯玩，妈妈在追剧，清晨收到了 SIGer 正在进行时的 “石榴派祺福-寄语未来” 的最新战报，150条寄语 - 29条捐赠 ，实现了第一个99目标，可以开始下单制作用于国际漂流的 “石榴派” 嘞，之所以要用这么 “费事” 的方式来征集寄语，还要一遍一遍给朋友们，老师和同学们解释，用开源社区见证祺缘的意义，和写本文是同样的道理，从心里记下这个愿望，要感谢 TINA 老师，到动笔整理学习资料，详细板书笔记，到整理成文，设计封面，每一个行动不是为了别的，它是仪式，也是再一次的学习。现在封笔，刚刚好，我感觉很满意，我记住了 Codasip 的名字，没有比记住新朋友的名字，再好的礼物啦。_ ）

请 Tina 老师笑纳，[祝愿 RISC-V 生态健康茁壮，伙伴们的事业 长长久久!](https://gitee.com/flame-ai/siger#note_6344102)  :pray: 