RV4Kids 1st 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link)》 目录：

- [1.1](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link) [Architecture, Algorithm, All ... based on FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md#architecture-algorithm-all--based-on-fpga)

- [1.2](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5318914_link) [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#%E5%85%A8%E6%A0%88%E5%AD%A6%E4%B9%A0%E5%85%AC%E7%A4%BE)

  - [RV一起学](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#rv%E4%B8%80%E8%B5%B7%E5%AD%A6)
  - [How to start 我们的研学之旅](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#how-to-start-%E6%88%91%E4%BB%AC%E7%9A%84%E7%A0%94%E5%AD%A6%E4%B9%8B%E6%97%85-) ?
  - [Shiliu Pi 99](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#shiliu-pi-99) 石榴派

- [1.3](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5330927_link) [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E4%BA%A6%E5%B8%88%E4%BA%A6%E5%8F%8B%E4%BA%A6%E5%90%8C%E5%AD%A6%E6%88%91%E6%98%AF%E4%BD%A0%E4%BB%AC%E7%9A%84%E5%8A%A9%E6%95%99%E6%9B%B4%E6%98%AF%E8%AF%BE%E4%BB%A3%E8%A1%A8)。

  - [两份学习笔记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E5%88%86%E4%BA%AB%E4%B8%A4%E4%BB%BD%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0%E7%BB%99%E5%90%8C%E5%AD%A6%E4%BB%AC)： FSF 的 GNU chess、FPGA 的高性能计算
  - [学习资料](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E6%9C%80%E5%90%8E%E4%B8%8A%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99)：GNU Chess - 1st RISCV shiliupi is building...

- [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md)

  - [课程安排，四大块](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%AE%89%E6%8E%92%E5%9B%9B%E5%A4%A7%E5%9D%97-)
  - [课程参考书：《基于FPGA与RISC-V的嵌入式系统设计》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%8F%82%E8%80%83%E4%B9%A6%E5%9F%BA%E4%BA%8Efpga%E4%B8%8Erisc-v%E7%9A%84%E5%B5%8C%E5%85%A5%E5%BC%8F%E7%B3%BB%E7%BB%9F%E8%AE%BE%E8%AE%A1)
  - [在线学习慕课：《计算机组成与设计：RISC-V》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E5%9C%A8%E7%BA%BF%E5%AD%A6%E4%B9%A0%E6%85%95%E8%AF%BE%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BB%84%E6%88%90%E4%B8%8E%E8%AE%BE%E8%AE%A1risc-v)
  - [RVWeekly 联合 SIGer 青少年开源期刊共推](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#rvweekly-%E8%81%94%E5%90%88-siger-%E9%9D%92%E5%B0%91%E5%B9%B4%E5%BC%80%E6%BA%90%E6%9C%9F%E5%88%8A%E5%85%B1%E6%8E%A8)

---

# [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5318914_link)

这的名字是在本期封面的制作时诞生的，它和这 119 个 issues 紧密相连。这是开源社区给我的灵感，记得新年伊始元月1日，我仪式感爆棚地注册了 GItee 账号，和学生们一起开启了开源之旅。这时，我和同学们一样，都是开源小白，都有一份渴望拥抱的感觉，SIGer 创刊号的标题，就是《拥抱未来》。5个月的时间，Issues 面板上的数字，成为了红点诱惑的替代品，它们就是自己的学习笔记，心路历程，为了给同学们示范如何开展社区学习，SIGer 期刊也一期一期地诞生，Issues 就是每期的线索。

它将延续到 RV4Kids ，首先的目标，就是提取封面标题（已经完成），在提取了 10 个仓，55个 issues 之后，如今封面故事都写完了。它不只是简单的转载，它必须经过，认真阅读，逐字逐句转发（有些站是禁止全选转发的，这正好是一个学习机会），再分类，再撰写提要。这都是再一次阅读。温故而知新，在开源社区畅游，反复阅读，为自己的知识地图逐渐丰富和加深标记的知识点，都大有裨益。这也是我希望其他同学，一起学习的态度，和分享的方式。

因此，RV4kids 将同样延续 SIGer 画风，成为学习笔记的集合体，同学们分享的又一阵地。每个同学都是编委，每期我们会由志愿者组成编委会，完成编辑成刊的工作。与 SIGer 的涉猎广泛不同，RV4Kids 将聚焦学习社群的焦点，以共同学习的内容为主题。结合学习过程，每周一刊，紧密围绕当期的学习焦点。就比如，即将发布的 GNUchess RV 版，基于石榴派 RV 版的构建，将成为下一期的焦点内容一样，由各位同学，以作业“笔记”的方式汇集。计划“周二”成刊。并采用，更适合的分享方式，包括 B 站。同时，继承 RVweekly 的画风，只有标题和摘要。当然，附加上我这个“自封”的大主编的串场词，可以让每一期都能保有生活的暖色。

### RV一起学

> Only 99 +1 best wish & donated.

- Full-stack Hacker and Company...

也许和同学们见面，要到6月25日以后了，因为，RV4kids 将和石榴派 RV 版正式登陆 CNRV 中国峰会，这是我们见面的大日子。海报的标题以及 SHIliu PI 99 的宣传单，将同框发布。这是一个有意义的时刻。所以，我要用篇幅来解释这大大的 “99”

- join 全栈学习公社  
  https://gitee.com/RV4kids/RVWeekly/  
  成为 Full-stack Hacker and Company...

这才是 99 的目标，在本期封面文章中已经重篇幅介绍的“人才观”，为我们的学习社区制定了方向，以科普的手段，完成人才建设的目标。它来自两个维度：

1. 秉承 SIGer 的青少年属性，希望由“小白”成为进入社群的主力，这与 FPGA 赋能作用或者说石榴派的赋能目标是一致的，石榴派整体，给个性化计算的需求，提供了能力建设的保证。后面，再展开 Shiliu Pi 99. 不只使用石榴派，还要重度 DIY 石榴派。

2. 来自北向的文化建设。自从《南征北战》专题诞生，以 RV 阵地 抽象为南向最南端的梗，一定会贯穿 RV4kids 始终。也就是 整个石榴派的定位，个性化计算平台，100% 个人造，直到 RV 核心。这是一个可能性，所以用 RV 来成为“一起学”的标的物，它是一个载体，自我决策的载体，所以以 RV 为开始，这符合整个 RISCV 生态的状态。说好的说北向，又跑到南向啦。这北向就是民族棋，这次充分说明 “民族的就是世界的！”全世界的棋类，都可以有自己的民族属性，GNU Chess 也不例外，无人不喜欢自己的游戏能被更多朋友接受。这就是 RV4kids 的北向逻辑。

- best wish & donated. 是什么梗？

99 说明的是我们招募的1期同学的上限，是99. 这是为了呼应石榴派的 SLOGAN PI99 , 我们的社区拓展模式就是以99为团进行扩展的，它可以是一个社区参与民族棋小游戏的人群数量，可以是99个这样的社区，集合起来共同完成一件大事的规模。可以是我们募集资源的工作单元。

+1 是每一个加入的同学的代表，每人需要发一个愿，祝福自己，家人，社区，甚至全世界。这是一个社群的加入仪式，我笃信每一个愿无论从心，只要出口，都会是一个箴言，成为社区的凝聚力，被共同实现。这个仪式，会在每一个场合被反复重现，这将时刻提醒我们自己曾经的初心。

donated. 是一个过去式，这里要有一个启示就是2021年6月1日，我们和全世界小朋友一起，发布了 RV4kids 的骨干，就是这期期刊封面和 CNRV 海报。当时我有了三个目标：

1. 首先是我们的公益伙伴，微笑明天基金会，他们的主打项目“唇腭裂修复”还有“明天书屋”，他们的愿景和石榴派高度同源，Pray happiness and Health for the whole world Kids. 它就在我们的最新的 《民族棋大团结》国际版志愿者说明书的底部，和志愿者证书还有社群入口二维码同框，占据 1/4 的A4纸，被收纳到了每一份，结缘用的“民族棋精简版”的自封袋中。

2. 然后就是石榴派的对标树莓派，我们用99对400，是希望树莓派400的目标，缩小数字鸿沟的最精简桌面计算平台，早日实现，再次感谢树莓派社区，基金会+硬件+软件，为石榴派社区提供了样板，更是感谢 3.14 π日 ANNA “妹妹”给自己的中国姐姐 Else 的一封回信，促成了这个世界友谊，共同祈愿世界孩童健康快乐。如今，这个旷世奇缘的见证 ANNA 的手书，作为石榴派99 的宣传单的背景图，见证社区的发展。

3. 本期封面吉祥物 GNU （我皮一下，给它起了个新名字“吉牛”，插曲暂放），它是开源的鼻祖，自由软件基金会的吉祥物，本期会有一个大大的专题全面分享对 FSF 的学习，崇敬也好，膜拜也好，他目标明确，旗帜鲜明的态度，才是我们学习的榜样，精神和文化，才是真正的北向，是指引。回想成为志愿者的起点，一致对 MISSION 这个词不是很理解，随着时间的延长，和很多舶来品一样，从新鲜感到深刻认识总是一个过程，这本身就是学习。我认为这是文化的差异，在牛角处我小小地借用了一朵莲花，它有“花开次第”的意思。我希望不只研究 GNU chess 还要学习 GNU 的哲学思想，而行动上支持 FSF 则是一个仪式。这有感于 π日的 3.14 捐赠。作为第 3 个连接。

- How to donated ? 

以上三个基金会，任何形式的捐赠证明，将成为入社的凭证。包含两个相连的9，并附言1个寄语，就是我们明志的仪式啦。

### How to start 我们的研学之旅 ?

这是 石榴派 宣传单的一角，再正式的 RVCN 的发布仪式后，将全文刊载。

- 学习：建立自己的知识地图

就是我们的学习目标，学习方式，就是来自以下 MOOC 源：

1. RVWeekly
2. PLCT
3. EEworld
4. B站
   ...

现有的课程，借用 PLCT 吴伟老师的一个串词“不重复”，我们需要博览群书，也只有大量的阅读，才能逐步补全我们知识地图中的缺失。这是学习方法，没有直通车可以抵达。

1. 计算机组成与设计：RISC-V
2. RISC-V 指令集开源软件生态
3. 方舟 编译技术入门与实践
4. V8 for RISC-V
5. Verilog 入门教学
6. 开发一个 RISC-V 上的操作系统

最后，我要强调的是，我们的知识地图的目标（专业，课程）：

- 自动化
- 微电子
- 电子信息
- 计算机

这些看似不同的专业，在如今信息技术融合的时代背景下越发趋同，课程的边界开始被打破：

1. 数字系统设计
2. 计算机体系结构
3. 软硬件接口
4. 操作系统
5. 计算机软硬件算法

等到石榴派宣传单发布之日，同学们会看到这个知识地图的背景是一副真图，是我们 RVWeekly 主笔 孔令军老师的习作，他将自己学习多年的能力树做了梳理，绘制了这么一副知识地图，以希望辅助同学们成长。这也是 RV4kids 的缘起，他也将成为我们第一位导师。

### Shiliu Pi 99

- A chess computer 
- PI400 替代方案，个人计算中心
- 树莓派 + RISC-V 双平台
- 旧派回收，新派采购，未来派制造
- 私有云：LAMP 站、爬虫机器、语音助手、内网穿透、智能网关
- BASED ON OS 操作系统
- 交付的是 SD 卡
- 面向北向的应用：机器博弈 AI，民族棋文化研究
- 定制的中间件平台：JAVA, PYTHON, JS, LUA, C, 还有自研的 FSscript

以上出自 石榴派 宣传单的直读关键词，已经为同学们展示了关于它的数字画像，而它深层次的内容是要强调的，就是它的“北向” 作用。它是以人工智能的垂直领域 “机器博弈” 为学术方向，对接国际最前沿的研究方向，机器博弈通用引擎。结合“民族棋中国史”的文化高度，推出的 LuckyLudii 项目，作为 石榴派的北向应用的标准应用。同时植入硬件实体，树莓派 或 RV 派。交付场景，则是社区的棋文化传播。

- 机器博弈 AI 的学术前沿
- 棋文化保护的社区交付场景
- 硬件实体的载体作用

> 让我们共同造一个给社区小朋友用的会下不同棋的计算机。

这就是我们社群的与众不同，与为了学习而学习，更加具体明确，相信在这样一个教育场景中，我们都能获得彼此的支持，共同成长。当我们拥有了更大的能力，就能攀登更高的科学高峰，担当更大的责任啦。这就是 石榴派的使命。

---

上一篇 1.1 [Architecture, Algorithm, All ... based on FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)

下一篇 1.3 [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md)。
