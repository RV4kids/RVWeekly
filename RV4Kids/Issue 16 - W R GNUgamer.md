借用了 [FSF 著作的封面](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5896380_link)，致敬 RMS ([old version](https://images.gitee.com/uploads/images/2021/0912/020247_3ca9c5c0_5631341.jpeg)/[1](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6642664_link)/[2](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6643728_link))

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0912/121331_95d55a9c_5631341.jpeg "GNUgamerII999.jpg")](https://images.gitee.com/uploads/images/2021/0912/121237_a9c1eb2a_5631341.jpeg)

# [We're GNUgamer](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ).

我喜欢这个标题。足够北向。
主要技术点是讨论和确定 石榴派发行版多版本启动的问题。

- ARM 树莓派
  - RASPBIAN 64 国际版 ANNA
  - RASPBIAN 64 国内版 LISA
  - OE 核 RASPBIAN 64 国内版 LISA OE
- X86 阳光版
  - RASPBIAN 64 国际版 ANNA
  - RASPBAIN 64 国内版 LISA
  - OE GNOME GNUgames 99
- FPGA RISCV 
  - OE RV rocketchip
  - RVOS RV nutshell
  - OE RV nutshell
- 一生一芯 RV 工具版
  - UBUNTU 21.04 for IOT
  - UBUNTU 21.04 X86

整理完，自己都蒙圈啦  :sunglasses: 好在需求彻底梳理完啦。
所有的版本，都会有石榴派标准的北向 GAMES 棋文化打底。
足够引领，足够北，更在 RV 的加持下，足够的南向。
将全栈的架构，拓展的足够大，足够称得上 开源社区的全栈学习平台啦。
我想，向 GNU 鼻祖 RMS 老爷子致敬的准备，已经够充足乐。

今天可以制作 封面交付给 @leo.ding 老师啦。

> 正式交付前，参加了 oE 社区的 RISCV sig 组会，并以此帖为分享主题，再明确了当前 PLCT 实验室设立 oE RV 分支是为了酝酿后，再提交，我的心就彻底踏实了，GNUgames 作为北向分支，一定会最终向社区交付的。而这个交付，要感谢 丁老师的分享，在 [Issues 12 TurboLinux](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2012%20-%20TurboLinux.md) 的学习是，回馈上游社区 的原则，让本期主题更显得那么有意义，任何北向社区的努力，都是开源社区应有的态度，而开发版则是满足不同需求，应用自由的表现。

（以下是本期学习笔记，目标是学会基础的自我复制技能 DD.）

### 学习笔记：How to DD ?

1. [在已安装Ventoy的移动设备上安装Linux与配置引导](https://blog.csdn.net/lpwmm/article/details/119056455) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6597613_link)】

   > 简单来说，Ventoy是一个制作可启动U盘的开源工具。
有了Ventoy你就无需反复地格式化U盘，你只需要把 ISO/WIM/IMG/VHD(x)/EFI 等类型的文件拷贝到U盘里面就可以启动了，无需其他操作。
– Ventoy官方介绍

   - [Shiliu Pi sunnies had the newest version GNUgames](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6599183_link).

     > - GNU-Games-LiveCD-x86_64-20210905.iso
     > - GNU-Games-LiveCD-x86_64-20210905.md5sum

     - [进入设置程序 | GNUGames , 详细的启动信息](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6633619_link)
     - [oE gnome desktop | apps | sudoku | GNUbg | GNUchess](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6633870_link)
     - [GNUgames 退出信息详情](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6637627_link)
     - 1st RISCV shiliupi is building... [RV 并不同](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6637795_link)
     - [Bing LINUX 启动信息解读](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6638361_link)
    
2. [Linux系统下使用DD命令克隆磁盘或者镜像](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6608444_link)

   - [Linux命令行烧录树莓派镜像至SD卡](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6608744_link) shumeipai.nxez.com
   - [LINUX 备份SD卡 制作IMG文件](https://www.cnblogs.com/ZQQH/p/8453763.html)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6609130_link)】
   - [linux镜像烧录工具,树莓派操作系统镜像烧录方法指南](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6610378_link)
   - [Flash. Flawless.](https://www.balena.io/etcher/)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6610487_link)】
   - [How to Make Disk Images in Linux with DD Command](https://linuxhint.com/make-disk-images-dd-command-linux) 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6615177_link)】

3. [如何制作Linux SD系统启动卡？](https://www.eefocus.com/embedded/450000)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6609736_link)】

4. [How to Flash an SD Card for Raspberry Pi](https://computers.tutsplus.com/articles/how-to-flash-an-sd-card-for-raspberry-pi--mac-53600)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6613547_link)】

   - [AUTHORS Johnny Winter](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6614965_link) **Editor and author** 
     > A regular contributor to Envato Tuts+, Johnny has been online  **since 1995**  aiming to use the internet for the creation and sharing of knowledge for the greater good. Via setting up mailing-lists, forums and e-commerce businesses, Johnny has been an accidental podcaster and is now Editor of the Computer Skills, 3D & Motion Graphics and Music & Audio sections of Envato Tuts+
   - GENERATEPRESS: The perfect foundation for your WordPress website. 【[About](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6615048_link)】
     > GeneratePress is a  **lightweight WordPress theme**  that focuses on speed, stability, and accessibility. Start your website off in the right direction whether you’re a hobbyist, freelancer, or agency.

5. [分区助手## 错误代码对照表及其解决方法](https://www.disktool.cn/jiaocheng/errorcode.html)  【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6614861_link)】

   - [U盘无法识别，多为分区信息混乱 磁盘精灵可以帮助你](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6614889_link)。
   - [这是 RASPBIAN 的分区信息](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6623931_link)。

6. [dd命令磁盘对拷及备份](https://www.linuxidc.com/Linux/2017-3/141749.htm)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6615226_link)】

   - [Linux系统下使用DD命令克隆磁盘或者镜像](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6624657_link)
   - [DD 命令克隆 石榴派 成与不成](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6624790_link)。

7. [主启动盘 DD Clone 失败的原因](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6625273_link)

   - SOLVED: [BusyBox built-in shell (ash) Enter 'help' for a list of built-in commands](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6625447_link)
   - [kali系统开机显示BusyBox v1.30.1(Debian 1:1.30.1-4) built-in shell(ash) 解决方法](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6625890_link)
   - [小白安装深度系统卡在命令行](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6625957_link)
   - [busybox v1.1.3 built-in shell(ash)问题的解决](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6626095_link)

8. [ **首次完成本体克隆** ](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631424_link)

   - [用工具检查 待复制 U 盘的 盘符。 确认后，如上贴，完成克隆。](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631442_link)
   - [除了 顶贴的错误 BusyBox v：还有这样的：grub rescue>](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631636_link)
   - [参照之前的提示，尝试 修复，结果发现驱动器盘符不对。](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631653_link)
   - [FILE SYSTEM WAS MODIFIED 修改后，可以正常启动了。](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631669_link)
   - [提示载入进度后，正确启动](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631686_link)。
   - [这是正确的启动界面，以及检查 石榴派 发行版 预装的软件版本的出入](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631687_link)。
   - [修正后，载入画面后，闪过，自动登录信息](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631692_link)。
     `shiliupi login: pi (automatic login)`

   [![输入图片说明](https://images.gitee.com/uploads/images/2021/0912/224734_35d2db7d_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/0911/011750_ab01c6fe_5631341.png)

9. [尝试自制小尺寸 IMG. 还未成功。主要是引导记录的问题](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631432_link)。

   - [安装信息失败之一，如下](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631636_link)：【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631715_link)】
   - [error: invalid arch independent ELF magic](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631719_link)
   - [Dual boot EFI error: "invalid arch independent ELF magic"](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631721_link)
   - https://forums.linuxmint.com/viewtopic.php?t=103563 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6631724_link)】

10. [技术|Linux 开机引导和启动过程详解](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6638472_link)

    - [作者：David Both 译者： penghuster 校对：wxy](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6638629_link)
    - 本文由 LCTT 原创编译，Linux中国 荣誉推出

11. [ **Bing** ](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6639197_link) `为什么GNU 没有KERNEL`

    - [GNU/Hurd的前景如何，它未来有没有可能取代GNU/Linux的地位](https://www.zhihu.com/question/20204436) … 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6639549_link)】
      > 虽然C已经有四五十年的历史了，但是现在活力依旧。决定一样产品有没有前景的是产品自身的品质，Hurd 的不景气不光是因为linux这些年的蓬勃发展，它自身存在问题也是很大的原因。引用维基百科上的一段话，“Debian GNU/Hurd已开发了数年，但仍未正式发布。

      EXPLORE FURTHER （Recommended to you based on what's popular • Feedback）

      - [（整理）GNU Hurd项目详解](http://www.ha97.com/3188.html) www.ha97.com 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6639718_link)】
      - [Hurd 内核接棒：GNU Guix 将终止对 Linux 的支持](https://tech.sina.com.cn/digi/2020-04-05/doc-iimxxsth3691010.shtml) ... tech.sina.com.cn 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6639829_link)】
      - [GNU Hurd - 知乎](https://www.zhihu.com/topic/19627051)	www.zhihu.com

    - [GNU/Linux_百度百科 - Baidu](https://baike.baidu.com/item/gnu/linux)

      | 简介 | 组成 | 发行版 |
      |---|---|---|

      GNU本来有一个官方的内核，称为GNUHurd，遗憾的是GNU Hurd 还没有准备好可以作为产品使用。幸运的是，另一个内核已经可以取得。在 一九九一年，Linus Torvald… （See more on baike.baidu.com）

12. [GNU Hurd](https://www.gnu.org/software/hurd/)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6640024_link)】

    - https://www.gnu.org/software/hurd/contact_us.html
    - [GNU Hurd/ news/ 2021-08-14-debian gnu hurd 2021](https://www.gnu.org/software/hurd/news/2021-08-14-debian_gnu_hurd_2021.html)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6640399_link)】

13. GUIX https://guix.gnu.org/ 【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6640466_link)】

    - 自由。Guix 是 GNU 项目开发的 GNU 操作系统发行版，它尊重计算机用户的自由。
    - 可靠。Guix 支持事务性的升级和回滚操作、无特权的包管理、 等功能。作为独立发行版时，Guix 支持通过声明系统的配置来得到透明的和可复现的操作系统。
    - 可折腾。Guix 提供 Guile Scheme 编程接口，以及高级嵌入式领域特定语言（EDSL），透过此您可以定义软件包和整个操作系统的配置。

    [下载 1.3.0 版本](https://guix.gnu.org/zh-CN/download/) [贡献](https://guix.gnu.org/zh-CN/contribute/)
    
    1. 发现 Guix

       - 图形登录界面
       - 以“guix system vm”启动的虚拟机
       - 运行在 Wayland 上的 Sway 窗口管理器
       - Enlightenment、Inkscape 和 Serbian text
       - Xfce 桌面环境
       - GNOME 桌面环境

    2. [关于项目](https://guix.gnu.org/zh-CN/about/)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6640893_link)】
    3. [下载](https://guix.gnu.org/zh-CN/download/)【[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6642172_link)】
    4. [guix colobot gold edition](https://gitee.com/RV4Kids/RVWeekly/issues/I499QZ#note_6642199_link)

### RVWeekly 封笔

| # | 标题 | 回帖 | 时间 |
|---|---|---|---|
| 1 | [PLCT实验室为你新开放了一个技术岗位：来一起变得更强吧！](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTJH) 置顶 | 1 | 2个月前 |
| 2 | [ICwiki: 2021年7月9日 Talk](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P) | 4 | 2个月前
| 3 | [面试中经常会遇到的FPGA基本概念，你会几个](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTDE)？ | 2 | 2个月前 |
| 4 | [RV4kids 5th FPGA study](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9) | 15 | 2个月前 |
| 5 | [FPGA based Any](https://gitee.com/RV4Kids/RVWeekly/issues/I3UEA7) | 40 | 3个月前 |
| 6 | [RV4Kids Weekly Select (3) 第44期精选 《RVfpga直播: 首个RISC-V中文课》](https://gitee.com/RV4Kids/RVWeekly/issues/I3TUEU) | 2 | 3个月前 |
| 7 | [ZYNQ7020折腾之路 - xddcore - ee专业 - chiselos.com](https://gitee.com/RV4Kids/RVWeekly/issues/I3R2GD) | 6 | 4个月前 |
| 8 | [过程记录：编译 RISC-V 工具链](https://gitee.com/RV4Kids/RVWeekly/issues/I3PXC1) | 1 | 4个月前 |

又经过了 2个月 的学程，RVweekly 暂告一段落啦。再次开启的时间未知，要看石榴派的正式交付的日期啦。经过这两个月的坚持，我的自定义题目的学习主线围绕着 石榴派 桌面展开，截止到这两日收到的 oE GNOME 版，石榴派的发行版适配工作基本覆盖了流行的发行版，对北向应用的支持也实现了 100% 覆盖，从最完善的 X86 阳光版发行版的角度，已经可以实现构建目标的直观展示啦。从另一个角度，也就是 [石榴派开发板 —— 石榴核](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md) 的适配目标清晰可见了。

北向是目标，南向则就是能力，即使 RMS 老爷子也未能构建真正的全栈，还是围绕他的初衷 软件栈，以及目标软件业最初的团结展开的。石榴派则补上了 “软件定义一切” 的真正全栈，并落地在基于 [FPGA](https://gitee.com/RV4Kids/RVWeekly/issues/I3R2GD) 现在看，应该是所有计算机基础设施基础上的，通用计算平台 —— [MagSi 所定义的 可编程 个人计算机](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)啦。

看上面的 8 条 Issues 来自 RVweekly 的 正在进行的 任务列表，除去保留 天璇 小姐姐的 FPGA 课程外，其他的均是学习帖，虽然与 RISCV 相关，但和石榴派的关联度并不大，从知识视野的拓展角度，是可以的，但就工程聚焦，明显不符，[RVwiki](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P) 是一个愿景，当所有分享通过互联网汇聚在一起后，Wiki 就自然行程了。就 前两期开始的 “差分测试学习法” 从某种角度，就是 Wiki 知识通过验证，得到学习的案例。这么看来，[PLCT 实验室，通过实习生有针对地 学习](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTJH)（解决实际问题，或者研究）而后的分享，更符合 互联网 大 Wiki，原创的原则。目前 RVweekly 学习小组的搬运([3](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTDE)/[4](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9)/[5](https://gitee.com/RV4Kids/RVWeekly/issues/I3UEA7)/[8](https://gitee.com/RV4Kids/RVWeekly/issues/I3PXC1))，只起到了记录学习和研究过程的作用，达不到高质量分享。

[![1](https://images.gitee.com/uploads/images/2021/0730/142725_fbb4e144_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md) [![2](https://images.gitee.com/uploads/images/2021/0730/142745_79256318_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md) [![3](https://images.gitee.com/uploads/images/2021/0730/142759_150691d2_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md) [![4](https://images.gitee.com/uploads/images/2021/0730/142812_deac95ef_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)  
[![5](https://images.gitee.com/uploads/images/2021/0913/010205_591051e4_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md) [![6](https://images.gitee.com/uploads/images/2021/0913/010306_7c0a5a46_5631341.png "屏幕截图.png")](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md) [![7](https://images.gitee.com/uploads/images/2021/0730/142909_4ed936bb_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P) [![8](https://images.gitee.com/uploads/images/2021/0730/142933_66038dc6_5631341.png "屏幕截图.png")](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E7%9F%B3%E6%A6%B4%E6%B4%BE%E3%80%91SIGer%20%E7%BC%96%E8%BE%91%E6%89%8B%E5%86%8C.md)  
[![9](https://images.gitee.com/uploads/images/2021/0913/005740_a4b5714a_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209%20-%20RV%20%E5%B0%91%E5%B9%B4.md) [![10](https://images.gitee.com/uploads/images/2021/0730/142948_8146d114_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/issue%2010%20-%20inspur.md) [![11](https://images.gitee.com/uploads/images/2021/0730/143001_70e491b8_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md) [![12](https://images.gitee.com/uploads/images/2021/0822/005347_7685185e_5631341.jpeg "12.jpg")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2012%20-%20TurboLinux.md)  
[![13](https://images.gitee.com/uploads/images/2021/0822/005400_41a88e14_5631341.jpeg "13.jpg")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%20Issue%2013%20-%20Codasip.md) [![14](https://images.gitee.com/uploads/images/2021/0913/005930_01c19b13_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014%20-%20Difftest%20Learning.md) [![15](https://images.gitee.com/uploads/images/2021/0913/010005_6b1884c1_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2015%20-%20Ubuntu%20heart.md) [![16](https://images.gitee.com/uploads/images/2021/0913/010038_22b146da_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2016%20-%20W%20R%20GNUgamer.md)

Issues 16，是一个殊胜的数字，自本期封笔，伴随第2期石榴派漂流的回归社团，聚焦 石榴核 南向研究，将成为开学第一课后的内容，并移步到 MagSi ，伴以 “一生一芯” 的学习内容。对个人学习的笔记，将归并到 SIGer 编委组织，将不限定 RISCV 主题。[面向行业的学习](https://gitee.com/RV4Kids/RVWeekly/issues/I3TUEU)，从 Issue [10](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/issue%2010%20-%20inspur.md)开始，[12](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2012%20-%20TurboLinux.md)/[13](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%20Issue%2013%20-%20Codasip.md)/[15](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2015%20-%20Ubuntu%20heart.md)，也将拓展更多领域。

用 包云岗 老师为《开学第一课》写的寄结束本期，特别有意义：“ **_期待有一天中学生在“一生一芯”计划中设计出人生的第一颗 CPU 芯片！_** ” 我要在这个期待上加个愿景：只要你想，都能通过社区的支持实现每一个目标，哪怕是一颗 CPU 芯片，或者你只是一名中学生。愿每一个人都能 畅游知识的海洋，获得学习的自由。