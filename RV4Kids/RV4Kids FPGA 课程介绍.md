RV4Kids 1st 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link)》 目录：

- [1.1](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link) [Architecture, Algorithm, All ... based on FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md#architecture-algorithm-all--based-on-fpga)

- [1.2](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5318914_link) [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#%E5%85%A8%E6%A0%88%E5%AD%A6%E4%B9%A0%E5%85%AC%E7%A4%BE)

  - [RV一起学](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#rv%E4%B8%80%E8%B5%B7%E5%AD%A6)
  - [How to start 我们的研学之旅](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#how-to-start-%E6%88%91%E4%BB%AC%E7%9A%84%E7%A0%94%E5%AD%A6%E4%B9%8B%E6%97%85-) ?
  - [Shiliu Pi 99](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#shiliu-pi-99) 石榴派

- [1.3](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5330927_link) [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E4%BA%A6%E5%B8%88%E4%BA%A6%E5%8F%8B%E4%BA%A6%E5%90%8C%E5%AD%A6%E6%88%91%E6%98%AF%E4%BD%A0%E4%BB%AC%E7%9A%84%E5%8A%A9%E6%95%99%E6%9B%B4%E6%98%AF%E8%AF%BE%E4%BB%A3%E8%A1%A8)。

  - [两份学习笔记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E5%88%86%E4%BA%AB%E4%B8%A4%E4%BB%BD%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0%E7%BB%99%E5%90%8C%E5%AD%A6%E4%BB%AC)： FSF 的 GNU chess、FPGA 的高性能计算
  - [学习资料](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E6%9C%80%E5%90%8E%E4%B8%8A%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99)：GNU Chess - 1st RISCV shiliupi is building...

- [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md)

  - [课程安排，四大块](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%AE%89%E6%8E%92%E5%9B%9B%E5%A4%A7%E5%9D%97-)
  - [课程参考书：《基于FPGA与RISC-V的嵌入式系统设计》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%8F%82%E8%80%83%E4%B9%A6%E5%9F%BA%E4%BA%8Efpga%E4%B8%8Erisc-v%E7%9A%84%E5%B5%8C%E5%85%A5%E5%BC%8F%E7%B3%BB%E7%BB%9F%E8%AE%BE%E8%AE%A1)
  - [在线学习慕课：《计算机组成与设计：RISC-V》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E5%9C%A8%E7%BA%BF%E5%AD%A6%E4%B9%A0%E6%85%95%E8%AF%BE%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BB%84%E6%88%90%E4%B8%8E%E8%AE%BE%E8%AE%A1risc-v)
  - [RVWeekly 联合 SIGer 青少年开源期刊共推](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#rvweekly-%E8%81%94%E5%90%88-siger-%E9%9D%92%E5%B0%91%E5%B9%B4%E5%BC%80%E6%BA%90%E6%9C%9F%E5%88%8A%E5%85%B1%E6%8E%A8)

---

 **RV4KIDS 课：** 本课程的目标群体是高中和大学1、2年级新生，依托人工智能机器博弈专业AI领域的RISCV FPGA 课程。由 RVweekly 主笔 孔令军老师，每周和同学们一起上一小时的在线实验课，课后2小时为同学们的相互讨论，安排课代表和助教老师代培。课程进度随 RVweekly 发布和预报。

 **名程释义: ** 缘起本课程的共同发起人，SIGer 兴趣小组 - 青少年开源文化期刊 主笔 袁德俊老师，他一直从事青少年科普工作，火焰棋AI实验室的发起人，担任中国人工智能学会 机器博弈专业委员会 常委 ，负责青少年相关的科普工作，该专委会维护了一个15年历史的全国大学生机器博弈竞赛，是AI机器博弈领域领先的学术组织。kids 是一个应用场景，机器博弈的一个应用场景，AI示教——由人工智能程序，根据游戏规则进行自动演示和人机陪练，改场景被应用与非遗文化保护中的民族棋类的保护和挖掘工作，尤其对几近失传的棋种意义重大，通过竞赛加学术研究结合的方式已经成功保护和恢复了一部分民族棋样本。基于此应用场景，设计了本课程的 RISCV IP —— 石榴派，寓意团结一致齐心协力攀登科学高峰的信心。

本课程，还将邀请相关领域的专家和资深玩家与同学们一起学习，并以课外辅导的形式，穿插在课程主线，其中就包括 操作系统设计，编译工具链优化，LINUX开源社区运营，机器博弈通用算法等。

本公告发布即招募，荣誉殿堂 《SIGer 大使》称号和证书，将是我们感谢您的唯一方式，它不只代表我和小孔老师还代表广大青少年对您无私的分享和开源精神的真诚致敬，每一张证书上，将会有每一位参加的学员的亲笔感谢的数字化版本。

### 课程安排，四大块 ：

- 一、数字电路基础与hdl语言，week(2)(3)(4)(5)(6)(7)(8)(9)(10)
- 二、数字系统设计，week(11)(12)(13)(14)(15)(16)(17)(18)(19)
- 三、fpga实验，week(1，石榴派RISCV软核预装) (20)(21)(21)(23)(24)(25)(26)(27)
- 四、cpu大实验。week(28, FPGA机器博弈竞赛云平台接口适配) (29)(30)(31)(32)(33)(34)(35)(36)


每个板块9次课，主讲1小时，安排2小时的课后联系，共3小时共同学习时间。  
知识点课上发布，以自学和课后作业的形式推送给同学  
按年安排，分上下两个学期。  
每个实验室学员，从一块FPGA卡配石榴派RISCV软核开始。  
共同完成，石榴派RISCV IP全球发布这个小目标。  
（课时以 9999 长长久久，36 顺心顺意，寓意我们的目标一定能够实现。）

### 课程参考书：《基于FPGA与RISC-V的嵌入式系统设计》 
作者：资深FPGA和嵌入式开发专家，美国南加州大学集成电路设计专业硕士；美国PulseRain Technology公司的创始人。2018年RISC-V基金会官方RISC-V Soft CPU 设计大赛季军， 2019年RISC-V基金会官方物联网安全设计大赛冠军，他主持设计的PulseRain Rattlesnake处理器成功挫败了所有的黑客模拟攻击，并获综合成绩第一而夺冠。

### 在线学习慕课：《计算机组成与设计：RISC-V》
讲师简介：刘鹏浙江大学 - 博导，面向于计算机体系结构与集成电路设计领域，负责完成国家自然科学基金2项，国家“863”项目3项，霍英东教育基金优选课题、国家重点实验室基金5项等纵向项目，授权发明专利30余项，近期科研成果主要发表于IEEE Transactions on Computers， TVLSI， TACO等国际期刊。2003年主持设计了国内首款RISC/DSP媒体数字信号处理器（“浙大数芯”），负责设计具有自主知识产权的微处理器CPU IP核，该CPU IP核授权合作单位杭州士兰微电子股份有限公司面向消费类的音频系统芯片，截止2018年底销售系统芯片超过1360万颗，产生较好的经济效益和社会效益。

### RVWeekly 联合 SIGer 青少年开源期刊共推

 《[RV4Kids FPGA 课程实践](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE)》
>  **[RV4KIDS](https://gitee.com/RV4Kids)** ： _旨在实践“通过科普建立相关人才储备”之路。_  本课程的目标群体是高中和大学1、2年级新生，依托人工智能机器博弈专业AI领域的RISCV FPGA 课程。由 RVweekly 主笔 孔令军老师主持，[“ SIGer 学习小组” 分布式学习模式](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5316070_link)：同学们一周一起上一小时的在线实验课，课后2小时为同学们的相互讨论，不定期推选议题邀请导师在线支持 由课代表和助教老师代培。课程进度随 RVweekly 发布和预报。

（[2021-04-20 11:05](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_4894070_link) 编辑）

---

结尾

上一篇 1.3 [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E4%BA%A6%E5%B8%88%E4%BA%A6%E5%8F%8B%E4%BA%A6%E5%90%8C%E5%AD%A6%E6%88%91%E6%98%AF%E4%BD%A0%E4%BB%AC%E7%9A%84%E5%8A%A9%E6%95%99%E6%9B%B4%E6%98%AF%E8%AF%BE%E4%BB%A3%E8%A1%A8)。