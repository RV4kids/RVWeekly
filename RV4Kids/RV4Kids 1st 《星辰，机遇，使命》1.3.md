RV4Kids 1st 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link)》 目录：

- [1.1](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5357728_link) [Architecture, Algorithm, All ... based on FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md#architecture-algorithm-all--based-on-fpga)

- [1.2](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5318914_link) [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#%E5%85%A8%E6%A0%88%E5%AD%A6%E4%B9%A0%E5%85%AC%E7%A4%BE)

  - [RV一起学](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#rv%E4%B8%80%E8%B5%B7%E5%AD%A6)
  - [How to start 我们的研学之旅](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#how-to-start-%E6%88%91%E4%BB%AC%E7%9A%84%E7%A0%94%E5%AD%A6%E4%B9%8B%E6%97%85-) ?
  - [Shiliu Pi 99](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#shiliu-pi-99) 石榴派

- [1.3](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5330927_link) [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E4%BA%A6%E5%B8%88%E4%BA%A6%E5%8F%8B%E4%BA%A6%E5%90%8C%E5%AD%A6%E6%88%91%E6%98%AF%E4%BD%A0%E4%BB%AC%E7%9A%84%E5%8A%A9%E6%95%99%E6%9B%B4%E6%98%AF%E8%AF%BE%E4%BB%A3%E8%A1%A8)。

  - [两份学习笔记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E5%88%86%E4%BA%AB%E4%B8%A4%E4%BB%BD%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0%E7%BB%99%E5%90%8C%E5%AD%A6%E4%BB%AC)： FSF 的 GNU chess、FPGA 的高性能计算
  - [学习资料](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.3.md#%E6%9C%80%E5%90%8E%E4%B8%8A%E5%AD%A6%E4%B9%A0%E8%B5%84%E6%96%99)：GNU Chess - 1st RISCV shiliupi is building...

- [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md)

  - [课程安排，四大块](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%AE%89%E6%8E%92%E5%9B%9B%E5%A4%A7%E5%9D%97-)
  - [课程参考书：《基于FPGA与RISC-V的嵌入式系统设计》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E8%AF%BE%E7%A8%8B%E5%8F%82%E8%80%83%E4%B9%A6%E5%9F%BA%E4%BA%8Efpga%E4%B8%8Erisc-v%E7%9A%84%E5%B5%8C%E5%85%A5%E5%BC%8F%E7%B3%BB%E7%BB%9F%E8%AE%BE%E8%AE%A1)
  - [在线学习慕课：《计算机组成与设计：RISC-V》](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#%E5%9C%A8%E7%BA%BF%E5%AD%A6%E4%B9%A0%E6%85%95%E8%AF%BE%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BB%84%E6%88%90%E4%B8%8E%E8%AE%BE%E8%AE%A1risc-v)
  - [RVWeekly 联合 SIGer 青少年开源期刊共推](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md#rvweekly-%E8%81%94%E5%90%88-siger-%E9%9D%92%E5%B0%91%E5%B9%B4%E5%BC%80%E6%BA%90%E6%9C%9F%E5%88%8A%E5%85%B1%E6%8E%A8)

---

## [亦师亦友亦同学，我是你们的助教，更是课代表](https://gitee.com/RV4Kids/RVWeekly/issues/I3MYFE#note_5330927_link)。

想着要开班儿了，为了给孔老师做好准备工作，预热咱们的班级，我先做个自我介绍。与其罗列过往，更要展望未来，这119个 Issues 就是我的见面礼啦。它记录了这五个月的开源学习的关注，正在进行时...

我将它们分了六个主题：

1. RISC-V 主题，封面故事中介绍它的任务，不做额述，是我接下来会继续啃的内容。

2. 树莓派相关，它是石榴派的缘起，也是第一个完整的交付原型搭载的平台，3B+ 64位OS以上，我们的RV版本，也是对标树莓派的。6个仓，15个 Issues ，以 shiliupi 组织这些内容。shiliupi99 则是 石榴派交付的重点主仓。

3. 其他，4个仓，4个 Issues 部分会被关闭，多数都是2个月以前的关注，而RV的主题，也才诞生2个月，能有现在的成就，当归功于石榴派的交付场景，和发的愿足够大。

4. Blesschess & LuckyLudii 这是民族棋北向文化的标志，大愿之上。5个仓 12 个 Issues 只是冰山一角，很多内容都有无限的展开，整个石榴派则作为赋能的引擎，为至上文化服务，我们重要的交付内容的呈现，也都是棋文化的部分。

5. SIGer 是我们的根，专题则是枝叶和呈现，RV4kids 已经独立成刊，它的缘起 RVWeekly 和其他专题或继续，或关闭，随我和其他社区的关联而定。共3个仓6个Issues.

6. SIGer 以开源文化期刊示人，从兴趣小组，演进出学习小组，是开源社区作为科普阵地的直接体现。11个仓，有我和其他老师，多是一起学习的同学，包括清华附中机器博弈社团的同学。它已经不专属于任何一个组织个人，它已是全体青少年的学习平台，唯一的关联是以棋会友。这棋早已不是简单的攻杀博弈啦。27个Issues 我自己就占据了16个，这就是我啦，一个最勤奋的课代表。我相信每一份耕耘都能有收获，或多或少，只在愿力大小。在通向目标的道路上，定会有各种诱惑和干扰，能否抵达，全在自身。期待，我们的同学，都能实现心中所想，实现心中所愿。

> Pray for Health and Happiness for the whole world Kids.

就在刚刚，我将这份祈愿，再一次分享给了我的一个学生，希望能牵手两位天使的心，共同为美好明天发源褀福。也许你我见面的当天，就是以这个形式开始的。而这是一份最简单，也是最主要的仪式，会贯穿我们的学习生活。

![输入图片说明](https://gitee.com/shiliupi/shiliupi99/raw/d7f9ee2b1a37c6fef9943386d36b4e05f2e5dcfd/LuckyStar20210521.jpg "在这里输入图片标题")

最后，附上石榴派缘起于的开源社区 OpenEuler 社区的 TC 委员会收到的一张 LuckyStar 作品（附言被抹去，属于私藏）它为石榴派社区，换来的前两块 树莓派 3B+, 这张照片，也将成为 石榴派交付 IMAGE 的标配桌面，被传承给更多人。当你的火种社区，有了自己的故事时，你可以更换掉桌面，为社区新人讲述新的故事，但它们都源于一个祈愿，就是上面的“褀福天下孩童，健康快乐！”

期待我们的相见，为您分享石榴派的故事，感谢您的聆听！  
（为什么用这个标题，就是一份对您的欢迎 :pray: 有些文不对题，请多多包含。）

### 分享两份学习笔记给同学们，

- 一个是 **最北端的精神高地 [FSF 的 GNU chess 缘起](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110107_link)** 
  
  - [关于 GNU](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110107_link)
  - [教育](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110108_link) & [自由软件与教育](https://www.gnu.org/education/education.en.html)
  - [为什么教育机构应该使用和教授自由软件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110131_link)
  - [为什么在学校中只应使用自由软件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110157_link)
  - Richard Stallman's TEDx video: "I[ntroduction to Free Software and the Liberation of Cyberspace](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110239_link)"

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0514/101257_cd1d1a36_5631341.png "在这里输入图片标题")  

  我以这个图作为我的习作的总结，看似圣诞老人的靴子一样的非自由软件，实际在剥夺用户的自由，天下没有免费的午餐是它的直接缘由，这不是自由软件的愿起，请同学们随我一同学习自由软件，开源社区的最北端。

  （也许同学们注意到了，这组习作，不是独立Issues，而是一个专题下的回复，它是我的学习线索，会以盖楼的方式增长，其中最主要的议题都是出自 SIGer 专题的选题，今天的引用，只是一个半成品的 FSF 研究，它甚至只是一个开端，一段时间之后，会独立成刊为 SIGer 社区治理主题的核心内容，今天不做展开，按下另表。）

- 一个是  **FPGA 的高性能计算如何护航星辰大海** 。

  首先是《[护航“毅力号”火星探索之路](https://gitee.com/RV4Kids/RVWeekly/issues/I3SJ77)》中提及的四个系统：

  1. “毅力号”火星车的视觉计算单元 (VCE) 采用了基于 FPGA 的硬件加速器，它将协助火星表面的着陆导航和自动驾驶。
  2. “毅力号”计算机视觉加速器卡 (CVAC) 中的可再编程视觉处理器采用了赛灵思的 FPGA 器件，用于加速某些立体视觉任务，如图像校正、过滤、检测和匹配等；
  3. 基于火星科学实验室 (MSL) 架构的数字盒中采用了赛灵思另一款 FPGA 器件，作用于部分车载仪器——如多光谱立体成像仪 Mastcam-Z；
  4. “有机物和化学品拉曼光谱与荧光光谱宜居环境扫描 (SHERLOC)”光谱仪配备的火星手持透镜成像仪 (MAHLI) 的摄像头系统中，也采用了赛灵思 FPGA 器件。

  这四个系统的关键字，同样出现在了较早期宣传《[采用了大量FPGA，即将发射的美国火星漫游车技术揭秘](https://gitee.com/RV4Kids/RVWeekly/issues/I3SISM)》而更多航天航空应用，同样有 FPGA 的身影：

  - 使用xilin FPGA的卫星和航天器示例
  - 在JAXA卫星中也有采用
  - 从勇气号/机遇号，火星漫游车开始使用FPGA

  而宣传文件中反复强调的优势，应该被我们注意到，FPGA 并不是为了模拟CPU 而诞生的，它只是一项实现能力，赋能给了视觉加速以及机器学习，也是 FPGA 应有的位置，至少当下和现在是这样的。摘录的这些内容，能更好地说明这一点。但这并不能阻止我们应用 FPGA 研究 RISC-V，包括学习场景。

  - XQR5VFX130被用作图像处理的硬件加速器，在勇气号/机遇号中只使用20MHz的CPU（RAD6000），计算时间约为160秒，而毅力号使用200MHz的CPU（RAD750）和FPGA之间进行分工处理，使其速度提高了18倍，降至仅8.8秒。
  - 从2000年左右开始，Xilinx就开始提供空间级FPGA。在太空中，FPGA具有与地面一样的优势，尤其是发射后可以重写。由于能够灵活地适应任务变化，它还将延长卫星本身的寿命。
  - xilinx 的太空FPGA。使用了特殊的陶瓷封装。降低了 "单次事件效应" 的发生为，每年2次。

  任何一个知识点都可以无限延伸，深入到你想抵达的点，一则 [SOLUTIONS FOR DEVELOPERS](https://gitee.com/RV4Kids/RVWeekly/issues/I3SISM#note_5331425_link) 的介绍就是，全方位介绍了 FPGA 赋能的技术构成，由众多关键字组成：[PLATFORMS](https://www.xilinx.com/products/silicon-devices/acap/versal.html)，[CREATE A PLATFORM](https://www.xilinx.com/applications/aerospace-and-defense.html)，[EMBEDDED COMPUTE](https://www.xilinx.com/products/design-tools/embedded-software.html#ecosystem)，[ALGORITHMS AI, ML & DSP](https://www.xilinx.com/applications/megatrends/machine-learning.html)，[ADAPTABLE HARDWARE](https://www.xilinx.com/products/silicon-devices/fpga.html)，[THERMAL SOLUTIONS](https://www.xilinx.com/applications/aerospace-and-defense.html)，[POWER](https://www.xilinx.com/products/technology/power.html)，[PCB DESIGN](https://www.xilinx.com/applications/aerospace-and-defense.html)，[CONNECTIVITY](https://www.xilinx.com/products/technology/connectivity.html)，[MEMORY & CONFIG](https://www.xilinx.com/products/intellectual-property/nav-memory-and-controllers/nav-memories.html)，[RF ANALOG](https://www.xilinx.com/applications/aerospace-and-defense.html)，[BOARDS](https://www.xilinx.com/products/boards-and-kits.html)，[DEVICES](https://www.xilinx.com/products/silicon-devices.html)，[SECURITY](https://www.xilinx.com/products/technology/design-security.html)，[SEU & MITIGATION](https://www.xilinx.com/support/quality/reliability.html#seu)，[SAFETY CERTIFICATION](https://www.xilinx.com/products/technology/functional-safety.html)，[QUALITY](https://www.xilinx.com/support/quality.html)

  - [VITIS](https://www.xilinx.com/products/design-tools/vitis/vitis-platform.html): EMBEDDED & ALGORITHM HIGH-LEVEL SOFTWARE DEVELOPMENT FLOWS
  - [VIVADO](https://www.xilinx.com/products/design-tools/vivado.html): PROGRAMMABLE LOGIC HARDWARE & ALGORITHM FLOWS INCLUDING IDF & DFX 
    （这是石榴派RV版本的首发平台 FPGA ZYNQ 7020 的开发平台）别忘记看 [技能地图](https://images.gitee.com/uploads/images/2021/0602/224002_ee4d6e82_5631341.png) 全貌啊。

    [![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/231254_7bf56fa0_5631341.png "What's New in Vivado 2020.2")](https://www.xilinx.com/video/hardware/whats-new-in-vivado-2020-2.html)  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/231317_63ab206b_5631341.png "Vivado Design Flows Overview")](https://www.xilinx.com/video/hardware/vivado-design-flows-overview.html)

    [![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/231354_3ac4d90c_5631341.png "Getting Started with the Vivado IDE")](https://www.xilinx.com/video/hardware/getting-started-with-the-vivado-ide.html)  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/231417_333f91d6_5631341.png "UltraFast Design Methodology for the Vivado Design Suite - Introduction and Overview")](https://www.xilinx.com/video/hardware/ultrafast-design-methodology-for-vivado-into.html)

### 最后上学习资料：

我们的 GNU Chess 的 RV 实现的资料有两部分：

1. 公布的演示视频的文字详情

   [1st RISCV shiliupi is building...](https://gitee.com/shiliupi/shiliupi99/issues/I3TIA9)

   ![输入图片说明](https://images.gitee.com/uploads/images/2021/0530/105648_10cf6719_5631341.gif "在这里输入图片标题")

   它从以下命令开始我们的 RV 之旅：

   `root@zynq:~$ ./fesvr-zynq +blkdev=/mnt/card/root/oe-rv64-rootfs.ext2 bbl`

2. 一份编译 GNU Chess 的指导文件

   《gnuchess 编译方法.pdf》摘要如下：
   - 操作系统要求：Ubuntu 18.04
   - 工具安装：apt install gcc-riscv64-linux-gnu
   - 编译方法：在 src 下即可找到编译好的 gnuchess

```
wget http://ftp.gnu.org/gnu/chess/gnuchess-5.05.tar.gz -O gnuchess-5.05.tar.gz
tar xvf gnuchess-5.05.tar.gz
cd chess
./configure --host=riscv64-linux-gnu CFLAGS="-static -std-gnu89 -Drpl_malloc=malloc"
make
```

> 更多内容，则需要同学们提问，导师予以回答后，大家按图索骥啦。  
> 至此，您理解了 封面 背景的 字符型 棋盘的意义啦吧。接下来我们的作业也有了，那就是：  
> - BACKGAMMON
> - GNOME Sudoku
> 
> 更多故事，我们课间聊吧。

---

上一篇 1.2 [全栈学习公社](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#%E5%85%A8%E6%A0%88%E5%AD%A6%E4%B9%A0%E5%85%AC%E7%A4%BE)

下一篇 [RV4Kids FPGA 课程介绍](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md)
