![输入图片说明](https://images.gitee.com/uploads/images/2021/0711/155316_11eb0086_5631341.jpeg "XILINX2010ii999.jpg")

- 封面：[从 85,000 到 25亿 晶体管 的 FPGA 超越](https://gitee.com/RV4Kids/RVWeekly/issues/I3UEA7#note_5781753_link)

- 封二：[国产FPGA的机遇与挑战](https://gitee.com/RV4Kids/RVWeekly/issues/I3Z8DN)

- [RV4kids 5th FPGA study](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9)

- FPGA大赛作品：[RISC-V开发平台](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699160_link)

- [基于平头哥的 SOC 软核 wujian100](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699166_link)

- [基于FPGA的机器博弈五子棋游戏 PYNQ-Z2](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699166_link)

- PLCT-Weekly：[《开发一个RISC-V上的操作系统》汪辰](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699167_link)

- [理想的计算机科学知识体系：数字电路基础十讲](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5715318_link)

- [石榴派开发板实验课1st：NutShell 撸码](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699158_link)

- [面试中经常会遇到的FPGA基本概念](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5766217_link)

- 封三：[PLCT实验室为你新开放了一个技术岗位：来一起变得更强吧！](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5766223_link)

- 封底：[《“一生一芯”时间计划》任务提纲](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5766223_link)

> 这是上周 @袁德俊 的学习笔记，汇集了自从结缘 RISCV 以来关于 FPGA 的全部笔记，这也是 SIGer 学习小组的正确姿势，整理文档即学习，同时也是分享的好形式。上面的目录是笔记提要，下面是笔记摘要的罗列，同学们可以移步到各 issues 中阅读笔记原文。

---

> @yuandj 非常高兴 [完成了梳理工作，将所有的 ISSUES 整理出3大板块](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699153_link)。就在24小时前，刚刚收到一名 SIGer 新编委的志愿者申请，我回复她的内容就是：“我们 SIGer 编委会已经形成了一定的工作规范。” 这份梳理就是，不只是编辑工作的方法，也同样适用于 “学习小组”，最近喜欢一个词 “花开次第”，在 RV4kids 的首秀 CNRV 中国峰会上展示的 POST 的最顶端 就有这样一个标志，它代表着认识事物的由表及里，也同样是分析问题，解决问题的方法论。无论编辑工作从搜集素材到分类，提炼，出刊，还是学习从记录知识点，到分类整理，形成一张知识地图。这个事物发展演变的客观规律是要遵守的。而成果的出现，一定要一个中心思想指导，可以是使命感，克服千难万险，可以求知欲，永攀科学高峰的劲头驱动，也可以是 “自我实现” 的使然。现在看来，所有的归因，皆始于初心 - 发愿，而动因则由愿力所表现，事物本无对错，皆因发心的不同方向，而使得两股力量产生的对抗，化解它可以是不断加码的一方最终吞噬另一方，也可以是方向转变而和力为一。想到这，我为当下这期 学习小组 新刊 FPGA 专题 充满了期待，与 “万能” 的它有着异曲同工的感觉。抒情暂告段落，开始动笔啦。

---

《RV4Kids / some_tiny_interpreters》一共有5条 ISSUES，该仓只是作者团队的其中之一，关联仓包括：

- [gd32vf103-vga-ps2keyboard](https://gitee.com/RV4Kids/gd32vf103-vga-ps2keyboard)
基于GD32VF103的vga和ps2键盘驱动：gd32vf103 是国内一款很不错的riscv架构微处理器，但是网上gd32vf103的应用还比较少，这里我决定分享一下利用这个微处理器制作的vga驱动和ps2键盘驱动的调试过程和思路，主要还是用来学习定时器、spi等这些单片机常用的功能，希望... 《[基于GD32VF103 的vga显示器 和ps2键盘 驱动](https://gitee.com/RV4Kids/gd32vf103-vga-ps2keyboard/issues/I3MAJO)》分享了一个实验案例。

- [EVE_IDE](https://gitee.com/RV4Kids/EVE_IDE)
配套研制了一个集成开发环境-Eve-IDE。这种 IDE 集成了整套 RISC-V GCC 工具链和诸如串口助手之类的常用调试工具，IDE 安装包大小仅 48Mbyte。

起点 ISSUE 《【FPGA大赛作品】[FPGA 上的RISC-V开发平台（一等奖）](https://gitee.com/RV4Kids/some_tiny_interpreters/issues/I3MCEK)》是本仓作者的荣誉，《学员风采 | [能者为师，学者为生，听听“小潘老师”这么说~](https://gitee.com/RV4Kids/some_tiny_interpreters/issues/I3MPYK)》是南京集成电路大学的课程选题，更详细的课程介绍《[案例课程二：基于FPGA的RISC-V开发](https://gitee.com/RV4Kids/some_tiny_interpreters/issues/I3MRB8)》，他们均源自《[FPGA创新设计竞赛](https://gitee.com/RV4Kids/some_tiny_interpreters/issues/I3MRGF)》，这个大赛由厂商赞助，其中包括《[上海安路信息科技股份有限公司](https://gitee.com/RV4Kids/some_tiny_interpreters/issues/I3MRXE)》，再次重温，更多关键字可以入眼啦：

- 基于 EG4D20EG176 和 ELF1A650 可编程逻辑器件的FPGA 开发板。
- Anlogic EG4D20EG176 FPGA, 总资源量为 19600LUT4/5
- 根植中国，面向世界，努力成为中国主导，世界之一。
- 由于 FPGA 的特性，在模拟 RISCV 时以验证为主，性能并不高，无法与专用 SOC 匹敌。

---

《RV4Kids/wujian100_open》拥有7个ISSUES与FPGA相关，皆因其较早的RISCV实践获得的口碑。经由推荐，作为石榴派的调研目标之一。其中《[基于wujian100 SoC的智能五子棋设备的设计实现及其与QQ游戏玩家的对战](https://gitee.com/RV4Kids/wujian100_open/issues/I3LREI)》是最吸引我的，机器博弈原型，它是来自一个 B站 推荐，与 UP 主交流过程获得了相当信息。

- 基于平头哥的 SOC 软核
- 专属设计，并没有跑 Linux 
- 是 FPGA 性能限制（模拟，验证，加速是三个不同的范畴，因万能而生，效率性能不可兼得。）

然后转载了一篇 复旦大学微电子学院 同学的文章：《[RISC-V的“Demo”级项目——Rocket-chip](https://gitee.com/RV4Kids/wujian100_open/issues/I3LRJR)》对 Rocket-chip 做了推介，它是 石榴派选型的参考之一。（实际上在确定仓促的条件下，没有调研那么多种。）

学习源 B站 再次推荐，这不《[自制-FPGA科普小短片](https://gitee.com/RV4Kids/wujian100_open/issues/I3LW8Z)》来了，将 B站视频一帧一帧的字母摘抄下来，本身就是学习：

> 我来给大家介绍一下FPGA
开场让我们先进一段wiki介绍
FPGA 现场可编程逻辑门阵列
英文：Field Programmable Gate Array，缩写为: FPGA
它是作为专用集成电路领域中的一种半定制电路而出现的
既解决了全定制电路的不足
又克服了原有可编程逻辑器件门电路数有限的缺点。
这一听，多少让人有点不知所云
下面说说我的理解
说起CPU,大家应该都很熟悉了
它是用来运算的
它是电脑，手机等电子设备的大脑
FPGA 跟 CPU 一样，也是用来计算的
但是，它的计算方式跟CPU很不一样。
CPU 进行的是“串行计算”
它按顺序每次执行一条指令。
....
FPGA 可以并行运算很多指令。

《[基于FPGA的机器博弈五子棋游戏 PYNQ-Z2 XILINX](https://gitee.com/RV4Kids/wujian100_open/issues/I3LWEK)》终于到我们了，这是石榴派和一生一芯的共同目标，红板。《[初识PYNQ-Z2软硬件系统框架](https://gitee.com/RV4Kids/wujian100_open/issues/I3LWEK#note_4876970_link)》当初那么大劲头，收录到了 XILINX 老家了，ARM 故乡。

- VIVADO
- PYNQ PYNQ-Z2 初级知识库
- PYNQ上手笔记（6）——HDL设计IP核
- PYNQ系列学习(五)——Jupyter Notebook介绍
- PYNQ上手笔记（4）——Zynq中断应用
- PYNQ上手笔记（3）——PS端+PL端点灯
- PYNQ上手笔记（2）——PL端和PS端的独立开发
- PYNQ上手笔记（1） ——启动Pynq
- PYNQ系列学习(四)——pynq与zynq对比(三)
- PYNQ系列学习(三)——pynq与zynq对比(二)
- Xilinx PYNQ PS与PL的接口说明
- PYNQ上手笔记（5）——采用Vivado HLS进行高层次综合设计

两个花絮：

- 《[RISC-V+FPGA组合能否为国内厂商带来希望](https://gitee.com/RV4Kids/wujian100_open/issues/I3LWI3)》较早前录影，有收录过同类题材，就好比 ZEGBOARD 还有红板等。
- 《[2020年以后fpga工程师和it工程师哪个更有前途呢？](https://gitee.com/RV4Kids/wujian100_open/issues/I3PQZ4)》

课程调研：

《[基于FPGA与RISC-V的嵌入式系统设计](https://gitee.com/RV4Kids/wujian100_open/issues/I3LWKE)》这也是行业刚需。只是这本教材被小孔老师否决啦，他还特地购买了，评估的结果，不太适合作为 RV4kids 的实验板。

- 硬禾学堂还推出了特惠套装——FPGA学习平台（原价499元） + 老古的书（京东价100元） = 499元 可惜啦，被小孔老师枪毙啦。

---

- RV4Kids / [OpenArkCompiler](https://gitee.com/RV4Kids/OpenArkCompiler)
[hellogcc-maple] [OpenArkCompiler Weekly](https://gitee.com/RV4Kids/OpenArkCompiler/issues/I3LQC8) - ...
#I3LQC8
袁德俊
3个月前

它是一个科普周刊，作为开源社区的情报呈现，PLCT 实验室是他的强大后盾，FPGA 专题正好和 石榴派实验课 《CPU体系设计领进门》与 PLCT 主题结合的一周，将用石榴派模拟一个软核，适配 PLCT 实验室退出的 从零开始系列 OS 篇，是由石榴派见证人 汪辰 老师主讲，浅显易懂。《[OpenArkCompiler Weekly](https://gitee.com/RV4Kids/OpenArkCompiler/issues/I3LQC8)》将作为我们学习的榜样，也是同学们的一个出口。

- [OpenArkCompiler](https://gitee.com/RV4Kids/OpenArkCompiler)
方舟编译器开源代码官方主仓库，新仓库地址-https://gitee.com/openarkcompiler/OpenArkCompiler

- [writing-your-first-riscv-simulator](https://gitee.com/RV4Kids/writing-your-first-riscv-simulator)
从零开始的RISC-V模拟器开发·第一季·2021春季·连载中

- [riscv-operating-system-mooc](https://gitee.com/RV4Kids/riscv-operating-system-mooc)
forked from unicornx / riscv-operating-system-mooc
- [becoming-a-compiler-engineer](https://gitee.com/RV4Kids/becoming-a-compiler-engineer)
实用性编译技术课程，不需要编译原理基础 课程配套代码及幻灯片地址（也是提问的地方） https://github.com/lazyparser/becoming-a-compiler-engineer 课程视频回看（包含所有直播及录播视频） https://space.bilibi...
- [PLCT-Weekly](https://gitee.com/RV4Kids/PLCT-Weekly)
每个月1号和16号，[PLCT开源进展] 半月刊更新最新工作进展 - https://www.zhihu.com/column/plct-lab


---

《shiliupi / [RP_Silicon_KiCad](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues)》是石榴核的缘起，其中关于树莓派芯片计划的部分已经发布，剩下的5个任务贴是为了回答 FPGA 能否模拟 UI/IO 接口的技术问题。在 SOC 设计中除了 CPU 外，还需要集成一定的 UI 比如最熟悉的 USB HDMI 等，这些都是 外设 IP 都是需要费用的。当然贵不贵再说，全自主是姿态，全栈则是方法。下面就来看看我的回答：

- [数字电路基础](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VF6U) 
  > 本节将介绍数字电路的基础知识。数字电路是利用数字信号的电子电路。近年来，绝大多数的计算机都是基于数字电路实现的。
  > 
  > 1. 什么是数字电路
  > 2. 数值表达
  > 3. 有符号二进制数
  > 4. MOSFET 的结构
  > 5. 逻辑运算
  > 6. CMOS 基本逻辑门电路
  > 7. 存储元件
  > 8. 组合电路和时序电路
  > 9. 时钟同步设计
  > 10. 小结
本节介绍了数字电路的基础。在数字电路中使用 1 和 0 表现信息，基于用 MOSFET 组合构成的 CMOS 来实现各种逻辑电路。近年来，绝大多数的计算机都是基于数字电子电路实现的。(收录[简书](https://www.jianshu.com/p/6e18e860bbaa)于：[理想的计算机科学知识体系](https://www.jianshu.com/nb/49542010))

  - [理想的计算机科学知识体系](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VF6U#note_5445260_link)，目录全图（知识地图），[全部52节课](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VF6U#note_5443174_link)：
    1. 分布式系统
    2. 《Concurrent Programming in Java》
    3. Web应用程序
    4. 《数据库系统实现》第二版
    5. 《数据库系统概念》第六版
    6. 《计算机网络》
    7. 迭代式开发项目管理
    8. 统一过程
    9. Scrum —— 一个真实的敏捷开发案例
    10. 《Thinking in UML》
    11. 《深入浅出设计模式》
    12. CPAN
    13. Qt 
    14. wxWidgets
    15. GTK
    16. MFC 
    17. Boost
    18. STL
    19. 跨平台编程（跨Windows/Linux）
    20. 《Unix环境高级编程》学习笔记：从点到面
    21. 《Windows 程序设计》基础
    22. Perl基础
    23. Python基础
    24. C#基础
    25. C、C++和C#区别概述
    26. 《Thinking In Java》笔记一
    27. 《Thinking In Java》笔记二
    28. 《C++ Primer》第五版
    29. C语言和C++的区别
    30. C语言基础
    31. 《编译原理》
    32. 《算法导论》
    33. 《算法分析》
    34. 《数据结构与算法分析》
    35. 《计算机程序设计艺术》作者高德纳
    36. 《orange's:一个操作系统的实现》
    37. 《现代操作系统》
    38. 机器语言
    39. 数字电路基础 :pray: :pray: :pray:
    40. 微机原理
    41. 计算机体系结构
    42. 计算机组成原理
    43. 计算机体系结构：量化研究方法（第5版） ——与时俱进的教科书
    44. 几何理论
    45. 形式语言与自动机方法总结
    46. 数论是什么
    47. 通俗易懂玩高数
    48. 基础数学是什么？
    49. 《数理逻辑》
    50. 《离散数学》
    51. 《具体数学》
    52. 理想的计算机科学知识体系

- [FPGA之组合逻辑与时序逻辑、同步逻辑与异步逻辑的概念](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFTH)
- [基于FPGA的DVI/HDMI接口实现方案](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFU1)
- [基于FPGA的RS485通信接口实验手册](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFVF)
  - [基于FPGA的USB2.0接口通信](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFVF#note_5445904_link)
  - [基于USB3．0协议的PC与FPGA通信系统的设计](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFVF#note_5445915_link)
- [想用FPGA做一块PCIe显卡，如何学习？](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFWJ)
  - [基于Zynq的HDMI视频解码方式和相关IP核介绍](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFWJ#note_5445951_link)
  - [Xilinx® HDMI 解决方法包含 HDMI TX 及 RX 子系统。](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFWJ#note_5446013_link)
  - [赛灵思股份有限公司 核心许可协议](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VFWJ#note_5446074_link)

结论是肯定的，至于授权与否与学习无关，能做即可:D

---

《[RV4kids 5th FPGA study](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9)》就是本帖啦，本刊立项之始。它的目标是分析 FPGA 的各个层次，体现 石榴派 选型 7020 的“厚道”，性价比已经不能体现它的价值了。全栈学习能力才是，无论是初学者入门还是深度研究的开发者，在赋能面前都应该一视同仁，这才是 “石榴派” 的使命。

《[石榴派开发板实验课1st](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL)》是上一期的主题，其实就是前天周六，石榴派正式漂流当日的内容，选择 NutShell 是对一生一芯的致敬。也是因为 openEuler 社区参照它在 7020 红板上做的适配，为石榴派选型提供了一个起点，在适配中，了解到，一个成熟的开发板，一个完成的验证实验，是今天我们看到石榴派的基础。都源自 ZegBoard 的实验，由 RISCV 创始团队选择在其上做的验证。我们有什么理由拒绝呢？

《[https://github.com/li3tuo4](https://gitee.com/RV4Kids/rc-fpga-zcu/issues/I3TIBV)》这是调研 FPGA 时最新的一个开发板 ZCU102 的仓，由 University of New South Wales School of Computer Science and Engineering 澳大利亚悉尼的新南威尔士大学的计算机科学与技术学院维护。[按图索骥，ZCU102 的工具链依然是 VIVADO](https://gitee.com/RV4Kids/rc-fpga-zcu/issues/I3TIBV#note_5283028_link)，在搜索列表中的一些关键词，现在看已经逐步理解啦 “我想在ZCU102开发板进行开发 首先我用vivado新建了一个PL侧工程”。石榴派的设计师在推荐 ZYNQ7020 时这样介绍，“它是一个里程碑，之后的历代 XILINX 的开发板都是基于它发展出来的，或是 PL 或是 PS 或者两者。”

- [ZedBoard™ is a complete development kit for designers interested in exploring designs using the Xilinx Zynq®-7000 All Programmable SoC. The board contains all the necessary interfaces and supporting functions to enable a wide range of applications.](https://gitee.com/RV4Kids/rc-fpga-zcu/issues/I3TIBV#note_5283047_link) 这个米字星板的影像太深入我心啦，难怪我第一次见到 石榴派开发板 旁边的 ZEGBOARD 那么的激动不已。

这是 研究 openEuler sig-RISC-V 的成果：ZedBoard & ZC102 早就眼熟啦。
- shiliupi / [RISC-V](https://gitee.com/shiliupi/RISC-V/issues)
  - [ZedBoard](https://gitee.com/shiliupi/RISC-V/issues/I3PKCD)
    - 社区帖子: [ZedBoard FMC HDMI + Sobel Edge Detection](https://gitee.com/shiliupi/RISC-V/issues/I3PKCD#note_5016770_link)
    - 经常问的问题: [编译qemu和zynq-linux ...](https://gitee.com/shiliupi/RISC-V/issues/I3PKCD#note_5016772_link)
  - [ZCU102 Evaluation Board Components](https://gitee.com/shiliupi/RISC-V/issues/I3PKDQ)
    - [Sidewinder-100TM](https://gitee.com/shiliupi/RISC-V/issues/I3PKDQ#note_5016783_link)

《[英集芯科技 群英集萃 芯领全球](https://gitee.com/RV4Kids/tinyriscv/issues/I3PQZ5)》这不是广告时间，它之所以出现在 tinyriscv 是因为我在学习和调研时找到的 《[从零开始写RISC-V处理器](https://liangkangnan.gitee.io/2020/04/29/%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%86%99RISC-V%E5%A4%84%E7%90%86%E5%99%A8/)》它给了我一个 用 FPGA 开发验证一个 RISCV 核的全案教程，首发时间是 2019 , 而貌似广告的内容则是 “北向” 产业，电源管理模块 SOC 的展示，让学习者可以了解到 它的出口。它是真金白银，不像袁老师只输出情怀。此时此刻，全将它当做风景看即可。若要停留，就错过了更多风景啦。

- 重温 [readme.md](https://gitee.com/RV4Kids/tinyriscv) "采用三级流水线，即取指，译码，执行" “支持RV32IM指令集” 现在才注意到，因为长水平啦吗，当时真的只看热闹啦。要不是有 RV一起学，我这个课代表怎么可能那么用功呢？

- tinyriscv的整体框架图：现在再看，它会是又一个[很棒的实验课内容。MARK 一下](https://gitee.com/RV4Kids/tinyriscv/issues/I3Z0ZC)。

---

《[10年来最重要创新 ARM发布AMRv9指令集：IPC性能大涨30%](https://gitee.com/shiliupi/Debian-Pi-Aarch64/issues/I3R409)》
- 《[图][ARM推出全新armv9架构：预估五年内装备3000亿颗芯片](https://gitee.com/shiliupi/Debian-Pi-Aarch64/issues/I3R409#note_5101088_link)》

这是来自一个 Debian 中文社区的转载和关注，因为树莓派是基于 Debian 的，而且 它是最成熟的 Linux 发行版之一，还有编译环境最多的 ubentu。找过来完全是因为 一个树莓派镜像 同时支持 4B+/4b/3b+/3B 全64位操作系统，这也正是石榴派的锁定。所以才将这两个 Issues 放在该仓的下面。以记录下学习的轨迹。

---

《[面试中经常会遇到的FPGA基本概念，你会几个？ 0](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTDE)》
- 《[追寻ARM的起源-Acorn电脑简史及FPGA实现](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTDE#note_5765767_link)》: OpenFPGA

本刊 FPGA 专题要给这篇做封稿啦，和前面的一篇 “[理想的计算机科学知识体系](https://gitee.com/shiliupi/RP_Silicon_KiCad/issues/I3VF6U#note_5445260_link)” 异曲同工，知识地图一直都是 RV4kids 创刊以来努力寻觅的，犹如建立一个图书馆，网络群书编目分类，及时未读也在心中默默记下，不日再拾起。延展阅读的数十篇推介，让人垂涎，对 OpenFPGA 小编的勤勉尤为敬佩，选最后一篇：  _ARM 缘起 Acorn 如 Interl 之 仙童半导体。_ 

1. 什么是Setup 和Holdup时间？
2. 什么是竞争与冒险现象？解决办法？
3. 如何解决亚稳态？Metastability
4. 说说静态、动态时序模拟的优缺点
5. 用VERILOG写一段代码，实现消除一个glitch。
6. 简述建立时间和保持时间
7. 简述触发器和锁存器之间的差别
8. 计算最小周期
9. 时钟抖动和时钟偏移的概念及产生原因，如何避免？
10. 同步复位和异步复位的区别
11. 什么是线与逻辑？在硬件电路上有什么要求？
12. 什么是竞争冒险？如何判断？怎么样消除？
13. 异步FIFO深度计算
14. 画出用D触发器实现2倍分频的逻辑电路
15. 系统最高速度计算（最快时钟频率）和流水线设计思想：
16. FPGA设计工程师努力的方向：
17. 异步信号同步处理
18. FPGA中可以综合实现为RAM/ROM/CAM的三种资源及其注意事项？
19. HDL语言的层次概念？
20. IC设计前端到后端的流程和EDA工具？
21. MOORE 与 MEELEY状态机的特征？
22. 说说静态. 动态时序模拟的优缺点？
23. FPGA内部结构及资源：
24. 名词解释，写出下列缩写的中文（或者英文）含义：
FPGA ：Field Programmable Gate Array 现场可编程门阵列
VHDL：（ Very-High-Speed Integrated Circuit Hardware Description Language） 甚高速集成电路硬件描述语言
HDL ：Hardware Description Language硬件描述语言
EDA：Electronic Design Automation 电子设计自动化
CPLD：Complex Programmable Logic Device 复杂可编程逻辑器件
PLD ：Programmable Logic Device 可编程逻辑器件
GAL：generic array logic 通用阵列逻辑
LAB：Logic Array Block 逻辑阵列块
CLB :Configurable Logic Block 可配置逻辑模块
EAB: Embedded Array Block 嵌入式阵列块
SOPC: System-on-a-Programmable-Chip可编程片上系统
LUT :Look-Up Table 查找表
JTAG: Joint Test Action Group 联合测试行为组织
IP: Intellectual Property 知识产权
ASIC :Application Specific Integrated Circuits 专用集成电路
ISP :In System Programmable 在系统可编程
ICR :In Circuit Re-config 在电路可重构
RTL: Register Transfer Level 寄存器传输级
25. FPGA内部LUT实现组合逻辑的原理：
26.  低功耗技术：
27. MOS管基本概念及画图：
28. FPGA详细设计流程（面试提问）
29. 时序约束相关有哪几种时序路径：
30. 创建时序约束的关键步骤：
31.  SRAM和DRAM的区别
32. CMOS和TTL电路区别是什么？
33. JTAG接口信号及功能
34. 上拉电阻用途：
35. 有四种复用方式，频分多路复用，写出另外三种
36. 基尔霍夫定理的内容
37. 三段式状态机
38. 什么是状态图？
39. 用你熟悉的设计方式设计一个可预置初值的 7 进制循环计数器 ,15 进制的呢？
40. 你所知道的可编程逻辑器件有哪些？
41. SRAM,FALSH MEMORY,DRAM ， SSRAM及 SDRAM的区别 ?
42. 有源滤波器和无源滤波器的区别
43. 什么是同步逻辑和异步逻辑？
44. 同步复位和异步复位的区别
45. FPGA芯片内有哪两种储存器资源？
46. 什么是竞争与冒险现象？怎样判断？怎样消除？

--- 

自本刊起，将正式发布 封底广告，高光时刻一定要致敬 RV 的耕耘者：

- 《[PLCT实验室为你新开放了一个技术岗位：来一起变得更强吧！](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTJH)》

  RV4kids 最早一批收录的 Issues 中一篇 PLCT 实验室招募实习生的 帖子是重要的学习线索，成为前几期 RISC-V 主题的期刊的重要内容来源，以新篇 PLCT 实验室的英雄帖作为本期封底，以期 RV4kids 的年轻读者在未来通过学习和努力，能够成为 PLCT 实验室的光荣的一员。本期推荐精品课：

  - 《[开发一个RISC-V上的操作系统](https://gitee.com/RV4Kids/RVWeekly/issues/I3I74V#note_4802931_link)》汪辰老师主讲

- 《[“一生一芯”时间计划](https://gitee.com/RV4Kids/NutShell/issues/I3YYLL#note_5754527_link)》任务提纲

  上期发布的 第三期 一生一芯 百人计划，本周正式发布入选名单，同时发布了任务计划，指导同学们自主学习，从计算机体系结构开始，分两个阶段学习。基础阶段：基于RISC-V指令集设计一款五级流水线，并且通过AXI总线与串口、时钟、SPI-flash进行通信，支持时钟中断，最终可运行RT-Thread的RV64I处理器。进阶阶段 ： 性能优化、时序优化、后端流程等。7月9日发刊，第一堂公开课导师已经就位：

  - 《Verilator介绍》洪志博（昨天群答疑还回复了我的问题，apt 的版本旧不要用。）

这两个 RV 生态的耕耘社区都是 RV4kids 学习小组的重要学习目标，石榴派 FPGA 版作为学习工具，将同样在这两个社区中漂流，实现学习资源共享的目的。期待更多同学的分享，让我们一起学习吧 ...

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0709/224136_1b65e298_5631341.png "屏幕截图.png")

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0709/224225_b373d348_5631341.png "屏幕截图.png")


---


【[目录](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5791587_link)】


- 封面：[从 85,000 到 25亿 晶体管 的 FPGA 超越](https://gitee.com/RV4Kids/RVWeekly/issues/I3UEA7#note_5781753_link)

- 封二：[国产FPGA的机遇与挑战](https://gitee.com/RV4Kids/RVWeekly/issues/I3Z8DN)

- [RV4kids 5th FPGA study](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9)

- FPGA大赛作品：[RISC-V开发平台](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699160_link)

- [基于平头哥的 SOC 软核 wujian100](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699166_link)

- [基于FPGA的机器博弈五子棋游戏 PYNQ-Z2](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699166_link)

- PLCT-Weekly：[《开发一个RISC-V上的操作系统》汪辰](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699167_link)

- [理想的计算机科学知识体系：数字电路基础十讲](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5715318_link)

- [石榴派开发板实验课1st：NutShell 撸码](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699158_link)

- [面试中经常会遇到的FPGA基本概念](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5766217_link)

- 封三：[PLCT实验室为你新开放了一个技术岗位：来一起变得更强吧！](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5766223_link)

- 封底：[《“一生一芯”时间计划》任务提纲](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5766223_link)