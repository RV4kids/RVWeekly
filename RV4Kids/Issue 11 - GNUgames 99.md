[![输入图片说明](https://images.gitee.com/uploads/images/2021/0730/124028_93b6891f_5631341.jpeg "GNU99III999.jpg")](https://images.gitee.com/uploads/images/2021/0730/124009_f42a3b5f_5631341.jpeg)

(点击放大图)

GNUgames 99 正式立项了，就是上面的图。“Real Games they're” 这是我初步研读后的成果。而且还仅仅调研了三个子类，还是整个 Software 中的 Game 的三个子类，有个别重叠的归类，但大多都是独立的游戏，彼此并无交集。这与最初的 构想 有一些出入。开卷有益，石榴派 SIGer 学习小组，就是这样一步一步，在愿力的驱动下，花开次第的。 :pray: 

> 石榴派 SIGer 学习小组 是 RV少年 的行动队，这是写下上面这段文字的注释。  
> GNUgames 将是一个大品牌，未来一定会增长，繁荣，就像阳光透过树梢，曙光照亮前方的意思  
> 99 = 19 + 33 + 47, 这是巧合，去重后的结果。原来 CNRV 的 1分钟演讲的 99 是这个意思。  
> GNUgames 99.69.56.50 是 GNU99 (音：吉牛久久) 的版本树，右往左依次是支持的游戏数量
  - 50 是第一次甄别 oE 的软件包中 GNU 391 个包中包含的数量，还甄别出了一个废弃包。
  - 56 是最容易实现的啦，石榴派的吉祥数字，Chess, BG, Sudoku, ... 可以即刻发布
  - 69 是昨天的 FLAG，这个要好好甄别下，Console / Terminal 版本的 CLI version 兼具的包（ Chess, BG ...）
  - 99 就是全部 Board games & puzzles，这其中有一个山，就是：Gnome。预示着翻过 99，这个山我们就征服啦。
> 继续封面解读，看过 CNRV 袁老师的 1分钟 POSTER 演讲的，都看过石榴派 RV 的海报，这期封面，也借用了其 99 和 GNU CHESS BG SUDOKU 的字样。最醒目的莫过于 “RV一起学”，这不被我用坏了，变成了 “一起玩”，玩中学的意思。其实我是反对这个说法的，玩就是玩，学就是学，开发就是开发，互相不掺和。石榴派就是一个赋能工具，支持社区开发游戏，以自由软件，自由游戏为目的的学习。

这里要感谢下 RMS 封面选自 自由软件 自由社会 的著作第三版，源自 [SVG 文件](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5896380_link)的截图制作的。

本期内容干练，下面是 oE-RISC-V-sig 组的 issues:

- #[I42X85](https://gitee.com/openeuler/RISC-V/issues/I42X85) [文档] HOW TO BUILD oE risc-v 4 shiliupi 99 - @yuandj 1天前

- #[I41QXI](https://gitee.com/openeuler/RISC-V/issues/I41QXI) openEuler RISCV 软件包 构建清单 征集 - @yuandj 9天前
  - [openEuler RISCV 支持 GNU CLI 构建](https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_5924085_link)
  - [21日例会收到的任务，OBS 服务，甄别 GNU 软件](https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_5950482_link)
  - [oE 中包含有 GNU 最新软件包 391 个中的 50个：](https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_5950578_link)
  - [oE 中包含有一个 GNU 已退役软件](https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_5950615_link)
  - [Free software to use for gameplaying](https://gitee.com/openeuler/RISC-V/issues/I41QXI?from=project-issue#note_6028612_link)
    详细的功课，参考：https://gitee.com/yuandj/siger/issues/I3DLD8#note_6028635_link

- #[I4178R](https://gitee.com/openeuler/RISC-V/issues/I4178R) openEuler 在 石榴派 FPGA 开发板上的运行。 - @yuandj 12天前
  - [HOW TO BUILD oE risc-v 4 shiliupi 99](https://gitee.com/openeuler/RISC-V/issues/I4178R?from=project-issue#note_6028563_link)
    https://gitee.com/shiliupi/shiliupi99/issues/I42A7W#note_6028249_link

### GNU99 吉牛久久

上 MAP，下面的 99 表，将作为 PR 目标，在后面的学习中，由 石榴派 SIGer 学习小组不断完善。

（[GNU99 吉牛久久](https://gitee.com/yuandj/siger/issues/I3DLD8#note_6037923_link) issue 可以做为临时工作区，定期同步）

| No. | [chess-related](https://directory.fsf.org/wiki/Category/Game/chess-related) | (19) | No. | [board-game](https://directory.fsf.org/wiki/Category/Game/board-game) | (33) | No. | [puzzle](https://directory.fsf.org/wiki/Category/Game/puzzle) | (47) |
|---|---|---|---|---|---|---|---|---|
| 1 | [Antimicro](https://directory.fsf.org/wiki/Antimicro) | 抗微 | 20 | [Auale](https://directory.fsf.org/wiki/Auale) | 奥勒 | 53 | [AdaGate](https://directory.fsf.org/wiki/AdaGate) | 艾达门 | | |
| 2 | [AppEngine GO](https://directory.fsf.org/wiki/AppEngine_GO) |  | 21 | [Braille Scrabble](https://directory.fsf.org/wiki/Braille_Scrabble) | 盲文拼字游戏 | 54 | [Amnesia: A Machine for Pigs](https://directory.fsf.org/wiki/Amnesia:_A_Machine_for_Pigs) | 健忘症：猪的机器
| 3 | [Arbitools](https://directory.fsf.org/wiki/Arbitools) | 仲裁工具 | 22 | [CGoban](https://directory.fsf.org/wiki/CGoban) |  | 55 | [Amnesia: The Dark Descent](https://directory.fsf.org/wiki/Amnesia:_The_Dark_Descent) | 失忆症：黑暗降临
| 4 | [Baby](https://directory.fsf.org/wiki/Baby) | 婴儿 | 23 | [Caro](https://directory.fsf.org/wiki/Caro) | 卡罗 | 56 | [Anagramarama](https://directory.fsf.org/wiki/Anagramarama) | 字谜
| 5 | [Chess](https://directory.fsf.org/wiki/Chess) ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png") | 国际象棋 赫克特 | 24 | [Carom](https://directory.fsf.org/wiki/Carom) | 卡罗姆 | 57 | [Angry, Drunken Dwarves](https://directory.fsf.org/wiki/Angry,_Drunken_Dwarves) | 愤怒的醉酒矮人
| 6 | [Cursewords](https://directory.fsf.org/wiki/Cursewords) | 诅咒语 | 25 | [ChessX](https://directory.fsf.org/wiki/ChessX) | 象棋X | 58 | [Anonymine](https://directory.fsf.org/wiki/Antimicro) | 匿名矿
| 7 | [Dragon Go Server](https://directory.fsf.org/wiki/Dragon_Go_Server) | 龙围棋服务器 | 26 | [Console Othello](https://directory.fsf.org/wiki/Console_Othello) | 控制台黑白棋 | 59 | [Blocks Of The Undead](https://directory.fsf.org/wiki/Blocks_Of_The_Undead) | 不死方块
| 8 | [Eboard](https://directory.fsf.org/wiki/Eboard) | 电子板 | 27 | [Couple quest](https://directory.fsf.org/wiki/Couple_quest) | 情侣任务 | 60 | [Bloom](https://directory.fsf.org/wiki/Bloom) | 盛开
| 9 | [Fianchetto](https://directory.fsf.org/wiki/Fianchetto) | 芬凯托 | 28 | [Danican](https://directory.fsf.org/wiki/Danican) | 丹尼坎 | 61 | [Cinetraverse](https://directory.fsf.org/wiki/Cinetraverse) | 
| 10 | [Ghronos](https://directory.fsf.org/wiki/Ghronos) | 格罗诺斯 | 29 | [GNOME Mastermind](https://directory.fsf.org/wiki/GNOME_Mastermind) | GNOME 主谋 | 62 | [Crossword Builder](https://directory.fsf.org/wiki/Crossword_Builder) | 填字游戏生成器
| 11 | [Gnome Chinese Checkers](https://directory.fsf.org/wiki/Gnome_Chinese_Checkers) | 侏儒跳棋 | 30 | [GRhino](https://directory.fsf.org/wiki/GRhino) | 格里诺 | 63 | [Cursewords](https://directory.fsf.org/wiki/Cursewords) | 诅咒语
| 12 | [Ktsolver](https://directory.fsf.org/wiki/Ktsolver) | 解算器 | 31 | [Gamazons](https://directory.fsf.org/wiki/Gamazons) | 伽马松 | 64 | [Digits](https://directory.fsf.org/wiki/Digits) | 数字
| 13 | [Miguedrez](https://directory.fsf.org/wiki/Miguedrez) | 米格德雷斯 | 32 | [GlParchis](https://directory.fsf.org/wiki/GlParchis) | 帕奇斯 | 65 | [Eliot](https://directory.fsf.org/wiki/Eliot) | 艾略特
| 14 | [PyChess](https://directory.fsf.org/wiki/PyChess) | 国际象棋 | 33 | [Gnome Nine Men's Morris](https://directory.fsf.org/wiki/Gnome_Nine_Men%27s_Morris) |  | 66 | [Enigma](https://directory.fsf.org/wiki/Enigma) | 谜
| 15 | [Scid](https://directory.fsf.org/wiki/Scid) |  | 34 | [GnomeGo](https://directory.fsf.org/wiki/GnomeGo) | 侏儒围棋 | 67 | [Free Hero Mesh](https://directory.fsf.org/wiki/Free_Hero_Mesh) | 免费英雄网格
| 16 | [Sjeng](https://directory.fsf.org/wiki/Sjeng) |  | 35 | [Gnubg](https://directory.fsf.org/wiki/Gnubg) ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png") | 咕噜咕噜 | 68 | [Frozen Bubble](https://directory.fsf.org/wiki/Frozen_Bubble) | 冰冻泡泡
| 17 | [Stockfish (chess engine)](https://directory.fsf.org/wiki/Stockfish_(chess_engine)) | （国际象棋引擎） | 36 | [Gnugo](https://directory.fsf.org/wiki/Gnugo) ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png") | 侏儒 | 69 | [Games2d](https://directory.fsf.org/wiki/Games2d) | 
| 18 | [Vulcan Chess](https://directory.fsf.org/wiki/Vulcan_Chess) | 火神象棋 | 37 | [Gnushogi](https://directory.fsf.org/wiki/Gnushogi) ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png") | 格努绍吉 | 70 | [Gbrainy](https://directory.fsf.org/wiki/Gbrainy) | 粒状
| 19 | [Xboard](https://directory.fsf.org/wiki/Xboard) ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png") | 主板 赫克特 | 38 | [LiliBoggie](https://directory.fsf.org/wiki/LiliBoggie) | 莉莉博吉 | 71 | [GlPortal](https://directory.fsf.org/wiki/GlPortal) | 门户网站
| |  |  | 39 | [Mah-Jong](https://directory.fsf.org/wiki/Mah-Jong) | 麻将 | 72 | [Gnome-klotski](https://directory.fsf.org/wiki/Gnome-klotski) | 侏儒克洛茨基
| |  |  | 40 | [Nullify](https://directory.fsf.org/wiki/Nullify) | 无效化 | 73 | [Gnome-mines](https://directory.fsf.org/wiki/Gnome-mines) | 侏儒扫雷
| |  |  | 41 | [Peces Pieces (Tangram's Game)](https://directory.fsf.org/wiki/Peces_Pieces_(Tangram%27s_Game)) |  | 74 | [Gnome-sudoku](https://directory.fsf.org/wiki/Gnome-sudoku) | 侏儒数独
| |  |  | 42 | [Peg Solitaire (HiQ or Senku game)](https://directory.fsf.org/wiki/Peg_Solitaire_(HiQ_or_Senku_game)) |  | 75 | [Gnome-tetravex](https://directory.fsf.org/wiki/Gnome-tetravex) | 侏儒四面体
| |  |  | 43 | [PicPuz](https://directory.fsf.org/wiki/PicPuz) | 拼图 | 76 | [Gnubik](https://directory.fsf.org/wiki/Gnubik) ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png") | 格努比克 赫克特 ![](https://directory.fsf.org/w/images/5/58/Heckert_gnu.tiny.png "gnu.tiny.png")
| |  |  | 44 | [PycTacToe](https://directory.fsf.org/wiki/PycTacToe) |  | 77 | [Klickety](https://directory.fsf.org/wiki/Klickety) | 敲击声
| |  |  | 45 | [Quatter](https://directory.fsf.org/wiki/Quatter) | 季特 | 78 | [Lightsoff](https://directory.fsf.org/wiki/Lightsoff) | 关灯
| |  |  | 46 | [SDLHana](https://directory.fsf.org/wiki/SDLHana) |  | 79 | [Make 7 Libre](https://directory.fsf.org/wiki/Make_7_Libre) | 使 7 自由
| |  |  | 47 | [TryChess](https://directory.fsf.org/wiki/TryChess) | 尝试国际象棋 | 80 | [Margana](https://directory.fsf.org/wiki/Margana) | 马加纳
| |  |  | 48 | [TuxMathScrabble](https://directory.fsf.org/wiki/TuxMathScrabble) | 数学拼字游戏 | 81 | [Me and My Shadow](https://directory.fsf.org/wiki/Me_and_My_Shadow) | 我和我的影子
| |  |  | 49 | [TuxWordSmith](https://directory.fsf.org/wiki/TuxWordSmith) | 燕尾服史密斯 | 82 | [Neverball](https://directory.fsf.org/wiki/Neverball) | 无球
| |  |  | 50 | [V-ttt](https://directory.fsf.org/wiki/V-ttt) |  | 83 | [OpenClonk](https://directory.fsf.org/wiki/OpenClonk) | 
| |  |  | 51 | [Vassal Engine](https://directory.fsf.org/wiki/Vassal_Engine) | 附庸引擎 | 84 | [Penumbra: Overture](https://directory.fsf.org/wiki/Penumbra:_Overture) | 半影：序曲
| |  |  | 52 | [Xmahjongg](https://directory.fsf.org/wiki/Xmahjongg) | 麻将 | 85 | [Pybik](https://directory.fsf.org/wiki/Pybik) | 皮比克
| |  |  |  |  |  | 86 | [Pynagram](https://directory.fsf.org/wiki/Pynagram) | 五线谱
| |  |  |  |  |  | 87 | [Quackey](https://directory.fsf.org/wiki/Quackey) | 庸医
| |  |  |  |  |  | 88 | [Reliquarium](https://directory.fsf.org/wiki/Reliquarium) | 圣物箱
| |  |  |  |  |  | 89 | [RufasCube](https://directory.fsf.org/wiki/RufasCube) | 立方体
| |  |  |  |  |  | 90 | [RufasGate](https://directory.fsf.org/wiki/RufasGate) | 鲁法斯门
| |  |  |  |  |  | 91 | [RufasSeven](https://directory.fsf.org/wiki/RufasSeven) | 七号
| |  |  |  |  |  | 92 | [RufasSliders](https://directory.fsf.org/wiki/RufasSliders) | 鲁法斯滑块
| |  |  |  |  |  | 93 | [RufasSwap](https://directory.fsf.org/wiki/RufasSwap) | 
| |  |  |  |  |  | 94 | [Sudokuki](https://directory.fsf.org/wiki/Sudokuki) | 数独
| |  |  |  |  |  | 95 | [Swell-foop](https://directory.fsf.org/wiki/Swell-foop) | 膨胀foop
| |  |  |  |  |  | 96 | [Tkhangman](https://directory.fsf.org/wiki/Tkhangman) | 康曼
| |  |  |  |  |  | 97 | [WorldCupSokerban](https://directory.fsf.org/wiki/WorldCupSokerban) | 世界杯足球赛
| |  |  |  |  |  | 98 | [Yawl](https://directory.fsf.org/wiki/Yawl) | 雅尔
| |  |  |  |  |  | 99 | [Yetris](https://directory.fsf.org/wiki/Yetris) | 雪人
