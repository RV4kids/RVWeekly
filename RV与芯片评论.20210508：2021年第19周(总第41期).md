本期概要

- 嘉楠科技宣布完成1.7亿美元的机构定增
- 2020中国IC公司报告




---

<a name="040e68498fe94af14a2d31d5106f68eb"></a>
#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：[第21期·2021年05月01日](https://zhuanlan.zhihu.com/p/369328066)
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅农亚历山大: [RISC-V双周报](https://www.rvmcu.com/article-show-id-557.html
)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：

---



<a name="27f27f1566779837ca690f0417d9ac7a"></a>
## 技术动态

"[龙芯中科成功发布新一代自主指令集，我国芯片系统架构开启新纪元](https://www.ittime.com.cn/news/news_46788.shtml) (From www.ittime.com.cn 2021.05.07)"
> 龙芯LoongArch指令集手册1.0版本提供公开下载 ：[http://www.loongson.cn/product/dc/](http://www.loongson.cn/product/dc/)

"[32位MCU已成为高端应用主流选择](https://ee.ofweek.com/2021-05/ART-11000-2805-30496761.html) (From ee.ofweek.com 2021.05.07)"
> 目前，国内主要的32位MCU厂商有：兆易创新，中颖电子，航顺芯片，东软载波

"[跃昉科技12纳米狮山处理器NB2计划6月底流片](https://laoyaoba.com/n/779880) (From laoyaoba.com 2021.05.08)"
> 跃昉科技自主研发的12纳米的NB2（应用处理器芯片，狮山）计划6月底流片，基于BF（细滘）、NB（狮山）自主芯片的离线语音平台、区块链系统等已应用于家电、冷链、能源等领域，并与国家电网、港华燃气等合作；开源芯片研究院框架已初步搭建，人员陆续到位，今年将开展面向高性能应用的RISC-V边缘智能平台关键技术等五大方面的研究。

<a name="d5341f1457ce4cc45dd68f9fe821e6d2"></a>
#### Linux动态


<a name="bd674270d844156c441d6c491a844358"></a>
#### QEMU动态


<a name="afed03f5250206b6e0a59dbb2c657056"></a>
#### 一周问答

"[常常看到RISC-V中的lui和auipc指令联合使用，作用是什么呢？](https://www.zhihu.com/question/450611616) (From www.zhihu.com 2021.05.08)"

<a name="1834af42f819fedd0144c869ec9b4fb2"></a>
#### 社区动态

"[可能鸿蒙系统推出RISC-V版本更有利于推广？](https://www.txrjy.com/thread-1181791-1-1.html) (From www.txrjy.com 2021.05.07)"  
"[RISC-V 大赛简评 RVB2601 板卡](https://developer.aliyun.com/article/783929) (From developer.aliyun.com 2021.05.08)"


---

<a name="3f25c0aec37715d98fb24001e6f90d0b"></a>
## 产业风云

"[芯片产业，久违的繁荣](https://www.hstong.com/news/detail/21050713553815895) (From www.hstong.com 2021.05.08)"
> 2020年半导体行业股权投资案例413起，投资金额超过1400亿元人民币，相比2019年约300亿人民币的投资额，增长近4倍，这也是中国半导体一级市场有史以来投资额最多的一年。这也推动了国内芯片公司的蓬勃发展。

"[2020年度中国IC设计公司调查报告——高管篇](https://www.eet-china.com/news/202105040624.html) (From www.eet-china.com 2021.05.07)"
> 管理层问卷侧重公司商业策略规划，如销售额、毛利润、融资计划、海外扩张计划等。近60%的人预估今年毛利率在20%以上，9%的公司预计毛利率能高于40%。对于目前很热的RISC-V架构，有超过40%的高管表示有计划采用或已经采用。13%的公司表示暂时不考虑RISC-V，这一比例较去年的20%下降了很多。还有10%左右的公司表示只用Arm或x86架构。

"[嘉楠科技宣布完成1.7亿美元的机构定增](https://www.hstong.com/news/detail/21050520451190137) (From www.hstong.com 2021.05.07)"
> 公司计划发行所得款项净额将用于研发和扩大生产规模，以及与本次发行有关的补充招股说明书中披露的营运资金和常规的公司费用。

"[睿思芯科打造中国第一颗稳定迭代的RISC-V高端芯片](http://tech.ce.cn/news/202105/07/t20210507_36538609.shtml) (From tech.ce.cn 2021.05.08)"
> 谭章熹的目标很明确：聚焦某项垂直领域，瞄准头部企业，快速让芯片与应用产品和场景进行适配，并在某一特定领域率先落地，做出旗舰机效应，从而辐射全行业。谭章熹告诉记者，第一个领域很可能是高端智能穿戴。
  - "[睿思芯科已有确定的落地场景和重量级客户，并已进入大客户主流产品流水线。](https://www.sohu.com/a/465108517_166680?scm=1004.783301797329698816.0.0.1273) (From www.sohu.com 2021.05.08)"

"[出货量突破6亿颗，GD32 MCU赋能电机驱动智能解决方案](https://laoyaoba.com/n/779885) (From laoyaoba.com 2021.05.08)"

"[中芯国际人事重大变动！](https://www.rvmcu.com/article-show-id-738.html) (From www.rvmcu.com 2021.05.08)"
> 据披露，因工作安排调整，国家集成电路产业基金董事代表路军辞职，辞任的是中芯国际第二类非执行董事以及董事会提名委员会委员的职务；从发布与生效时间来看，该项人事变动是在4月29日已经生效。

<a name="ad4e97987f4fce7ae157a484526e2762"></a>
#### 评论文集

"[MIPS落幕,转身加入RISC-V阵营](http://m.elecfans.com/article/1575962.html) (From m.elecfans.com 2021.05.07)"  
"[RISC-V未来发展空间如何?](http://m.elecfans.com/article/1575278.html) (From m.elecfans.com 2021.05.07)"  
"[RISC-V将在AIoT领域引发的变化分析](http://m.elecfans.com/article/1575364.html) (From m.elecfans.com 2021.05.07)"


---



<a name="6395d96e54fcd2acd131c715687c276b"></a>
## 其他动态


<a name="1af25ae617de29d87abbdf1175b21a48"></a>
#### 视频推荐

「从零开始的 RISC-V 模拟器开发」第四课：Spike 设备模拟
> https://www.bilibili.com/video/BV12Z4y1c74c?p=4  
> 本次是Spike模拟器的最后一课，下周开始进入QEMU模拟器的讲解

<a name="a713c779ca5999a6954be10d0219afb2"></a>
#### 博文推荐

"[CS61C 自学总结](https://zhuanlan.zhihu.com/p/369585201) (From zhuanlan.zhihu.com 2021.05.07)"  
"[ncnn risc-v 优化——五一流水账](https://zhuanlan.zhihu.com/p/369942129) (From zhuanlan.zhihu.com 2021.05.07)"

---

<a name="2010a69b8aaca066239ad10c98eb0e7c"></a>
## 一周论文

#### [RISC-V Barrel Processor for Deep Neural Network Acceleration](https://ieeexplore.ieee.org/abstract/document/9401617/)
MH AskariHemmat, O Bilaniuk, S Wagner, Y Savaria… - 2021 IEEE International …, 2021  
This paper presents a barrel  **RISC-V**  processor designed to control a deep neural
network accelerator. Our design has a 5-stage pipeline data path with 8 hardware
threads (harts). Each thread is executed under a strict round robin scheduler and is …

#### [A comparative survey of open-source application-class RISC-V processor implementations](https://dl.acm.org/doi/abs/10.1145/3457388.3458657)
A Dörflinger, M Albers, B Kleinbeck, Y Guan, H Michalik… - Proceedings of the 18th …, 2021  
The numerous emerging implementations of  **RISC-V**  processors and frameworks
underline the success of this Instruction Set Architecture (ISA) specification. The free
and open source character of many implementations facilitates their adoption in …

#### [A Low-Cost Bug Hunting Verification Methodology for RISC-V-based Processors](https://ieeexplore.ieee.org/abstract/document/9401510/)
C Rojas, H Morales, E Roa - 2021 IEEE International Symposium on Circuits and …, 2021  
Agile hardware design strategies have shown a fast adoption in academia and
industry by bringing ideas from the software development side. However, adopted
design methodologies exhibit traditional verification scenarios based on handmade …

#### [Enabling OpenCL and SYCL for RISC-V processors](https://dl.acm.org/doi/abs/10.1145/3456669.3456687)
R Burns, C Davidson, A Dodds - International Workshop on OpenCL, 2021  
ABSTRACT  **RISC-V**  is a non-profit, member managed organization and is gaining
momentum in the processor space, with more than 900 members. One of the goals of
the organization is to build an open software platform, providing software developers …

#### [Ultra-compact binary neural networks for human activity recognition on RISC-V processors](https://dl.acm.org/doi/abs/10.1145/3457388.3458656)
F Daghero, C Xie, DJ Pagliari, A Burrello, M Castellano… - Proceedings of the 18th …, 2021  
ABSTRACT Human Activity Recognition (HAR) is a relevant inference task in many
mobile applications. State-of-the-art HAR at the edge is typically achieved with
lightweight machine learning models such as decision trees and Random Forests …

#### [Tapeout of a RISC-V crypto chip with hardware trojans: a case-study on trojan design and pre-silicon detectability](https://dl.acm.org/doi/abs/10.1145/3457388.3458869)
A Hepp, G Sigl - Proceedings of the 18th ACM International Conference …, 2021  
This paper presents design and integration of four hardware Trojans (HTs) into a
post-quantum-crypto-enhanced  **RISC-V**  micro-controller, which was taped-out in
September 2020. We cover multiple HTs ranging from a simple denial-of-service HT …

#### [PDF] [VOSySmonitoRV: a mixed-criticality solution on 64bit Linux-capable RISC-V platform](https://webthesis.biblio.polito.it/18147/1/tesi.pdf)
L Lavagno, M Paolino, F Caforio  
This thesis has been developed during a six months internship in Virtual Open
Systems SAS company located in Grenoble, France. The company is specialized in
design and implementation of high-performance mixed-critical virtualization solutions …

#### [PDF] [UVM environment for RISC-V processors](https://webthesis.biblio.polito.it/18053/1/tesi.pdf)
EES Sanchez, PDA Ruospo, L Barraco - 2021  
In the VLSI design flow, functional verification is the task of checking that the digital
design is compliant with the specifications in order to find bugs in the hardware
description before being mass-produced. Due to the fast growth of the design size …

#### [PDF] [Interfacing a Neuromorphic Coprocessor with a RISC-V Architecture](https://webthesis.biblio.polito.it/18187/1/tesi.pdf)
PDG Urgese, E Forno, A Spitale  
The concept of neural network is nowadays spread everywhere. Commonly meant
as a mean to provide what is called artificial intelligence, neural network constitute
one of few state of the art technologies which are constantly growing in complexity …

#### [Towards RISC-V CSR Compliance Testing](https://ieeexplore.ieee.org/abstract/document/9422767/)
N Bruns, V Herdt, D Große, R Drechsler - IEEE Embedded Systems Letters, 2021  
Recently, the critical compliance testing problem for  **RISC-V**  has received significant
attention. However, Control and Status Registers (CSRs), which form the backbone
of the  **RISC-V**  privileged architecture specification, have been mostly neglected in …

#### [TPE: a Hardware-based TLB Profiling Expert for Workload Reconstruction](https://ieeexplore.ieee.org/abstract/document/9422779/)
L Zhou, Y Zhang, Y Makris - IEEE Journal on Emerging and Selected Topics in …, 2021  
… software tampering. A prototype of TPE is demonstrated in Linux on two
representative architectures, ie, 32-bit x86 and 64-bit  **RISC-V** , implemented in the
Simics and Spike simulation environment respectively. Experimental …

#### [HTML] [The TaPaSCo Open-Source Toolflow](https://link.springer.com/article/10.1007/s11265-021-01640-8)
C Heinz, J Hofmann, J Korinth, L Sommer, L Weber… - Journal of Signal …, 2021  
… The tool is actively used in a variety of projects, some of which are discussed
in Section 4. The process of including  **RISC-V**  softcores, which are originally
not designed to be used with the tool, as a PE in a TaPaSCo design, is …

#### [HTML] [6 Performance/Energy Relative to GPU](https://toc.123doc.net/document/2754740-6-performance-energy-relative-to-gpu.htm)
B Liebig  
… 3.1.2. We have chosen an embedded processor based upon  **RISC-V**  architecture to
highlight the usability of our proposed debugging system … Software compilation can be
carried out through the available  **RISC-V**  cross compiler toolchain …

#### [PDF] [Simple, Light, Yet Formally Verified, Global Common Subexpression Elimination and Loop-Invariant Code Motion](https://hal.archives-ouvertes.fr/hal-03212087/document)
D Monniaux, C Six - 2021  
Page 1. HAL Id: hal-03212087 https://hal.archives-ouvertes.fr/hal-
03212087 Preprint submitted on 1 May 2021 HAL is a multi-disciplinary
open access archive for the deposit and dissemination of sci- entific …

#### [A Reconfigurable Capacitive Power Converter With Capacitance Redistribution for Indoor Light-Powered Batteryless Internet-of-Things Devices](https://ieeexplore.ieee.org/abstract/document/9423810/)
HC Cheng, PH Chen, YT Su, PH Chen - IEEE Journal of Solid-State Circuits, 2021  
… Japan, in 2012. In 2011, he was a Visiting Scholar with the Univer- sity of California
at Berkeley, Berkeley, CA, USA, where he conducted research in fully integrated power
management circuits for  **RISC-V**  processors. He is currently …

#### [PDF] [High-assurance field inversion for curve-based cryptography](https://eprint.iacr.org/2021/549.pdf)
BS Hvass, DF Aranha, B Spitters  
Page 1. High-assurance field inversion for curve-based cryptography
Benjamin S. Hvass, Diego F. Aranha and Bas Spitters Concordium
Blockchain Research Center (COBRA) Department of Computer Science …

#### [PDF] [A Survey on Advance Black/Grey hole Detection and Prevention Techniques in DSR & AODV Protocols](http://technology.eurekajournals.com/index.php/IJWNMCI/article/download/641/755)
TA Kumar, A Devi, N Padmapriya, S Jayalakshmi… - International Journal on …, 2021  
… 13. Design and Development of an Efficient Branch Predictor for an In-order
 **RISC-V**  Processor [Текст] / C. Arul Rathi, G. Rajakumar, T. Ananth Kumar,
TS Arun Samuel // Журнал нано- та електронної фізики.-2020.-Т. 12, № …

#### [HTML] [Error analysis (linguistics)](https://en.nrme.net/detail6974188.html)
A Photos

#### [Carbon Nanotube Transistors](https://api.taylorfrancis.com/content/chapters/edit/download?identifierName=doi&identifierValue=10.1201/9781003043089-4&type=chapterpdf)
M Zhang, C Du, Q Huang, Z Liao, Y Deng, W Huang… - 2021  
Page 1. Chapter 4 Carbon Nanotube Transistors Min Zhang, Chunhui Du, Qiuyue
Huang, Zhiqiang Liao, Yanyan Deng, Weihong Huang, and Xiaofang Wang School
of Electronic and Computer Engineering, Peking University …
 
#### [PDF] [ISA extensions in the Snitch Processor for Signal Processing](https://webthesis.biblio.polito.it/18144/1/tesi.pdf)
L Benini, A Macii, S Riedel, M Cavalcante, S Mazzola  
… Snitch is a tiny RV32IMA core based on the  **RISC-V**  open instruction set architecture
(ISA), paired with an application-tunable accelerator. In this work we present
Xpulpimg, an extension of the  **RISC-V**  instruction set including …

#### [Post-Quantum Cryptographic Hardware and Embedded Systems](http://books.google.com/books?hl=en&lr=lang_en&id=lUIsEAAAQBAJ&oi=fnd&pg=PA228&dq=risc-v&ots=-eoIalTyoG&sig=iP1ipjpZs7ALUQZNGPKM7LfMgzg)
B Koziel, MM Kermani, R Azarderakhsh - Emerging Topics in Hardware Security  
… Farahmand et al.[26] report a high-speed implementation for NTRU. Howe et al.[27]
implemented FrodoKEM for high-performance FPGA. One lightweight research approach
has been to add ISA extensions to the  **RISC-V**  architecture [28–30] …

#### [PDF] [Edge Deep Learning Applied to Granulometric Analysis on Quasi-particles from the Hybrid Pelletized Sinter (HPS) Process](https://www.researchgate.net/profile/Mateus-Silva-19/publication/351229485_Edge_Deep_Learning_Applied_to_Granulometric_Analysis_on_Quasi-particles_from_the_Hybrid_Pelletized_Sinter_HPS_Process/links/608c0ba392851c490fa9c643/Edge-Deep-Learning-Applied-to-Granulometric-Analysis-on-Quasi-particles-from-the-Hybrid-Pelletized-Sinter-HPS-Process.pdf)
NFC Meira, MC Silva, RAR Oliveira, A Souza…  
… Table 1: Embedded platform performance numbers. Parameter Characteristics
CPU 64-bit  **RISC-v**  processor and core Chipset K210 - RISC - V Image
Recognition qvg at 60fps / vg at 30fps Clock (GHz) 0.40 AI resources KPU …

#### [Security Enhancement of Contactless Tachometer-Based Cyber Physical System](http://books.google.com/books?hl=en&lr=lang_en&id=zegrEAAAQBAJ&oi=fnd&pg=PA165&dq=risc-v&ots=6fbOB8QJx0&sig=97dwrTLeE7obPEfrDnbd1BbNPeM)
KKR Saraf, P Malathi, K Shaw - Machine Learning Approaches for Urban Computing  
… ESP8266 Cost in| 190/- 752.84/- 129/- Release Year 2019 2016
Microcontroller Xtensa single-core 32-bit LX7 Xtensa single/dual-core
32-bit LX6 2014 Xtensa single-core 32-bit L106 240 MHz 160/240 MHz 80 …

#### [PDF] [Neural Architecture Search Techniques for the Optimized Deployment of Temporal Convolutional Networks at the Edge](https://webthesis.biblio.polito.it/18048/1/tesi.pdf)
DDJ Pagliari, DA Burrello, M Risso  
Page 1. POLITECNICO DI TORINO Master degree course in Ingegneria
Elettronica (Electronic Engineering) Master Degree Thesis Neural
Architecture Search Techniques for the Optimized Deployment of Temporal …
 
#### [AES Sbox Acceleration Schemes for Low-Cost SoCs](https://ieeexplore.ieee.org/abstract/document/9401539/)
C Duran, H Gomez, E Roa - 2021 IEEE International Symposium on Circuits and …, 2021  
… access scheme. Measurement results from a fabricated SoC featuring a  **RISC-V**  based
32-bit processor indicate a 900 fold improvement of AES-256 computing energy
efficiency compared to pure-software implementations. The …

#### [PDF] [Gemmini: Enabling systematic deep-learning architecture evaluation via full-stack integration](https://people.eecs.berkeley.edu/~ysshao/assets/papers/genc2021-dac.pdf)
H Genc, S Kim, A Amid, A Haj-Ali, V Iyer, P Prakash… - Proceedings of the 58th …, 2021  
… Unlike existing DNN accelerator generators that focus on standalone accelerators,
Gemmini also provides a complete solution spanning both the hardware and software
stack, and a complete SoC integration that is compatible with the  **RISC-V**  ecosystem …

#### [Development of On-Chip Calibration for Hybrid Pixel Detectors](https://ieeexplore.ieee.org/abstract/document/9417021/)
P Skrzypiec, R Szczygieł - 2021 24th International Symposium on Design and …, 2021  
… The proposed solution integrates the  **RISC-V** -based microprocessor, Pixel Matrix Controller,
and pixel matrix detector inside the single integrated circuit … Index Terms— **RISC-V** ,
System-on-Chip, SoC, Hybrid pixel detectors, HPD, X-ray, detectors calibration …

#### [FireMarshal: Making HW/SW Co-Design Reproducible and Reliable](https://ieeexplore.ieee.org/abstract/document/9408192/)
N Pemberton, A Amid - 2021 IEEE International Symposium on Performance …, 2021  
… This is especially true in the realm of software where applications are often
updated on a monthly, or even daily, cadence. In this paper we introduce
FireMarshal, a software workload management tool for  **RISC-V**  based …


---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

