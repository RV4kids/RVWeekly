本期概要
- 芯来沙龙：国产自主安全芯
- 阿里云峰会

---

# 重点聚焦

#### 阿里云峰会

"[阿里云张建锋：云计算在高速增长中 近期已完成组织架构升级](https://finance.sina.com.cn/tech/2021-05-30/doc-ikmyaawc8324958.shtml)(From finance.sina.com.cn 2021.06.04)"
> 阿里云今年将加码投入三个方面,做好服务、云钉一体、数据智能,行业方面,将重点发展教育、医疗、农业领域。  
> 叶军认为，相比mendix等全球低代码厂商，钉钉的优势在于更好的生态环境。“钉钉的低代码平台上有氚云、简道云等等工具，我们有非常好的生态环境，我们要做的是把基础能力平台做好，剩下的交给生态。”  

"[阿里“云钉一体”下一步：50亿投入数字化转型](http://www.eeo.com.cn/2021/0531/489974.shtml)(From www.eeo.com.cn 2021.06.04)"

"[把握AIoT时代RISC-V发展机遇 芯片平台选择攻略](https://www.eet-china.com/mp/a54020.html)(From www.eet-china.com 2021.06.04)"
> 目前，大部分新RISC-V芯片是针对IoT和AIoT的应用型SoC，而AIoT无疑是嵌入式领域的重点和热点，因此，何小庆表示，RISC-V是业界需要广泛拥抱的架构。但当前，产业界的RISC-V芯片“专用性”过强、“通用性”不足，相较于ARM平台内的无缝切换，仍有待改善，此外，RISC-V尚未形成可持续发展的生态环境，由此，何小庆表示，RISC-V发展仍需要更多的耐心。

"[阿里云的“一云多芯”强在哪](https://zhuanlan.zhihu.com/p/376420407)(From zhuanlan.zhihu.com 2021.06.04)"
> 阿里云最新的一云多芯的方案，其实就是用一套云操作系统来管理不同架构的硬件服务器集群，它最大的特点就是可以将不同架构CPU的算力标准化，从而解决从根本上解决不同类型CPU共存所带来的多云管理问题

"[平头哥连发三款RISC-V开发板，搭载玄铁910、906处理器](https://www.eet-china.com/mp/a54331.html)(From www.eet-china.com 2021.06.04)"
> 平头哥基于自研玄铁系列处理器再次推出RVB -ICE、RVB-D1、RVB 2601三款开发板，并公布端云一体软件平台，进一步为开发者和企业提供了体验RISC-V技术的桥梁。 

"[没让马云失望，科技巨头养大芯片黑马，成功绕开美国技术](https://www.sohu.com/a/470063660_465536)(From www.sohu.com 2021.06.04)"


<a name="040e68498fe94af14a2d31d5106f68eb"></a>
#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：[5月 / 第三期 / 2021](http://tinylab.org/tinylab-weekly-5-3rd-2021/)
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅亚农历山大: [RISC-V双周报](https://www.rvmcu.com/article-show-id-557.html) ：[5月21日-6月3日](https://mp.weixin.qq.com/s?__biz=MzU2MjM4MTcyNQ==&mid=2247489824&idx=1&sn=ab61804989effe13cd6f9317aa702d3b&chksm=fc6b0fd2cb1c86c4745c76cff2fd6231d2018fb04e8ec2dc23f9ce163da0decb977e2cb52f09&scene=178&cur_album_id=1713615520249217030#rd)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：


---



<a name="27f27f1566779837ca690f0417d9ac7a"></a>
## 技术动态

#### Linux动态

RISC-V CPU Idle Support: https://lwn.net/Articles/856397/  
KVM RISC-V Support: https://lwn.net/Articles/856544/  
riscv: Add DMA_COHERENT support: https://lwn.net/Articles/856549/  
--- 来自：泰晓咨询

#### 社区动态

"[RISC-V 芯片厂商赛昉科技加入华为 openEuler 社区](https://www.sohu.com/a/469890388_114760)(From www.sohu.com 2021.06.04)"  
"[万物智联，安全护航|国民技术AIoT安全论坛成功举办](https://www.eet-china.com/news/202106030925.html)(From www.eet-china.com 2021.06.04)"

---

<a name="3f25c0aec37715d98fb24001e6f90d0b"></a>
## 产业风云

#### 产品动态

"[推出全球首款云原生服务器CPU的Ampere，明年将再破核心数纪录](https://www.leiphone.com/category/chipdesign/JudXM1PVHUv6WiRV.html)(From www.leiphone.com 2021.06.04)"
> 2017年10月注册成立的Ampere（安晟培）在2020年投产了采用Arm IP的80核业界首款云原生服务器CPU Ampere Altra，今年第三季度，Ampere还将按计划投产128核的云原生Ampere Altra Max 服务器CPU，再次刷新服务器CPU核心数纪录。

"[北京君正：目前公司已经展开RISC-V CPU核的研发](https://www.sohu.com/a/469808003_115362)(From www.sohu.com 2021.06.04)"

#### 企业动态


"[主营模拟芯片的智浦芯联近日宣布全系列产品上调售价15%至30%不等](https://stock.stockstar.com/SS2021060200000103.shtml)(From stock.stockstar.com 2021.06.04)"
> 主因近期上游晶圆以及封装成本进一步上涨。此外，主营MCU的瑞纳捷半导体也宣布再次涨价5%至20%，该公司今年4月初曾上调产品售价。

"[纳思达：MCU涨价潮持续 国产化替代加速](http://finance.eastmoney.com/a/202106031947987925.html)(From finance.eastmoney.com 2021.06.04)"


<a name="6395d96e54fcd2acd131c715687c276b"></a>
## 其他动态

#### 评论文集

"[【微处理器】最高性能RISC-V处理器和Arm比起来，究竟如何？](https://www.eet-china.com/mp/a53905.html)(From www.eet-china.com 2021.06.04)"
> SiFive的7系列与8系列在当今RISC-V生态中可能没有充足的代表性，不过如SiFive所言，这些高性能核心IP对于扩展RISC-V生态边界还是有相当价值的。即便就现在看来，Arm生态在中高端市场仍然占据着性能和技术上相当的优势。  
> 另外作为IoT与嵌入式领域的重要组成部分，RISC-V如今在很多MCU产品中越来越占到一席之地。比如兆易创新首发全球RISC-V通用MCU（Bumblebee核心），即是对RISC-V生态构建的重要组成部分。

"[RISC-V新机遇](https://zhuanlan.zhihu.com/p/376157758)(From zhuanlan.zhihu.com 2021.06.04)"
> 华泰证券-集成电路行业产业系列报告之二：内核架构意义凸显，RISC-V现新机” 很有意思，里面介绍到RISC-V新机遇，2021年中国上海举办RISC-V峰会，众多厂商、科研院所都在参与RISC-V生态建设，RISC-V未来可期。

"[后摩尔时代，集成电路的发展机遇](http://caifuhao.eastmoney.com/news/20210528171259580557930)(From caifuhao.eastmoney.com 2021.06.04)"

"[一家年营收不足20亿美元的公司，是如何卡苹果、三星、 华为、高通脖子的？](https://finance.sina.com.cn/chanjing/gsnews/2021-05-31/doc-ikmxzfmm5710013.shtml)(From finance.sina.com.cn 2021.06.04)"
> ARM是一家“小公司”，至今年营收不超过20亿美元。可ARM又是一家“大公司”，从苹果到三星，从华为到高通，全球超过90%以上的手机芯片里都有着它的技术，几乎卡住了全世界的“脖子”。

#### 视频推荐

"[RT-Thread在RISC-V芯片GD32V 上跑起来啦](https://www.bilibili.com/video/BV1Y4411q7JY/)(From www.bilibili.com 2021.06.04)"

#### 博文推荐

"[认识RISC-V](https://blog.csdn.net/weixin_46831482/article/details/117447270)(From blog.csdn.net 2021.06.04)"
> 一、RISC-V指令集特点  
> 二、设计简单的RISC-V指令  
> 附表  
> RISC-V常用寄存器调用名  
> 关于R、I、U、S简单解释  
> RISC-V手册摘要

"[RISC-V内核架构下的编译环境搭建](https://zhuanlan.zhihu.com/p/373550735)(From zhuanlan.zhihu.com 2021.06.04)"
> 1.系统安装  
> 2.RISC-V交叉编译器  
> 3.spike模拟器


---

<a name="2010a69b8aaca066239ad10c98eb0e7c"></a>
## 一周论文

#### [RISC VBased Reconfigurable Manager for Event Transmission in SpaceFibre Networks](https://ieeexplore.ieee.org/abstract/document/9435599/)
E Suvorova - 2021 29th Conference of Open Innovations Association …, 2021

#### [The Semiconductor Industry: A Strategic Look at China's Supply Chain](https://link.springer.com/chapter/10.1007/978-3-030-69812-6_8)
Y Li - The New Chinese Dream, 2021

#### [PDF][A Flexible FPGA-Based ISA Configurable SoC platform](https://arxiv.org/pdf/2105.12678)
SY Yuan, BY Zhu - arXiv preprint arXiv:2105.12678, 2021

#### [PDF][Partial RISCV 128 support in QEMU for a x86_64 bit host](https://ensiwiki.ensimag.fr/images/d/dc/IRL_Benjamin_Cathelineau.pdf)
B Cathelineau, F Pétrot

#### [PDF][PERFORMANT SOFTWARE HARDENING UNDER HARDWARE SUPPORT](https://gts3.org/assets/papers/2021/ding:thesis.pdf)
R Ding - 2021

#### [PDF][OpenSerDes: An Open Source Process-Portable All-Digital Serial Link](https://arxiv.org/pdf/2105.13256)
B Chatterjee, S Sen - arXiv preprint arXiv:2105.13256, 2021

#### [PDF][Scalable Memory Protection in the PENGLAI Enclave](https://ipads.se.sjtu.edu.cn/zh/publications/FengOSDI21-preprint.pdf)
E Feng, X Lu, D Du, B Yang, X Jiang, Y Xia, B Zang…

#### [PDF][A Latency-Optimized Network-on-Chip with Rapid Bypass Channels](https://www.mdpi.com/2072-666X/12/6/621/pdf)
W Ma, X Gao, Y Gao, N Yu - Micromachines, 2021

#### [PDF][Self-supervised Monocular Multi-robot Relative Localization with Efficient Deep Neural Networks](https://arxiv.org/pdf/2105.12797)
S Li, C De Wagter, GCHE de Croon - arXiv preprint arXiv:2105.12797, 2021

#### [Fast DSE of reconfigurable accelerator systems via ensemble machine learning](https://link.springer.com/article/10.1007/s10470-021-01885-0)
A Lopes, M Pereira - Analog Integrated Circuits and Signal Processing, 2021

#### [The MareNostrum Experimental Exascale Platform (MEEP)](https://superfri.org/superfri/article/view/369)
A Fell, DJ Mazure, TC Garcia, B Perez, X Teruel… - … Frontiers and Innovations, 2021

#### [Enabling software security mechanisms through architectural support](https://open.bu.edu/handle/2144/42594)
L Delshadtehrani - 2021

---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

