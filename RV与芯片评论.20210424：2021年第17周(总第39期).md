# RV与芯片评论.20210424：2021年第17周(总第39期)

本期概要
- OpenFive 5nm芯片 tape out
- Linux5.13支持 SiFive FU740
- 瑞萨与SiFive合作车规芯片
- 全志D1使用平头哥玄铁906
- Jim Keller入职RISC-V初创公司

---

## 重点聚焦

### 相关周报
- ![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png "屏幕截图.png") [semi engineering](https://semiengineering.com/) Week In Review: 
  - Blog Review：
  - Design, Low Power：
  - Manufacturing, Test：
  - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：[4月 / 第三期 / 2021](http://tinylab.org/tinylab-weekly-4-3rd-2021/)
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅农亚历山大: [RISC-V 双周报](https://www.rvmcu.com/article-show-id-557.html)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：

---

## 技术动态

"[SiFive Tapes Out Their First 5nm RISC-V Processor Core](https://www.phoronix.com/scan.php?page=news_item&px=SiFive-RISC-V-5nm) (From www.phoronix.com 2021.05.02)"
> SiFive的OpenFive业务部门今天宣布，他们已经使用台积电的5纳米工艺完成了RISC-V处理器内核的首次带出。

"[OpenFive Tapes Out SoC for Advanced HPC/AI Solutions on TSMC 5nm Technology](https://openfive.com/Pressrelease/openfive-tapes-out-soc-for-advanced-hpc-ai-solutions-on-tsmc-5nm-technology/)  (From openfive.com 2021.04.13)"
> OpenFive HBM3和Die-2-Die（D2D）接口与SiFive E76 RISC-V CPU内核相结合，可实现高性能的芯片和基于2.5D的系统级芯片（SoC）设计。

"[全志科技宣布推出D1处理器，这是全球首颗量产的搭载平头哥玄铁906 RISC-V的应用处理器](https://laoyaoba.com/n/778422) (From laoyaoba.com 2021.05.02)"

"[瑞萨电子携手SiFive共同开发面向汽车应用的新一代高端RISC-V解决方案](https://finance.sina.com.cn/tech/2021-04-22/doc-ikmyaawc1129302.shtml) (From finance.sina.com.cn 2021.05.02)"
> 瑞萨电子集团与先进RISC-V处理器和芯片解决方案供应商SiFive公司近日共同宣布，将合作开发面向汽车应用的新一代高端RISC-V解决方案。战略合作内容包括瑞萨电子将获得SiFive RISC-V核心IP组合的使用授权。

"[SiFive Intelligence针对AI应用，满足机器学习负载变化需求](https://www.ednchina.com/products/6823.html) (From www.ednchina.com 2021.05.02)"
> SiFive最近开发了新的以AI为中心的指令，专门针对加速通用机器学习处理而进行了调整。SiFive Intelligence可以以满足现代机器学习工作负载的不断变化的需求，并创建加速的处理平台。

"[X86架构大师、处理器游侠-吉姆·凯勒“游向”RISC-V](https://www.eet-china.com/mp/a47502.html) (From www.eet-china.com 2021.05.02)"
> （Jim Keller）在去年离开了英特尔公司半年之后，已经加入了多伦多的一家开发人工智能芯片的初创公司Tenstorrent。  
> "[Jim Keller也看上了RISC-V](http://news.moore.ren/industry/274134.htm) (From news.moore.ren 2021.05.02)"

"[基于RISC-V芯片的“树莓派”即将面世](http://news.moore.ren/industry/273186.htm) (From news.moore.ren 2021.05.02)"
> 嵌入式系统博客CNX-Software上的帖子称，Raspberry Pi潜在的新竞争对手将很快面世。那就是基于开源RISC-V指令集体系结构芯片打造的Allwinner D1开发板。据报道，该开发版是一个信用卡大小的单板计算机，具有运行在1GHz的单核XuanTie C906 64位RISC-V CPU和1GB DDR3 RAM。

---

### Linux动态

"[SiFive FU740 PCIe Support Queued Ahead Of Linux 5.13](https://www.phoronix.com/scan.php?page=news_item&px=SiFive-FU740-PCIe-Linux-5.13) (From www.phoronix.com 2021.05.02)"  
KVM RISC-V Support：https://lwn.net/Articles/851468/  
RISC-V: Add kexec/kdump support: https://lwn.net/Articles/851646/

### 社区动态

"[【校企共建】向量处理与RISC-V向量拓展](https://dmne.sjtu.edu.cn/dmne/blog/2021/04/21/%E3%80%90%E6%A0%A1%E4%BC%81%E5%85%B1%E5%BB%BA%E3%80%91%E5%90%91%E9%87%8F%E5%A4%84%E7%90%86%E4%B8%8Erisc-v%E5%90%91%E9%87%8F%E6%8B%93%E5%B1%95/) (From dmne.sjtu.edu.cn 2021.05.02)"

---

## 产业风云

"[万向区块链“区块链+”技术部高级总监屠文慧受邀参加 RISC-V 基金会安全专题论坛](https://www.chainnews.com/articles/711976433746.htm) (From www.chainnews.com 2021.05.02)"

"[兆易创新2020年营收净利双增](https://laoyaoba.com/n/778203) (From laoyaoba.com 2021.05.02)"
> GD32VF103 凭借其基于开源RISC-V指令集架构的新型Bumblebee 处理器内核，在国际顶级展会—Embedded World 2020上赢得了硬件类奖项的殊荣。

"[2021 展锐开启技术提升年](https://www.ednchina.com/products/6803.html) (From www.ednchina.com 2021.05.02)"
> 在紫光展锐创见未来大会上，展锐CEO楚庆阐述了展锐的产业责任与定位—做数字世界的生态承载者。

"[ARM中国“宫斗”再升级](https://finance.sina.com.cn/tech/2021-04-17/doc-ikmyaawc0138427.shtml) (From finance.sina.com.cn 2021.05.02)"
> ARM中国已对此前由ARM中国董事会指定并空降至该公司的三名高管提起诉讼。外媒认为，该诉讼旨在让ARM中国的控制权继续保留在现任首席执行官吴雄昂的手中。

### 评论文集

"[华为获取ARM V9授权面临困扰，中国该全力发展Risc-V了](https://www.sohu.com/a/461393503_187675)  (From www.sohu.com 2021.05.02)"
> ARM已推出全新的V9架构，目前尚未明确华为是否能获得V9的授权，这对中国芯片产业已造成重大影响，在如此情况下，中国发展Risc-V架构才是最好的应对办法。

"[手持开源“大刀”，RISC-V如何从“低端量变”到“高端质变”？](https://www.163.com/dy/article/G85D0DGF0520LN3U.html?f=post2020_dy_recommends) (From www.163.com 2021.05.02)"

"[RISC-V一众开源硬件们，能否复制Linux的成功？](https://www.eet-china.com/news/202104160452.html) (From www.eet-china.com 2021.05.02)"
> 当前的RISC-V是一种开放的架构，作为一项标准提供，让开发人员可以自由、灵活且迅速地用它来进行产品设计。但另一方面，硬件更加复杂，有多层堆栈，因此不像软件包那样简单。本文针对RISC-V硬件生态系统的利益相关业者进行了调查，描述开源软硬件的相似性、导入的障碍以及支持社群和生态系统的重要性，最后并探讨开源硬件对商业芯片制造商的意义。

"[龙芯中科发布自主CPU指令系统架构，下一步生态建设要怎么走？](https://www.36kr.com/p/1183080676491777) (From www.36kr.com 2021.05.02)"
> 生态搭建上，据介绍，龙芯在全国成立8个子公司，与60余家公司共同组建龙芯生态适配服务产业联盟，实现资源互动。目前龙芯有超过1000多家合作伙伴，超过2万的开发人员，打造全产业链生态体系。

---

## 其他动态

### 项目推荐

https://gitee.com/liangkangnan/tinyriscv
> 本项目实现的是一个单核32位的小型RISC-V处理器核(tinyriscv)，采用verilog语言编写。设计目标是对标ARM Cortex-M3系列处理器。

### 博文推荐
"[龙芯LoongArch自主指令集](https://jishuin.proginn.com/p/763bfbd562b3) (From jishuin.proginn.com 2021.05.02)"  
"[痞子衡嵌入式：RISC-V指令集架构MCU开发那些事 - 索引](https://blog.51cto.com/u_12094353/2713906) (From blog.51cto.com 2021.05.02)"  
"[开源RISC-V处理器(蜂鸟E203)学习（一）修改仿真环境（vcs2018+verdi2018）](https://www.riscv-mcu.com/community-topic-id-488.html) (From www.riscv-mcu.com 2021.05.02)"  
" [对于RISC-V，我们对它有些误解？](https://bbs.21ic.com/icview-3126014-1-1.html) (From bbs.21ic.com 2021.05.02)"

### 其它声音
"[美国 RISC-V 开源 就是一个陷阱 犹如谷歌开源断供华为](http://bbs.wuyou.net/forum.php?mod=viewthread&tid=424936&extra=) (From bbs.wuyou.net 2021.05.02)"

---

## 一周论文

### [Implementation of a Hebbian Learning Algorithm as an Accelerator to a RISC-V-Based Processor](http://books.google.com/books?hl=en&lr=lang_en&id=7JQoEAAAQBAJ&oi=fnd&pg=PA241&dq=risc-v&ots=G8lduWVUuv&sig=J98tHzPy2O_B32GeDCj1zL1hGD8)
R Kubsad, SV Siddamal, PA Kurkuri - Advances in VLSI, Signal Processing, Power …, 2021

### [Implementation of Cryptographic Algorithm (One-Time-Pad) with a RISC-V Processor](http://books.google.com/books?hl=en&lr=lang_en&id=7JQoEAAAQBAJ&oi=fnd&pg=PA233&dq=risc-v&ots=G8lduWVUuv&sig=FD3AnPE4jI73eoDizX4E81XARZg)
PA Kurkuri, SV Siddamal, R Kubsad - Advances in VLSI, Signal Processing, Power …, 2021

### [PDF] [RISC architectures](http://grazulis.lt/~saulius/paskaitos/VU/kompiuteri%C5%B3-architekt%C5%ABra/skaidr%C4%97s/12_RISC-V_en.pdf)
S Gražulis

### [HTML] [PERI: A Configurable Posit Enabled RISC-V Core](https://dl.acm.org/doi/fullHtml/10.1145/3446210)
S Tiwari, N Gala, C Rebeiro, V Kamakoti - ACM Transactions on Architecture and …, 2021
> 由于Dennard's scaling的失败，在过去的十年中，利用计算机架构中的机会的突出的新范式急剧增长。两个值得关注的技术是Posit和RISC-V。Posit作为IEEE-754的可行替代方案于2017年中期推出，而RISC-V提供了一个商业级的开源指令集架构（ISA）。在这篇文章中，我们将这两种技术结合起来，提出了一个可配置的Posit启用的RISC-V核心，称为PERI 。
> 
> 文章提供了关于如何利用RISC-V的单精度浮点（"F"）扩展来支持正算的见解。我们还介绍了一个参数化和功能完整的pos浮点单元（FPU）的实现细节。该单元的可配置性和参数化特征产生了最佳的硬件，它满足了应用所要求的精度和能量/面积的权衡，这是IEEE-754实现不了的。posit FPU已经与符合RISC-V标准的SHAKTI C级内核整合为一个执行单元。为了进一步发挥posit的潜力，我们加强了posit FPU，以支持两种不同的指数大小（posit-size为32位），从而在运行时实现多精度。为了能够在PERI上编译和执行C程序，我们对GNU C编译器（GCC）进行了最小的修改，目标是RISC-V的 "F "扩展。我们在硬件面积、应用精度和运行时间方面与IEEE-754进行了比较。我们还提出了另一种方法，即利用RISC-V的自定义操作码空间，将posit FPU与RISC-V核心集成为一个加速器。

### [Lock-V: A heterogeneous fault tolerance architecture based on Arm and RISC-V](https://www.sciencedirect.com/science/article/pii/S002627142100086X)
I Marques, C Rodrigues, A Tavares, S Pinto, T Gomes - Microelectronics Reliability, 2021
> 本文介绍了Lock-V，一个异构的容错架构，它探索了一种双核锁步（DCLS）技术，以缓解单一事件破坏（SEU）和共模故障（CMF）问题。通过在指令集架构（ISA）层面上应用两个处理器架构的设计多样性，Lock-V被部署为两个版本，即Lock-VA和Lock-VM。Lock-VA的特点是Arm Cortex-A9和RISC-V RV64GC，而Lock-VM包括Arm Cortex-M3和RISC-V RV32IMA处理器。该解决方案利用现场可编程门阵列（FPGA）技术来部署RISC-V处理器的软核版本，以及专用加速器来执行错误检测和触发用于错误恢复的软件回滚系统。为了测试两个版本的Lock-V，实施了一个故障注入机制，以引起处理器寄存器中的位翻转，这是一个通常在重度辐射环境中出现的常见问题。

### [PDF] [MicroRV32-A SpinalHDL based RV32I Implementation Suitable for FPGAs](http://www.informatik.uni-bremen.de/agra/doc/work/date21_unibooth_microrv32.pdf)
S Ahmadi-Pour, V Herdt, R Drechsler
> 我们提出了一个轻量级RISC-V实现的演示，称为MicroRV32，适用于FPGAs。整个设计流程是基于开源工具的。核心本身是用基于Scala的现代SpinalHDL硬件描述语言实现的。对于FPGA流程，利用的是IceStorm套件。在iCE40 HX8K FPGA上，该设计需要大约50%的资源，可以在34.02 MHz的最大时钟频率下运行。除了内核，该设计还包括基本的外设和软件实例。MicroRV32特别适合作为研究和教育的轻型实现。完整的设计流程可以通过开源工具在Linux系统上执行，这使得该平台非常方便。

### [PDF] [Real-time detection of hardware trojan attacks on General-Purpose Registers in a RISC-V processor](https://www.jstage.jst.go.jp/article/elex/advpub/0/advpub_18.20210098/_pdf)
SW Yuan, L Li, J Yang, Y He, WT Zhou, J Li - IEICE Electronics Express, 2021
> 硬件木马（HT）是硬件安全的主要威胁之一，特别是对处理器的通用寄存器（GPR）的攻击。本文提出了一种新的方法，通过实时比较GPR和嵌入式参考模型的状态来检测HT引起的攻击。首先，用四个原则重新调整指令序列。其次，基于重新调整的指令流，建立了一个GPR的参考模型。最后，通过比较参考模型中预期的GPRs状态和处理器中采样的GPRs状态，可以发现HT诱导的对GPRs的攻击。我们将这种方法集成到PULpino的RISC-V内核中，并通过不同的程序研究了其可行性。实验结果表明，所有随机插入的HT攻击都可以在两个时钟周期的延迟内被实时检测到。

### [PDF] [CodeAPeel: An Integrated and Layered Learning Technology For Computer Architecture Courses](https://arxiv.org/pdf/2104.09502)
AY Oruc, A Atmaca, YN Sengun, A Yeniyol - arXiv preprint arXiv:2104.09502, 2021
> 在本文中，我们介绍了一种多功能、多层次的技术，以帮助支持计算机结构概念的教学和学习。这项被称为CodeAPeel的技术已经以一种特殊的形式实现，通过对取-解码-执行过程的全面模拟以及CPU寄存器、RAM、VRAM、STACK存储器和通用指令集结构的各种控制寄存器的行为动画，来描述汇编和机器层的指令处理。与大多数教育性的CPU模拟器不同，CodeAPeel并不模拟真正的处理器，如MIPS或RISC-V，但它被设计和实现为一个通用的RISC指令集架构，同时具有标量和矢量操作数，使其成为一个双架构，支持Flynn的SISD和SIMD指令集架构。

### [PDF] [Masked Accelerators and Instruction Set Extensions for Post-Quantum Cryptography](https://eprint.iacr.org/2021/479.pdf)
T Fritzmann, M Van Beirendonck, DB Roy, P Karl…
> 侧信道攻击可以破坏数学上安全的密码系统，导致应用密码学中的一个主要问题。虽然后量子密码学（PQC）的密码分析和安全评估已经得到了越来越多的研究，但仍然缺乏对有效的侧信道反措施的成本分析。在这项工作中，我们提出了一种适合NIST PQC定义者Kyber和Saber的屏蔽式硬件/软件代码设计，以适应其不同的特点。其中，我们提出了一种新型的非二乘法模数的屏蔽式密码文本压缩算法。为了加速线性性能瓶颈，我们开发了一个通用的数字理论变换（NTT）乘法器，与以前发表的加速器相比，它也是eﬃ古的，适用于不基于NTT的方案。对于关键的非线性运算，开发了屏蔽的HW加速器，允许使用RISC-V指令集扩展的安全执行。我们的实验结果显示，与最新优化的ARM Cortex-M4实现相比，Kyber（K:245k/E:319k/D:339k）的周期数减少系数为3.18，Saber（K:229k/E:308k/D:347k）为2.66。虽然Saber在密钥生成和封装方面的表现稍好，但Kyber在解封装方面有轻微的性能优势。包括随机性生成在内的一阶安全解封装操作的屏蔽开销，Kyber（D:1403k）约为4.14，Saber（D:915k）为2.63。

### [A DLL-based Body Bias Generator with Independent P-well and N-well Biasing for Minimum Energy Operation](https://www.jstage.jst.go.jp/article/transele/advpub/0/advpub_2020CTP0002/_article/-char/ja/)
K NAGAI, J SHIOMI, H ONODERA - IEICE Transactions on Electronics, 2021
> 本文提出了一种基于DLL的面积和能源效率的体偏置发生器（BBG），用于最低能源操作，独立控制p阱和n阱偏置。在nMOSFET和pMOSFET之间的偏移工艺条件下，BBG可以使目标电路的总能耗最小化。所提出的BBG由数字单元组成，与基于单元的设计兼容，可以在不增加电源电压的情况下实现节能和节省面积。一个测试电路在65纳米FDSOI工艺中实现。在同一芯片上使用32位RISC处理器的测量结果表明，所提出的BBG可以在3%的能量损失范围内将能量消耗降到接近最低。在这种情况下，BBG的能量和面积开销分别为0.2%和0.12%。

### [PDF] [VHDL Design for Out-of-Order Superscalar Processor of A Fully Pipelined Scheme](https://www.koreascience.or.kr/article/JAKO202111037332629.pdf)
J Lee - The Journal of the Institute of Internet, Broadcasting …, 2021
> 今天，超标量处理器是多核处理器、SoC和GPU的基本单元或重要组成部分。因此，这些系统必须采用高性能的失序超标量处理器，以使其性能最大化。超标量处理器通过利用重排缓冲器和预留站，在流水线方案中动态地安排指令，在每个周期内获取、发出、执行和回写多条指令。在本文中，用VHDL设计了一个完全流水线外的超标量处理器，并用GHDL进行了验证。作为仿真的结果，由ARM指令组成的程序被成功执行。

### [PDF] [SME: A High Productivity FPGA Tool for Software Programmers](https://arxiv.org/pdf/2104.09768)
CJ Johnsen, A Thegler, K Skovhede, B Vinter - arXiv preprint arXiv:2104.09768, 2021
> 几十年来，CPU一直是大多数计算中使用的标准模型。虽然CPU在某些领域确实很出色，但异构计算，如可重构硬件，在并行化、性能和功耗等方面正显示出越来越大的潜力。这在有利于深度流水线或严格延迟要求的问题上尤为突出。然而，由于这些问题的性质，它们可能很难编程，至少对软件开发人员来说是如此。同步消息交换（SME）是一个运行环境，它允许用C#语言开发、测试和验证FPGA设备的硬件设计，并可使用现代调试和代码功能。其目的是为软件开发人员创建一个框架，使其能够轻松地实现FPGA设备的系统，而不必获得繁重的硬件编程知识。本文介绍了SME模型的简短介绍，以及SME的新更新。最后，将介绍一些学生项目和实例，以显示在SME中创建相当复杂的结构是如何可能的，即使是没有硬件经验的学生。

### [PDF] [Implementing CNN Layers on the Manticore Cluster-Based Many-Core Architecture](https://arxiv.org/pdf/2104.08009)
A Kurth, F Schuiki, L Benini - arXiv preprint arXiv:2104.08009, 2021
> 本文介绍了基本卷积神经网络（CNN）层在基于Manticore集群的多核架构上的实现，并讨论了它们的特点和权衡。

### [PDF] [Lightweight of ONNX using Quantization-based Model Compression](https://www.koreascience.or.kr/article/JAKO202111037332625.pdf)
D Chang, J Lee, J Heo - The Journal of the Institute of Internet, Broadcasting …, 2021
> 由于深度学习和人工智能的发展，该模型的规模越来越大，它已经被整合到其他领域，融入我们的生活。然而，在嵌入式设备等资源有限的环境中，存在着模型应用困难和电力短缺等问题。为了解决这个问题，人们提出了一些轻量级的方法，如云化或卸载技术，减少模型中的参数数量，或优化计算。本文将所学模型的量化应用于各种框架交换格式中使用的ONNX模型，将神经网络结构和推理性能与现有模型进行比较，并分析了量化的各种模块方法。实验表明，与原始模型相比，权重参数的大小被压缩，推理时间比以前更加优化。

### [PDF] [Work In Progress: Discovering Emergent Computation Across Abstraction Boundaries](https://langsec.org/spw21/papers/Boddy_LangSec21.pdf)
M Boddy, J Carciofini, T Carpenter, A Mahrer…
> 在这份进展报告中，我们描述了正在进行的DECIMAL项目的研究，该项目解决的问题是以足够的延迟性对计算机制进行建模，以推理程序的跨抽象边界的执行语义。我们开发的基于自动机的形式主义是专门用来支持对多个自动机组件的定时行为进行推理的，这些自动机为所研究系统的不同部分建模。我们展示了如何使用组合来模拟各种构造，包括跨越抽象边界的同步化、通信异步进程，以及指定可以在不同的架构和程序规格化的局部变化中通用的程序。

### [Efficient, event-driven feature extraction and unsupervised object tracking for embedded applications](https://ieeexplore.ieee.org/abstract/document/9400234/)
JP Sengupta, M Villemur, AG Andreou - 2021 55th Annual Conference on Information …, 2021
> 神经形态视觉传感器为从场景中提取突出的视觉信息提供了一种低功耗、高带宽的方式，是节能嵌入式系统的候选方案。本文概述并演示了一种用于嵌入式、事件驱动的特征提取和物体跟踪的算法，以利用这种传感器。近距离传感器数据稀疏化和信息提取分三个不同的步骤进行：内存效率高的噪声过滤，关键点的快速可扩展识别，以及随后的聚类来识别场景中的物体。该处理流程已经证明了接近100倍的数据减少率，5倍的特征提取吞吐量，以及212kAEps的事件处理率的持续存在。

### [multiPULPly: A Multiplication Engine for Accelerating Neural Networks on Ultra-low-power Architectures](https://dl.acm.org/doi/abs/10.1145/3432815)
A Eliahu, R Ronen, PE Gaillardon, S Kvatinsky - ACM Journal on Emerging …, 2021
> 计算密集型的神经网络应用往往需要在资源有限的低功耗设备上运行。许多硬件加速器已经被开发出来，以加快神经网络应用的性能并降低功耗；然而，大多数都集中在数据中心和成熟的系统上。超低功耗系统中的加速器仅被部分解决。在这篇文章中，我们介绍了multiPULPly，一个在标准的低功耗CMOS技术中整合了记忆技术的加速器，以加速超低功耗系统上神经网络推理的乘法。该加速器被指定用于PULP，一个使用低功耗RISC-V处理器的开源微控制器系统。记忆体被集成到加速器中，以便只在存储器处于活动状态时才耗电，在没有上下文恢复开销的情况下继续完成任务，并实现高度并行的模拟乘法。为了降低能耗，我们提出了新的数据流，以处理常见的乘法情况，并为我们的架构量身定做。该加速器在FPGA上进行了测试，达到了19.5 TOPS/W的峰值能效，比最先进的加速器高出1.5倍到4.5倍。

### [PDF] [Faster Coverage Convergence with Automatic Test Parameter Tuning in Constrained Random Verification](https://capra.cs.cornell.edu/latte21/paper/31.pdf)
Q Huang, H Shojaei, F Zyda, A Nazi, S Vasudevan… - 2021
> 工业环境中的受限随机验证（CRV）涉及手动参数化测试生成，这是一个昂贵而低效的过程。我们将测试参数配置表述为一个黑箱优化问题，并介绍了智能回归规划器（SRP），这是一种自动配置可调整测试参数的方法，以更好地探索输入空间并加速向覆盖率收敛。SRP中的优化器可以用两种方法驱动参数更新：一种是轻量级的随机搜索，另一种是使用夜间回归的覆盖率作为反馈的贝叶斯优化技术。我们对开源以及大型工业设计的实验评估表明，频繁的扰动和优化测试参数会导致比人类基线更高的覆盖率。重要的是，它收敛到覆盖率里程碑的速度明显快于人类基线。通过高水平的测试参数优化，我们引入了一个问题空间和机会，在工业环境中以非常低的开销实现更高的覆盖率。

### [PDF] [SISA: Set-Centric Instruction Set Architecture for Graph Mining on Processing-in-Memory Systems](https://arxiv.org/pdf/2104.07582)
M Besta, R Kanakagiri, G Kwasniewski… - arXiv preprint arXiv …, 2021
> 简单的图算法，如PageRank，最近已经成为众多硬件加速器的目标。然而，也存在着更复杂的图挖掘算法，如聚类或最大悬崖列表问题。这些算法是受内存约束的，因此可以通过硬件技术加速，如内存中处理（PIM）。然而，它们也伴随着非直接的并行性和复杂的内存访问模式。在这项工作中，我们通过一个简单但令人惊讶的强大观察来解决这个问题：对顶点集合的操作，如相交或联合，构成了许多复杂的图挖掘算法的很大一部分，并且可以在多个层面提供丰富而简单的并行性。这一观察推动了我们的跨层设计，其中我们(1)使用一种新的编程范式暴露了集合操作，(2)用精心设计的以集合为中心的ISA扩展高效地表达和执行这些操作，称为SISA，(3)使用PIM来加速SISA指令。关键的设计思想是通过将集合操作映射到两种类型的PIM来缓解SISA指令的带宽需求：代表高度顶点的位向量的DRAM内批量位计算，以及代表低度顶点的整数阵列的近内存逻辑层。以集合为中心的SISA增强算法是有效的，并且优于手工调整的基线，比既定的Bron-Kerbosch算法的速度提高了10倍以上，用于列出最大的悬臂。我们提供了10多种以SISA为中心的算法方案，说明了SISA的广泛适用性。

### [Accelerating DES and AES Algorithms for a Heterogeneous Many-core Processor](https://link.springer.com/article/10.1007/s10766-021-00692-4)
B Xing, DD Wang, Y Yang, Z Wei, J Wu, C He - International Journal of Parallel …, 2021
> 数据安全是信息安全的重点。作为一种主要方法，文件加密被采用来确保数据安全。为满足数据加密标准（DES）和高级加密标准（AES）而创建的加密算法在各种系统中被广泛使用。这些算法在计算上非常复杂，因此，加密或解密大文件的效率会急剧下降。为此，我们提出了一种优化的算法，通过在Sunway TaihuLight计算机系统的单个异构多核处理器上并行处理任务来有效地加密和解密大文件。首先，我们将串行的DES和AES程序转换到我们的实验平台。然后，我们实施了一个任务分配策略来测试转换后的算法。最后，为了优化并行化算法并提高数据传输性能，我们应用了主从通信优化、三级并行流水线和矢量化。广泛的实验表明，我们的优化算法比最先进的DES和AES的开源实现要快。与串行处理算法相比，我们并行化的DES和AES的性能分别快了近40倍和72倍。本文描述的工作利用了现有的方法，并为数据加密的未来研究方向提供了一个良好的基础。

### [CLU: A Near-Memory Accelerator Exploiting the Parallelism in Convolutional Neural Networks](https://dl.acm.org/doi/abs/10.1145/3427472)
P Das, HK Kapoor - ACM Journal on Emerging Technologies in Computing …, 2021
> 卷积/深度神经网络（CNN/DNNs）是新兴的基于人工智能的系统中快速增长的工作负载。多核系统中处理速度和内存访问延迟之间的差距影响了CNN/DNN任务的性能和能源效率。本文旨在通过提供一个简单而高效的基于近内存的加速器系统来缓解这一差距，以加速CNN推理。为了实现这一目标，我们首先设计了一个高效的并行算法来加速CNN/DNN任务。数据被分割到多个内存通道（vaults）中，以协助并行算法的执行。其次，我们设计了一个硬件单元，即卷积逻辑单元（CLU），它实现了并行算法。为了优化推理，我们设计了卷积逻辑单元，它分三个阶段工作，对数据进行分层处理。最后，为了利用近内存处理（NMP）的好处，我们在三维存储器的逻辑层上集成了同质的CLU，特别是混合存储器立方体（HMC）。这些的综合效应导致了一个高性能和高能效的CNN/DNN系统。与基于多核CPU和GPU的系统相比，所提出的系统在性能和能量降低方面取得了巨大的进步，而面积开销却只有2.37%。

### [PDF] [RDAMS: An Efficient Run-Time Approach for Memory Fault and Hardware Trojans Detection](https://www.mdpi.com/2078-2489/12/4/169/pdf)
J Wang, Y Li - Information, 2021
> 确保物联网设备和芯片在运行时的安全性已成为一项紧迫的任务，因为它们已被广泛用于人类生活。嵌入式存储器是这些设备中SoC（片上系统）的重要组成部分。如果它们在运行时受到攻击或发生故障，将带来巨大的损失。在本文中，我们提出了一个内存安全的运行时检测架构（RDAMS）来检测内存威胁（故障和硬件木马攻击）。该架构由安全检测核心（SDC）和内存包装器（MEM_wrapper）组成，前者作为 "安全大脑 "控制和执行检测程序，后者与内存互动以协助检测。我们还设计了一种低延迟的响应机制，以解决运行时检测造成的SoC性能下降问题。我们提出了一种基于块的多粒度检测方法，使设计具有灵活性，并利用FPGA的动态部分可重构（DPR）技术降低了实施成本，该技术可以根据要求进行在线检测模式重构。实验结果表明，RDAMS可以正确地检测和识别10个模型化的内存故障和两种类型的硬件木马（HTs）攻击，而不会导致系统性能大幅下降。

### [PDF] [Machine Learning Migration for Efficient Near-Data Processing](http://web.inf.ufpr.br/mazalves/wp-content/uploads/sites/13/2021/04/PDP_2021_1.pdf)
AS Cordeiro, SR dos Santos, FB Moreira, PC Santos…
> 机器学习（ML）作为一种非常有用的工具出现，用于分析当今每个科学领域产生的大量数据。同时，由于对时间和能源消耗的影响很大，计算机系统内的数据移动获得了更多关注。在这种情况下，近数据处理（NDP）架构作为一种突出的解决方案出现，通过大幅减少所需的数据移动量来增加数据。对于NDP，我们看到三种主要的方法，应用专用集成电路（ASIC），完整的中央处理单元（CPU）和图形处理单元（GPU），或矢量单元集成。然而，以前的工作在内存内执行ML算法时只考虑了ASIC、CPU和GPU。在本文中，我们提出了一种在近数据处执行ML算法的方法，使用一个通用的矢量架构，并将近数据并行性应用于KNN、MLP和CNN算法的内核。为了促进这个过程，我们还提出了一个NDP的内涵库，以减轻评估和调试任务。我们的结果显示，与高性能x86基线相比，在处理近数据时，KNN的速度提高了10倍，MLP提高了11倍，而卷积提高了3倍。

### [PDF] [Accelerator Integration for Open-Source SoC Design](http://www.cs.columbia.edu/~davide_giri/pdf/giri_ieeemicro21.pdf)
D Giri, KL Chiu, G Eichler, P Mantovani, LP Carloni
> 开源硬件社区提供了各种处理器和加速器，但将它们有效地组合成一个完整的SoC仍然是一项困难的任务。我们提出了一个设计流程，用于将加速器的硬件和软件无缝集成到一个完整的SoC中，并通过基于FPGA的快速原型设计对其进行评估。通过利用ESP，一个用于敏捷异构SoC设计的开源平台，我们展示了各种SoC设计的FPGA原型，其中包括NVIDIA深度学习加速器和Ariane RISC-V 64位处理器内核。

### [PDF] [Synch: A framework for concurrent data-structures and](https://raw.githubusercontent.com/openjournals/joss-papers/joss.03143/joss.03143/10.21105.joss.03143.pdf)
ND Kallimanis
> 最近多核机器的进步突出了简化并发编程的需要，以利用其计算能力。实现这一目标的方法之一是设计高效的并发数据结构（如堆栈、队列、哈希表等）和9种同步技术（如锁、组合技术等），这些技术在拥有大量内核的机器上表现良好。与普通的、顺序的数据结构相比，11并发的数据结构允许多个线程同时访问和/或修改它们。13 Synch是一个开源的框架，它不仅提供了一些常见的高性能14并发数据结构，而且还为研究人员提供了设计和测试高性能并发数据结构的工具。Synch框架包含了大量的并发数据结构，如队列、堆栈、组合对象、哈希表、锁等，它还提供了一个用户友好的运行时间，用于开发和测试18种并发数据结构。在其他功能中，所提供的运行时提供了轻松创建线程的功能（包括POSIX和用户级线程），测量性能的工具等。此外，所提供的并发数据结构和运行时对当代NUMA多处理器如AMD Epyc和Intel Xeon进行了高度优化。

### [Arbitrary Reduced Precision for Fine-grained Accuracy and Energy Trade-offs](https://www.sciencedirect.com/science/article/pii/S0026271421000652)
NA Said, M Benabdenbi, K Morin-Allory - Microelectronics Reliability, 2021
> 全精度浮点单元（FPU）可能是大量硬件开销（功耗、面积、内存占用等）的来源。由于一些现代应用具有对精度损失的内在容忍度，一种新的计算范式已经出现了。跨精度计算（TC）。TC提出了一些工具和技术，用精度换取能源效率。然而，这些工具大多要求开发者重写部分或全部现有的软件堆栈，这往往是不可行的、复杂的，或者需要大量的开发工作。除了它们的侵入性，TC工具只能模拟精度损失的影响，而且它们不提供相应的硬件设计，以利用模拟结果。这项工作提出了一种非侵入性的面向硬件的方法，不需要修改源代码，在汇编的低层次上应用近似。该方法可用于近似所有类型的可执行二进制文件（裸机应用程序、单/多线程用户应用程序、操作系统/RTOS等）。我们介绍AxQEMU：一个基于众所周知的QEMU动态二进制翻译器的软件。我们展示了我们的方法如何确定FP近似值对应用级结果质量（QoR）的影响，以及它如何与文献中的其他工具对接。我们提出了一个关于28纳米FD-SOI实现的硬件级案例研究，展示了如何通过浮点任意降低精度（ARP）来实现细粒度的能量/精度权衡。例如，考虑到众所周知的arclength FP应用，在使用ARP时，如果精度阈值为10位有效数字，FPU计算能耗可达到19.4%，如果精度为4位有效数字，可达到60.7%。这些节能效果与使用标准变量类型优化工具所提供的7.7%的有限节能效果相比更有优势。

### [PDF] [Compact Implementation of ARIA on 16-Bit MSP430 and 32-Bit ARM Cortex-M3 Microcontrollers](https://www.mdpi.com/2079-9292/10/8/908/pdf)
H Seo, H Kim, K Jang, H Kwon, M Sim, G Song, S Uhm - Electronics, 2021
> 在本文中，我们提出了MSP430和高级RISC机器（ARM）微控制器上的第一个ARIA区块密码。为了在目标嵌入式处理器上实现优化的ARIA实施，我们为MSP430（德州仪器，达拉斯，德克萨斯州，美国）和ARM Cortex-M3微控制器（意法半导体，日内瓦，瑞士）精心设计了ARIA的核心操作，如替代层和扩散层。特别是，ARIA区块密码中的两个字节的输入数据被连接起来，以重新构建16位明智的字。16位明智的操作与16位指令一起一次性执行，以提高16位MSP430微控制器的性能。这种方法还优化了所需的寄存器、内存访问以及对半数的操作，而不是8位的明智字的实现。对于ARM Cortex-M3微控制器，基于8×32查找表的ARIA区块密码实现通过新的内存访问进一步优化。内存访问被精确地安排，以充分利用ARM Cortex-M3微控制器的3级流水线结构。此外，通过预计算技术，计数器（CTR）的运行模式比电子密码本（ECB）的运行模式更加优化。最后，提议的ARIA在两个低端目标微控制器（MSP430和ARM Cortex-M3）上的实现分别达到了（128位安全级别的209和96），（192位安全级别的241和111），以及（256位安全级别的274和126）。与之前的工作相比，在低端目标微控制器（MSP430和ARM Cortex-M3）上的运行时间分别提高了（128位安全级别的92.20%和10.09%）、（192位安全级别的92.26%和10.87%）以及（256位安全级别的92.28%和10.62%）。与MSP430和ARM Cortex-M3微控制器的ARIA-ECB实现相比，拟议的ARIA-CTR实现分别提高了6.6%和4.0%的性能。

### [PDF] [AutoSVA: Democratizing Formal Verification of RTL Module Interactions](https://arxiv.org/pdf/2104.04003)
M Orenes-Vera, A Manocha, D Wentzlaff, M Martonosi - arXiv preprint arXiv …, 2021
> 现代的SoC设计依赖于对IP块相对于其自身规格的单独验证能力。使用SystemVerilog断言（SVA）的形式化验证（FV）是一种有效的方法，可以详尽地验证单元级的块。不幸的是，形式化验证有一个陡峭的学习曲线，需要工程上的努力，这使硬件设计师在RTL模块开发过程中不愿意使用它。我们提出了AutoSVA，这是一个自动生成FV测试平台的框架，可以验证涉及模块交互的控制逻辑的有效性和安全性。我们在广泛使用的开源硬件项目的死锁关键模块上证明了AutoSVA的有效性和高效性。

### [PDF] [GhostMinion: A Strictness-Ordered Cache System for Spectre Mitigation](https://arxiv.org/pdf/2104.05532)
S Ainsworth - arXiv preprint arXiv:2104.05532, 2021
> 失序投机是一种自20世纪九十年代初以来无处不在的技术，仍然是一个基本的安全缺陷。通过像Spectre和Meltdown这样的攻击，攻击者可以在一个完全正确的程序中欺骗受害者，通过错误推测的执行效果泄露其秘密，而这种方式对于程序员的模型是完全不可见的。这对应用程序的沙盒和进程间通信有严重的影响。设计有效的缓解措施，以保持失序执行的性能，一直是一个挑战。文献中的猜测隐藏技术已被证明不能全面地关闭这种通道，允许对手重新设计攻击。强有力的、精确的保证是必要的，但同时，缓解措施必须达到高性能才能被采用。我们提出了Strictness Ordering，这是一个新的约束系统，显示了我们如何全面地消除瞬时的旁路攻击，同时仍然允许复杂的投机和投机指令间的数据转发。然后，我们介绍了GhostMinion，这是一个使用各种新技术构建的缓存修改，旨在以仅2.5%的开销提供Strictness Order。

### [PDF] [Fixed-Posit: A Floating-Point Representation for Error-Resilient Applications](https://arxiv.org/pdf/2104.04763)
V Gohil, S Walia, J Mekie, M Awasthi - arXiv preprint arXiv:2104.04763, 2021
> 今天，几乎所有的计算机系统都使用IEEE-754浮点来表示实数。最近，posit被提议作为IEEE-754浮点的替代品，因为它有更好的精度和更大的动态范围。posit的可定义性，即制度和指数位数的变化，对其采用起到了阻碍作用。为了克服这一缺陷，我们提出了制度位和指数位数量固定的固定posit表示，并提出了固定posit乘法器的设计。我们在AxBench和OpenBLAS基准以及神经网络的抗错应用中评估了固定位乘法器。与正则乘法器相比，拟议的固定位置乘法器在功率、面积和延迟方面分别节省了47%、38.5%和22%，与32位IEEE-754乘法器相比，在功率、面积和延迟方面分别节省了70%、66%和26%。在OpenBLAS和AxBench工作负载中，这些节省伴随着最小的输出质量损失（1.2%的平均相对误差）。此外，对于像ImageNet上的ResNet-18这样的神经网络，我们观察到使用ﬁxed-posit乘法器的准确性损失可以忽略不计（0.12%）。
 
### [PDF] [DICE⋆: A Formally Verified Implementation of DICE Measured Boot](https://www.usenix.org/system/files/sec21fall-tao.pdf)
Z Tao, A Rastogi, N Gupta, K Vaswani, AV Thakur
> 测量性启动是一类重要的启动协议，它确保设备信任链中的每一层硬件和软件都被测量，并可靠地记录测量结果，以供后续验证。本文介绍了DICE，一个正式的规范，以及DICE的正式验证实现，一个行业标准的测量启动协议。DICE被证明在功能上是正确的，对内存是安全的，并能抵抗基于时间和缓存的侧信道。DICE的一个关键组成部分是一个经过验证的X.509片段的证书创建库。我们已经将DICE集成到STM32H753ZI微控制器的启动软件中。我们的评估表明，与现有的未验证的实施方案相比，使用完全验证的实施方案对代码大小和启动时间的影响很小甚至没有。

---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |