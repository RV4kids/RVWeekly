
本期概要
- 英特尔收购SiFive
- 平头哥获奖
- 96核长安链

---

# 重点聚焦

#### 英特尔收购SiFive

包永刚："[英特尔为什么会对RISC-V明星公司SiFive感兴趣？](https://www.leiphone.com/category/chipdesign/IaSVNSvXXXg9xB9D.html) (From www.leiphone.com 2021.07.02)"
> 导语：这或许能够部分解答为什么英特尔会考虑20亿美元收购SiFive。

相关新闻：

- "[英特尔又做了一个违背祖宗的决定：布局RISC-V](https://www.36kr.com/p/1263478177388416) (From www.36kr.com 2021.07.02)"
- "[欲将RISC-V架构收入囊中，英特尔拟花费20亿美元收购SiFive](https://posts.careerengine.us/p/60c2eb9ebfad086935b4d7f7?nav=post_hottest&p=60c2ebb380e090697baeb2a2) (From posts.careerengine.us 2021.07.02)"
- "[曝英特尔拟 20 亿美元收购 SiFive：后者为基于 RISC-V 指令集架构的芯片设计初创公司](https://www.ithome.com/0/556/726.htm) (From www.ithome.com 2021.07.02)"
- "[英特尔计划收购SiFive 进一步巩固“护城河”？](https://www.cnbeta.com/articles/tech/1139935.htm) (From www.cnbeta.com 2021.07.02)"
- "[英特尔有意收购SiFive 改善芯片技术击败Arm](https://zhuanlan.zhihu.com/p/379978690) (From zhuanlan.zhihu.com 2021.07.02)"
- "[都怪苹果(AAPL.US)？](https://finance.sina.com.cn/stock/hkstock/hkstocknews/2021-06-12/doc-ikqcfnca0614195.shtml) (From finance.sina.com.cn 2021.07.02)"
- "[英特尔(INTC.US)CEO：芯片业将迎来黄金十年，对Nvidia(NVDA.US)收购Arm表示担忧](https://finance.sina.com.cn/stock/hkstock/hkstocknews/2021-06-17/doc-ikqcfnca1484008.shtml) (From finance.sina.com.cn 2021.07.02)"

"[英特尔CEO称赞美国参议院批准520亿美元用于芯片研发](https://www.eet-china.com/news/202106110755.html) (From www.eet-china.com 2021.07.02)"
> 英特尔首席执行官帕特·盖尔辛格 (Pat Gelsinger) 称赞美国参议院批准520亿美元用于国内半导体制造和研究，称其是“加强”美国在该行业领先地位的“重要一步”。

#### 平头哥获浙江省技术发明一等奖

"[平头哥获浙江省技术发明一等奖](https://finance.sina.com.cn/tech/2021-06-15/doc-ikqcfnca1174110.shtml) (From finance.sina.com.cn 2021.07.02)"  
"[国产嵌入式处理器阿里巴巴平头哥玄铁907/910实现关键技术突破](https://www.eet-china.com/kj/63628.html) (From www.eet-china.com 2021.07.02)"
> 平头哥从最基础的指令集切入，突破了变长指令编码、对称编码、锚地址立即数等关键技术  
> 5月18日，平头哥发布旗下玄铁系列新款处理器——玄铁907，该处理器对开源RISC-V架构进行优化设计，兼顾高性能及低功耗特点，可应用于MPU（微处理器）、智能语音、导航定位、存储控制等领域。  
> 据悉，玄铁907对开源RISC-V架构进行了扩展增强，采用5级流水线设计，最高工作频率超过1GHz，单位性能可达3.8 Coremark/MHz。  
> 该处理器还首次实现了RISC-V最新的DSP指令标准，拥有出色的计算能效，适用于存储、工业控制等对计算性能要求较高的实时计算场景。

#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅亚农历山大: （[RVMCU](https://www.rvmcu.com/article-show-id-557.html)，[wechat](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzU2MjM4MTcyNQ==&action=getalbum&album_id=1713615520249217030&scene=173&from_msgid=2247489824&from_itemidx=1&count=3&nolastread=1#wechat_redirect)）：
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：


---

## 技术动态

"[官方确认，RISC-V将走向HPC和AI](https://posts.careerengine.us/p/60c5701e8d8ea84cc7866e0e?from=latest-posts-panel&type=title) (From posts.careerengine.us 2021.07.02)"
> 目前，RISC-V SIG-HPC的邮件列表中有141名成员，研究、学术和芯片行业有 10 名活跃成员。不断壮大的SIG的关键任务是提出各种新的HPC特定指令和扩展，并与其他技术小组合作，确保为不断发展的ISA考虑HPC要求。作为这项任务的一部分，SIG需要定义AI/HPC/边缘要求，并绘制一条特征和能力路径，以达到 RISC-V 与 Arm、x86 和其他架构竞争的程度。  
> 2021年，该集团将专注于HPC软件生态系统

"[CSEM工程师打造可依赖太阳能运行的低功耗AI片上系统](https://www.cnbeta.com/articles/tech/1141877.htm) (From www.cnbeta.com 2021.07.02)"
> CESM 工程师们，刚刚提出了一个新颖的解决方案 —— 让 AI 运算能够在基于微型电池（或光伏发电板）的本地小尺寸装置上运行  
> 将在今年 6 月于京东举办的 2021 VLSI 电路研讨会上展示现有的装置，上面的集成电路能够执行复杂的 AI 操作，比如面部 / 语音 / 手势识别，以及心脏监测

#### Linux动态

"[Linux 上的 RISC-V 将支持 Transparent Hugepages](https://www.oschina.net/news/145823/risc-v-linux-will-support-thp) (From www.oschina.net 2021.07.02)"
> 该补丁正在排队进入“next” 分支，如无意外，将会在未来几周内的 Linux 5.14 周期中合并到主线。

"[Linux 5.13-rc6发布 新版内核发布周期临近](https://www.cnbeta.com/articles/tech/1140255.htm) (From www.cnbeta.com 2021.07.02)"
> 变化规模较小，有一些RISC-V修复

#### RV学习

"[从零开始学RISC-V之CSR访问](https://zhuanlan.zhihu.com/p/379723433) (From zhuanlan.zhihu.com 2021.07.02)"  
"[RISC-V工具链搭建](https://www.riscv-mcu.com/community-topic-id-757.html) (From www.riscv-mcu.com 2021.07.02)"  
"[RISC-V学习笔记【存储器与总线】](https://icode9.com/content-4-1022714.html) (From icode9.com 2021.07.02)"

---

## 产业风云

#### 产品动态

"[Cortus 为高效能运算(HPC) 开发次世代高端 RISC-V CPU内核](https://www.cortus.com/zh/2021/06/16/cortus-%E4%B8%BA%E9%AB%98%E6%95%88%E8%83%BD%E8%BF%90%E7%AE%97hpc-%E5%BC%80%E5%8F%91%E6%AC%A1%E4%B8%96%E4%BB%A3%E9%AB%98%E7%AB%AF-risc-v-cpu%E5%86%85%E6%A0%B8/) (From www.cortus.com 2021.07.02)"
> 这个为 eProcessor 项目所贡献的处理器，包括完整的缓存一致性(Cache coherency)设计，以此支持具有非常多处理器的超大型运算系统，并适用于全规模的超级计算机。

"[如何利用RISC-V SoC同时处理5G与AI工作负载？](https://www.eet-china.com/mp/a57361.html) (From www.eet-china.com 2021.07.02)"
> EdgeQ利用5G和AI之间的相似性来设计可以同时加速两者的芯片

"[国产化机遇正当时，景略半导体深潜车载和工业网络通信芯片蓝海市场](https://laoyaoba.com/n/783413) (From laoyaoba.com 2021.07.02)"
> 而在2021年世界半导体大会期间，基于多年来在网络通信行业的技术积累，景略半导体发布了BlueWhale™全新一代L2/L2+网络交换机Switch芯片技术。  
> 景略半导体采用RISC-V架构，并成为行业首个应用RISC-V内核的L2/L2+ Switch的芯片厂商。  
> Switch芯片则是对帧数据的内容做处理。Switch芯片技术的发布，也意味着景略半导体在以太网芯片这一领域打造出完整的解决方案。  
> 这一技术结合EtherNext™高速以太网PHY技术架构，聚焦L2/L2+车载和工业交换机市场，使得景略半导体能够快速推出针对不同应用的Switch和PHY产品组合，满足数百亿美元规模车载、工业和网通市场高速增长的需求。

"[长安链发布全球首款自主可控96核区块链芯片](http://www.chinaemail.com.cn/blog/content/13120/%E9%95%BF%E5%AE%89%E9%93%BE%E5%8F%91%E5%B8%83%E5%85%A8%E7%90%83%E9%A6%96%E6%AC%BE%E8%87%AA%E4%B8%BB%E5%8F%AF%E6%8E%A796%E6%A0%B8%E5%8C%BA%E5%9D%97%E9%93%BE%E8%8A%AF%E7%89%87.html) (From www.chinaemail.com.cn 2021.07.02)"
> 北京微芯区块链与边缘计算研究院发布全球首款96核区块链专用加速芯片和“长安链·协作网络”等重大成果。本次发布的区块链专用加速芯片基于RISC-V开放指令集定制设计专用处理器内核，保障核心技术自主可控。超高性能区块链专用加速板卡可将区块链数字签名、验签速度提升20倍，区块链转账类智能合约处理速度提升50倍，为突破大规模区块链网络交易性能瓶颈提供硬科技支撑。

"[芯旺微电子入选2020中国IC独角兽企业](https://laoyaoba.com/n/783376) (From laoyaoba.com 2021.07.02)"
> 6月9日，以“创新求变，同芯共赢”为主题的2021世界半导体大会
> 芯旺微电子专注基于自主KungFu处理器架构的高可靠、高品质MCU器件的研发设计，是国内最早面向汽车和工业领域的芯片设计公司之一。KungFu内核是自主嵌入式处理器IP 内核，是继ARM、MIPS、RISC-V之后的又一商用嵌入式处理器内核架构，现已发展出多系列的内核IP，包括KungFu8-8位MCU内核，KungFu32-32位通用MCU内核，KungFu32D-32位DSP内核及KungFu32DA-多核处理器内核。
> 产品应用覆盖汽车、工业、AIoT三大领域。核心产品车规级MCU通过AEC-Q100品质认证，近期推出的车规级 32位MCU KF32A156，主要应用于车身车载模块控制，拥有512KB Flash、64KB RAM，支持2路CANFD，同时工作范围达到了Grade 1（-40～125℃）车规等级。KF32A156量产后将使芯旺微的车规产品从现有MCU覆盖30%车身控制单元模块扩展到70%的范围。

#### 企业动态

"[飞天是如何从“骗子”变成“娇子”的？](https://www.ofweek.com/ai/2021-06/ART-201712-8500-30503660.html) (From www.ofweek.com 2021.07.02)"
> 2008年,阿里巴巴确立云计算战略,开启了自研飞天的征程。

"[英特尔为RISC-V前景打光 晶心科成受益者](https://www.chinatimes.com/cn/realtimenews/20210617002972-260410?chdtv) (From www.chinatimes.com 2021.07.02)"
> "英特尔为RISC-V前景打光 晶心科成受益者 (From www.chinatimes.com 2021.07.02)"

---

## 其他动态

#### 评论文集

"[AI是RISC-V进军数据中心的特洛伊木马](https://zhuanlan.zhihu.com/p/380577159) (From zhuanlan.zhihu.com 2021.07.02)"
> 译自：Nicole Hemsoth的AI Is RISC-V’s Trojan Horse into the Datacenter

"[中国芯迎来“救兵”，MIPS正式入局，RISC-V时代正式来临](https://www.sohu.com/a/472328111_121031316)  (From www.sohu.com 2021.07.02)"  
"[华为、英特尔都来“搅局”，RISC-V的机会越来越大了？](http://m.elecfans.com/article/1638945.html) (From m.elecfans.com 2021.07.02)"

#### 项目推荐

- "[无第三方库不到5000行C语言 risc-v虚拟机juicevm](https://zhuanlan.zhihu.com/p/377048003) (From zhuanlan.zhihu.com 2021.07.02)"

> https://github.com/juiceRv/JuiceVm  
> juice vm诞生于2020年，以实现可运行最新kernel主线的RISC-V最小虚拟机为目标而诞生的，设计之初秉承着可以在 RAM 只有 百KB 级别的平台上运行，不引入除了c99标准外的第三方依赖。

#### 博文推荐

"[华为云openCPU智联模组](https://bbs.huaweicloud.com/blogs/246215) (From bbs.huaweicloud.com 2021.07.02)"
> 基于上海汉枫WIFI模组（型号HF-LPX70，RISC-V架构）直接进行openCPU二次开发，通过汉枫模组内置的hwcloud IoTLink协议（mqtt/lwm2m/coap等），实现上电即上华为云。

"[全志首款RISCV芯片D1终极探索](https://www.zhihu.com/column/c_1382754923101081600) (From www.zhihu.com 2021.07.02)"
> 全志第一款RISCV SOC，采用平头哥的玄铁906，单核RISCV64,主频1.008GHZ,外设很齐全，基本沿用全志各种成熟IP外设，算比较能让人心动的一颗芯片。


#### 社区动态

- "[分享 移植E203中遇到的时序问题](https://www.rvmcu.com/community-topic-id-740.html) (From www.rvmcu.com 2021.07.02)"  
- "[基于RISCV的AI芯片设计](https://www.riscv-mcu.com/community-topic-id-741.html) (From www.riscv-mcu.com 2021.07.02)"  

> 对于比赛总结了一些方法和建议提供给大家

"[ARM的今天就是RISC-V的明天](https://bbs.21ic.com/icview-3139932-1-1.html) (From bbs.21ic.com 2021.07.02)"

---

## 一周论文

#### [PDF] [DMR: An Out-of-Order Superscalar General-Purpose CPU Core Based on RISC-V](https://crad.ict.ac.cn/EN/article/downloadArticleFile.do?attachType=PDF&id=4436)
S Caixia, Z Zhong, D Quan, S Bingcai, W Yongwen… - Journal of Computer …, 2021
> DMR is a RISC-V based out-of-order superscalar general-purpose CPU core from the College of Computer Science and Technology, National University of Defense Technology. Three privilege levels, user-mode, supervisor-mode and machine …

#### [PDF] [Extending the RISC-V ISA for exploring advanced reconfigurable SIMD instructions](https://arxiv.org/pdf/2106.07456)
P Papaphilippou, PHJ Kelly, W Luk - arXiv preprint arXiv:2106.07456, 2021
> This paper presents a novel, non-standard set of vector instruction types for exploring custom SIMD instructions in a softcore. The new types allow simultaneous access to a relatively high number of operands, reducing the instruction count where …

#### [RISC-V Benchmarking for Onboard Sensor Processing](http://d-scholarship.pitt.edu/40400/)
MJ Cannizzaro - 2021
> When designing embedded systems, especially for space-computing needs, finding the ideal balance between size, weight, power, and cost (SWaP-C) is the primary goal in the processor selection process. One variable that can have a significant …

#### [PDF] [Leveraging RISC-V to build an open-source (hardware) OS framework for reconfigurable IoT devices](https://www.researchgate.net/profile/Miguel-Silva-48/publication/352258188_Leveraging_RISC-V_to_build_an_open-source_hardware_OS_framework_for_reconfigurable_IoT_devices/links/60c0e2cf4585157774c20960/Leveraging-RISC-V-to-build-an-open-source-hardware-OS-framework-for-reconfigurable-IoT-devices.pdf)
M Silva, T Gomes, S Pinto
> With the growing interest in RISC-V systems and the endless possibilities of creating customized hardware architectures, we introduce the first proof of concept (PoC) implementation of ChamelIoT, the first open-source agnostic hardware operating …

#### [PDF] [A Methodology For Creating Information Flow Specifications of Hardware Designs](https://arxiv.org/pdf/2106.07449)
C Deutschbein, A Meza, F Restuccia, R Kastner… - arXiv preprint arXiv …, 2021
> … specifications. We develop a tool, Isadora, to evaluate our methodology. We demonstrate Isadora may define the information flows within an access control module in isolation and within an SoC and over a RISC-V design. Over …

#### [Development of Network Information Technology in the World](https://link.springer.com/chapter/10.1007/978-981-33-6938-2_2)
… Academy of Cyberspace Studies yangshuzhen@ cac … - World Internet Development …, 2021
> … access to participants. Currently, open source hardwares with wide attention are mainly in the fields of RISC-V and MIPS. Driven by RISC-V Open Source Foundation, RISC-V Instruction Set is developing fast. In controlling field …

#### [PDF] [First-Order Masked Kyber on ARM Cortex-M4](https://csrc.nist.gov/CSRC/media/Presentations/first-order-masked-kyber-on-arm-cortex-m4/images-media/session-4-heinz-first-order-masked-kyber.pdf)
D Heinz, MJ Kannwischer, G Land, T Pöppelmann…
> … CCA2-secure Ring-Learning with errors (RLWE) scheme [BDK+20] presents a first-order masked implementation of Saber (Cortex-M4) [BGR+21] presents
masked versions of Kyber on Cortex-M0 [FBR+21] presents masked hardware …

#### [INTROSPECTRE: A Pre-Silicon Framework for Discovery and Analysis of Transient Execution Vulnerabilities](https://conferences.computer.org/iscapub/pdfs/ISCA2021-4ghucdBnCWYB7ES2Pe4YdT/333300a874/333300a874.pdf)
M Ghaniyoun, K Barber, Y Zhang, R Teodorescu
> … We implement INTROSPECTRE on an RTL simulator and use it to perform transient leakage analysis on the RISC-V BOOM processor … We implement
INTROSPECTRE on top of Verilator [1], an open-source RTL simulator and …

#### SNAFU: [An Ultra-Low-Power, Energy-Minimal CGRA-Generation Framework and Architecture](https://conferences.computer.org/iscapub/pdfs/ISCA2021-4ghucdBnCWYB7ES2Pe4YdT/333300b027/333300b027.pdf)
GGAO Atli, K Mai, B Lucia, N Beckmann
> … We further present SNAFU-ARCH, a complete ULP system that integrates an instantiation of the SNAFU fabric alongside a scalar RISC-V core and
memory … We describe SNAFU-ARCH, a complete ULP system-on-chip with …

#### [Failure Sentinels: Ubiquitous Just-in-time Intermittent Computation via Low-cost Hardware Support for Voltage Monitoring](https://conferences.computer.org/iscapub/pdfs/ISCA2021-4ghucdBnCWYB7ES2Pe4YdT/333300a665/333300a665.pdf)
H Williams, M Moukarzel, M Hicks
> … to current solutions. We also implement a RISC-V-based FPGA prototype that validates our design space exploration and shows the overheads of incorporating Failure Sentinels into a system-on-chip. I. INTRODUCTION Continuous …

#### [Superconducting Computing with Alternating Logic Elements](https://conferences.computer.org/iscapub/pdfs/ISCA2021-4ghucdBnCWYB7ES2Pe4YdT/333300a651/333300a651.pdf)
G Tzimpragos, J Volk, A Wynn, JE Smith, T Sherwood
> … The resulting systems are shown to deliver energy-delay product (EDP) gains over conventional SFQ even with pipeline hazard ratios (HR) below 1%.
For hazard ratios equal to 15% and 20% and a design resembling a …

 

#### [PDF] [Unifying Performance and Security Evaluation for Microarchitecture Design Exploration](https://repository.library.northeastern.edu/files/neu:bz60xp35s/fulltext.pdf)
G Knipe - 2021
> … 5 2.2 Simple 5-stage RISC-V pipelined data-path (from Hennessy and Patterson [2]). . . 6 … This thesis presents Yori, a RISC-V microarchitecture simulator that aims to enable computer architects to evaluate …

#### [PDF] [Integration Verification Across Software and Hardware for a Simple Embedded System](https://samuelgruetter.net/assets/lightbulb_pldi21.pdf)
A Erbsen, S Gruetter, J Choi, C Wood, A Chlipala - PLDI, 2021
> … We report on the first verification of a realistic embedded system, with its application software, device drivers, compiler, and RISC-V processor represented inside the Coq proof assistant as one mathematical object …

#### [PDF] [Quantum-Resistant Security for Software Updates on Low-power Networked Embedded Devices](https://arxiv.org/pdf/2106.05577)
G Banegas, K Zandberg, A Herrmann, E Baccelli… - arXiv preprint arXiv …, 2021
> … Ed25519 and ECDSA). Our benchmarks are carried out on a variety of IoT hardware in- cluding ARM Cortex-M, RISC-V, and Espressif (ESP32), which form the bulk of  modern 32-bit microcontroller architectures. We inter- pret …

#### [PDF] [Property-driven Automatic Generation of Reduced-ISA Hardware](http://people.ece.umn.edu/~luo../jsartori/papers/dac21.pdf)
N Bleier, J Sartori, R Kumar
> … If RTL is available, support for some instruction set exten- sions can be removed easily in some modularly-implemented designs, especially for modular ISAs such as RISC-V. For example, the Ibex core RTL [19] uses …

#### [A graph placement methodology for fast chip design](https://www.nature.com/articles/s41586-021-03544-w)
A Mirhoseini, A Goldie, M Yazgan, JW Jiang… - Nature, 2021
> Chip floorplanning is the engineering task of designing the physical layout of a computer chip. Despite five decades of research1, chip floorplanning has defied automation, requiring months of intense effort by physical design …

#### [HTML] [Large-Capacity and High-Speed Instruction Cache Based on Divide-by-2 Memory Banks](https://www.sciencedirect.com/science/article/pii/S1674862X21000732)
QQ Li, ZG Yu, Y Sun, JH Wei, XF Gu - Journal of Electronic Science and Technology, 2021 
> JavaScript is disabled on your browser. Please enable JavaScript to use all the features on this page. Skip to main content Skip to article …

#### [PDF] [Lightweight Implementation of Saber Resistant Against Side-Channel Attacks](https://csrc.nist.gov/CSRC/media/Events/third-pqc-standardization-conference/documents/accepted-papers/abdulgadir-lightweight-implementation-gmu-pqc2021.pdf)
A Abdulgadir, K Mohajerani, VB Dang, JP Kaps, K Gaj
> … [23]. In April 2021, Fritzmann et al. presented a masked SW/HW co-design that supports Saber and Kyber. Their design is based on an open-source RISC-V implementation where they added accelerators and instruction-set extensions for PQC algorithms …

#### [AI system outperforms humans in designing floorplans for microchips](https://www.nature.com/articles/d41586-021-01515-9)
AB Kahng - 2021
> … circuit design evolves. Each iteration is produced manually by human engineers, over days or weeks. a, This floorplan for a chip (the Ariane RISC-V processor 8 ) is considered by human designers to be a good one. Its 37 macro … 

#### [A Binary Translation Framework for Automated Hardware Generation](https://ieeexplore.ieee.org/abstract/document/9453149/)
N Paulino, J Bispo, JC Ferreira, J Cardoso - IEEE Micro, 2021
> … Currently, these include significant subsets of the 32 bit Mi- croBlaze from Xilinx, the 32 bit ARMv8, and the riscv32ima subset of the RISC-V specification … For example, the following information is required for RISC-V instructions …

 


#### [PDF] [Isla: Integrating full-scale ISA semantics and axiomatic concurrency models](https://www.cl.cam.ac.uk/~pes20/isla/isla-cav2021.pdf)
A Armstrong, B Campbell, B Simner, C Pulte, P Sewell
> … 1 University of Cambridge, Cambridge, UK 2 University of Edinburgh, Edinburgh, UK
Abstract. Architecture specifications such as Armv8-A and RISC-V are the ultimate foundation for software verification and the correct- ness criteria for hardware verification …

#### [PDF] [Isla: Integrating full-scale ISA semantics and axiomatic concurrency models (extended version)](https://www.cl.cam.ac.uk/~pes20/isla/isla-cav2021-extended.pdf)
A Armstrong, B Campbell, B Simner, C Pulte, P Sewell
> … 1 University of Cambridge, Cambridge, UK 2 University of Edinburgh, Edinburgh, UK
Abstract. Architecture specifications such as Armv8-A and RISC-V are the ultimate foundation for software verification and the correct- ness criteria for hardware verification …

#### [PDF] [Inferring Custom Synthesizable Kernel for Generation of Coprocessors with Out-of-Order Execution](http://embeddedcomputing.meconet.me/wp-content/uploads/2021/05/86.pdf)
A Antonov
> … been significantly democratized for a wide community of developers in recent decades thanks to various assets, including affordable entry-level and high-density FPGA boards, high-level design tools, open instruction set …

#### [PDF] [The nanoPU: A Nanosecond Network Stack for Datacenters](http://web.stanford.edu/~jepsen/papers/nanoPU-OSDI-21.pdf)
S Ibanez, A Mallery, S Arslan, T Jepsen, M Shahbaz…
> … tail response time for RPCs. We built an FPGA prototype of the nanoPU fast path by modifying an open-source RISC-V CPU, and evaluated its per- formance using cycle-accurate simulations on AWS FPGAs. The wire-to-wire …

#### [PDF] [Europe's Capacity to Act in the Global Tech Race: Charting a Path for Europe in Times of Major Technological Disruption](https://www.ssoar.info/ssoar/bitstream/handle/document/73445/ssoar-2021-sahin_et_al-Europes_Capacity_to_Act_in.pdf?sequence=1&isAllowed=y&lnkname=ssoar-2021-sahin_et_al-Europes_Capacity_to_Act_in.pdf)
K Sahin, T Barker - 2021
> … Instead of trying to compete directly with the US and the other world leader, Taiwan, the EU should instead focus on preserving and supporting local manufacturing capabilities as well as promoting open standards in chip design like RISC-V …

#### [An MPI-based MPSoC Platform in FPGA](https://ieeexplore.ieee.org/abstract/document/9448553/)
R Uhlendorf, E Silva, F Viel, C Zeferino - IEEE Latin America Transactions, 2021
> Page 1. IEEE LATIN AMERICA TRANSACTIONS, VOL. 19, NO. 4, APRIL 2021 697
An MPI-based MPSoC Platform in FPGA Roseli Uhlendorf, Eduardo Silva, Felipe Viel, Member, IEEE and Cesar Zeferino, Member, IEEE Abstract …

#### [PDF] [SimpliFI: Hardware Simulation of Embedded Software Fault Attacks](https://www.mdpi.com/2410-387X/5/2/15/pdf)
J Grycel, P Schaumont - Cryptography, 2021
> Fault injection simulation on embedded software is typically captured using a high-level fault model that expresses fault behavior in terms of programmer-observable quantities. These fault models hide the true sensitivity …

#### [Information-flow control on ARM and POWER multicore processors](https://link.springer.com/article/10.1007/s10703-021-00376-2)
G Smith, N Coughlin, T Murray - Formal Methods in System Design, 2021
> Weak memory models implemented on modern multicore processors are known to affect the correctness of concurrent code. They can also affect whether or not t.

#### 【发明】[基于FPGA实现的RISC-V处理器、FPGA芯片及片上系统](https://scjg.cnki.net/kcms/detail/detail.aspx?filename=CN112099853A&dbcode=SCPD&dbname=SCPD2021&v=) 
(From scjg.cnki.net 2021.07.02)"
> 广东高云半导体科技股份有限公司
本发明提供了一种基于FPGA实现的RISC-V处理器、FPGA芯片及片上系统,所述RISC-V处理器包括配置在同一FPGA芯片内的RISC-V处理器核以及至少一个扩展模块,所述RISC-V处理器核包括至少一个通用寄存器组和至少一个接口模块,所述接口模块与所述扩展模块一一对应设置并相互连接,所述接口模块包括指令接口、操作数接口、数据返回接口、操作请求接口、写回请求接口、等待请求接口和运算完成接口。本发明为RISC-V处理器核和扩展模块之间的接口制定了统一化的标准接口,降低了RISC-V处理器核和扩展模块之间的耦合性,且结合了RISC-V指令集模块化的特点和FPGA可编程、易扩展的特点,实现了RISC-V处理器的模块化开发和模块化配置,降低了RISC-V处理器设计复杂度,提高了RISC-V处理器开发效率。

#### [基于RISC-V GCC编译器的指令延迟调度](https://scjg.cnki.net/kcms/detail/detail.aspx?filename=DZRU202008029&dbcode=CJFQ&dbname=CJFD2020&v=)
 (From scjg.cnki.net 2021.07.02)"
> 本文通过对RISC-V GCC编译器的改进和优化,为进一步研究和优化RSIC-V GCC奠定了编译器基础,更为快速研究新型的RISC-V指令集架构处理器起着重要的推动作用。在深入分析RISC-V GCC中表调度算法得出编译器可以计算出指令停顿的位置和拍数,指令延迟调度技术是在编译器中静态调度指令达到动态调度的效果以避免流水线冒险和特殊时序带来的问题。验证了使用指令延迟调度技术的编译器的性能和正确性,并在火苗原型系统和超导模拟器上运行测试通过。 

---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

