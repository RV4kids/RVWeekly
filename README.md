# RVWeekly
RISC-V Weekly New, Papers and Conferences in Chinese



RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报

每周六发布

欢迎批评，指正，评论和加入



关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)



| 标题 | 内容 |
|---|---|
| 语雀 | [RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />https://www.yuque.com/riscv/rvnews |
| Inspur | http://open.inspur.com/riscv/RVWeekly |
| 微信公众号 | 高效服务器和存储技术国家重点实验室<br /> http://www.hsslab.com <br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "屏幕截图.png") |
| Github | https://github.com/inspur-risc-v/RVWeekly |
| Gitee | https://gitee.com/inspur-risc-v/RVWeekly |
| [RV少年](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) | [RV4Kids FPGA 课程](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%20FPGA%20%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.md) ([1](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)/[2](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)/[3](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)/[4](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)/[5](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)/[6](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)/[7](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P)/[8](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E7%9F%B3%E6%A6%B4%E6%B4%BE%E3%80%91SIGer%20%E7%BC%96%E8%BE%91%E6%89%8B%E5%86%8C.md)/[9](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209%20-%20RV%20%E5%B0%91%E5%B9%B4.md)/[10](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/issue%2010%20-%20inspur.md)/[11](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md)) |