本期概要

- 中科蓝汛冲上市，以RISC-V研发TWS耳机芯片



---

# 重点聚焦

<a name="040e68498fe94af14a2d31d5106f68eb"></a>
#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：[5月 / 第一期 / 2021](http://tinylab.org/tinylab-weekly-5-1st-2021/)
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅农亚历山大: [RISC-V双周报](https://www.rvmcu.com/article-show-id-557.html)
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：

---



<a name="27f27f1566779837ca690f0417d9ac7a"></a>
## 技术动态

"[易灵思®宣布提供三种RISC-V®SoC系统芯片](https://mbb.eet-china.com/blog/3947088-420193.html) (From mbb.eet-china.com 2021.05.15)"

"[RISC-V 和 seL4 基金会宣布新的安全里程碑](https://www.oschina.net/news/140771/security-milestone-for-riscv-sel4) (From www.oschina.net 2021.05.15)"
> 2021 年 5 月 5 日，seL4 基金会和 RISC-V International 宣布，在 CSIRO 的 Data61 中，经过验证的 RV64 架构上的 seL4 微内核已经被证明到可执行代码

<a name="d5341f1457ce4cc45dd68f9fe821e6d2"></a>
#### Linux动态

"[RISC-V Patches for the 5.13 Merge Window, Part 1](http://lkml.iu.edu/hypermail/linux/kernel/2105.0/04492.html) (From lkml.iu.edu 2021.05.15)"  
"[A Number Of Exciting RISC-V Improvements For Linux 5.13](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.13-RISC-V) (From www.phoronix.com 2021.05.15)"

<a name="bd674270d844156c441d6c491a844358"></a>
#### QEMU动态

"[QEMU 6.0 Released With AMD SEV-ES Encrypted Guest Support, Experimental Multi-Process](https://www.phoronix.com/scan.php?page=news_item&px=QEMU-6.0-Released) (From www.phoronix.com 2021.05.15)"

<a name="afed03f5250206b6e0a59dbb2c657056"></a>
#### 一周问答

[RISC-V 答疑课上JALR指令的解释后续 – IC知识库](http://www.icfedu.cn/forums/topic/12447)  
[关于RISC_V中ALU模块中操作数扩展问题 – IC知识库](http://www.icfedu.cn/forums/topic/12490)



---

<a name="3f25c0aec37715d98fb24001e6f90d0b"></a>
## 产业风云

"[第十一届松山湖中国IC创新高峰论坛2021及2020年十大中国创芯推介IC](https://www.eet-china.com/news/11574.html) (From www.eet-china.com 2021.05.15)"
> 第十一届松山湖中国IC创新高峰论坛于2021年5月14日在广东东莞松山湖召开，开幕式上，戴伟民董事长回顾了2020年第十届松山湖十大"中国创芯“推介的IC，同时大会推介了2021年十大“中国创芯“中国最需要的国产IC。在松山湖历届中国IC创新高峰论坛上亮相的IC90%以上都得到了量产，并受到投资界的青睐，多数公司已经上市。下面是2021及2020十大“中国创芯”IC。

"[莱迪思Automate解决方案集合加速工业自动化系统的开发](https://finance.sina.com.cn/tech/2021-05-12/doc-ikmyaawc4816315.shtml) (From finance.sina.com.cn 2021.05.15)"

"[华强北TWS耳机芯片厂商冲上市 主营白牌产品一年营收飙升2.8亿](https://finance.sina.com.cn/tech/2021-05-11/doc-ikmxzfmm1836619.shtml) (From finance.sina.com.cn 2021.05.15)"
> 不同于恒玄科技采用市面主流的 Arm 芯片架构，中科蓝讯基于 RISC-V 架构研发产品；  
> "[TWS芯片厂中科蓝讯冲刺科创板：产品侧重白牌市场 申报前夕实控人大笔套现](http://finance.ce.cn/stock/gsgdbd/202105/12/t20210512_36552573.shtml) (From finance.ce.cn 2021.05.15)"

"[Dialog半导体公司成为SiFive RISC-V开发平台优选电源管理合作伙伴](https://www.eefocus.com/analog-power/493766) (From www.eefocus.com 2021.05.15)"
> Dialog将作为SiFive HiFive Unmatched平台的优选电源管理方案合作供应商，HiFive Unmatched是一款基于SiFive Freedom U740 RISC-V SoC的PC级RISC-V Linux开发平台。

SiFive："[满天芯计划](https://starfivetech.com/site/plan) (From starfivetech.com 2021.05.15)"
> 在中国注册的企业，可免费获得StarFive自主产权的商用RISC-V 处理器IP S2。 S2为32bit，2-3级流水线的可配置的RISC-V架构处理器，支持RV32I(E) MAC指令集，DMIPS/MHz最高得分1.47，CoreMark最高得分3.1。  
> 加入该计划的用户，通过在线的“CPU生成器”可以自由对S2处理器进行配置和下载，并可通过官方讨论社区获得不定期支持。

"[小家电产业订单爆满，却深受缺芯困扰停产](https://www.eet-china.com/news/202105101550.html) (From www.eet-china.com 2021.05.15)"
> 近年来，中国厨房小家电、生活小家电以及个人护理等小家电产品深受欧美市场的喜爱。海关总署日前发布的数据显示，今年一季度，中国出口机电产品2.78万亿元，同比增长43%，占出口总值的60.3%，预计今年下半年海外订单将会持续火爆。但是受芯片以及原材料价格等多因素影响，尤其是微控制芯片（MCU）的短缺，直接影响了小家电企业的正常生产和按时交付……

<a name="ad4e97987f4fce7ae157a484526e2762"></a>
#### 评论文集

"[戴辉：发展芯片业离不开中国](https://laoyaoba.com/n/780380) (From laoyaoba.com 2021.05.15)"  
"[5G市场对于RISC-V的机会有多少？从基站到手机](http://news.moore.ren/industry/275394.htm) (From news.moore.ren 2021.05.15)"


---



<a name="6395d96e54fcd2acd131c715687c276b"></a>
## 其他动态


#### 项目推荐

"[太素 TisuOS](https://gitee.com/belowthetree/tisu-os) (From gitee.com 2021.05.15)"
> 太素OS是一个用 Rust 编写的基于 RISCV 架构的操作系统，主要用于教学目的，为初学者提供参考。

<a name="a713c779ca5999a6954be10d0219afb2"></a>
#### 博文推荐

"[RISCV的B、K、V扩展简介以及运行他们的回归测试。](https://zhuanlan.zhihu.com/p/368516136) (From zhuanlan.zhihu.com 2021.05.15)"

"[回顾MIPS发展历程，当年被SGI收购后几无授权项目](https://moore.live/news/275465/detail/) (From moore.live 2021.05.15)"

"[「哪吒开发板」用Rust探索RISC-V主板D1之GPIO](https://www.eefocus.com/embedded/493501) (From www.eefocus.com 2021.05.15)"

"[处理器指令集，不是一本武功秘籍](https://www.rvmcu.com/article-show-id-743.html) (From www.rvmcu.com 2021.05.15)"

"[RISC-V fence指令](https://zhuanlan.zhihu.com/p/372433134) (From zhuanlan.zhihu.com 2021.05.15)"

---

<a name="2010a69b8aaca066239ad10c98eb0e7c"></a>
## 一周论文

#### [基于RISC-V的卫星通信终端处理器指令集优化研究](https://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CPFD&dbname=CPFDLAST2020&filename=ZGTH202006001032&v=6pkV7BE492hcyrDGUb4RR%25mmd2Bpnfzva2Ohnh6JDfZamLtyusNtQ3DghpsWgSxS9OH1h%25mmd2BOeHSgx6H8U%3d)
> 本文基于开源的RISC-V指令集架构,提出一种适用于卫星通信终端信号处理器的自定义指令优化方法。RISC-V具有模块化和可扩展的优点。为了使其适用于软件无线电应用领域,可以在RV32I基础指令集上扩充常用的定点数运算指令,如定点数的乘加运算、饱和运算、并行子字运算、有限域运算等,极大地提高了对应数字信号处理算法的性能。本文进一步通过对其中具有特殊访存特性指令的数据通路改进,在GEM5架构仿真中实现了相对于原有架构37%的执行速率提升。 

#### [CARE: Lightweight Attack Resilient Secure Boot Architecture with Onboard Recovery for RISC-V based SOC](CARE: Lightweight Attack Resilient Secure Boot Architecture with Onboard Recovery for RISC-V based SOC)
A Dave, N Banerjee, C Patel - 2021 22nd International Symposium on Quality …, 2021

#### [SERVAS! Secure Enclaves via RISC-V Authenticryption Shield](https://ui.adsabs.harvard.edu/abs/2021arXiv210503395S/abstract)
S Steinegger, D Schrammel, S Weiser, P Nasahl… - arXiv e-prints, 2021

#### [Flush-Reload Attack and its Mitigation on an FPGA Based Compressed Cache Design](https://ieeexplore.ieee.org/abstract/document/9424252/)
P Mata, N Rao - 2021 22nd International Symposium on Quality …, 2021

#### [PDF] [Parallelized sequential composition, pipelines, and hardware weak memory models](https://arxiv.org/pdf/2105.02444)
RJ Colvin - arXiv preprint arXiv:2105.02444, 2021

#### [Bridging the Gap between RTL and Software Fault Injection](https://dl.acm.org/doi/abs/10.1145/3446214)
J Laurent, C Deleuze, F Pebay-Peyroula, V Beroulle - ACM Journal of Emerging …, 2021

#### [Six-track Standard Cell Libraries with Fin Depopulation, Contact over Active Gate, and Narrower Diffusion Break in 7nm Technology](https://ieeexplore.ieee.org/abstract/document/9424315/)
TH Wang, CC Hsu, L Kao, BY Li, TC Wu, TH Peng… - 2021 22nd International …, 2021

#### [PDF] [Machine Learning for Side-Channel Disassembly](https://www.epfl.ch/labs/mlo/wp-content/uploads/2021/05/crpmlcourse-paper832.pdf)
O Glamocanin, R Islambouli, D Mahmoud - work

#### [PDF] [Benchmarking and Comparison of Two Open-source RTOSs for Embedded Systems Based on ARM Cortex-M4 MCU](https://sciresol.s3.us-east-2.amazonaws.com/IJST/Articles/2021/Issue-16/IJST-2021-387.pdf)
Y Mazzi, A Gaga, F Errahimi - Indian Journal of Science and Technology, 2021

#### [Integration of Minimum Energy Point Tracking and Soft Real-Time Scheduling for Edge Computing](https://ieeexplore.ieee.org/abstract/document/9424343/)
T Komori, Y Masuda, J Shiomi, T Ishihara - 2021 22nd International Symposium on …, 2021

#### [Metis: An Integrated Morphing Engine CPU to Protect against Side Channel Attacks](https://ieeexplore.ieee.org/abstract/document/9424552/)
F Antognazza, A Barenghi, G Pelosi - IEEE Access, 2021

#### [PDF] [BasicBlocker: ISA Redesign to Make Spectre-Immune CPUs Faster](https://cr.yp.to/papers/basicblocker-20210504.pdf)
JP Thoma, J Feldtkeller, M Krausz, T Güneysu…

#### [PDF] [A Comparative Study of ISA Multimedia Extensions for HPC](http://www.ayazakram.com/papers/simd_perf.pdf)
A Akram, S Rahnama

#### [Trimming Feature Extraction and Inference for MCU-based Edge NILM: a Systematic Approach](https://ieeexplore.ieee.org/abstract/document/9426443/)
E Tabanelli, D Brunelli, A Acquaviva, L Benini - IEEE Transactions on Industrial …, 2021

#### [PDF] [DynPTA: Combining Static and Dynamic Analysis for Practical Selective Data Protection](https://www3.cs.stonybrook.edu/~mikepo/papers/dynpta.sp21.pdf)
T Palit, JF Moon, F Monrose, M Polychronakis

#### [Securing Silicon Photonic NoCs Against Hardware Attacks](http://scholar.google.com/scholar?cluster=10050978469291836806&hl=en&oi=scholaralrt&hist=AQxAsJ0AAAAJ:3812021249043908228:AAGBfm19OZ2uvk2g_ev6ffIC03BRSiTYQw&html=&folt=kw)
IG Thakkar, SVR Chittamuru, V Bhat, SS Vatsavai… - Network-on-Chip Security …, 2021

#### [Secure Cryptography Integration: NoC-Based Microarchitectural Attacks and Countermeasures](http://scholar.google.com/scholar?cluster=7703545426835499104&hl=en&oi=scholaralrt&hist=AQxAsJ0AAAAJ:3812021249043908228:AAGBfm19OZ2uvk2g_ev6ffIC03BRSiTYQw&html=&folt=kw)
J Sepúlveda - Network-on-Chip Security and Privacy, 2021

#### [PDF] [High-Speed NTT-based Polynomial Multiplication Accelerator for CRYSTALS-Kyber Post-Quantum Cryptography](https://eprint.iacr.org/2021/563.pdf)
M Bisheh-Niasar, R Azarderakhsh…

#### [Study on cyber‐security for IoT edge utilizing pattern match accelerator](https://onlinelibrary.wiley.com/doi/abs/10.1002/eej.23333)
M Kashiyama, R Kashiyama, H Seki, H Hosono - Electrical Engineering in Japan, 2021

#### [PDF] [Conveyor Belt Longitudinal Rip Detection Implementation with Edge AI](https://www.scitepress.org/Papers/2021/104472/104472.pdf)
E Klippel, RAR Oliveira, D Maslov, AGC Bianchi… - 2021

#### [PDF] [Security Properties for Stack Safety](https://arxiv.org/pdf/2105.00417)
SN ANDERSON, L LAMPROPOULOS, B ROBERTO… - arXiv preprint arXiv …, 2021

#### [Survey of Transient Execution Attacks and Their Mitigations](https://dl.acm.org/doi/abs/10.1145/3442479)
W Xiong, J Szefer - ACM Computing Surveys (CSUR), 2021

#### [PDF] [A Survey on Hardware Security of DNN Models and Accelerators](https://www.researchgate.net/profile/Sparsh-Mittal-2/publication/351357734_A_Survey_on_Hardware_Security_of_DNN_Models_and_Accelerators/links/6093a728a6fdccaebd0dfbb7/A-Survey-on-Hardware-Security-of-DNN-Models-and-Accelerators.pdf)
S Mittal, H Gupta, S Srivastava


---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

