本期概要

- 国家科技体制改革和创新体系建设领导小组第十八次会议：面向后摩尔时代的集成电路潜在颠覆性技术
- 2021年第十一届松山湖中国IC创新高峰论坛
- Graphcore：为AI计算而生的IPU
- 《循序渐进，学习开发一个RISC-V上的操作系统》完成
- 平头哥玄铁907发布
- 中国计算机学会青年精英大会（YEF2021）技术论坛【芯片智造——敏捷设计与开源EDA之路】
- Tenstorrent宣布完成C轮融资，金额超2亿美元


---

# 重点聚焦

<a name="040e68498fe94af14a2d31d5106f68eb"></a>
#### 相关周报

- ![image.png](https://images.gitee.com/uploads/images/2021/0503/191026_0f045506_5631341.png) [semi engineering](https://semiengineering.com/) Week In Review: 
   - Blog Review：
   - Design, Low Power：
   - Manufacturing, Test：
   - Auto, Security, Pervasive Computing：
- IoT News：（[Site](https://staceyoniot.com/)）：
- OSDT Weekly：([zhihu](https://www.zhihu.com/column/c_1252285504848613376)，[Github](https://github.com/hellogcc/osdt-weekly/tree/master/weekly))：
- 泰晓咨询：（[Site](http://tinylab.org/)）：[5月 / 第一期 / 2021](http://tinylab.org/tinylab-weekly-5-1st-2021/)
- PLCT开源进展：([Github](https://github.com/isrc-cas/PLCT-Weekly)，[zhihu](https://www.zhihu.com/column/plct-lab)）：
- RT-Thread：（[oschina](https://my.oschina.net/u/4428324)）：
- 科技爱好者周刊：（[yuque](https://www.yuque.com/ruanyf/weekly)）：
- 硅亚农历山大: RISC-V双周报：（[wechat](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzU2MjM4MTcyNQ==&action=getalbum&album_id=1713615520249217030&scene=173&from_msgid=2247489739&from_itemidx=1&count=3&nolastread=1#wechat_redirect)）：[5月7日-5月20日](https://mp.weixin.qq.com/s/QvYW6G-s2TT2xeOWKq7c-w)
  - RISC-V国际基金会组织的RISC-V暑期实习项目
  - 创思远达发布基于RISC-V的WiFi+BLE芯片的鸿蒙操作系统发行版
  - CNRV在Bilibili上开通了[RISC-V国际基金会](https://space.bilibili.com/1121469705/video)
  - 玄铁907
  - 2021全国大学生嵌入式芯片与系统设计竞赛
  - 易灵思三种基于普及的RISC-V®内核软件定义的SoC系统芯片
  - 第十一届松山湖中国IC创新高峰论坛
  - 中科蓝讯的科创板IPO申报
  - 第一届「RISC-V中国峰会」：6月21日至26日
  - 澎峰科技与华秋电子：RVBoards-哪吒
  - 2021阿里云开发者大会：5月29日
  - openEuler Developer Day 2021
- 痞子衡嵌入式半月刊: （[zhihu](https://www.zhihu.com/column/c_1265712198858485760)）：
- 半导体一周要闻－莫大康：（[eet-china](https://www.eet-china.com/mp/u3949295)）：


---



<a name="27f27f1566779837ca690f0417d9ac7a"></a>
## 技术动态

#### 工艺

"[台积电1nm工艺最新消息:硅片触及物理极限](https://www.eet-china.com/kj/63360.html) (From www.eet-china.com 2021.05.22)"
> 当有一个新技术、新工艺出现时，先是不急着推向市场，而是让上一代技术充分利用价值，盈利后，再来择机上市。前段时间IBM公布了2nm工艺技术路线，让人倍感振奋。虽然摩尔定律速度放缓，但硅晶片微缩的前景依然广阔。

#### 系统生态

"[创思远达发布基于RISC-V的WiFi+BLE芯片的鸿蒙操作系统发行版](http://china.qianlong.com/2021/0518/5799278.shtml) (From china.qianlong.com 2021.05.22)"
> 为了满足嵌入式WiFi+BLE单芯片解决方案的鸿蒙应用需求，中科创达旗下子公司创思远达Ainergy推出了面向嵌入式WiFi+BLE芯片的鸿蒙操作系统发行版。

"[Automate：加速工业自动化开发进程的秘密武器](https://www.eet-china.com/news/11681.html) (From www.eet-china.com 2021.05.22)"
> 继2018年推出sensAI，2020年公布mVision 1.0和Sentry 1.0，2021年3月升级为mVision 2.0和Sentry 2.0版本后，日前，Lattice面向智能工业系统的Automate解决方案集合也正式面世。  
> Automate支持莱迪思Propel™，使用嵌入式RISC-V处理器，通过软件和硬件协处理简化工业自动化系统的开发。  
> Automate的底层硬件平台基于Certus-NX马达控制开发板和基于MachXO3D的硬件安全开发板，在此之上是一系列IP核(EtherConnect、CNN处理单元、PDM数据采集器)、软件工具(RADIANT、DIAMOND、Propel、RISC-V+硬件协处理)、参考设计与演示示例(多轴马达控制、预测性维护、实时工业网络、硬件安全)和定制设计服务，共同构成了完整的Automate方案。

<a name="bd674270d844156c441d6c491a844358"></a>
#### QEMU动态

<a name="afed03f5250206b6e0a59dbb2c657056"></a>
#### 一周问答

#### 社区动态

“[南京RISC-V生态与应用技术研讨会](https://www.rvmcu.com/article-show-id-773.html)” (From www.rvmcu.com 2021.05.22)"
> 为推动新技术落地，加速南京地区集成电路聚集地RISC-V生态建设，南京集成电路产业服务中心ICisC特此举办此次技术研讨会，邀请芯来科技、中移物联、兆易创新、纽创信安等RISC-V领域的一线企业，为IC企业带来RISC-V市场需求及技术发展趋势的主题演讲。



---

<a name="3f25c0aec37715d98fb24001e6f90d0b"></a>
## 产业风云

#### 产品动态

"[时擎智能科技：基于RISC-V架构的端侧智慧视觉芯片](https://www.eet-china.com/news/202105151447.html) (From www.eet-china.com 2021.05.22)"
> 在日前举办的第十一届松山湖中国 IC 创新高峰论坛上，时擎智能科技带来了自主研发的新一代 RISC-V架构的端侧智慧视觉芯片 AT5055。  
> AT5055，搭载自研RV32 IMCF的TM800处理器@800MHz，存储上32-bit DDR2/DDR3，安全支持AES/ECC/SHA/TRNG/OTP。AI能力上，搭载Timesformer Bumblebee 600V处理器

"[谁动了GPU的奶酪？](https://www.rvmcu.com/article-show-id-764.html) (From www.rvmcu.com 2021.05.22)"
> “在计算机历史上只发生过三次革命，第一次是70年代的CPU，第二次是90年代的GPU，而Graphcore就是第三次革命。”这句话是英国半导体之父、Arm联合创始人Hermann Hauser所说。他所指的正是Graphcore率先提出的就是为AI计算而生的IPU。

"[十款国产IC芯片轮番登场，剑指智慧物联网市场](https://www.esmchina.com/news/7855.html) (From www.esmchina.com 2021.05.22)"
> - 2021年第十一届松山湖中国IC创新高峰论坛在上周五(14日)举行，并推荐了十款面向“智慧物联网”应用方向/领域的国产IC新品。
> - HME-P1P60：60K中端高性能国产FPGA
> - VN1110/VN1810：新一代PonCAN工业控制网络芯片
> - AX630A：高性能、低功耗的人工智能视觉处理器芯片
> - AT5055：RISC-V架构的端侧智慧视觉芯片
> - TH2608：低功耗人工智能人机语音交互芯片
> - WTM2101：面向可穿戴设备的超低功耗存算一体芯片
> - XP5127：5G n77 1T2R射频前端功率放大器芯片
> - ML-2670-3525-DB1：高信噪比MEMS麦克风
> - da7xx系列：超低功耗三轴加速度传感器
> - AT58MP1T1RS32A：全球首款可探测呼吸的5.8GHz微波雷达传感SoC

"[阿里巴巴的平头哥半导体于5月18日发布了新款处理器玄铁907](https://www.sohu.com/a/467204012_187675) (From www.sohu.com 2021.05.22)"
> 此前，平头哥已推出基于RISC-V架构的玄铁902、906及910等多款产品  
> "[阿里平头哥发布玄铁907处理器：RISC-V架构，已向多家企业授权](https://post.smzdm.com/p/ar66p7v7/) (From post.smzdm.com 2021.05.22)"  
> "[玄铁系列处理器出货量已超20亿](https://www.eefocus.com/mcu-dsp/494317) (From www.eefocus.com 2021.05.22)"
>
> 玄铁907对开源RISC-V架构进行了扩展增强，采用5级流水线设计，最高工作频率超过1GHz，单位性能可达3.8 Coremark/MHz；该处理器还首次实现了RISC-V 最新的DSP指令标准，拥有出色的计算能效，适用于存储、工业控制等对计算性能要求较高的实时计算场景。  
> ![输入图片说明](https://images.gitee.com/uploads/images/2021/0525/122020_601cb8ba_5631341.png "屏幕截图.png")

"[RISC-V如何融入汽车系统？](https://zhuanlan.zhihu.com/p/373571911) (From zhuanlan.zhihu.com 2021.05.22)"
> 欧洲的GaNext项目(下一代氮化镓功率模块的简称)旨在使使用氮化镓功率半导体设计功率转换器更加容易，同时提高用于电动汽车充电器等系统的基于氮化镓的功率模块的效率和紧凑性。GaNext主要致力于开发基于GaN电源开关的智能电源模块(IPM)。GaNext的一个关键部分是开发一个具有高分辨率定时器、集成前端和外部SPI接口的定制RISC-V控制器。  
> IPM将把RISC-V控制器与栅极驱动器和一个可编程、有安全保障的脉宽调制控制单元相结合，该控制单元具有集成的保护电路和安全功能、滤波器单元和广泛的可编程时序，以实现对氮化镓功率元件驱动需求的最佳适应。

#### 企业动态

"[IP企业芯耀辉科技完成A轮近6亿元融资，不到一年累计获得近10亿](https://www.eet-china.com/news/202105190912.html) (From www.eet-china.com 2021.05.22)"
> 芯耀辉联席CEO兼澳门董事总经理余成斌表示: “芯耀辉研发团队正在集中力量自主研发28/14/12纳米先进工艺IP研发和服务，已陆续推出覆盖DDR、PCIe、HDMI、USB、SATA、MIPI等产品解决方案。我们相信芯耀辉的技术和产品将为芯片创新带来新的力量，赋能中国的数字化转型。“

"[Zen架构之父Jim Keller获13亿元支持：RISC-V架构AI芯片要超NVIDIA](https://news.mydrivers.com/1/758/758315.htm) (From news.mydrivers.com 2021.05.22)"
> Tenstorrent宣布完成C轮融资，金额超2亿美元（约合12.8亿元），公司估值已经高达10亿美元。

---



<a name="6395d96e54fcd2acd131c715687c276b"></a>
## 其他动态

#### 评论文集

"[后摩尔时代半导体潜在颠覆性技术猜想：找新材料，做新架构](https://finance.sina.com.cn/tech/2021-05-15/doc-ikmyaawc5446137.shtml) (From finance.sina.com.cn 2021.05.22)"
> 5月14日，在北京召开会议专题讨论了面向后摩尔时代的集成电路潜在颠覆性技术：新材料，新架构，先进封装，特色工艺。  
> 在架构方面，朱晶认为，以RISC-V为代表的开放指令集及其相应的开源SoC芯片设计、高级抽象硬件描述语言和基于IP的模板化芯片设计方法，将取代传统芯片设计模式，更高效应对快速迭代、定制化与碎片化的芯片需求。类似脑神经结构的存内计算架构将数据存储单元和计算单元融合为一体，能显著减少数据搬运，极大地提高计算并行度和能效。计算存储一体化在硬件架构方面的革新，将突破AI算力瓶颈。基于芯粒的模块化设计方法将实现异构集成，被认为是增强功能及降低成本的可行方法，有望成为延续摩尔定律的新路径。
相关新闻
  - "[半导体“颠覆性技术”有哪些？](https://www.laoyaoba.com/n/780762) (From www.laoyaoba.com 2021.05.22)"

"[Arm高性能之路：从全军覆没，到重整旗鼓](https://www.eet-china.com/news/202105170951.html) (From www.eet-china.com 2021.05.22)"
> Arm近两年在低功耗之外的高性能领域，生态的高歌猛进超出所有人的意料。尽管市场还有国际贸易环境、英伟达收购、RISC-V崛起等不确定因素的掺杂，Arm触及高性能领域而且取得阶段性成果是不容置疑的  
> 以英伟达目前在数据中心的地位，助推Arm在基础设施市场的发展是一拍即合的。或者说传统市场参与者对此应该感觉到真正的压力。而且今年的GTC大会上，英伟达还宣布即将面向HPC AI市场，推出自家的CPU，基于Arm Neoverse。  
> RISC-V成为Arm之外的重要竞争对手，加上国际贸易环境变化正在促成RISC-V的快速发展，RISC-V这两年正在崛起。

"[EDA开源，前路几何？ ](http://m.chinaaet.com/article/3000132043)(From m.chinaaet.com 2021.05.22)"
> 5月14日下午召开的中国计算机学会青年精英大会（YEF2021）技术论坛【芯片智造——敏捷设计与开源EDA之路】


#### 课程推荐

"[公开课：循序渐进，学习开发一个RISC-V上的操作系统](https://zhuanlan.zhihu.com/p/372774335) (From zhuanlan.zhihu.com 2021.05.22)"

"[开源RISC-V CPU内核设计实践](https://www.eecourse.com/archive/course/409/lesson/12168) (From www.eecourse.com 2021.05.22)"

#### 博文推荐

"[萌新的交叉编译OpenJDK11 for RV32G的踩坑之路](https://zhuanlan.zhihu.com/p/372067562) (From zhuanlan.zhihu.com 2021.05.22)"

"[【tee小白的第一篇随笔】keystone代码略读](https://www.cnblogs.com/bows7ring/p/14775208.html) (From www.cnblogs.com 2021.05.22)"

"[菜鸟的Qemu系统模式下运行Embench的心酸之路](https://zhuanlan.zhihu.com/p/373077383) (From zhuanlan.zhihu.com 2021.05.22)"

#### 嵌入式应用

"[【中科蓝讯AB32VG1 RISC-V板“碰上”RTT测评】1小时快速入门RISC-V&RT-Thread](http://bbs.eeworld.com.cn/thread-1165719-1-1.html) [ (From bbs.eeworld.com.cn 2021.05.22)"


---

<a name="2010a69b8aaca066239ad10c98eb0e7c"></a>
## 一周论文

#### [PDF] [HeapSafe: Securing Unprotected Heaps in RISC-V](https://arxiv.org/pdf/2105.08712)
A De, S Ghosh - arXiv preprint arXiv:2105.08712, 2021

#### [PDF] [Accelerated RISC-V for Post-Quantum SIKE](https://eprint.iacr.org/2021/597.pdf)
R Elkhatib, R Azarderakhsh, M Mozaffari-Kermani

#### [A Low-Power Elliptic Curve Pairing Crypto-Processor for Secure Embedded Blockchain and Functional Encryption](https://ieeexplore.ieee.org/abstract/document/9431552/)
U Banerjee, AP Chandrakasan - 2021 IEEE Custom Integrated Circuits Conference …, 2021

#### [PDF] [MetaSys: A Practical Open-Source Metadata Management System to Implement and Evaluate Cross-Layer Optimizations](https://arxiv.org/pdf/2105.08123)
N Vijaykumar, A Olgun, K Kanellopoulos, N Bostanci… - arXiv preprint arXiv …, 2021

#### [Can consumer electronics be truly open](https://ieeexplore.ieee.org/abstract/document/9432771/)
E Moguel, M Linaje, J Galan-Jimenez… - IEEE Consumer Electronics …, 2021

#### [PDF] [Ultra-low energy consumption pest detection for smart agriculture](https://www.researchgate.net/profile/Tommaso-Polonelli/publication/347542896_Ultra-low_energy_pest_detection_for_smart_agriculture/links/6087b8e88ea909241e28dc35/Ultra-low-energy-pest-detection-for-smart-agriculture.pdf)
D Brunelli, T Polonelli, L Benini

#### [Efficient Verification of Optimized Code](https://link.springer.com/chapter/10.1007/978-3-030-76384-8_19)
M Schoolderman, J Moerman, S Smetsers… - NASA Formal Methods …, 2021

#### [PDF] [Revizor: Fuzzing for Leaks in Black-box CPUs](https://arxiv.org/pdf/2105.06872)
O Oleksenko, C Fetzer, B Köpf, M Silberstein - arXiv preprint arXiv:2105.06872, 2021

#### [PDF] [Stop! Hammer Time: Rethinking Our Approach to Rowhammer Mitigations](https://stefan.t8k2.com/publications/hotos/2021/hammertime.pdf)
K Loughlin, S Saroiu, A Wolman, B Kasikci - 2021

#### [PDF] [Taming the Zoo: The Unified GraphIt Compiler Framework for Novel Architectures](http://emilyfurst.com/papers/isca21.pdf)
A Brahmakshatriya, E Furst, VA Ying, C Hsu, C Hong…

#### [PDF] [Integer-Only Approximated MFCC for Ultra-Low Power Audio NN Processing on Multi-Core MCUs](https://cps4eu.eu/wp-content/uploads/2021/05/Integer-Only-Approximated-MFCC-for-Ultra-LowPower-Audio-NN-Processing-on-Multi-Core-MCUs.pdf)
M Fariselli, M Rusci, J Cambonie, E Flamand

#### [Supply and Threshold Voltage Scaling for Minimum Energy Operation over a Wide Operating Performance Region](https://www.jstage.jst.go.jp/article/transfun/advpub/0/advpub_2020KEP0013/_article/-char/ja/)
S SONODA, J SHIOMI, H ONODERA - IEICE Transactions on Fundamentals of …, 2021

#### [PDF] [Course Title Advanced Computer Architecture Course Code ACOE301 Course Type Compulsory Level BSc (Level 1)](https://www.frederick.ac.cy/fu_documents/cips/5479.pdf)
ALU Design, ALU Full

#### [PDF] [An Open-Source Tool for Classification Models in Resource-Constrained Hardware](https://arxiv.org/pdf/2105.05983)
LT da Silva, V Souza, GE Batista - arXiv preprint arXiv:2105.05983, 2021

#### [PDF] [Source-Level Bitwise Branching for Temporal Verification of Lifted Binaries](https://arxiv.org/pdf/2105.05159)
YC Liu, C Pang, D Dietsch, E Koskinen, TC Le… - arXiv preprint arXiv …, 2021

#### [PDF] [Physical Fault Injection and Side-Channel Attacks on Mobile Devices: A Comprehensive Survey](https://arxiv.org/pdf/2105.04454)
C Shepherd, K Markantonakis, N van Heijningen… - arXiv preprint arXiv …, 2021

#### [PDF] [Mecanismos de baipás eficientes para redes en chip de baja latencia](https://repositorio.unican.es/xmlui/bitstream/handle/10902/21662/Tesis%20IPG.pdf?sequence=1)
I Pérez Gallardo - 2021

#### [PDF] [Benchmarking a New Paradigm: An Experimental Analysis of a Real Processing-in-Memory Architecture](https://arxiv.org/pdf/2105.03814)
J Gómez-Luna, IE Hajj, I Fernandez, C Giannoula… - arXiv preprint arXiv …, 2021

#### [PDF] [Zerializer: Towards Zero-Copy Serialization](http://cs.yale.edu/homes/soule/pubs/hotos2021.pdf)
A Wolnikowski, S Ibanez, J Stone, C Kim, R Manohar… - 2021

#### [PDF] [Verifying and Optimizing the HMCS Lock for Arm Servers](https://people.mpi-sws.org/~viktor/papers/netys2021-hmcs.pdf)
J Oberhauser, L Oberhauser, A Paolillo, D Behrens…

#### [PDF] [High-Efficiency Parallel Cryptographic Accelerator for Real-Time Guaranteeing Dynamic Data Security in Embedded Systems](https://www.mdpi.com/2072-666X/12/5/560/pdf)
Z Zhang, X Wang, Q Hao, D Xu, J Zhang, J Liu, J Ma - Micromachines, 2021



---

RISC-V与芯片评论编辑部 - RISC-V和芯片动态周报<br />每周六发布<br />欢迎批评，指正，评论和加入<br />
<br />关于本刊: 

- 非特殊注明，本刊消息均来自于网络，如有版权问题，我们会立刻处理。
- [本刊部分消息来源](https://www.yuque.com/riscv/rvnews/overview#vHVQ5)

| 语雀 | 微信公众号 | Gitee | Github | Inspur |
| :---: | :---: | :---: | :---: | --- |
| <br />[RISC-V和芯片动态简报](https://www.yuque.com/riscv/rvnews)<br />[riscv  rvnews](https://www.yuque.com/riscv/rvnews) | 高效服务器和存储技术国家重点实验室<br />![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/164534_65fbfcf0_5631341.png "RVWeekly qr") | [inspur-risc-v  RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) | [inspur-risc-v  RVWeekly](https://github.com/inspur-risc-v/RVWeekly) | [riscv  RVWeekly](http://open.inspur.com/riscv/RVWeekly) |

